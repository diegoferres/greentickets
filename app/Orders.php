<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Orders extends Model
{
    //use Notifiable;
    protected $table = 'orders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'subtotal', 'status_id', 'user_id'
    ];

    public function status()
    {
        return $this->belongsTo('App\OrderStatus', 'status_id');
    }

    public function detail()
    {
        return $this->hasMany('App\OrderDetail', 'order_id');
    }

    public function tickets()
    {
        return $this->hasManyThrough('App\Tickets', 'App\OrderDetail', 'order_id', 'order_detail_id');
    }

    public function payment()
    {
        return $this->hasOne('App\Payments',  'order_id');
    }

    public function client()
    {
        return $this->belongsTo('App\User',  'user_id');
    }


}
