<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class PaymentMethods extends Model
{
    //use Notifiable;

    protected $table = 'payment_methods';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'method'
    ];

    public function orders()
    {
        return $this->hasMany('App\Orders', 'payment_method_id');
    }
}
