<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class SectorTypes extends Model
{
    //use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description', 'places'
    ];

    public function category()
    {
        return $this->belongsTo('App\Categories', 'event_category');
    }
}
