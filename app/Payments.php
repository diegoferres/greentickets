<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Payments extends Model
{
    //use Notifiable;
    protected $table = 'payments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id',
        'payment_method_id',
        'voucher_number',
        'order_number',
        'paid',
        'method',
        'method_id',
        'paid_date',
        'amount',
        'max_pay_date',
        'hash',
        'cancelled',
        'token',
    ];

    public function order()
    {
        return $this->belongsTo('App\Orders', 'order_id');
    }

    public function payment_method()
    {
        return $this->belongsTo('App\PaymentMethods', 'payment_method_id');
    }
}
