<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlacesFiles extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'places_files';
    protected $fillable = [
        'file_path', 'file_name', 'file_label', 'sort', 'active', 'place_id',
    ];

    public function scopeOrdered($query)
    {
        return $query->orderBy('sort', 'asc')->get();
    }

    public function place()
    {
        return $this->belongsTo('App\Places', 'place_id');

    }
}
