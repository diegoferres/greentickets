<?php

namespace App\Http\Livewire\Web\Events;

use App\Categories;
use App\EventCovers;
use App\EventDates;
use App\EventRestrictions;
use App\Events;
use App\EventSectors;
use App\EventTags;
use App\Venues;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Livewire\Component;
use Livewire\WithFileUploads;

class Edit extends Component
{
    use WithFileUploads;
    protected $listeners = ['saveEvent' => 'saveEvent'];

    public $confirmDelete = false;
    public $event;
    /*
     * STEP 1
     */
    public $published,$event_name, $event_organizer, $event_category, $event_tags, $addedTag;
    /*
     * STEP 2
     */
    public $event_format = 1,
        $event_frecuency = 1,
        $venue_name,
        $venue_address,
        $event_link,
        $event_date_start,
        $event_date_end,
        $event_time_start,
        $event_time_end,
        $event_id,
        $dates;
    /*
     * STEP 3
     */
    public $sector_type = 1,
        $restrict_type = false,
        $restrict_value,
        $event_sale_type = 1,
        $sectors,
        $array_dates;

    public $updateMode = false;

    public $banners, $bannerAdded, $event_description;

    public function mount($event)
    {
        //dd($event->published);

        foreach($event->tags as $tag){
            $tags[] = $tag['event_tag'];
        }

        if ($event){
            /*
             * Step 1
             */
            $this->published        = $event->published;
            $this->event_name       = $event->event_name;
            $this->event_organizer  = $event->event_organizer;
            $this->event_category   = $event->event_category;
            $this->event_tags =  $event->tags->pluck('event_tag', 'id');
            $this->event_id = $event->id;

            /*
             * Step 2
             */
            $this->event_format     = $event->event_format;
            $this->venue_name       = isset($event->venue->venue_name) ? $event->venue->venue_name : null;
            $this->venue_address    = isset($event->venue->venue_address) ? $event->venue->venue_address : null;
            $this->event_link       = $event->event_link;

            $this->event_frecuency      = $event->event_frecuency;
            $this->event_date_start     = $event->event_date_start;
            $this->event_time_start     = $event->event_time_start;
            $this->event_date_end       = $event->event_date_end;
            $this->event_time_end       = $event->event_time_end;

            if ($event->dates) {
                foreach ($event->dates->toArray() as $key => $date) {
                    $this->dates[$date['id']]       = $date;
                }
            }

            /*
             * Step 3
             */
            $this->event_sale_type        = $event->event_sale_type;

            /* RESTRICTIONS */
            if ($event->restrictions->first()) {

                $this->restrict_type  = $event->restrictions->first()->restriction_id;
                $this->restrict_value = $event->restrictions->first()->value;
            }

            foreach ($event->sectors->toArray() as $key => $sector) {
                $this->sectors[$sector['id']]       = $sector;
            }

            $this->array_dates = null;
            if ($this->dates) {

                foreach ($this->dates as $key => $date) {
                    $this->array_dates[$date['id']] = $date['event_date'];
                }
            }

            /*
             * Step 4
             */
            $this->event_description        = $event->event_description;

            foreach ($event->covers as $cover) {

                $this->banners[$cover->id] = $cover->url.$cover->file_name;
            }
        }
    }

    public function firstBlock()
    {
        $this->validate([
            'event_name' => 'required',
            'event_organizer' => 'required',
            'event_category' => 'required'
        ]);

        try {
            DB::beginTransaction();

            $this->event->user_id = Auth::id();
            $this->event->event_name = $this->event_name;
            $this->event->event_organizer = $this->event_organizer;
            $this->event->event_category = $this->event_category;
            $this->event->published = $this->published;
            $this->event->save();

            if ($this->event_tags) {
                EventTags::where('event_id', $this->event->id)->delete();
                foreach ($this->event_tags as $tag):
                    $event_tag = new EventTags();
                    $event_tag->event_id = $this->event->id;
                    $event_tag->event_tag = $tag;
                    $event_tag->save();
                endforeach;
            }

            DB::commit();
            $this->dispatchBrowserEvent('alert', ['type' => 'success', 'message' => 'Cambios guardados']);
        }catch (\Exception $e){
            DB::rollBack();
            $this->dispatchBrowserEvent('alert', ['type' => 'warning', 'message' => $e->getMessage()]);
        }
    }

    public function secondBlock()
    {
        $this->validate([
            'event_format' => 'required',
            'venue_name' => 'required_if:event_format,==,1|required_if:event_format,==,3',
            'venue_address' => 'required_if:event_format,==,1|required_if:event_format,==,3',

            'event_date_start' => 'required_if:event_frecuency,==,1',
            'event_date_end' => 'required_if:event_frecuency,==,1',
            'event_time_start' => 'required_if:event_frecuency,==,1',
            'event_time_end' => 'required_if:event_frecuency,==,1',

            'dates.*.event_date' => 'required_if:event_frecuency,==,2',
            'dates.*.event_time_start' => 'required_if:event_frecuency,==,2',
            'dates.*.event_time_end' => 'required_if:event_frecuency,==,2'
        ]);

        try {
            DB::beginTransaction();

            $this->event->event_format          = $this->event_format;
            $this->event->event_frecuency       = $this->event_frecuency;
            $this->event->event_link            = $this->event_link;
            $this->event->save();

            //dd($this->event->venue);
            if ($this->event->venue) {
                $this->event->venue->venue_name     = $this->venue_name;
                $this->event->venue->venue_address  = $this->venue_address;
                $this->event->venue->save();
            }

            /**
             * event_dates
             */
            if ($this->event_frecuency == 1) {

                $this->event->event_date_start    = $this->event_date_start;
                $this->event->event_date_end      = $this->event_date_end;
                $this->event->event_time_start    = $this->event_time_start;
                $this->event->event_time_end      = $this->event_time_end;
                $this->event->save();
            } elseif ($this->event_frecuency == 2) {

                $eventIndex = null;

                foreach ($this->dates as $key => $date):
                    if (!$eventDate = EventDates::where(['id' => $key, 'event_id' => $this->event->id])->first()){
                        $eventDate = new EventDates();
                    }

                    $eventDate->event_id = $this->event->id;
                    $eventDate->event_date = $date['event_date'];
                    $eventDate->event_time_start = $date['event_time_start'];
                    $eventDate->event_time_end = $date['event_time_end'];
                    $eventDate->save();
                endforeach;
            }

            DB::commit();
            $this->dispatchBrowserEvent('alert', ['type' => 'success', 'message' => 'Cambios guardados']);
        }catch (\Exception $e){
            DB::rollBack();
            $this->dispatchBrowserEvent('alert', ['type' => 'warning', 'message' => $e->getMessage()]);
        }
    }

    public function thirdBlock()
    {

        $this->validate([
            'sectors.*.sector_name' => 'required',
            'sectors.*.sector_price' => 'sometimes',
            'sectors.*.sector_capacity' => 'required',
        ]);

        try {
            DB::beginTransaction();

            /*
            * events_restrictions
            */
            if ($this->restrict_type) {

                if ($restriction = $this->event->restrictions()->where('restriction_id', $this->restrict_type)->first()) {
                    $restriction->value = $this->restrict_value;
                    $restriction->save();
                }else{
                    $this->event->restrictions()->delete();
                    $restriction = new EventRestrictions();
                    $restriction->event_id = $this->event->id;
                    $restriction->restriction_id = $this->restrict_type;
                    $restriction->value = $this->restrict_value;
                    $restriction->save();
                }
            }else{
                $this->event->restrictions()->delete();
            }

            /**
             * Sectors
             */
            foreach ($this->sectors as $key => $sector):
                if (!$eventSectors = EventSectors::where(['id' => $key, 'event_id' => $this->event->id])->first()){
                    $eventSectors = new EventSectors();
                }

                $eventSectors->event_id = $this->event->id;
                $eventSectors->venue_id = isset($this->event->venue->id) ? $this->event->venue->id : null;

                $eventSectors->sector_name = $sector['sector_name'];
                $eventSectors->sector_price = $sector['sector_price'] ?: 0;
                $eventSectors->sector_capacity = $sector['sector_capacity'];

                if (isset($sector['date_id']) && !empty($sector['date_id'])){
                    $eventSectors->date_id = $sector['date_id'];
                }

                $eventSectors->save();

            endforeach;

            DB::commit();
            $this->dispatchBrowserEvent('alert', ['type' => 'success', 'message' => 'Cambios guardados']);
        }catch (\Exception $e){
            DB::rollBack();
            $this->dispatchBrowserEvent('alert', ['type' => 'warning', 'message' => $e->getMessage()]);
        }
    }

    public function fourthdBlock()
    {
        /*$this->validate([
            'sectors.*.sector_name' => 'required',
            'sectors.*.sector_price' => 'sometimes',
            'sectors.*.sector_capacity' => 'required',
        ]);*/

        try {
            DB::beginTransaction();

            /**
             * Event
             */
            $this->event->event_description = $this->event_description;
            $this->event->save();

            /**
             * Event Covers
             */
            /*if ($this->banners) {
                $path = 'events/' . $this->event->id . '/covers/';
                foreach ($this->banners as $key => $banner):

                    if (!file_exists('storage/'.$banner)) {
                        $data = substr($banner, strpos($banner, ',') + 1);
                        $extension = explode('/', mime_content_type($banner))[1];

                        $data = base64_decode($data);
                        $fileName = Str::uuid() . '.' . $extension;
                        Storage::put('public/'.$path . '/' . $fileName, $data);

                        $cov = new EventCovers();
                        $cov->url = $path;
                        $cov->file_name = $fileName;
                        $cov->order = $key + 1;
                        $cov->event_id = $this->event->id;
                        $cov->save();
                    }
                endforeach;
            }*/

            DB::commit();
            $this->dispatchBrowserEvent('alert', ['type' => 'success', 'message' => 'Cambios guardados']);
        }catch (\Exception $e){
            DB::rollBack();
            $this->dispatchBrowserEvent('alert', ['type' => 'warning', 'message' => $e->getMessage()]);
        }
    }

    public function addTag()
    {
        $this->event_tags[] = $this->addedTag;
        $this->addedTag = null;
    }

    public function destroy()
    {
        //Storage::deleteDirectory('public/events/99');
        //dd('delete');
        try{
            DB::beginTransaction();

            $event = $this->event;
            $this->event->delete();

            Storage::delete('public/events/'.$event->id);

            DB::commit();

            session()->flash('success', 'El evento '.$event->event_name.' se ha eliminado correctamente');
            return redirect()->route('web.organizer.index');

        }catch (\Exception $e){
            DB::rollBack();
            Log::info('Error | Livewire/Events/Edit@destroy: '.$e->getMessage());

            session()->flash('error', 'Error al eliminar el evento');
            return redirect()->route('web.organizer.event.edit', $this->event->id);
        }
    }

    public function removeTag($key)
    {
        unset($this->event_tags[$key]);
    }

    public function addDate()
    {
        $this->dates[] = ['event_date' => null, 'event_time_start' => null, 'event_time_end' => null];
    }

    public function removeDate($key)
    {
        EventDates::destroy($key);
        unset($this->dates[$key]);
    }

    public function addSector()
    {
        $this->sectors[] = ['sector_name' => null, 'sector_price' => null, 'sector_capacity' => null, 'date_id' => null];
    }

    public function removeSector($key)
    {
        EventSectors::destroy($key);
        unset($this->sectors[$key]);
    }

    public function removeBanner($key)
    {
        if (count($this->banners) == 1) {
            $this->dispatchBrowserEvent('alert', ['type' => 'warning', 'message' => 'Debes agregar una imagen antes de eliminar la última']);

            return true;
        }

        $cover = EventCovers::where(['id' => $key, 'event_id' => $this->event->id])->first();
        if ($cover){
            if (Storage::exists('public/'.$cover->url.$cover->file_name)) {
                Storage::delete('public/'.$cover->url.$cover->file_name);
            }

            $cover->delete();
        }
        unset($this->banners[$key]);
    }

    public function bannedAdd()
    {
        $this->banners[] = $this->bannerAdded;
    }

    public function confirmDelete($result)
    {
        if ($result) {
            $this->confirmDelete = true;
        }else{
            $this->confirmDelete = false;
        }
    }

    public function render()
    {

        if ($this->bannerAdded) {

            if ($this->bannerAdded != 'undefined') {

                $path = 'events/' . $this->event->id . '/covers/';
                $data = substr($this->bannerAdded, strpos($this->bannerAdded, ',') + 1);
                $extension = explode('/', mime_content_type($this->bannerAdded))[1];

                $data = base64_decode($data);
                $fileName = Str::uuid() . '.' . $extension;
                Storage::put('public/'.$path . '/' . $fileName, $data);

                $cov = new EventCovers();
                $cov->url = $path;
                $cov->file_name = $fileName;
                $cov->order = EventCovers::orderBy('order', 'desc')->get()->last()->order + 1;
                $cov->event_id = $this->event->id;
                $cov->save();

                $this->banners[$cov->id] = $cov->url.$cov->file_name;
                //$this->banners[] = $this->bannerAdded;
                $this->bannerAdded = null;


                $this->dispatchBrowserEvent('alert', ['type' => 'success', 'message' => 'Imagen añadida']);
            }else{
                $this->bannerAdded = null;
                $this->dispatchBrowserEvent('alert', ['type' => 'warning', 'message' => 'Primero debes cargar una imagen']);
            }
        }

        $categories = Categories::pluck('category_name', 'id');
        return view('livewire.web.events.edit', compact('categories'));
    }

    public function goToIndexOrganizar()
    {

        return redirect()->to('/organizer/event/'.$this->event_id.'/show');
//        return redirect()->route('web.organizer.event.show',[$this->event_id]);
    }
}
