<?php

namespace App\Http\Livewire\Web\Events;

use App\Categories;
use App\EventCovers;
use App\EventDates;
use App\EventRestrictions;
use App\Events;
use App\EventSectors;
use App\EventTags;
use App\Venues;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Livewire\Component;

class Create extends Component
{
    protected $listeners = ['saveEvent' => 'saveEvent'];

    public $step = 1;
    /*
     * STEP 1
     */
    public $event_name, $event_organizer, $event_category, $event_tags, $addedTag;
    /*
     * STEP 2
     */
    public $event_format = 1,
        $event_frecuency = 1,
        $venue_name,
        $venue_address,
        $event_link,
        $event_date_start,
        $event_date_end,
        $event_time_start,
        $event_time_end,
        $dates;
    /*
     * STEP 3
     */
    public $sector_type = 1,
        $restrict_type = false,
        $restrict_value,
        //$restrict_type_2,
        //$restrict_value_2,
        $event_sale_type = 1,
        $sectors,
        $array_dates;

    public $banners, $bannerAdded, $event_description;

    public function secondStep()
    {
        $this->validate([
            'event_name' => 'required',
            'event_organizer' => 'required',
            'event_category' => 'required'
        ]);

        if (empty($this->dates)) {

            //  $this->dates[] = ['event_date' => null, 'event_time_start' => null, 'event_time_end' => null];
        }

        $this->step = 2;
    }

    public function thirdStep()
    {
       // dd($this->event_frecuency);
        $this->validate([
            'event_format' => 'required',
            'venue_name' => 'required_if:event_format,==,1|required_if:event_format,==,3',
            'venue_address' => 'required_if:event_format,==,1|required_if:event_format,==,3',

            'event_date_start' => 'sometimes|required_if:event_frecuency,1|nullable|after_or_equal:' . date('Y-m-d', strtotime(date('Y-m-d').'+ 1 day')),
            'event_date_end' => 'sometimes|required_if:event_frecuency,1|nullable|after:event_time_start, ',
            'event_time_start' => 'sometimes|required_if:event_frecuency,1|nullable',
            'event_time_end' => 'sometimes|required_if:event_frecuency,1|nullable',

            'dates.*.event_date' => 'required_if:event_frecuency,==,2',
            'dates.*.event_time_start' => 'required_if:event_frecuency,==,2',
            'dates.*.event_time_end' => 'required_if:event_frecuency,==,2'
        ]);

        $this->step = 3;
    }

    public function fourthStep()
    {
        $this->validate([
            'restrict_value'            => 'required_if:restrict_type,==,1|required_if:restrict_type,==,2',
            'sectors.*.sector_name'     => 'required',
            'sectors.*.sector_price'    => 'required_if:sector_type,==,1',
            'sectors.*.sector_count'    => 'required',
        ]);

        $this->step = 4;
    }

    public function saveEvent()
    {
        if (empty($this->banners)){
            $this->dispatchBrowserEvent('alert', ['type' => 'warning', 'message' => 'Debes agregar al menos una imagen']);
            return false;
        }

       try {
           DB::beginTransaction();

           /**
            * Event
            */
           $event = new Events();
           $event->user_id = Auth::id();
           $event->event_name = $this->event_name;
           //$event->event_subtitle = $request->event_subtitle;
           $event->event_description = $this->event_description;
           $event->event_organizer = $this->event_organizer;
           $event->event_category = $this->event_category;
           $event->event_format = $this->event_format;
           $event->event_frecuency = $this->event_frecuency;
           $event->event_sale_type = $this->event_sale_type;
           $event->event_link = $this->event_link;
           $event->published = true;
           $event->save();

           /*
            * events_restrictions
            */
           if ($this->restrict_type) {

               $restriction = new EventRestrictions();
               $restriction->event_id = $event->id;
               $restriction->restriction_id = $this->restrict_type;
               $restriction->value = $this->restrict_value;
               $restriction->save();
           }

           /*$restriction = new EventRestrictions();
           $restriction->event_id = $event->id;
           $restriction->restriction_id = $this->restrict_type;
           $restriction->value = $this->restrict_value;
           $restriction->save();*/

           /**
            * event_tags
            */
           if ($this->event_tags) {

               foreach ($this->event_tags as $tag):
                   $event_tag = new EventTags();
                   $event_tag->event_id = $event->id;
                   $event_tag->event_tag = $tag;
                   //dd($event_tag);
                   $event_tag->save();
               endforeach;
           }

           /**
            * venues
            */
           if ($this->venue_name) {

               $venue = new Venues();
               $venue->event_id = $event->id;
               $venue->venue_name = $this->venue_name;
               $venue->venue_address = $this->venue_address;
               $venue->save();
           }


           //dd($this->sectors);
           /**
            * event_dates
            */
           if ($this->event_frecuency == 1) {

               $event->event_date_start = $this->event_date_start;
               $event->event_date_end = $this->event_date_end;
               $event->event_time_start = $this->event_time_start;
               $event->event_time_end = $this->event_time_end;
               $event->save();
           } elseif ($this->event_frecuency == 2) {

               //dd('AA');
               $eventIndex = null;

               foreach ($this->dates as $key => $date):
                   //dd($date);
                   //if ($key != 99){

                   $eventDate = new EventDates();
                   $eventDate->event_id = $event->id;
                   $eventDate->event_date = $date['event_date'];
                   $eventDate->event_time_start = $date['event_time_start'];
                   $eventDate->event_time_end = $date['event_time_end'];
                   $eventDate->save();
                   //dd($eventDate);

                   $eventIndex[$key] = $eventDate->id;
                   //}
               endforeach;
           }

           /**
            * Sectors
            */
           //dd($this->sectors[1]);
           foreach ($this->sectors as $key => $sector):
               Log::info('SECTOR DATE: '.json_encode($sector));
               $eventSectors = new EventSectors();
               $eventSectors->event_id = $event->id;
               $eventSectors->venue_id = isset($venue->id) ? $venue->id : null;
               //$eventSectors->sector_type_id = \session('ste1')['venue_'];
               $eventSectors->sector_name = $sector['sector_name'];
               $eventSectors->sector_price = $sector['sector_price'] ?: 0;
               $eventSectors->sector_capacity = $sector['sector_count'];
               if (isset($sector['date_id']) && !empty($sector['date_id'])){
                   $eventSectors->date_id = $eventIndex[$sector['date_id']] ?: null;
               }
               $eventSectors->save();

           endforeach;

           /**
            * Event Covers
            */
           if ($this->banners) {
               $path = 'events/' . $event->id . '/covers/';
               foreach ($this->banners as $key => $banner):

                   $data = substr($banner, strpos($banner, ',') + 1);
                   $extension = explode('/', mime_content_type($banner))[1];

                   $data = base64_decode($data);
                   $fileName = Str::uuid() . '.' . $extension;
                   Storage::put('public/'.$path . '/' . $fileName, $data);

                   $cov = new EventCovers();
                   $cov->url = $path;
                   $cov->file_name = $fileName;
                   $cov->order = $key + 1;
                   $cov->event_id = $event->id;
                   $cov->save();
               endforeach;
           }
           DB::commit();

           request()->session()->flash(
               'success',
               'Se ha creado el evento correctamente'
           );

           return redirect()->route('web.event.show', $event->event_slug);
           //session()->flash('success', 'El evento se ha guardado correctamente');

       } catch (\Exception $e) {
           DB::rollBack();
           Log::warning('WARNING EVENT CREATE '. json_encode($e->getMessage()));

           $this->dispatchBrowserEvent('alert', ['type' => 'error', 'message' => 'Hubo un error al guardar el evento']);
       }
    }

    public function back()
    {
        $this->step = $this->step - 1;
    }

    public function addTag()
    {
        $this->event_tags[] = $this->addedTag;
        $this->addedTag = null;
    }

    public function removeTag($key)
    {
        unset($this->event_tags[$key]);
    }

    public function addDate()
    {
        $this->dates[] = ['event_date' => null, 'event_time_start' => null, 'event_time_end' => null];
    }

    public function removeDate($key)
    {
        unset($this->dates[$key]);
    }

    public function addSector()
    {
        $this->sectors[] = ['sector_name' => null, 'sector_price' => null, 'sector_count' => null, 'date_id' => null];
    }

    public function removeSector($key)
    {

        unset($this->sectors[$key]);
    }

    public function removeBanner($key)
    {
        //dd($this->banners[$key]);
        unset($this->banners[$key]);
    }

    public function bannedAdd()
    {
        //if (empty($this->banners)){
        $this->banners[] = $this->bannerAdded;
        //}
    }

    public function render()
    {
        if ($this->dates && $this->step == 3) {
            $this->array_dates = null;
            foreach ($this->dates as $date) {
                //dd($date['event_date']);
                $this->array_dates[] = $date['event_date'];
            }
        }
        if ($this->event_frecuency == 2 && $this->step == 2) {
            if (empty($this->dates)) {
                $this->dates[] = ['event_date' => null, 'event_time_start' => null, 'event_time_end' => null];
            }
        }

        if (empty($this->sectors)) {
            $this->sectors[] = ['sector_name' => null, 'sector_price' => null, 'sector_count' => null];
        }

        if ($this->bannerAdded) {
            if ($this->bannerAdded != 'undefined') {

                $this->banners[] = $this->bannerAdded;
                $this->bannerAdded = null;
                $this->dispatchBrowserEvent('alert', ['type' => 'success', 'message' => 'Imagen añadida']);
            }else{
                $this->bannerAdded = null;
                $this->dispatchBrowserEvent('alert', ['type' => 'warning', 'message' => 'Primero debe elegir una imagen']);
            }

            //if (!empty($this->banners)){
            //if (!array_search($this->bannerAdded,$this->banners,false)){
            //}
            //}else{
            //    $this->banners[] = $this->bannerAdded;
            //    $this->bannerAdded = null;
            //}

            //dd($this->banners);
        }

        //dd($this->banners);
        $categories = Categories::pluck('category_name', 'id');
        return view('livewire.web.events.create', compact('categories'));
    }
}
