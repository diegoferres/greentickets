<?php

namespace App\Http\Livewire\Web\Events;

use App\EventDates;
use App\Events;
use App\EventSectors;
use Illuminate\Support\Facades\Log;
use Livewire\Component;

class Tickets extends Component
{
    public $event;
    public $sectors, $sector, $date_id, $sector_id = 1, $count, $subtotal, $unitPrice, $ticket_data_doc, $ticket_data_name;

    public function updateSectors()
    {
        $sectors = EventSectors::query();
        $sectors->where('event_id', $this->event->id);
        if ($this->date_id) {
            $sectors->where('date_id', $this->date_id);
        }

        $this->sectors = $sectors->pluck('sector_name', 'id');

        if ($this->count && $this->sector_id) {

            $this->sector = EventSectors::where('id', $this->sector_id)->first();

            $this->subtotal = $this->count * (isset($this->sector->sector_price) ? $this->sector->sector_price : 0);
        } else {
            $this->sector = null;
            $this->subtotal = 0;
        }
    }

    public function submit()
    {

        //dd($this->event->order_details);
        //dd(\Cart::getContent());
        $event = $this->event;

        $this->validate([
            //'count' => 'required|numeric|min:1'.(isset($event->restrictions->first()->value) ? '|max:'.$event->restrictions->first()->value : ''),
            'count' => 'required|numeric|min:1',
            'sector_id' => 'required',
            'ticket_data_doc' => 'nullable|string|max:10',
            'ticket_data_name' => 'nullable|string|max:100'
        ], [
            'ticket_data_doc.max' => 'El campo no puede tener más de 10 caracteres',
        ]);/*->setAttributeNames([
            'count' => 'cantidad',
            'sector_id  ' => 'sector',
            'ticket_data_doc' => 'documento',
            'ticket_data_name' => 'nombre'
        ]);*/

        /*if ($validator->fails()) {
            session()->flash('warning', 'No se pudo añadir al carrito');
            //return redirect()->back()->withErrors($validator)->withInput()->with('warning', 'No se pudo añadir al carrito');
        }*/

        try {
            $count = 0;
            $eventCount = 0;
            //if ($restriction = $this->event->restrictions->first()) {


                foreach (\Cart::getContent() as $item):
                    if ($sector = EventSectors::where('id', explode(',', $item->id)[0])
                        ->whereHas('event', function ($q) use ($event) {
                            $q->where('id', $this->event->id);
                        })->first()) {

                        if (EventSectors::where('id', $this->sector_id)->first()->event_id == $this->event->id) {
                            $eventCount = $eventCount + $item->quantity;

                            if ($restriction = $this->event->restrictions->first()) {
                                /*
                                 * If the event has restriction  buy per document
                                 */
                                if ($restriction->restriction_id == 1) {
                                    if ($item->attributes->ticket_data_doc == $this->ticket_data_doc) {

                                        if ($count <= $restriction->first()->where('restriction_id', 1)->value) {
                                            session()->flash('warning', 'Ya existe una entrada asociada a este documento');
                                            return redirect()->route('web.event.show', $event->event_slug)->withInput();
                                        } else {
                                            $count++;
                                        }
                                    }

                                /*
                                 * If the event has restriction buy per quantity
                                 */
                                } elseif ($restriction->restriction_id == 2) {
                                    if ($restriction->value == $this->count || $this->count >= $restriction->value) {
                                        session()->flash('warning', 'El evento solo permite ' . $restriction->value . ' entradas por usuario');
                                        return redirect()->route('web.event.show', $event->event_slug)->withInput();
                                    }// else {
                                    //    $count++;
                                    //}
                                }
                            }// else {

                                //$count++;
                            //}
                        }


                        if (EventSectors::where('id', $this->sector_id)->first()->event_id == $this->event->id) {

                        }
                    }
                endforeach;

                    //dd($eventCount);
            if ($restriction = $this->event->restrictions->first()) {
                if ($restriction->value < ($this->count + $eventCount)) {
                    session()->flash('warning', 'El evento solo permite ' . $restriction->value . ' entradas por usuario');
                    return redirect()->route('web.event.show', $event->event_slug);
                }
            }

            /*if(){

            }*/


                /*if ($event->restrictions->first()->value == $this->count || $this->count >= $event->restrictions->first()->value) {
                    session()->flash('warning', 'El evento solo permite ' . $event->restrictions->first()->value . ' entradas por usuario');
                }*/
            //}


            $this->sector = EventSectors::find($this->sector_id);

            $id = $this->sector_id;
            //$id .= $this->date_id ? ',' . $this->date_id : '';
            $id .= $this->ticket_data_doc ? ',' . $this->ticket_data_doc : '';

            if (\Cart::get($id)) {
                //dd($this->count);


                \Cart::update($id, [/*'attributes' => [
                ],*/ 'quantity' => [
                    'relative' => true,
                    'value' => $this->count
                ],
                    'price' => $this->sector->sector_price]);

            } else {
                \Cart::add([
                    'id' => $id,
                    'name' => $this->sector->sector_name,
                    'price' => $this->sector->sector_price,
                    'quantity' => $this->count,
                    'attributes' => [
                        'date' => $this->date_id ? EventDates::find($this->date_id)->event_date : null,
                        'event_slug' => $this->event->event_slug,
                        'event_name' => $this->event->event_name,
                        'event_cover' => $this->event->covers->first()->url . $this->event->covers->first()->file_name,
                        'ticket_data_name' => $this->ticket_data_name ?: null,
                        'ticket_data_doc' => $this->ticket_data_doc ?: null,
                        /*'coupons'       => [
                            'A10120B' => 15
                        ]*/
                    ]
                ]);
            }

            session()->flash('success', 'Se ha añadido al carrito');
            return redirect()->route('web.event.show', $event->event_slug);

        } catch (\Exception $e) {

            Log::error('Livewire/Events/Tickets@addToCard', array($e->getLine() . ': ' . $e->getMessage()));

            session()->flash('error', 'No se pudo añadir al carrito');
        }

        return redirect()->route('web.event.show', $event->event_slug);
    }

    public function render()
    {
        $this->updateSectors();
        if (!$this->sectors) {
            $sectors = EventSectors::query();
            $this->sectors = $sectors->where('event_id', $this->event->id)->pluck('sector_name', 'id');
        }

        //$sectors->where('event_id', $this->event->id);

        //if ($this->date_id){

        //$sectors->where('date_id', $this->date_id);
        //}

        //$this->sectors = $sectors->pluck('sector_name', 'id');

        return view('livewire.web.events.tickets');
    }
}
