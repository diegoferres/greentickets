<?php

namespace App\Http\Controllers\Admin;

use App\Cart;
use App\Categories;
use App\EventDates;
use App\Events;
use App\EventSectors;
use App\EventTags;
use App\Http\Controllers\Controller;
use App\PlaceBranches;
use App\Places;
use App\PlacesClients;
use App\PlacesFiles;
use App\PlacesRegisters;
use App\PlacesReservations;
use App\PlacesSuggestions;
use App\User;
use App\Venues;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class PlacesController extends Controller
{
    /**
     * Instantiate a new UserController instance.
     */
    public function __construct()
    {
        /*$this->middleware('auth');*/
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (Auth::user()->hasRole('admin')){
            $places = Places::orderBy('created_at', 'DESC')->paginate(12);

        }else if (Auth::user()->hasRole('place')){

            $places = Places::where('user_id', Auth::id())->orderBy('created_at', 'DESC')->paginate(12);
        }


        return view('admin.places.index', compact('places'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.places.form');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'          => 'required|unique:places,name',
            'cell_phone'    => 'required',
            'logo'          => 'required',
            'file'          => 'required',
            'background_img'=> 'required'
        ])->setAttributeNames([
            'name'          => 'Nombre',
            'cell_phone'    => 'Número',
            'logo'          => 'Logo',
            'file'          => 'Archivo',
            'background_img'=> 'Fondo'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('info', 'Faltan algunos datos')->withErrors($validator)->withInput();
        }

        try{
            $place = new Places();
            $place->name        = $request->name;
            $place->slug        = $request->name;
            $place->cell_phone  = $request->cell_phone;
            $place->facebook    = $request->facebook;
            $place->instagram   = $request->instagram;
            $place->twitter     = $request->twitter;
            $place->whatsapp    = $request->whatsapp;

            $file = basename($request->file('file')->store('public/places/menu'));
            $place->file_name = $file;
            $place->file_type = $request->file('file')->getMimeType();

            $logo = basename($request->file('logo')->store('public/places/logo'));
            $place->logo = $logo;

            $background = basename($request->file('background_img')->store('public/places/background'));
            $place->background_img = $background;

            $place->font_color   = $request->font_color;

            $place->save();
        }catch (\Exception $e){

            return redirect()->back()->with('error', 'No se ha podido crear el registro');
        }

        return redirect()->route('admin.places.edit', $place->id)->with('success', 'Se ha registrado exitosamente');
    }

    public function edit($id)
    {
        $place = Places::findOrFail($id);

        /*$im = imagecreatefrompng('https://covid.entrasinpapel.com/storage/places/logo/zkk0NEqJfo7POzKiCCAxxBYuVPkm2D7KCDS9jA28.png');
        imagefilter($im, IMG_FILTER_GRAYSCALE,'0','0', '0');
        imagepng($im, 'dave.png');*/
        //imagedestroy($im);

        $url = 'https://covid.entrasinpapel.com/'.$place->name;

        return view('admin.places.form', compact('place', 'url'));
    }

    public function update(Request $request, $id)
    {
        //dd($request->except(['_token','_method']));
        $validator = Validator::make($request->all(), [
            'name'              => Auth::user()->hasRole('admin') ? 'required' : '',
            'cell_phone'        => 'required',
            'file_label'        => 'required_with:file',
            //'file'              => 'required_with:file_label',
            'file_extra_label'  => 'required_with:file_extra',
            //'file_extra'        => 'required_with:file_extra_label',
            'font_color'        => Auth::user()->hasRole('admin') ? 'required' : '',
        ])->setAttributeNames([
            'name'              => 'Nombre',
            'cell_phone'        => 'Número',
            'file_label'        => 'Titulo Archivo 1',
            'file'              => 'Archivo 1',
            'file_extra_label'  => 'Titulo Archivo 2',
            'file_extra'        => 'Archivo 2',
            'font_color'        => 'Color de Títulos',
        ]);



        if ($validator->fails()) {
            return redirect()->back()->with('info', 'Faltan algunos datos')->withErrors($validator)->withInput();
        }
        //dd('A');

        try{
            $place = Places::where('id', $id)->first();
            $place->update($request->except(['_token','_method', 'logo', 'file', 'file_extra']));

            //dd($place);
            /*$place->name        = $request->name;
            $place->slug        = strtolower(str_replace(' ', '', $request->name));
            $place->cell_phone  = $request->cell_phone;
            $place->facebook    = $request->facebook;
            $place->instagram   = $request->instagram;
            $place->twitter     = $request->twitter;
            $place->whatsapp    = $request->whatsapp;*/

            if ($request->exists('logo')){
                if (Storage::exists('public/places/logo/'.$place->logo)){
                    Storage::delete('public/places/logo/'.$place->logo);
                }

                $logo = basename($request->file('logo')->store('public/places/logo'));
                $place->logo = $logo;
            }

            if ($request->exists('file')){

                if (Storage::exists('public/places/menu/'.$place->file_name)){
                    Storage::delete('public/places/menu/'.$place->file_name);
                }

                $file = basename($request->file('file')->store('public/places/menu'));
                $place->file_name = $file;
                $place->file_type = $request->file('file')->getMimeType();
            }
            $place->file_label = $request->file_label;

            if ($request->exists('file_extra')){

                if (Storage::exists('public/places/menu/'.$place->file_extra)){
                    Storage::delete('public/places/menu/'.$place->file_extra);
                }

                $file = basename($request->file('file_extra')->store('public/places/menu'));
                $place->file_extra = $file;
            }
            $place->file_extra_label = $request->file_extra_label;

            if ($request->exists('background_img')){

                if (Storage::exists('public/places/background_img/'.$place->file_name)){
                    Storage::delete('public/places/background_img/'.$place->file_name);
                }

                $background = basename($request->file('background_img')->store('public/places/background'));
                $place->background_img = $background;
            }

            $place->font_color    = $request->font_color;
            $place->save();

        }catch (\Exception $e){
            Log::error('Update Place Error | Admin/PlacesController',array($e->getMessage()));
            return redirect()->back()->with('error', 'No se han podido actualizar los datos');
        }

        return redirect()->back()->with('success', 'Los datos se han actualizado correctamente');
    }

    public function storeFile(Request $request)
    {
        try {
            $place = Places::find($request->place_id);

            $file = basename($request->file('file')->store('public/places/'.$place->slug.'/files'));

            $placefile = new PlacesFiles();
            $placefile->file_path     = 'places/'.$place->slug.'/files';
            $placefile->file_name     = $file;
            $placefile->file_label    = $request->file_label;
            $placefile->sort          = PlacesFiles::where('place_id', $request->place_id)->get()->max('sort') ? PlacesFiles::where('place_id', $request->place_id)->get()->max('sort') +1 : 0 +1;
            $placefile->place_id      = $request->place_id;
            $placefile->active        = 1;
            $placefile->save();

            return redirect()->back()->with('success', 'Archivo agregado correctamente');

        }catch (\Exception $e){

            return redirect()->back()->with('error', 'No se pudo agregar el archivo');
        }
    }

    public function sortFiles(Request $request)
    {
        $i = 1;
        foreach ($request->sort as $key => $sort) {

            $file = PlacesFiles::find($key);
            $file->sort = $i;

            $file->active = isset($request->active[$key]) ? 1 : 0;

            $file->save();

            $i++;
        }

        return redirect()->back()->with('success', 'Cambios guardados');

    }

    public function deleteFile($file, $place)
    {
        $file = PlacesFiles::find($file);

        if (Storage::exists('public/places/'.$place.'/files/'.$file->file_name)){
            Storage::delete('public/places/'.$place.'/files/'.$file->file_name);
        }

        $file->delete();

        return redirect()->back()->with('success', 'Archivo eliminado');

    }

    public function clients(Request $request, $place)
    {
        /*$clients = PlacesClients::whereHas('registers', function ($q) use ($place){
            $q->where('place_id', $place);
        })->orderBy('created_at', 'DESC')->paginate(15);

        return view('admin.places.clients', compact('clients'));*/

        if ($request->ajax()) {
            $data = PlacesClients::whereHas('registers', function ($q) use ($place){
                $q->where('place_id', $place);
            })->orderBy('created_at','DESC')->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('born_date', function($row){
                    /*$btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editItem">Edit</a>';

                    $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteItem">Delete</a>';*/
                    return date('d-m-Y', strtotime($row->born_date));
                })
                ->addColumn('created_at', function($row){
                    /*$btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editItem">Edit</a>';

                    $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteItem">Delete</a>';*/
                    return date('d-m-Y H:m', strtotime($row->created_at));
                })
                /*->addColumn('action', function($row){
                    $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editItem">Edit</a>';

                    $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteItem">Delete</a>';
                    return $btn;
                })
                ->rawColumns(['action'])*/
                ->make(true);
        }

        return view('admin.places.clients',compact('items', 'place'));
    }

    public function registers(Request $request, $place)
    {
        if ($request->ajax()) {
            $data = PlacesRegisters::where('place_id', $place)->with('client')->orderBy('created_at','DESC')->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('created_at', function($row){
                    /*$btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editItem">Edit</a>';

                    $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteItem">Delete</a>';*/
                    return date('d-m-Y H:m', strtotime($row->created_at));
                })
                /*->addColumn('action', function($row){
                    $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editItem">Edit</a>';

                    $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteItem">Delete</a>';
                    return $btn;
                })
                ->rawColumns(['action'])*/
                ->make(true);
        }

        return view('admin.places.registers',compact('items', 'place'));
        //$registers = PlacesRegisters::where('place_id', $place)->orderBy('created_at', 'DESC')->paginate(15);

        //return view('admin.places.registers', compact('registers'));
    }

    public function reservations($place)
    {
        $reservations = PlacesReservations::where('place_id', $place)->orderBy('created_at', 'DESC')->paginate(15);

        return view('admin.places.reservations', compact('reservations'));
    }

    public function suggestions($place)
    {
        $suggestions = PlacesSuggestions::where('place_id', $place)->orderBy('created_at', 'DESC')->paginate(15);

        return view('admin.places.suggestions', compact('suggestions'));
    }
}
