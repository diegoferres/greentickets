<?php

namespace App\Http\Controllers\Admin;

use App\Categories;
use App\EventDates;
use App\Events;
use App\EventSectors;
use App\Http\Controllers\Controller;
use App\Venues;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        $events = Events::orderBy('created_at', 'desc')->get();

        return view('admin.events.index', compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function step1()
    {
        $categories = Categories::pluck('category_name', 'id');

        return view('web.event.create-event', compact('categories'));
    }

    public function step2(Request $request)
    {
        //dd(\session('step2'));
        if ($request->all()){
            $request->session()->put('step1', $request->all());
        }

        return view('web.event.create-event-2');
    }

    public function step3(Request $request)
    {
        //dd(\session('step2'));
        if ($request->all()){
            $request->session()->put('step2', $request->all());
        }

        return view('web.event.create-event-3');
    }

    public function step4(Request $request)
    {
        //dd($request->all());
        if ($request->all()){
            $request->session()->put('step3', $request->all());
        }

        return view('web.event.create-event-4');
    }

    public function event_detail(Request $request)
    {
        if ($request->all()){
            $file = $request->file('event_cover')->store('public');
            //dd($file);
            $request->merge(['event_cover'=>$file]);
            $request->session()->put('step4', $request->except('event_cover'));
        }

        return view('admin.event.event-detail');
    }

    public function event_save(Request $request)
    {
        $event = new Events();
        $event->event_name          = \session('step1')['event_name'];
        $event->event_description   = \session('step4')['event_description'];
        $event->event_organizer     = \session('step1')['event_organizer'];
        //$event->event_cover         = \session('step4')['event_cover'];
        $event->event_category      = \session('step1')['event_category'];
        $event->save();

        $eventDate = new EventDates();
        $eventDate->event_id            = $event->id;
        $eventDate->event_date          = \session('step2')['event_date'];
        $eventDate->event_time_start    = \session('step2')['event_time_start'];
        $eventDate->event_time_end      = \session('step2')['event_time_end'];
        $eventDate->save();

        $venue = new Venues();
        $venue->venue_name      = \session('step2')['venue_name'];
        $venue->venue_address   = \session('step2')['venue_address'];
        $venue->save();

        foreach(session('step3')['ticket_name'] as $key => $ticket):
            $eventSectors = new EventSectors();
            $eventSectors->event_id         = $event->id;
            $eventSectors->venue_id         = $venue->id;
            //$eventSectors->sector_type_id = \session('ste1')['venue_'];
            $eventSectors->sector_name      = session('step3')['ticket_name'][$key];
            $eventSectors->sector_price     = session('step3')['ticket_price'][$key];
            $eventSectors->sector_capacity  = session('step3')['ticket_count'][$key];
            $eventSectors->save();
        endforeach;

        \session()->forget('step1');
        \session()->forget('step2');
        \session()->forget('step3');
        \session()->forget('step4');

        return redirect('/');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
