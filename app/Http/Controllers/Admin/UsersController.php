<?php

namespace App\Http\Controllers\Admin;

use App\DocTypes;
use App\Http\Controllers\Controller;
use App\Sexs;
use App\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;
use Spatie\Permission\Models\Role;
use Yajra\DataTables\DataTables;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|Response|View
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = User::latest()->with('roles')->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('roles', function($row){
                    $roles = null;
                    foreach ($row->roles as $rol) {
                        $roles .= '<div class="badge badge-info">'.$rol->name.'</div><br>';
                    }

                    return $roles;
                })
                ->addColumn('action', function($row){
                    $btn = '<a href="'.route('admin.users.edit', $row->id).'" data-toggle="tooltip"  data-original-title="Editar" class="edit btn btn-primary btn-sm editItem">Editar</a>';

                    $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Eliminar" class="btn btn-danger btn-sm deleteItem">Eliminar</a>';
                    return $btn;
                })
                ->rawColumns(['roles','action'])
                ->make(true);
        }

        return view('admin.users.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        $doc_types  = DocTypes::pluck('doctype_name', 'id');
        $sexs       = Sexs::pluck('sex_name', 'id');
        $roles      = Role::pluck('name', 'id');
        $cities         = [];
        $neighborhoods  = [];
        return view('admin.users.form', compact('doc_types', 'sexs', 'cities', 'neighborhoods', 'roles'));
    }

    public function auxUpdatePwd($email, $password){
        $user = User::where('email', $email)->first();
        $user->password = Hash::make($password);
        $user->save();
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'name'          => ['required', 'string', 'max:255'],
            'email'         => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'born_date'     => ['required', 'date'],
            'cell_phone'    => ['required', 'min:10', 'max:12'],
            'password'      => ['required', 'string', 'min:8', 'confirmed'],
            'role_id'       => ['required'],
        ]);

        $user               = new User();
        $user->name         = $request->name;
        $user->email        = $request->email;
        $user->born_date    = $request->born_date;
        $user->cell_phone   = $request->cell_phone;
        $user->password     = Hash::make($request->password);
        $user->save();

        if ($user){
            $user->assignRole($request->role_id);
            $user->save();
            return redirect()->back()->with('success', 'El usuario '.$user->name.' se ha creado exitósamente');
        }else{
            return redirect()->back()->with('error', 'No se pudo crear el usuario. Favor, intenta nuevamente');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $user           = User::find($id);
        $doc_types      = DocTypes::pluck('doctype_name', 'id');
        $sexs           = Sexs::pluck('sex_name', 'id');
        $roles          = Role::pluck('name', 'id');
        $cities         = [];
        $neighborhoods  = [];

        return view('admin.users.form', compact('user', 'doc_types', 'sexs', 'cities', 'neighborhoods', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->password);


        //dd($request->all());
        $user = User::find($id);
        $user->update($request->except(['_token', 'city_id', 'role_id', 'password', 'password_confirmation']));
        if ($request->has('password')){
            $user->password = Hash::make($request->password);
        }
        $user->save();

        if ($user){
            $user->syncRoles($request->role_id);
            //$user->syncRoles(['writer', 'admin']);
            $user->save();

            return redirect()->back()->with('success', 'Los datos se han actualizado con éxito');
        }

        //dd($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
