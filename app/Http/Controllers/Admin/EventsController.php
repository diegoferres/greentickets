<?php

namespace App\Http\Controllers\Admin;

use App\Categories;
use App\EventDates;
use App\EventFrecuency;
use App\Events;
use App\EventSectors;
use App\EventFormats;
use App\Http\Controllers\Controller;
use App\OrderDetail;
use App\Orders;
use App\Payments;
use App\Venues;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;

class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        $events = Events::orderBy('created_at', 'desc')->get();
        //$events = Events::first();

        //dd($events->sectors[0]->tickets->count());
        //dd($events->orders_detail->sum('quantity'));
        //dd($events);
        return view('admin.events.index', compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Categories::pluck('category_name', 'id');
        return view('admin.events.form', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'event_name'            => 'required|min:1',
            'event_organizer'       => 'required',
            'event_category'        => 'required|numeric',
            'event_description'     => 'required|max:255',
            'event_cover'           => 'required|file',
            'venue.venue_name'      => 'required',
            'venue.venue_address'   => 'required',
        ])->setAttributeNames([
            'event_name'            => 'nombre',
            'event_organizer'       => 'organizador',
            'event_category'        => 'categoría',
            'event_description'     => 'descripción',
            'event_cover'           => 'banner',
            'venue.venue_name'      => 'nombre',
            'venue.venue_address'   => 'dirección'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('error', 'Error al guardar los datos del evento')->withErrors($validator)->withInput();
        }

        //dd('BB');

        $event = new Events();
        $event->user_id             = Auth::id();
        $event->event_name          = $request->event_name;
        $event->event_organizer     = $request->event_organizer;
        $event->event_category      = $request->event_category;
        $event->event_description   = $request->event_description;
        $event->save();

        if ($request->event_cover){
            $file = basename($request->file('event_cover')->store('public/events/'.$event->id));
            $event->event_cover = $file;
            $event->save();
        }

        $venue = new Venues();
        $venue->event_id        = $event->id;
        $venue->venue_name      = $request->venue['venue_name'];
        $venue->venue_address   = $request->venue['venue_address'];
        $venue->save();

        return redirect()->route('admin.events.edit', $event->id)->with('success', 'Se ha creado exitósamente el evento');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showEvent(Request $request)
    {
        //dd($request->all());
        Log::info('Request | Admin/EventsController@showEvent: '.$request->event_id);
        $event = Events::whereId($request->event_id)->first();
        //$event = Events::whereId($request->event_id)->whereUserId(Auth::id())->first();

        /*if (!$event = Events::whereId($request->event_id)->whereUserId(Auth::id())->first()){
            return redirect()->back()->with('info', 'El evento seleccionado no existe.');
        }*/
        Session::put('event', $event);
        Session::save();

        return redirect()->route('admin.event.info');

        //return view('admin.event.dashboard');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function eventDashboard()
    {
        if (Session::get('event')){
            if (!$event = Events::whereId(Session::get('event')->id)/*->whereUserId(Auth::id())*/->first()){
                return redirect()->back()->with('info', 'El evento seleccionado no existe.');
            }
        }else{
            return redirect()->back()->with('info', 'El evento seleccionado no existe.');
        }

        return view('admin.event.dashboard');
    }


    /**
     * Basic Information
     */
    public function eventInformation()
    {
        if (Session::get('event')){
            if (!$event = Events::whereId(Session::get('event')->id)/*->whereUserId(Auth::id())*/->first()){
                return redirect()->back()->with('info', 'El evento seleccionado no existe.');
            }
        }else{
            return redirect()->back()->with('info', 'El evento seleccionado no existe.');
        }

        $categories = Categories::pluck('category_name', 'id');

        $eventFormats = EventFormats::all();

        return view('admin.event.sections.1_event_information', compact('event', 'categories', 'eventFormats'));
    }

    /**
     * Basic Information
     */
    public function eventInfoStore(Request $request)
    {
        if (Session::get('event')){
            if (!$event = Events::whereId(Session::get('event')->id)/*->whereUserId(Auth::id())*/->first()){
                return redirect()->back()->with('info', 'El evento seleccionado no existe.');
            }
        }else{
            return redirect()->back()->with('info', 'El evento seleccionado no existe.');
        }

        $validatorEvent = Validator::make($request->all(), [
            'event_name'        => 'required',
            'event_organizer'   => 'required',
            'event_category'    => 'required',
            'event_description' => 'required|max:30000',
            'event_format'      => 'required',
            'venue_name'        => 'required_if:event_format,==,1',
            'venue_address'     => 'required_if:event_format,==,1'
        ])->setAttributeNames([
            'event_name'        => 'nombre',
            'event_organizer'   => 'organizador',
            'event_category'    => 'categoría',
            'event_description' => 'descripción',
            'event_format'      => 'tipo de evento',
            'venue_name'        => 'nombre',
            'venue_address'     => 'dirección'
        ]);
        if ($validatorEvent->fails()) {
            return redirect()->back()->with('error', 'Error al guardar los datos')->withErrors($validatorEvent)->withInput();
        }

        try{
            DB::beginTransaction();

            $event = Events::find(\session('event')['id']);
            $event->update(
                [
                    'event_name'        => $request->event_name,
                    'event_organizer'   => $request->event_organizer,
                    'event_category'    => $request->event_category,
                    'event_format'        => $request->event_format,
                    'event_description' => $request->event_description,
                ]
            );

            if ($request->venue_name){
                $event->venue->venue_name       = $request->venue_name;
                $event->venue->venue_address    = $request->venue_address;
                $event->venue->save();
            }

            DB::commit();

            return redirect()->back()->with('success', 'Datos guardados correctamente');
        }catch (\Exception $e){
            DB::rollBack();

            return redirect()->back()->withInput()->with('error', 'Hubo un error al guardar los datos');
        }
    }

    /**
     * Event Sectors
     */
    public function eventSectors($sector = null)
    {
        if (Session::get('event')){
            if (!$event = Events::whereId(Session::get('event')->id)/*->whereUserId(Auth::id())*/->first()){
                return redirect()->back()->with('info', 'El evento seleccionado no existe.');
            }
        }else{
            return redirect()->back()->with('info', 'El evento seleccionado no existe.');
        }

        if ($sector){
            $sector = EventSectors::find($sector);
        }

        return view('admin.event.sections.2_event_sectors', compact('event', 'sector'));
    }

    /**
     * Store or Update Sector
     */
    public function sectorStore(Request $request)
    {
        if (Session::get('event')){
            if (!$event = Events::whereId(Session::get('event')->id)/*->whereUserId(Auth::id())*/->first()){
                return redirect()->back()->with('info', 'El evento seleccionado no existe.');
            }
        }else{
            return redirect()->back()->with('info', 'El evento seleccionado no existe.');
        }

        $validator = Validator::make($request->all(), [
            'sector_name'     => 'required',
            'sector_price'    => 'required|numeric',
            'sector_capacity' => 'required|numeric',
        ])->setAttributeNames([
            'sector_name'     => 'sector',
            'sector_price'    => 'precio',
            'sector_capacity' => 'capacidad',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('error', 'Error al actualizar los datos del sector')->withErrors($validator)->withInput();
        }

        try{
            if ($request->sector_id){
                /*if (EventSectors::find($request->sector_id)->tickets->count() <= $request->sector_capacity){
                    return redirect()->back()->with('info', 'No se puede especificar una capacidad menor a la cantidad ya vendida');
                }*/
            }

            $sector = EventSectors::updateOrCreate(
                ['id' => $request->sector_id],
                [
                    'sector_name'       => $request->sector_name,
                    'sector_price'      => $request->sector_price,
                    'sector_capacity'   => $request->sector_capacity,
                    'event_id'          => \session('event')['id'],
                ]
            );
            return redirect()->back()->with('success', 'Datos guardados correctamente');
        }catch (\Exception $e){
            return redirect()->back()->withInput()->with('error', 'Hubo un error al guardar los datos');
        }
    }

    /**
     * Event Dates
     */
    public function eventDates($date = null)
    {
        if (Session::get('event')){
            if (!$event = Events::whereId(Session::get('event')->id)/*->whereUserId(Auth::id())*/->first()){
                return redirect()->back()->with('info', 'El evento seleccionado no existe.');
            }
        }else{
            return redirect()->back()->with('info', 'El evento seleccionado no existe.');
        }

        if ($date){
            $date = EventDates::find($date);
        }

        $eventFrecuency = EventFrecuency::all();


        return view('admin.event.sections.3_event_dates', compact('event', 'date', 'eventFrecuency'));
    }

    /**
     * Store or Update Sector
     */
    public function dateStore(Request $request)
    {
        //dd($request->all());
        if (Session::get('event')){
            /*if (!$event = Events::whereId(Session::get('event')->id)->whereUserId(Auth::id())->first()){
                return redirect()->back()->with('info', 'El evento seleccionado no existe.');
            }*/
        }else{
            return redirect()->back()->with('info', 'El evento seleccionado no existe.');
        }

        $validator = Validator::make($request->all(), [
            'event_date_start' => 'required_if:event_frecuency,==,1',
            'event_date_end'   => 'required_if:event_frecuency,==,1',
            'event_time_st'    => 'required_if:event_frecuency,==,1',
            'event_time_ed'    => 'required_if:event_frecuency,==,1',
            'event_date'       => 'required_if:event_frecuency,==,2',
            'event_time_start' => 'required_if:event_frecuency,==,2',
            'event_time_end'   => 'required_if:event_frecuency,==,2',
        ],[
            'event_date_start.required_if'  => 'La fecha inicio es requerida',
            'event_date_end.required_if'    => 'La fecha fin es requerida',
            'event_time_st.required_if'     => 'La hora inicio es requerida',
            'event_time_ed.required_if'     => 'La hora fin es requerida',
            'event_date.required_if'        => 'La fecha es requerida',
            'event_time_start.required_if'  => 'La hora inicio es requerida',
            'event_time_end.required_if'    => 'La hora termino es requerida',
        ])->setAttributeNames([
            'event_date'       => 'fecha',
            'event_time_start' => 'hora inicio',
            'event_time_end'   => 'hora fin',
            'event_date_start' => 'fecha inicio',
            'event_date_end'   => 'fecha fin',
            'event_time_st'    => 'hora inicio',
            'event_time_ed'    => 'hora fin',
        ]);
        if ($validator->fails()) {
            //dd($validator->errors());
            return redirect()->back()->with('info', 'Faltan algunos datos')->withErrors($validator)->withInput();
        }

        try{
            DB::beginTransaction();

            if ($request->event_frecuency == 1){
                $event = Events::where('id', \session('event')['id'])->update(
                    [
                        'event_frecuency'   => $request->event_frecuency,
                        'event_date_start'  => $request->event_date_start,
                        'event_date_end'    => $request->event_date_end,
                        'event_time_start'  => $request->event_time_st,
                        'event_time_end'    => $request->event_time_ed,
                    ]
                );

                EventDates::where('event_id', \session('event')['id'])->delete();

            }elseif ($request->event_frecuency == 2){

                $dates = EventDates::create(
                    [
                        'event_date'       => $request->event_date,
                        'event_time_start' => $request->event_time_start,
                        'event_time_end'   => $request->event_time_end,
                        'event_id'         => \session('event')['id'],
                    ]
                );

                $event = Events::where('id', \session('event')['id'])->update(
                    [
                        'event_frecuency'   => $request->event_frecuency,
                        'event_date_start'  => null,
                        'event_date_end'    => null,
                        'event_time_start'  => null,
                        'event_time_end'    => null,
                    ]
                );
            }

            DB::commit();
            return redirect()->back()->with('success', 'Datos guardados correctamente');
        }catch (\Exception $e){
            DB::rollBack();

            dd($e->getMessage());
            return redirect()->back()->withInput()->with('error', 'Hubo un error al guardar los datos');
        }
    }

    /**
     * Event Dates
     */
    public function eventPromotions($promotion = null)
    {
        if (Session::get('event')){
            if (!$event = Events::whereId(Session::get('event')->id)->first()){
                return redirect()->back()->with('info', 'El evento seleccionado no existe.');
            }
        }else{
            return redirect()->back()->with('info', 'El evento seleccionado no existe.');
        }

        if ($promotion){
            $promotion = EventDates::find($promotion);
        }

        return view('admin.event.sections.4_event_promotions', compact('event', 'promotion'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = Events::find($id);
        //$event = Events::where('id', $id)->with('venue')->first();
        $categories = Categories::pluck('category_name', 'id');

        return view('admin.events.form', compact('event', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        $event = Events::find($id);
        switch ($request->form_name){
            /**
             * Event
             */
            case 'formEvent':
                $validatorEvent = Validator::make($request->all(), [
                    'event_name'        => 'required',
                    'event_organizer'   => 'required',
                    'event_category'    => 'required|numeric',
                    'event_description' => 'required|max:255',
                    'event_cover'       => 'required|file',
                ])->setAttributeNames([
                    'event_name'        => 'nombre',
                    'event_organizer'   => 'organizador',
                    'event_category'    => 'categoría',
                    'event_description' => 'descripción',
                    'event_cover'       => 'banner',
                ]);

                if ($validatorEvent->fails()) {
                    return redirect()->back()->with('error', 'Error al guardar los datos del evento')->withErrors($validatorEvent)->withInput();
                }

                if ($event = Events::where('id',$id)->first()){
                    $event->user_id             = Auth::id();
                    $event->event_name          = $request->event_name;
                    $event->event_organizer     = $request->event_organizer;
                    $event->event_category      = $request->event_category;
                    $event->event_description   = $request->event_description;
                    $event->event_cover         = $request->event_cover;
                    $event->save();
                }
                break;

            /**
             * Venue
             */
            case 'formVenue':
                $validatorVenue = Validator::make($request->all(), [
                    'venue.venue_name'    => 'required',
                    'venue.venue_address' => 'required',
                ])->setAttributeNames([
                    'venue.venue_name'    => 'nombre',
                    'venue.venue_address' => 'dirección'
                ]);
                if ($validatorVenue->fails()) {

                    //dd($validatorVenue->errors());
                    return redirect()->back()->with('error', 'Error al guardar los datos del lugar')->withErrors($validatorVenue)->withInput();
                }

                if ($venue = Venues::where(['event_id' => $id])->first()){
                    $venue->venue_name     = $request->venue['venue_name'];
                    $venue->venue_address  = $request->venue['venue_address'];
                    $venue->save();
                }
                break;

            /**
             * Sectors
             */
            case 'formSectors':

                $validatorSector = Validator::make($request->all(), [
                    'sector_name.*'     => 'required',
                    'sector_price.*'    => 'required|numeric',
                    'sector_capacity.*' => 'required|numeric',
                ])->setAttributeNames([
                    'sector_name.*'     => 'sector',
                    'sector_price.*'    => 'precio',
                    'sector_capacity.*' => 'capacidad',
                ]);
                if ($validatorSector->fails()) {
                    //dd($validatorSector->errors());
                    return redirect()->back()->with('error', 'Error al actualizar los datos del sector')->withErrors($validatorSector)->withInput();
                }

                if ($request->sector_name){

                    foreach ($request->sector_name as $key =>$sector) {

                        if ($sector = EventSectors::where(['id'=> $key,'event_id' => $id])->first()){
                            $sector->sector_name        = $request['sector_name'][$key];
                            $sector->sector_price       = $request['sector_price'][$key];
                            $sector->sector_capacity    = $request['sector_capacity'][$key];
                            $sector->save();
                        }
                    }
                }

                if ($request->sector_name_n || $request->sector_price_n || $request->sector_capacity_n){

                    $validatorSectorN = Validator::make($request->all(), [
                        'sector_name_n'     => 'required_if:event_price_n,!=,""|required_if:event_capacity_n,!=,""',
                        'sector_price_n'     => 'required_if:sector_name_n,!=,""|required_if:event_capacity_n,!=,""',
                        'sector_capacity_n'  => 'required_if:sector_name_n,!=,""|required_if:event_price_n,!=,""',
                    ])->setAttributeNames([
                        'sector_name_n'     => 'sector',
                        'sector_price_n'    => 'precio',
                        'sector_capacity_n' => 'capacidad',
                    ]);
                    if ($validatorSectorN->fails()) {
                        //dd($validatorSector->errors());
                        return redirect()->back()->with('error', 'Error al crear el sector')->withErrors($validatorSectorN)->withInput();
                    }

                    $sector = new EventSectors();
                    $sector->event_id           = $id;
                    $sector->venue_id           = $event->venue->id;
                    $sector->sector_name        = $request->sector_name_n;
                    $sector->sector_price       = $request->sector_price_n;
                    $sector->sector_capacity    = $request->sector_capacity_n;
                    $sector->save();
                }
                break;
            /**
             * Dates
             */
            case 'formDates':
                $validatorDate = Validator::make($request->all(), [
                    'event_date.*'          => 'required|date',
                    'event_time_start.*'    => 'required',
                    'event_time_end.*'      => 'required',
                ])->setAttributeNames([
                    'event_date.*'          => 'fecha',
                    'event_time_start.*'    => 'hora inicio',
                    'event_time_end.*'      => 'hora fin',
                ]);
                if ($validatorDate->fails()) {
                    //dd($validatorDate->errors());
                    return redirect()->back()->with('error', 'Error al actualizar los datos de las fechas')->withErrors($validatorDate)->withInput();
                }

                if ($request->event_date){

                    foreach ($request->event_date as $key => $date) {

                        if ($date = EventDates::where(['id'=> $key,'event_id' => $id])->first()){
                            $date->event_date        = $request['event_date'][$key];
                            $date->event_time_start  = $request['event_time_start'][$key];
                            $date->event_time_end    = $request['event_time_end'][$key];
                            $date->save();
                        }
                    }
                }

                //if (!is_null($request->event_date_n) || !is_null($request->event_time_start_n) || !is_null($request->event_time_end_n)){
                if ($request->event_date_n || $request->event_time_start_n || $request->event_time_end_n){
                    //dd('A');
                    $validatorDateN = Validator::make($request->all(), [
                        'event_date_n'          => 'required_if:event_time_start_n,!=,""|required_if:event_time_end_n,!=,""',
                        'event_time_start_n'    => 'required_if:event_date_n,!=,""|required_if:event_time_end_n,!=,""',
                        'event_time_end_n'      => 'required_if:event_date_n,!=,""|required_if:event_time_start_n,!=,""',
                    ])->setAttributeNames([
                        'event_date_n'          => 'fecha',
                        'event_time_start_n'    => 'hora inicio',
                        'event_time_end_n'      => 'hora fin',
                    ]);
                    if ($validatorDateN->fails()) {
                        return redirect()->back()->with('error', 'Error al crear la fecha fechas')->withErrors($validatorDateN)->withInput();
                    }

                    $date = new EventDates();
                    $date->event_id          = $id;
                    $date->event_date        = $request->event_date_n;
                    $date->event_time_start  = $request->event_time_start_n;
                    $date->event_time_end    = $request->event_time_end_n;
                    $date->save();
                }
                break;
        }

        return redirect()->back()->with('success', 'Se han actualizado los datos del evento correctamente');
    }

    public function delete_sector($event, $sector)
    {
        //var_dump(EventSectors::where(['event_id' => $event, 'id' => $sector])->first());
        if (EventSectors::where('event_id', $event)->where('id', $sector)->first()->delete()){
            return redirect()->back()->with('success', 'Se ha eliminado el sector correctamente');
        }else{
            return redirect()->back()->with('error', 'No se pudo eliminar el sector');
        }
    }

    public function dateDelete(Request $request, $id)
    {
        if (EventDates::where('id', $id)->delete()){
            return redirect()->back()->with('success', 'Se ha eliminado la fecha correctamente');
        }else{
            return redirect()->back()->with('error', 'No se pudo eliminar la fecha');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        try{
            DB::beginTransaction();
            $event = Events::find($id);
            $event->delete();

            Storage::delete('file.jpg');

            DB::commit();

            return redirect()->back()->with('success', 'El evento '.$event->event_name.' se ha eliminado correctamente');

        }catch (\Exception $e){
            DB::rollBack();
            Log::info('Error | Admin/EventsController@destroy: '.$e->getMessage());;
            return redirect()->back()->with('error', 'Error al eliminar el evento');
        }
    }

    public function eventPayments()
    {
        $event = Events::whereId(Session::get('event')->id)->first();

        $orders= Orders::whereHas('detail',function ($q) use ($event){
            $q->whereIn('sector_id',$event->sectors()->get()->pluck('id')->toArray());
        })->with('payment','status','client')->get();



        return \view('admin.event.sections.5_event_payments',compact('orders'));

    }

    public function eventPaymentsDetails($order_id)
    {
        $details = OrderDetail::with('sector','order')->where('order_id',$order_id)->get();
        $order = Orders::find($order_id);

        return \view('admin.event.sections.orders.details',compact('details','order'));
    }

    public function eventPaymentsOrders($order_id)
    {
        $order = Orders::find($order_id);
        $payments = Payments::with('payment_method')->where('order_id',$order_id)->get();

        return \view('admin.event.sections.orders.payments',compact('payments','order'));
    }
}
