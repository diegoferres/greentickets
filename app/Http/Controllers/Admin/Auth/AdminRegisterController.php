<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Categories;
use App\EventDates;
use App\Events;
use App\EventSectors;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Controller;
use App\Venues;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AdminRegisterController extends RegisterController
{
    /**
     * Show the application registration form.
     *
     * @return \Illuminate\View\View
     */
    public function showRegistrationForm()
    {
        return view('web.auth.register');
    }
}
