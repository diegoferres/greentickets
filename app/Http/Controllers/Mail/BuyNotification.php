<?php

namespace App\Http\Controllers\Mail;

use App\Orders;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use QrCode;

class BuyNotification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Orders
     */
    public $order;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->view('web.mailing.mailing-paymet')
            ->subject('EntraSinPapel | Confirmación de Compra');


        $this->from('noreply@helpers.com.py','EntraSinPapel');

        foreach ($this->order->tickets as $i => $ticket) {
            $location = 'tickets/'.Carbon::now()->timestamp;
            $file_qr_location = public_path($location);
            $qr = QrCode::size(500)->format('png')->generate($ticket->hash,$file_qr_location);
            $pdf = \PDF::loadView('web.mailing.pdf', ['ticket' => $ticket,'qr'=>$location]);
            $this->attachData($pdf->output(), 'Entrada-nro-'.$i.'.pdf');
        }


        return $this;


        /*return $this->send([], [], function($message) use ($order){

            $pdf = \PDF::loadView('web.mailing.pdf', ['order' => $order]);
            //$pdf = \PDF::loadView('admin.reportes.factura2', ['invoice' => $invoice, 'invoiceConfig' => $invoiceConfig]);

            $message->to('diegoferreiraescobar@gmail.com','Diego Ferreira')->subject('Comprobante ESP');

            $message->from('noreply@helpers.com.py','EntraSinPapel');

            $message->attachData($pdf->output(), 'filename1.pdf');

            $message->attachData($pdf->output(), 'filename2.pdf');

        });*/
        /*return $this->view('web.mailing.mailing-paymet')
        ->subject('EntraSinPapel | Confirmación de Compra');*/


    }
}
