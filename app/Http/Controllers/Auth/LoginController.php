<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Laravel\Socialite\Facades\Socialite as Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\View\View
     */
    public function showLoginForm()
    {
        $showForm = 'login';
        return view('web.auth.form', compact('showForm'));
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateLogin(Request $request)
    {
        $request->validate([
            $this->username() => 'required|string|exists:users',
            'password' => 'required|string',
        ]);
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        if ($response = $this->authenticated($request, $this->guard()->user())) {
            return $response;
        }

        return $request->wantsJson()
            ? new Response('', 204)
            : redirect()->intended($this->redirectPath())->with('success', 'Has iniciado sesión correctamente');
    }

    /**
     * Check login status from client
     *
     * @return \Illuminate\Http\Response
     */
    protected function checklogin()
    {
        if (Auth::check()){
            return response()->json(true);
        }else{
            return response()->json(false);
        }
    }

    protected function authenticated() {

        foreach (Auth::user()->getRoleNames() as $roleName) {

//            if ($roleName === 'place'){
//                return redirect()->route('admin.places.index');
//            }

            if ($roleName === 'admin'){
                return redirect()->route('admin.events.index');
            }

        }
    }

    public function redirectToProvider(string $provider)
    {

        Log::info('redirectToProvider: '.$provider);
        try {
            $scopes = config("services.$provider.scopes") ?? [];
            if (count($scopes) === 0) {
                return Socialite::driver($provider)->redirect();
            } else {
                return Socialite::driver($provider)->scopes($scopes)->redirect();
            }
        } catch (\Exception $e) {
            abort(404);
        }
    }
    public function handleProviderCallback(string $provider)
    {

        Log::info('handleProviderCallback: '.$provider);
        /*try {*/
            $data = Socialite::driver($provider)->user();
            return $this->handleSocialUser($provider, collect($data));
        /*} catch (\Exception $e) {
            return redirect('/')->withErrors(['authentication_deny' => 'Login with '.ucfirst($provider).' failed. Please try again.'])->with('error', 'No se pudo iniciar sesión con'.$provider);
        }*/
    }
    public function handleSocialUser(string $provider, $data)
    {
        $user = User::where([
            "social->{$provider}->id" => $data['id'],
        ])->first();


        if (!$user) {
            $user = User::where([
                'email' => $data['email'],
            ])->first();
        }
        if (!$user) {
            return $this->createUserWithSocialData($provider, $data);
        }

        $social = array($user->social);

        $social[$provider] = [
            'id' => $data['id'],
            'token' => $data['token']
        ];
        //dd($social);
        $user->social = json_encode($social);

        $user->save();

        return $this->socialLogin($user);
    }
    public function createUserWithSocialData(string $provider, $data)
    {
        try {
            $user = new User;
            $user->name = $data['name'];
            $user->email = $data['email'];
            $social[$provider] = [
                'id' => $data['id'],
                'token' => $data['token']
            ];
            $user->social = $social;


            // Check support verify or not
            if ($user instanceof MustVerifyEmail) {
                $user->markEmailAsVerified();
            }
            $user->save();
            return $this->socialLogin($user);
        } catch (\Exception $e) {
        //    dd('A');
            return redirect('login')->withErrors(['authentication_deny' => 'Login with '.ucfirst($provider).' failed. Please try again.']);
        }
    }
    public function socialLogin(User $user)
    {
        auth()->loginUsingId($user['id']);
        return redirect($this->redirectTo)->with('success', 'Has iniciado sesión correctamente');
    }
}
