<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Laravel\Socialite\Facades\Socialite;

class SocialController extends Controller
{
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProviderFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallbackFacebook()
    {
        $user = Socialite::driver('facebook')->user();

        try{

            $userbd = User::firstOrCreate([
                'name' => $user->getName(),
                'email' => $user->getEmail(),
            ]);
            $userbd->social = $user->getId();
            $userbd->save();

            Auth::loginUsingId($userbd->ids,true);

            return redirect()->route('home')->with('success', 'Has iniciado sesión correctamente');

        }catch (\Exception $e){
            Log::error('handreProviderCallback | SocialController : '.$e->getMessage());

            return redirect()->back()->with('Error', 'Hubo un error al intentar iniciar sesión');
        }
    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function GoogleredirectToProvider()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function GooglehandleProviderCallback()
    {
        $user = Socialite::driver('google')->user();

        try{

            $userbd = User::firstOrCreate([
                'name' => $user->getName(),
                'email' => $user->getEmail(),
            ]);
            $userbd->social = $user->getId();
            $userbd->save();

            Auth::loginUsingId($userbd->ids,true);

            return redirect()->route('home')->with('success', 'Has iniciado sesión correctamente');

        }catch (\Exception $e){
            Log::error('handreProviderCallback | SocialController : '.$e->getMessage());

            return redirect()->back()->with('Error', 'Hubo un error al intentar iniciar sesión');
        }
    }
}