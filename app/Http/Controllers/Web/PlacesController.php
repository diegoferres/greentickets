<?php

namespace App\Http\Controllers\Web;

use App\Cart;
use App\Categories;
use App\EventDates;
use App\Events;
use App\EventSectors;
use App\EventTags;
use App\Http\Controllers\Controller;
use App\Places;
use App\PlacesClients;
use App\PlacesRegisters;
use App\PlacesReservations;
use App\PlacesSuggestions;
use App\Venues;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class PlacesController extends Controller
{
    /**
     * Instantiate a new UserController instance.
     */
    public function __construct()
    {
        /*$this->middleware('auth');*/
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index($place)
    {
        return redirect()->route('home')->with('warning', 'No existe el comercio');
        if ($place === 'long' || $place === 'Long'){
            $place = 'longbeach';
        }

        $place = Places::where('slug', str_replace(' ', '',strtolower($place)))->firstOrFail();

        return view('web.places.choices', compact('place'));
    }

    public function register($place)
    {
        $place = Places::where('slug', $place)->firstOrFail();

        return view('web.places.register', compact('place'));
    }

    public function validateCI($placeid, $ci)
    {
        $place = PlacesRegisters::where('place_id', $placeid)->whereHas('client', function ($q) use ($ci){
            $q->where('doc_number', $ci);
        })->whereDate('created_at', date('Y-m-d'))->first();

        $exists = $place ? true : false;

        return response()->json(['exists' => $exists]);
    }

    public function registerStore(Request $request, $place)
    {
        $plc = Places::where(['slug' =>  $place])->first();
        $ci = $request->doc_number;
        if (PlacesRegisters::where('place_id', $plc->id)->whereHas('client', function ($q) use ($ci){
            $q->where('doc_number', $ci);
        })->whereDate('created_at', date('Y-m-d'))->first()){
            return redirect()->route('web.places.showfile', $plc->slug)->with('success', '¡Ya te has registrado!');
        }
        $validator = Validator::make($request->all(), [
            'doc_number'    => 'required|numeric',
            'name'          => 'required',
            'born_date'     => 'required|date|before:15 years ago|date_format:d-m-Y',
            'address'       => 'required|max:300',
            'city'          => 'required|max:300',
            'table_number'  => 'required|numeric',
            'cell_phone'    => 'required|numeric',
            'email'         => 'required|email',
        ],[
            'born_date.before' => 'La fecha debe ser anterior a 15 años atrás',
            'born_date.date_format' => 'La fecha debe ser ej.: 01-01-1990',
        ])->setAttributeNames([
            'doc_number'    => 'Cédula',
            'name'          => 'Nombre y Apellido',
            'born_date'     => 'Fecha de Nacimiento',
            'address'       => 'Domicilio',
            'city'          => 'Ciudad',
            'table_number'  => 'Nro. de Mesa',
            'cell_phone'    => 'Teléfono',
            'email'         => 'Correo Electrónico',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('info', 'Faltan algunos datos')->withErrors($validator)->withInput();
        }

        try{

            DB::beginTransaction();

            $place = Places::where(['slug' =>  $place])->first();

            $client = PlacesClients::firstOrNew(['doc_number' =>  $request->doc_number]);
            $client->doc_number  = $request->doc_number;
            $client->name        = $request->name;
            $client->born_date   = date('Y-m-d', strtotime($request->born_date));
            $client->address     = $request->address;
            $client->city        = $request->city;
            $client->cell_phone  = $request->cell_phone;
            $client->email       = $request->email;
            $client->save();

            $register = new PlacesRegisters();
            $register->table_number= $request->table_number;
            $register->place_id    = $place->id;
            $register->client_id   = $client->id;
            $register->save();

            DB::commit();

            return redirect()->route('web.places.showfile', $place->slug)->with('success', '¡Gracias por registrar tu visita!');

        }catch (\Exception $e){

            DB::rollBack();
            Log::error('Error Places Register | PlacesController'. json_encode($e->getMessage()));

            return redirect()->back()->with('error', 'Hubo un error al registrarse');
        }
    }

    public function reservation($place)
    {
        $place = Places::where('slug', $place)->firstOrFail();

        return view('web.places.reservation', compact('place'));
    }

    public function reservationStore(Request $request, $place)
    {
        $validator = Validator::make($request->all(), [
            'doc_number'    => 'required|numeric',
            'name'          => 'required',
            'reserved_date' => 'required|date|after_or_equal:today|date_format:d-m-Y',
            'amount_people' => 'required|numeric',
            'cell_phone'    => 'required|numeric'
        ],[
            'reserved_date.after_or_equal' => 'La fecha debe ser superior o igual a la fecha de hoy',
            'born_date.date_format' => 'La fecha debe ser ej.: 01-01-2020',
        ])->setAttributeNames([
            'doc_number'    => 'Cédula',
            'name'          => 'Nombre y Apellido',
            'reserved_date' => 'Fecha de Reserva',
            'amount_people' => 'Cantidad de Personas',
            'cell_phone'    => 'Teléfono',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('info', 'Faltan algunos datos')->withErrors($validator)->withInput();
        }

        try{

            DB::beginTransaction();

            $place = Places::where(['slug' =>  $place])->firstOrFail();

            $client = PlacesClients::firstOrNew(['doc_number' =>  $request->doc_number]);
            $client->doc_number  = $request->doc_number;
            $client->name        = $request->name;
            $client->cell_phone  = $request->cell_phone;
            $client->save();

            $reserved = new PlacesReservations();
            $reserved->reserved_date    = date('Y-m-d', strtotime($request->reserved_date));;
            $reserved->amount_people    = $request->amount_people;
            $reserved->place_id         = $place->id;
            $reserved->client_id        = $client->id;
            $reserved->save();

            DB::commit();

            return redirect()->route('web.places.thanks', $place->slug)->with('success', '¡Gracias por registrar tu reserva!');

        }catch (\Exception $e){

            DB::rollBack();
            Log::error('Error Places Reserva | PlacesController'. json_encode($e->getMessage()));

            return redirect()->back()->with('error', 'Hubo un error al reservar');
        }
    }

    public function suggestion($place)
    {
        $place = Places::where('slug', $place)->firstOrFail();

        return view('web.places.suggestion', compact('place'));
    }

    public function suggestionStore(Request $request, $place)
    {
        $validator = Validator::make($request->all(), [
            'message'       => 'required'
        ])->setAttributeNames([
            'message'    => 'Mensaje'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('info', 'Faltan algunos datos')->withErrors($validator)->withInput();
        }

        try{

            DB::beginTransaction();

            $place = Places::where(['slug' =>  $place])->firstOrFail();

            $suggestion = new PlacesSuggestions();
            $suggestion->name       = $request->name;
            $suggestion->message    = $request->message;
            $suggestion->place_id   = $place->id;
            $suggestion->save();

            DB::commit();

            return redirect()->route('web.places.thanks', $place->slug)->with('success', '¡Gracias por tu sugerencia!');

        }catch (\Exception $e){

            DB::rollBack();
            Log::error('Error Places Suggestion | PlacesController'. json_encode($e->getMessage()));

            return redirect()->back()->with('error', 'Hubo un error al guardar los datos');
        }
    }

    public function showFile($place)
    {

        $place = Places::where(['slug' =>  $place])->firstOrFail();

        return view('web.places.showFile', compact('place'));
    }

    public function showThanks($place)
    {
        $place = Places::where(['slug' =>  $place])->firstOrFail();

        return view('web.places.thanks', compact('place'));
    }
}
