<?php

namespace App\Http\Controllers\Web;

use App\Cart;
use App\Categories;
use App\EventCovers;
use App\EventDates;
use App\EventRestrictions;
use App\Events;
use App\EventSectors;
use App\EventTags;
use App\Http\Controllers\Controller;
use App\Venues;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class EventController extends Controller
{
    /**
     * Instantiate a new UserController instance.
     */
    public function __construct()
    {
        /*$this->middleware('auth');*/
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('web.home.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function step1()
    {
        $categories = Categories::pluck('category_name', 'id');

        return view('web.event.create-event', compact('categories'));
    }

    public function step2(Request $request)
    {
        //dd(session('step2'));
        if ($request->method() == 'POST') {
            $validator = Validator::make($request->all(), [
                'event_name' => 'required',
                'event_organizer' => 'required',
                'event_category' => 'required',
            ])->setAttributeNames([
                'event_name' => 'nombre',
                'event_organizer' => 'organizador',
                'event_category' => 'categoría',
            ]);
            if ($validator->fails()) {
                return redirect()->back()->with('warning', 'Faltan algunos datos')->withErrors($validator)->withInput();
            }

            $request->session()->put('step1', $request->all());
        }

        if (session('step2')) {
            $step2 = session('step2');
            unset($step2['event_date'][99]);
            $request->session()->put('step2', $step2);
        }

        return view('web.event.create-event-2');
    }

    public function step3(Request $request)
    {
        //dd($request->all());
        if ($request->method() == 'POST') {
            $validator = Validator::make($request->all(), [
                'event_format' => 'required',
                'venue_name' => 'required_if:event_format,==,1|required_if:event_format,==,3',
                'venue_address' => 'required_if:event_format,==,1|required_if:event_format,==,3',
                'event_date_start' => 'required_if:event_frecuency,==,1',
                'event_date_end' => 'required_if:event_frecuency,==,1',
                'event_time_st' => 'required_if:event_frecuency,==,1',
                'event_time_ed' => 'required_if:event_frecuency,==,1',
                'event_date.*' => 'required_if:event_frecuency,==,2',
                'event_time_start.*' => 'required_if:event_frecuency,==,2',
                'event_time_end.*' => 'required_if:event_frecuency,==,2',
            ], [
                'venue_name.required_if' => 'El nombre del lugar es requerido',
                'venue_address.required_if' => 'La dirección del lugar es requerido',
                'event_date_start.required_if' => 'La fecha inicio es requerida',
                'event_date_end.required_if' => 'La fecha fin es requerida',
                'event_time_st.required_if' => 'La hora inicio es requerida',
                'event_time_ed.required_if' => 'La hora fin es requerida',
                'event_date.*.required_if' => 'La fecha es requerida',
                'event_time_start.*.required_if' => 'La hora inicio es requerida',
                'event_time_end.*.required_if' => 'La hora termino es requerida',
            ])->setAttributeNames([
                'event_format' => 'formato de evento',
                'venue_name' => 'nombre del lugar',
                'venue_address' => 'direccion del lugar',
            ]);

            if ($validator->fails()) {
                //dd($validator->errors->all());
                return redirect()->back()->with('warning', 'Faltan algunos datos')->withErrors($validator)->withInput();
            }

            $input = $request->all();

            if ($request->event_frecuency == 1) {

                unset(session('step2')['event_date'][99]);
                unset($request['event_date']);
                unset($request['event_time_start']);
                unset($request['event_time_end']);
            } else {

                $input['event_date'][99] = 'Para todo el evento';

                unset($request['event_date_start']);
                unset($request['event_date_end']);
                unset($request['event_time_st']);
                unset($request['event_time_ed']);
            }

            $request->session()->put('step2', $input);
        }

        return view('web.event.create-event-3');
    }

    public function step4(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'event_sale_type'   => 'sometimes|required',
            'sector_name.*'     => 'required',
            'sector_price.*'    => 'required',
            'event_count.*'     => 'required',
            'date_id.*'         => 'required_if:event_sale_type,==,2',
        ], [
            'date_id.*.required_if' => 'El campo fecha es requerido',
        ])->setAttributeNames([
            'sector_name.*'     => 'nombre',
            'sector_price.*'    => 'precio',
            'event_count.*'     => 'cantidad',
            'date_id.*'         => 'fecha',
        ]);

        if ($validator->fails()) {

            return redirect()->back()->with('warning', 'Faltan algunos datos')->withErrors($validator)->withInput();
        }
        //dd('A');
        if ($request->all()) {
            $request->session()->put('step3', $request->all());
        }

        return view('web.event.create-event-4');
    }

    public function event_save(Request $request)
    {
        $validator = Validator::make($request->all(), [
        ], [
            'event_covers' => 'required',
            'event_subtitle' => 'required|string|max:5',
            'event_description' => 'required|string|max:5',
        ])->setAttributeNames([
            'event_covers' => 'imágenes',
            'event_subtitle' => 'subtitulo',
            'event_description' => 'descripción',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('warning', 'Faltan algunos datos')->withErrors($validator)->withInput()->withInput();
        }

        try {
            DB::beginTransaction();
            /**
             * event
             */
            $event = new Events();
            $event->user_id = Auth::id();
            $event->event_name = \session('step1')['event_name'];
            $event->event_subtitle = $request->event_subtitle;
            $event->event_description = $request->event_description;
            $event->event_organizer = \session('step1')['event_organizer'];
            $event->event_category = \session('step1')['event_category'];
            $event->event_format = \session('step2')['event_format'];
            $event->event_frecuency = \session('step2')['event_frecuency'];
            $event->event_sale_type = isset(\session('step3')['event_sale_type']) ? \session('step3')['event_sale_type'] : 1;
            $event->published = true;

            $event->save();

            /*
             * events_restrictions
             */
            $restriction = new EventRestrictions();
            $restriction->event_id = $event->id;
            $restriction->restriction_id = \session('step3')['restric_type'];
            $restriction->value = \session('step3')['restric_value'];
            $restriction->save();


            /**
             * event_tags
             */
            if (empty(\session('step2')['event_tags'])) {

                foreach (explode(',', \session('step1')['event_tags']) as $tag):
                    $event_tag = new EventTags();
                    $event_tag->event_id = $event->id;
                    $event_tag->event_tag = $tag;
                    $event_tag->save();
                endforeach;
            }

            /**
             * event_dates
             */
            if (session('step2')['event_frecuency']) {

                if (session('step2')['event_frecuency'] == 1) {

                    Log::info('Frecuency UNICA VEZ');

                    $event->event_date_start = session('step2')['event_date_start'];
                    $event->event_date_end = session('step2')['event_date_end'];
                    $event->event_time_start = session('step2')['event_time_st'];
                    $event->event_time_end = session('step2')['event_time_ed'];
                    $event->save();
                } elseif (session('step2')['event_frecuency'] == 2) {
                    $eventIndex = null;

                    Log::info('Frecuency RECURRENTE');

                    foreach (session('step2')['event_date'] as $key => $date):

                        if ($key != 99){

                            $eventDate = new EventDates();
                            $eventDate->event_id = $event->id;
                            $eventDate->event_date = \session('step2')['event_date'][$key];
                            $eventDate->event_time_start = \session('step2')['event_time_start'][$key];
                            $eventDate->event_time_end = \session('step2')['event_time_end'][$key];
                            $eventDate->save();

                            $eventIndex[$key] = $eventDate->id;
                        }


                    endforeach;
                }
            }

            /**
             * venues
             */
            if (!empty(\session('step2')['venue_name'])) {

                $venue = new Venues();
                $venue->event_id = $event->id;
                $venue->venue_name = \session('step2')['venue_name'];
                $venue->venue_address = \session('step2')['venue_address'];
                $venue->save();
            }

            /**
             * Sectors
             */

            foreach (session('step3')['sector_name'] as $key => $ticket):
                //Log::info('SECTOR DATE: '.$eventIndex[session('step3')['date_id'][$key]]);
                $eventSectors = new EventSectors();
                $eventSectors->event_id = $event->id;
                $eventSectors->venue_id = isset($venue->id) ? $venue->id : null;
                //$eventSectors->sector_type_id = \session('ste1')['venue_'];
                $eventSectors->sector_name = session('step3')['sector_name'][$key];
                $eventSectors->sector_price = session('step3')['sector_price'][$key];
                $eventSectors->sector_capacity = session('step3')['sector_count'][$key];
                $eventSectors->date_id = isset($eventIndex[session('step3')['date_id'][$key]]) ? $eventIndex[session('step3')['date_id'][$key]] : null;
                $eventSectors->save();
            endforeach;

            /**
             * Event Covers
             */
            if ($request->event_covers) {
                $path = '/events/1/covers/';
                foreach ($request->event_covers as $key => $cover):
                    $file = basename($cover->store('public' . $path));

                    $cov = new EventCovers();
                    $cov->url = $path;
                    $cov->file_name = $file;
                    $cov->order = $key + 1;
                    $cov->event_id = $event->id;
                    $cov->save();
                endforeach;
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            dd($e->getMessage());

            //return redirect()->back()->with('error', 'Hubo un error al guardar los datos')->withInput();
            return redirect()->back()->with('error', $e->getMessage())->withInput();
        }

        /**
         * kill session
         */
        \session()->forget('step1');
        \session()->forget('step2');
        \session()->forget('step3');
        \session()->forget('step4');

        return redirect()->route('web.event.show', $event->event_slug)->with('finish', true);


    }

    public function eventFinish($slug)
    {
        $event = Events::where('event_slug', $slug)->first();

        //dd($event->dates);
        return view('web.event.event-finish', compact('event'));
    }

    public function create_finish($data)
    {

        return view('web.event.event-finish');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function show($slug)
    {
        if (!$event = Events::where('event_slug', $slug)->first()){
            return redirect()->back()->with('info', 'Este evento no existe');
        }
        //$event = Events::where('event_slug', $slug)->first();

        return view('web.event.event-detail', compact('event'));
    }

    /**
     * Get Sector price and calculate
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function sum_price(Request $request)
    {
        $sector = EventSectors::find($request->sector_id);

        $price = $sector->sector_price * $request->count;

        return response()->json(['unit_price' => $sector->sector_price, 'subtotal' => $price], 200);
    }

    public function getSectors(Request $request)
    {
        $sectors = EventSectors::query();

        if (!$request->date_id) {
            $sectors->where('date_id', null)->where('event_id', $request->event_id);
        } else {
            $sectors->where('date_id', $request->date_id);
        }
        //dd($sectors);
        $sectors = $sectors->pluck('sector_name', 'id');


        return response()->json($sectors, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
