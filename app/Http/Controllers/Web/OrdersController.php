<?php

namespace App\Http\Controllers\Web;

use App\EventDates;
use App\Events;
use App\EventSectors;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Mail\BuyNotification;
use App\OrderDetail;
use App\Orders;
use App\Payments;
use App\Tickets;
use Barryvdh\DomPDF\PDF;
use Dotenv\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Darryldecode\Cart\Cart;
use Illuminate\Support\Facades\Mail;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class OrdersController extends Controller
{
    public function testMail()
    {
        $order = OrderDetail::first();

        $pdf = \PDF::loadView('web.mailing.pdf', ['detail' => $order]);
        $pdf->download('filename.pdf');
        //$this->attachData($pdf->output(), 'filename.pdf');


        Mail::to('diegoferreiraescobar@gmail.com')->send(new BuyNotification($order));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $order = Orders::whereHas('detail')->first();

        $pdf = \PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('web.mailing.pdf', ['detail' => $order]);
        //$pdf->is
        return $pdf->download('filename.pdf');

        //$pdf = \PDF::loadView('web.mailing.pdf', ['order' => $order]);
        //return $pdf->download('invoice.pdf');

        //return view('web.mailing.pdf', compact('order'));

        //Mail::send(new BuyNotification($order));
        //Mail::to('diegoferreiraescobar@gmail.com')->send(new BuyNotification($order));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function showCart()
    {
        if (\Cart::getContent()->count() == 0) {
            return redirect()->back()->with('info', 'El carrito se encuentra vacío');
        }

        return view('web.order.cart');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addToCart(Request $request)
    {
        $event = Events::find($request->event_id);

        $validator = \Validator::make($request->all(), [
            'count' => 'required|numeric|min:1'.(isset($event->restrictions->first()->value) ? '|max:'.$event->restrictions->first()->value : ''),
            'sector_id' => 'required',
            'ticket_data_doc' => 'sometimes|required|string|max:10',
            'ticket_data_name' => 'sometimes|required|string|max:100'
        ],[
            'ticket_data_doc.max' => 'El campo no puede tener más de 10 caracteres',
        ])->setAttributeNames([
            'count' => 'cantidad',
            'sector_id  ' => 'sector',
            'ticket_data_doc' => 'documento',
            'ticket_data_name' => 'nombre'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput()->with('warning', 'No se pudo añadir al carrito');
        }

        try {
            if ($request->ticket_data_doc){
                $count = 0;
                foreach (\Cart::getContent() as $item) {
                    if ($sector = EventSectors::where('id', explode(',', $item->id)[0])->whereHas('event',function ($q) use ($event){
                        $q->where('id', $event->id);
                    })->first()){
                        if ($item->attributes->ticket_data_doc == $request->ticket_data_doc){
                            return redirect()->back()->withInput()->with('warning', 'Ya existe una entrada asociada a este documento');
                        }else{
                            $count++;
                        }
                    }
                }

                if ($event->restrictions->first()->value == $count || $request->quantity >= $event->restrictions->first()->value ){
                    return redirect()->back()->withInput()->with('warning', 'El evento solo permite '.$event->restrictions->first()->value.' entradas por usuario');
                }
            }
            //dd('A');

            $sector = EventSectors::find($request->sector_id);

            $id = $request->sector_id;
            $id .= $request->date_id ? ',' . $request->date_id : '';
            $id .= $request->ticket_data_doc ? ',' . $request->ticket_data_doc : '';

            if (\Cart::get($id)) {

                \Cart::update($id, [/*'attributes' => [
                ],*/ 'quantity' => ['relative' => false, 'value' => $request->count],
                    'price' => $sector->sector_price]);

            } else {
                \Cart::add([
                    'id' => $id,
                    'name' => $sector->sector_name,
                    'price' => $sector->sector_price,
                    'quantity' => $request->count,
                    'attributes' => [
                        'date' => $request->date_id ? EventDates::find($request->date_id)->event_date : null,
                        'event_slug' => $sector->event->event_slug,
                        'event_name' => $sector->event->event_name,
                        'event_cover' => $sector->event->covers->first()->url . $sector->event->covers->first()->file_name,
                        'ticket_data_name' => $request->ticket_data_name ?: null,
                        'ticket_data_doc' => $request->ticket_data_doc ?: null,
                        /*'coupons'       => [
                            'A10120B' => 15
                        ]*/
                    ]
                ]);
            }

        } catch (\Exception $e) {

            Log::error('OrdersController@addToCard', array($e->getMessage()));

            return redirect()->back()->with('error', 'No se pudo añadir al carrito');
        }

        return redirect()->back()->with('success', 'Se ha añadido al carrito');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function removeFromCart($id)
    {
        try {
            if (\Cart::remove($id)) {
                return redirect()->back()->with('success', 'Se ha eliminado del carrito');
            }
        } catch (\Exception $e) {
            Log::error('OrdersController@addToCard', array($e->getMessage()));
            return redirect()->back()->with('error', 'No se pudo eliminar del carrito');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function checkout()
    {
        if (\Cart::isEmpty()) {
            return redirect('/')->with('info', 'El carrito está vacío');
        }

        return view('web.order.checkout');
    }

    public function checkoutPayment()
    {
        if (\Cart::isEmpty()) {
            return redirect('/')->with('info', 'El carrito está vacío');
        }

        if (\Cart::getTotal() == 0){
            return redirect()->route('web.orders.checkout.finish');
        }

        return view('web.order.checkout-payment');
    }

    public function checkoutFinish(Request $request, $hash = false)
    {
        //$payment = Payments::where('hash', $hash)->first();

        $payment = new PaymentController();

        if (!$hash) {

            if (\Cart::isEmpty()) {
                return redirect('/')->with('info', 'El carrito está vacío');
            }

            $validator = \Validator::make($request->all(), [
                'payment_method_id' => 'sometimes',
                'voucher' => 'required_if:payment_method_id,==,1'
            ],
                [
                    'voucher.required_if' => 'El número de operación es requerido cuando el método de pago es Transferencia'
                ]);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->with('error', 'Error al procesar el pedido');
            }


            try {
                DB::beginTransaction();
                $order = new Orders();
                $order->subtotal = \Cart::getSubTotal();
                //$order->payment_method_id = $request->payment_method_id;
                if ($request->payment_method_id == 1) {
                    $order->status_id = 2;
                } elseif ($request->payment_method_id == 2) {
                    $order->status_id = 1;
                }else{
                    $order->status_id = 1;
                }
                $order->user_id = Auth::id();
                $order->save();

                foreach (\Cart::getContent() as $item):
                    $ord_detail = new OrderDetail();
                    $ord_detail->price = $item->price;
                    $ord_detail->quantity = $item->quantity;
                    $ord_detail->sector_id = explode(',', $item->id)[0];
                    $ord_detail->date_id = isset(explode(',', $item->id)[1]) ? explode(',', $item->id)[1] : null;
                    $ord_detail->order_id = $order->id;
                    $ord_detail->save();

                    for ($i = 1; $i <= $item->quantity; $i++):
                        $ticket = new Tickets();
                        $ticket->order_detail_id = $ord_detail->id;
                        $ticket->status_id = 1;
                        $ticket->hash = Hash::make($ticket->id);
                        $ticket->save();
                    endfor;
                endforeach;

                DB::commit();


                if ($order->subtotal > 0):
                    if ($request->payment_method_id == 2) :
                        $hash = $payment->insert($order);
                        return redirect('https://www.pagopar.com/pagos/' . $hash);
                    else:
                        Mail::to($order->client->email)->send(new BuyNotification($order));
                        return redirect()->route('web.orders.checkout.finish', ['hash' => $order->id]);
                    endif;
                else:
                    Mail::to($order->client->email)->send(new BuyNotification($order));
                    return redirect()->route('web.orders.checkout.finish', ['hash' => $order->id]);
                endif;

            } catch (\Exception $e) {
                DB::rollback();
                \Log::error($e->getMessage());

                return redirect()->back()->with('error', 'No se pudo procesar la orden.');
            }
        } else {

            $pay = false;
            if (ctype_alpha($hash)) {

                $payment = new PaymentController();
                $checkOrder = $payment->checkOrder($hash);
                $pay = Payments::where('hash', $hash)->first();
            }else{

                $order = Orders::where('id', $hash)->first();
            }

            //dd($checkOrder = $payment->checkOrder($hash));
            //if (!$order = Orders::where('id', $hash)->first()){


            //$order = Orders::where('id', $payment->order_id)->first();
            //}


            //$payment = false;
            if (isset($checkOrder)){
                $pay->paid = $checkOrder['resultado'][0]->pagado;
                $pay->order_number = $checkOrder['resultado'][0]->numero_pedido;
                $pay->method = $checkOrder['resultado'][0]->forma_pago;
                $pay->method_id = $checkOrder['resultado'][0]->forma_pago_identificador;
                $pay->paid_date = $checkOrder['resultado'][0]->fecha_pago;
                $pay->amount = intval($checkOrder['resultado'][0]->monto);
                $pay->max_pay_date = $checkOrder['resultado'][0]->fecha_maxima_pago;
                $pay->hash = $hash;
                $pay->cancelled = $checkOrder['resultado'][0]->cancelado;
                $pay->save();
            }

        }

        $cartContent['subtotal'] = \Cart::getSubTotal();
        $cartContent['total'] = \Cart::getTotal();
        $cartContent['totalcount'] = \Cart::getTotalQuantity();
        $cartContent['items'] = \Cart::getContent();

        return view('web.order.checkout-finish', compact('pay', 'order', 'cartContent'))->with('success', '¡Se ha generado la orden correctamente!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
