<?php

namespace App\Http\Controllers\Web;

use App\Categories;
use App\DocTypes;
use App\Events;
use App\Http\Controllers\Controller;
use App\Orders;
use App\Sexs;
use App\Tickets;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        $user = User::find(Auth::id());
        //$user->assignRole(1);
        $sexs = Sexs::pluck('sex_name', 'id');
        $doctypes = DocTypes::pluck('doctype_name', 'id');

        return view('web.profile.index', compact('user', 'sexs', 'doctypes'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function profileUpdate(Request $request, $id)
    {
        //dd($request->sex_id);
        $user = User::find($id)->update($request->except('_token'));

        return redirect()->back()->with('success', 'Sus datos se han actualizado con éxito');

        //return view('web.profile.index', compact('user', 'sexs', 'doctypes'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function history()
    {
        $orders = Orders::where('user_id', Auth::id())->get();

        //dd($orders);
        return view('web.profile.history', compact('orders'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function historyDetail($orderid)
    {
        $order = Orders::where(['id' => $orderid, 'user_id' => Auth::id()])->first();

        return view('web.profile.history-detail', compact('order'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function payments()
    {
        return view('web.profile.payment');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function purchased_events()
    {
        $tickets = Tickets::whereHas('order_detail.order', function ($q) {
            $q->where('user_id', Auth::id());
        })->get();

        return view('web.profile.purchased_events', compact('tickets'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
