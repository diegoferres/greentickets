<?php

namespace App\Http\Controllers\Web;

use App\Categories;
use App\DocTypes;
use App\Events;
use App\Http\Controllers\Controller;
use App\OrderDetail;
use App\Orders;
use App\Sexs;
use App\Tickets;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class OrganizerController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $events = Events::where('user_id', Auth::id())->get();

        return view('web.organizer.index', compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function eventEdit($id)
    {
        $event = Events::find($id);

        return view('web.organizer.edit-event', compact('event'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function eventShow($id)
    {
        $event = Events::with('orders_detail')->find($id);

        return view('web.organizer.list-event', compact('event'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function promotionCreate()
    {
        //$event = Events::find($id);

        return view('web.organizer.create-sale');
    }

    public function eventShowTicketsStatus($id,$order_details_id)
    {
        $details = OrderDetail::with('tickets.status')->find($order_details_id);
        $event = Events::find($id);


        return view('web.organizer.list-ticket-status',compact('event','details'));
    }

    public function controlTickets($id)
    {
        $event = Events::find($id);

        return view('web.organizer.control-tickets',compact('event'));
    }

    public function saleTickets($id)
    {
        $event = Events::find($id);

        return view('web.organizer.sale-tickets',compact('event'));
    }
}
