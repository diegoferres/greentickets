<?php

namespace App\Http\Controllers;

use App\Events;
use App\Http\Controllers\Admin\UsersController;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class SearchController extends Controller
{
    public function find(Request $request)
    {
        return Events::search($request->get('q'))->with('category')->get();
    }

    public function searchAll(Request $request)
    {
        if ($request->all()){

            $events = Events::search($request->get('q'))->get();

            $q = $request->q;
            return view('web.home.search', compact('events', 'q'));

        }else{

            $events = Events::orderBy('created_at', 'DESC')->paginate(10);
            $q = null;
            return view('web.home.search', compact('events', 'q'));
        }
    }

    /*public function search()
    {
        return view('web.home.search');

    }*/
}
