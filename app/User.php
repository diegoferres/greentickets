<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Nicolaslopezj\Searchable\SearchableTrait;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable implements MustVerifyEmail
{
    use SearchableTrait;
    use HasRoles;
    use Notifiable;

    protected $searchable = [
        'columns' => [
            'users.name' => 10,
            /*'profiles.username' => 5,
            'profiles.bio' => 3,
            'profiles.country' => 2,
            'profiles.city' => 1,*/
        ],
        /*'joins' => [
            'profiles' => ['users.id','profiles.user_id'],
        ],*/
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email','password', 'doc_number', 'born_date', 'telephone',
        'cell_phone', 'business_name', 'ruc_number', 'street1',
        'street2', 'sex_id', 'doc_type_id', 'city_id', 'neighborhood_id'

    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'social' => 'array',
    ];

    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = strtolower($value);
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function sex()
    {
        $this->belongsTo('App\Sexs', 'sex_id');
    }

    public function events()
    {
        $this->hasMany('App\Events', 'user_id');
    }

    public function getSocialAttribute($value)
    {
        return json_decode($value);
    }
}
