<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class EventTags extends Model
{
    //use Notifiable;
    protected $table = 'event_tags';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'event_tag', 'event_id'
    ];

    public function event()
    {
        return $this->belongsTo('App\Events', 'event_id');
    }
}
