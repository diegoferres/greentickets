<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Tickets extends Model
{
    //use Notifiable;
    protected $table = 'tickets';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_detail_id', 'status_id'
    ];

    public function sector()
    {
        return $this->belongsTo('App\EventSectors', 'sector_id');
    }

    public function status()
    {
        return $this->belongsTo('App\OrderStatus', 'status_id');
    }

    public function order_detail()
    {
        return $this->belongsTo('App\OrderDetail', 'order_detail_id');
    }
}
