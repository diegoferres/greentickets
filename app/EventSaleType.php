<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class EventSaleType extends Model
{
    //use Notifiable;
    protected $table = 'sale_type';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    public function events()
    {
        return $this->hasMany('App\Events', 'event_sale_type');
    }
}
