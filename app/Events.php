<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Nicolaslopezj\Searchable\SearchableTrait;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Events extends Model
{
    //use Notifiable;
    use SearchableTrait;
    use HasSlug;
    use SoftDeletes;

    protected $searchable = [
        'columns' => [
            'events.event_name' => 10,
            'events.event_organizer' => 9,
            'events.event_description' => 8,
            'categories.category_name' => 7,
            'event_tags.event_tag' => 6
        ],
        'joins' => [
            'categories' => ['events.event_category', 'categories.id'],
            'event_tags' => ['events.id', 'event_tags.event_id'],
            //'profiles' => ['users.id','profiles.user_id'],
        ],
    ];

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('event_name')
            ->saveSlugsTo('event_slug');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'event_name',
        'event_slug',
        'event_subtitle',
        'event_description',
        'event_organizer',
        'event_cover',
        'user_id',
        'event_category',
        'event_format',
        'event_date_start',
        'event_date_end',
        'event_time_start',
        'event_time_end',
        'event_frecuency',
        'event_sale_type',
        'event_link',
        'published'
    ];

    public function covers()
    {
        return $this->hasMany('App\EventCovers', 'event_id');
    }

    public function category()
    {
        return $this->belongsTo('App\Categories', 'event_category');
    }

    public function frecuency()
    {
        return $this->belongsTo('App\EventFrecuency', 'event_frecuency');
    }

    public function sale_type()
    {
        return $this->belongsTo('App\EventSaleType', 'event_sale_type');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function venue()
    {
        return $this->hasOne('App\Venues', 'event_id');
    }

    public function dates()
    {
        return $this->hasMany('App\EventDates', 'event_id');
    }

    public function sectors()
    {
        return $this->hasMany('App\EventSectors', 'event_id');
    }

    public function tags()
    {
        return $this->hasMany('App\EventTags', 'event_id');
    }

    public function orders_detail()
    {
        return $this->hasManyThrough('App\OrderDetail', 'App\EventSectors', 'event_id', 'sector_id');
    }

    public function restrictions()
    {
        return $this->hasMany('App\EventRestrictions', 'event_id');
    }
}
