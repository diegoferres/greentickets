<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class EventSectors extends Model
{
    //use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'event_id', 'venue_id', 'sector_type_id', 'sector_name', 'sector_price', 'sector_capacity', 'date_id'
    ];

    public function category()
    {
        return $this->belongsTo('App\Categories', 'event_category');
    }

    public function event()
    {
        return $this->belongsTo('App\Events');
    }

    public function tickets()
    {
        return $this->hasManyThrough('App\Tickets', 'App\OrderDetail', 'sector_id', 'order_detail_id');
    }
}
