<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class EventCovers extends Model
{
    //use Notifiable;
    protected $table = 'event_covers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'url', 'file_name', 'order', 'event_id'
    ];

    public function event()
    {
        return $this->belongsTo('App\Events', 'event_id');
    }
}
