<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class EventDates extends Model
{
    //use Notifiable;
    protected $table = 'event_dates';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'event_date', 'event_time_start', 'event_time_end', 'event_id'
    ];

    public function event()
    {
        return $this->belongsTo('App\Events', 'event_id');
    }
}
