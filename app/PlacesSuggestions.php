<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlacesSuggestions extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'message', 'place_id'
    ];
}
