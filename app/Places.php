<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Places extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'name', 'slug', 'cell_phone', 'logo', 'file_name', 'file_type', 'file_label', 'file_extra', 'file_extra_label', 'font_color', 'background_img', 'user_id', 'facebook', 'instagram', 'twitter', 'whatsapp'
    ];

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = strtolower(str_replace(' ', '', $value));
    }

    public function clients()
    {
        return $this->hasMany('App\PlacesClients')->whereHas('registers', function($q){
            $q->where('place_id');
        });
    }

    public function registers()
    {
        return $this->hasMany('App\PlacesRegisters', 'place_id');
    }

    public function reservations()
    {
        return $this->hasMany('App\PlacesReservations', 'place_id');
    }

    public function suggestions()
    {
        return $this->hasMany('App\PlacesSuggestions', 'place_id');
    }

    public function files()
    {
        return $this->hasMany('App\PlacesFiles', 'place_id');
    }
}
