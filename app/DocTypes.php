<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class DocTypes extends Model
{
    //use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'descripcion', 'places'
    ];

    public function category()
    {
        return $this->belongsTo('App\Categories', 'event_category');
    }

    public function event()
    {
        return $this->belongsTo('App\Events', 'event_id');
    }
}
