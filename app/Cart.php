<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Cart extends Model
{
    //use Notifiable;
    protected $table = 'cart';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'count', 'subtotal', 'event_id', 'user_id'
    ];

    public function event()
    {
        return $this->belongsTo('App\Events', 'event_id');
    }
}
