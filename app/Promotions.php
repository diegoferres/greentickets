<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promotions extends Model
{
    protected $fillable = [
        'promotion_type',
        'discount_type',
        'event_id',
        'sector_id',
        'start_validity',
        'end_validity',
        'value',
        'quantity',
        'self_applied',
    ];

    public function promotionType()
    {
        return $this->belongsTo('App\PromotionTypes', 'promotion_type');
    }

    public function discountType()
    {
        return $this->belongsTo('App\DiscountTypes', 'discount_type');
    }
}
