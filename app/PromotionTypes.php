<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PromotionTypes extends Model
{
    protected $fillable = [
        'type',
    ];

    public function promotions()
    {
        return $this->hasMany('App\Promotions', 'promotion_type');
    }
}
