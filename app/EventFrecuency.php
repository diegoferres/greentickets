<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class EventFrecuency extends Model
{
    //use Notifiable;
    protected $table = 'event_frecuency';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'frecuency'
    ];

    public function events()
    {
        return $this->hasMany('App\Events', 'event_frecuency');
    }
}
