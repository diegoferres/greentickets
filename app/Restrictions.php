<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Restrictions extends Model
{
    //use Notifiable;
    protected $table = 'restrictions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'restriction'
    ];

    public function events()
    {
        return $this->hasMany('App\EventsRestrictions', 'restriction_id');
    }
}
