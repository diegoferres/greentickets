<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class OrderStatus extends Model
{
    //use Notifiable;

    protected $table = 'order_status';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status'
    ];

    public function orders()
    {
        return $this->hasMany('App\Orders', 'status_id');
    }
}
