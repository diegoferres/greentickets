<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiscountTypes extends Model
{
    protected $fillable = [
        'type',
    ];

    public function promotions()
    {
        return $this->hasMany('App\Promotions', 'discount_ype');
    }
}
