<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class OrderDetail extends Model
{
    //use Notifiable;
    protected $table = 'order_detail';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'price', 'quantity', 'date_id', 'sector_id', 'order_id'
    ];

    public function tickets()
    {
        return $this->hasMany('App\Tickets', 'order_detail_id');
    }

    public function sector()
    {
        return $this->belongsTo('App\EventSectors', 'sector_id');
    }

    public function order()
    {
        return $this->belongsTo('App\Orders', 'order_id');
    }

    public function date()
    {
        return $this->belongsTo('App\EventDates', 'date_id');
    }
}
