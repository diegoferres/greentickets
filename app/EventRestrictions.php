<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class EventRestrictions extends Model
{
    //use Notifiable;
    protected $table = 'events_restrictions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'event_id', 'restriction_id', 'value'
    ];

    public function event()
    {
        return $this->belongsTo('App\Events', 'event_id');
    }

    public function restriction()
    {
        return $this->belongsTo('App\Restrictions', 'restriction_id');
    }
}
