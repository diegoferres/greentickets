<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Coupons extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'type', 'value', 'percent_off'
    ];

    public static function findByCode($code){
        return self::where('code', $code)->first();
    }

    public function discount($total)
    {
        if ($this->type == 'fixed'){
            return $this->value;
        }elseif ($this->type == 'percent'){
            return ($this->percent_off / 100)*$total;
        }else{
            return 0;
        }
    }

    public function orders()
    {
        return $this->belongsTo('App\Events', 'event_id');
    }
}
