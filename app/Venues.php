<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Venues extends Model
{
    //use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'venue_name', 'venue_address', 'event_id'
    ];

    public function event()
    {
        return $this->belongsTo('App\Events');
    }
}
