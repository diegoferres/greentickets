<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlacesClients extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'doc_number', 'name', 'born_date', 'address', 'city', 'cell_phone', 'email'
    ];

    public function registers()
    {
        return $this->hasMany('App\PlacesRegisters', 'client_id');
    }
}
