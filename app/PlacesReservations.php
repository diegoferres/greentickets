<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlacesReservations extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'reserved_date', 'amount_people', 'place_id', 'client_id'
    ];

    public function client()
    {
        return $this->belongsTo('App\PlacesClients', 'client_id');
    }
}
