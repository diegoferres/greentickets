<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlacesRegisters extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'table_number', 'client_id', 'place_id'
    ];

    public function place()
    {
        return $this->belongsTo('App\Places', 'place_id');
    }

    public function client()
    {
        return $this->belongsTo('App\PlacesClients', 'client_id');
    }
}
