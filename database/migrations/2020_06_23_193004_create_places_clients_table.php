<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlacesClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('places_clients', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('doc_number');
            $table->string('name');
            $table->date('born_date')->nullable();
            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->string('cell_phone')->nullable();
            $table->string('email')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('places_clients');
    }
}
