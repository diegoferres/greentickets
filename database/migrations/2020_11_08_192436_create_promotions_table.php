<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePromotionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('promotion_type')->references('id')->on('promotion_types');
            $table->foreignId('discount_type')->references('id')->on('discount_types');
            $table->foreignId('event_id')->references('id')->on('events')->onDelete('cascade');
            $table->foreignId('sector_id')->nullable()->references('id')->on('event_sectors')->onDelete('cascade');
            $table->date('start_validity')->nullable();
            $table->date('end_validity')->nullable();
            $table->integer('value');
            $table->integer('quantity')->nullable();
            //$table->boolean('self_applied');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotions');
    }
}
