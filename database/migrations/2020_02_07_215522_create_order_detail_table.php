<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_detail', function (Blueprint $table) {
            $table->id();
            $table->decimal('price');
            $table->integer('quantity');

            $table->foreignId('order_id')
                ->references('id')
                ->on('orders');

            $table->foreignId('date_id')
                ->nullable()
                ->references('id')
                ->on('event_dates');

            $table->foreignId('sector_id')
                ->references('id')
                ->on('event_sectors');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_detail');
    }
}
