<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('places', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug');
            $table->integer('cell_phone')->nullable();
            $table->string('logo');
            $table->string('file_name');
            $table->string('file_type');
            $table->string('background_img');

            $table->string('facebook', 100)->nullable();
            $table->string('instagram', 100)->nullable();
            $table->string('twitter', 100)->nullable();
            $table->string('whatsapp', 100)->nullable();
            $table->timestamps();

            $table->string('file_label')->nullable();
            $table->string('file_extra')->nullable();
            $table->string('file_extra_label')->nullable();

            $table->string('font_color')->nullable();


            $table->foreignId('user_id')->nullable()->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('places');
    }
}
