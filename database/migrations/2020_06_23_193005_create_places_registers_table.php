<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlacesRegistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('places_registers', function (Blueprint $table) {
            $table->id();
            $table->integer('table_number')->nullable();
            $table->foreignId('place_id')
                ->references('id')
                ->on('places');
            $table->foreignId('client_id')
                ->references('id')
                ->on('places_clients');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('places_registers');
    }
}
