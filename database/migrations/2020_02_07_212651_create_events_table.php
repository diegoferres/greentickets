<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->string('event_name', 255);
            $table->string('event_slug', 300)->nullable();
            $table->string('event_description', 30000)->nullable();
            $table->string('event_organizer', 200);
            $table->string('event_cover')->nullable();
            $table->date('event_date_start')->nullable();
            $table->date('event_date_end')->nullable();
            $table->time('event_time_start')->nullable();
            $table->time('event_time_end')->nullable();

            $table->string('event_link')->nullable();

            $table->foreignId('user_id')
                ->references('id')
                ->on('users');

            $table->foreignId('event_category')
                ->references('id')
                ->on('categories');

            $table->foreignId('event_format')
                ->references('id')
                ->on('event_formats');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
