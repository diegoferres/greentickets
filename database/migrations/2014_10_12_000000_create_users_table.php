<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->integer('doc_number')->nullable();
            $table->date('born_date')->nullable();
            $table->string('telephone', 12)->nullable();
            $table->string('cell_phone', 12)->nullable();
            $table->string('business_name')->nullable();
            $table->string('ruc_number')->nullable();
            $table->string('street1')->nullable();
            $table->string('street2')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            //$table->json('social')->nullable();
            $table->rememberToken();


            $table->foreignId('doc_type_id')->nullable()
                ->references('id')
                ->on('doc_types');

            $table->foreignId('sex_id')->nullable()
                ->references('id')
                ->on('sexs');

            $table->foreignId('neighborhood_id')->nullable()
                ->references('id')
                ->on('neighborhoods');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
