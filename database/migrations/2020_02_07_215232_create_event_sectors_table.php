<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventSectorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_sectors', function (Blueprint $table) {
            $table->id();
            $table->string('sector_name', 50);
            $table->decimal('sector_price', 8, 0);
            $table->integer('sector_capacity');

            $table->foreignId('date_id')->nullable()->references('id')->on('event_dates');

            $table->foreignId('event_id')
                ->references('id')
                ->on('events')
                ->onDelete('cascade');;

            $table->foreignId('venue_id')
                ->nullable()
                ->references('id')
                ->on('venues');

            $table->foreignId('sector_type_id')->nullable()
                ->references('id')
                ->on('sector_types');



            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_sectors');
    }
}
