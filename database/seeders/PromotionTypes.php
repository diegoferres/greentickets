<?php

use Illuminate\Database\Seeder;

class PromotionTypes extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        DB::table('discount_types')->insert([
            'type' => 'Descuento',
        ]);

        DB::table('discount_types')->insert([
            'type' => 'Cupón',
        ]);
    }
}
