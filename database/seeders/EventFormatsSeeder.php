<?php

use Illuminate\Database\Seeder;

class EventFormatsSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        DB::table('event_format')->insert([
            'event_format' => 'Presencial',
        ]);

        DB::table('event_format')->insert([
            'event_format' => 'Online',
        ]);

        DB::table('event_format')->insert([
            'event_format' => 'Presencial/Online',
        ]);
    }
}
