<?php

use Illuminate\Database\Seeder;

class DiscountTypes extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        DB::table('discount_types')->insert([
            'type' => 'Porcentaje',
        ]);

        DB::table('discount_types')->insert([
            'type' => 'Fijo',
        ]);
    }
}
