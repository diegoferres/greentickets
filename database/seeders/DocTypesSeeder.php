<?php

use Illuminate\Database\Seeder;

class DocTypesSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        DB::table('doc_types')->insert([
            'doctype_name' => 'Cédula',
        ]);

        DB::table('doc_types')->insert([
            'doctype_name' => 'Pasaporte',
        ]);
    }
}
