<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sexs')->insert([
            'sex_name' => 'Masculino',
        ]);

        DB::table('sexs')->insert([
            'sex_name' => 'Femenino',
        ]);

        DB::table('doc_types')->insert([
            'doctype_name' => 'Cédula',
        ]);

        DB::table('doc_types')->insert([
            'doctype_name' => 'Pasaporte',
        ]);

        DB::table('payment_methods')->insert([
            'method'        => 'Transferencia Bancaria',
            'status'        => true
        ]);
        DB::table('payment_methods')->insert([
            'method'        => 'Pagopar',
            'status'        => true
        ]);

        DB::table('order_status')->insert([
            'status'        => 'Abonado',
        ]);

        DB::table('order_status')->insert([
            'status'        => 'Pendiente',
        ]);

        DB::table('ticket_status')->insert([
            'status'        => 'Pre-orden',
        ]);

        DB::table('ticket_status')->insert([
            'status'        => 'Abonado',
        ]);

        DB::table('ticket_status')->insert([
            'status'        => 'Utilizado',
        ]);

        DB::table('event_formats')->insert([
            'event_format'    => 'Presencial',
        ]);

        DB::table('event_formats')->insert([
            'event_format'    => 'Online',
        ]);

        DB::table('event_formats')->insert([
            'event_format'    => 'Online/Presencial',
        ]);

        /**
         * Payment Methods
         */

        /*/**
         * Event
         */
        /*DB::table('events')->insert([
            'event_name' => 'Evento 1',
            'event_slug' => 'evento-1',
            'event_organizer' => 'Organizador 1',
            'event_category' => 1,
            'event_format' => 1,
            'event_description' => 'Descripción del evento',
            'event_cover' => 'U4teh10SburDb5pXExJUC5UTit8WavrGgsBDgclG.png',
            'user_id' => '1',
        ]);

        DB::table('venues')->insert([
            'venue_name'    => 'Lugar 1',
            'venue_address' => 'Dirección del lugar 1',
            'event_id'      => 1,
        ]);

        DB::table('event_dates')->insert([
            'event_date'        => '2021-01-19',
            'event_time_start'  => '20:00',
            'event_time_end'    => '03:00',
            'event_id'          => 1,
        ]);

        DB::table('event_sectors')->insert([
            'sector_name'       => 'General',
            'sector_price'      => '140000',
            'sector_capacity'   => '250',
            'event_id'          => 1,
        ]);
        DB::table('event_sectors')->insert([
            'sector_name'       => 'VIP',
            'sector_price'      => '250000',
            'sector_capacity'   => '350',
            'event_id'          => 1,
        ]);

        DB::table('events')->insert([
            'event_name' => 'Evento 2',
            'event_slug' => 'evento-2',
            'event_organizer' => 'Organizador 2',
            'event_category' => 2,
            'event_format' => 2,
            'event_description' => 'Descripción del evento',
            'event_cover' => 'pSRUOVvqxFFdySEhzGdflJaYLI1YhxqOhJ7Ov0KX.png',
            'user_id' => 1,
        ]);

        DB::table('venues')->insert([
            'venue_name'    => 'Lugar 2',
            'venue_address' => 'Dirección del lugar 2',
            'event_id'      => 2,
        ]);

        DB::table('event_dates')->insert([
            'event_date'        => '2021-01-19',
            'event_time_start'  => '20:00',
            'event_time_end'    => '03:00',
            'event_id'          => 2,
        ]);

        DB::table('event_sectors')->insert([
            'sector_name'       => 'General',
            'sector_price'      => '140000',
            'sector_capacity'   => '250',
            'event_id'          => 2,
        ]);
        DB::table('event_sectors')->insert([
            'sector_name'       => 'VIP',
            'sector_price'      => '250000',
            'sector_capacity'   => '350',
            'event_id'          => 2,
        ]);*/
    }
}
