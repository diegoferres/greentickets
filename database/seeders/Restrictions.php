<?php

use Illuminate\Database\Seeder;

class Restrictions extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        DB::table('restrictions')->insert([
            'restriction' => 'Cédula',
        ]);

        DB::table('restrictions')->insert([
            'restriction' => 'Cantidad máxima',
        ]);
    }
}
