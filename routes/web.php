<?php

use App\Http\Controllers\Mail\BuyNotification;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Web\HomeController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Admin\UsersController;
use App\Http\Controllers\Admin\HomeController as AdminHomeController;
use App\Http\Controllers\Admin\EventsController;
use \App\Http\Controllers\Admin\CategoriesController;
use App\Http\Controllers\Admin\PlacesController;
use App\Http\Controllers\Admin\CheckController;
use App\Http\Controllers\Web\EventController as WebEventController;
use App\Http\Controllers\Web\UserController as WebUserController;
use App\Http\Controllers\Web\OrganizerController;
use App\Http\Controllers\Web\OrdersController;
use App\Http\Controllers\Web\PaymentController;
use App\Http\Controllers\Web\CouponsController;
use App\Http\Controllers\Web\PlacesController as WebPlacesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/**
 * Typeahead Search
 */
/*Route::get('testpay', function (){
    $order = \App\Orders::orderBy('created_at', 'desc')->first();

    //dd($order->detail);
    return view('web.mailing.mailing-paymet', compact('order'));
});*/

Route::get('/test/mail/',function () {

    //dd(QrCode::generate("Chupame esta",public_path('test_qr')));


    $order = \App\Orders::with('tickets','detail.sector')->find(108);

    Mail::to('rarrua2009@gmail.com')->send(new BuyNotification($order));
    echo "se envio el mail";

});

Route::get('check/tikect/hash/{hash}',[HomeController::class,'checkTicketHast'])->name('check.ticket.hash');

Route::get('find', [SearchController::class,'find']);

//Route::get('search', 'Admin\UsersController@usersSearch')->name('search');

Route::get('search', [SearchController::class,'searchAll'])->name('web.search');


Route::get('/login/{provider}', [LoginController::class,'redirectToProvider'])
    ->name('social.login');
Route::get('/login/{provider}/callback', [LoginController::class,'handleProviderCallback'])
    ->name('social.callback');



/**
 * Admin
 */
/*Route::livewire('/catsave', 'admin.categories.create')->name('save');*/
Route::group(['middleware' => ['auth', 'role:admin'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/auxpwd/{email}/{pwd}', [UsersController::class,'auxUpdatePwd'])->name('auxpwd');

    Route::get('/', [AdminHomeController::class,'index'])->name('home');
    Route::get('/users/create', [UsersController::class,'create'])->name('users.create');

    Route::resource('users', UsersController::class);
    Route::resource('events', EventsController::class);
    Route::resource('categories', CategoriesController::class);


    /**
     * Event Menu
     */
    Route::post('/event/show', [EventsController::class,'showEvent'])->name('event.show');
    Route::get('/event/dashboard', [EventsController::class,'eventDashboard'])->name('event.dashboard');
    Route::get('/event/info', [EventsController::class,'eventInformation'])->name('event.info');
    Route::get('/event/payments', [EventsController::class,'eventPayments'])->name('event.payments');
    Route::get('/event/payments/details/{order_id}', [EventsController::class,'eventPaymentsDetails'])->name('event.payments.details');
    Route::get('/event/orders/payments/{order_id}', [EventsController::class,'eventPaymentsOrders'])->name('event.orders.paynments');
    Route::post('/event/info/store', [EventsController::class,'eventInfoStore'])->name('event.info.store');

    /* Sectors */
    Route::get('/event/sectors/{id?}', [EventsController::class,'eventSectors'])->name('event.sectors');
    //Route::post('/event/sectors/', 'Admin\EventsController@sectorStore')->name('event.sectors.store');
    Route::post('/event/sectors/store', [EventsController::class,'sectorStore'])->name('event.sectors.store');
    //Route::post('/event/sectors/update', 'Admin\EventsController@sectorStore')->name('event.sectors.store');
    Route::delete('/event/sector/delete/{sector}', [EventsController::class,'sectorDelete'])->name('event.sectors.delete');

    /* Dates */
    Route::get('/event/dates/{id?}', [EventsController::class,'eventDates'])->name('event.dates');
    Route::post('/event/dates/store', [EventsController::class,'dateStore'])->name('event.dates.store');
    Route::delete('/event/dates/delete/{date}', [EventsController::class,'dateDelete'])->name('event.dates.delete');

    /* Promotions */
    Route::get('/event/promotions/{id?}', [EventsController::class,'eventPromotions'])->name('event.promotions');
    Route::post('/event/promotions/store', [EventsController::class,'promotionsStore'])->name('event.promotions.store');
    Route::get('/event/promotions/delete/{event}/{date}', [EventsController::class,'promotionsDelete'])->name('event.promotions.delete');


    /**
     * Scan ticket
     */
    Route::get('/scan/', [CheckController::class,'scan'])->name('scan');

});

Route::group(['middleware' => ['auth', 'role:admin|place'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
    /* Places */
    Route::get('/places', [PlacesController::class,'index'])->name('places.index');
    Route::get('/places/create', [PlacesController::class,'create'])->name('places.create');
    Route::post('/places/store', [PlacesController::class,'store'])->name('places.store');
    Route::get('/places/{id}/edit', [PlacesController::class,'edit'])->name('places.edit');
    Route::post('/places/file/store', [PlacesController::class,'storeFile'])->name('places.file.store');
    Route::post('/places/file/sort', [PlacesController::class,'sortFiles'])->name('places.file.sort');
    Route::get('/places/file/delete/{file}/{place}', [PlacesController::class,'deleteFile'])->name('places.file.delete');
    Route::put('/places/update/{date}', [PlacesController::class,'update'])->name('places.update');

    Route::get('/places/{id}/clients', [PlacesController::class,'clients'])->name('places.clients');
    Route::get('/places/{id}/registers', [PlacesController::class,'registers'])->name('places.registers');
    Route::get('/places/{id}/reservations', [PlacesController::class,'reservations'])->name('places.reservations');
    Route::get('/places/{id}/suggestions', [PlacesController::class,'suggestions'])->name('places.suggestions');
});

Route::get('/', [HomeController::class,'index'])->name('home');

Auth::routes(['verify' => true]);

Route::group(['middleware' => ['auth']], function () {
    /**
     * Events
     */
    Route::get('/event/step1', [WebEventController::class,'step1'])->name('web.event.step1');
    Route::match(['get', 'post'], '/event/step2', [WebEventController::class,'step2'])->name('web.event.step2');
    Route::match(['get', 'post'], '/event/step3', [WebEventController::class,'step3'])->name('web.event.step3');
    Route::match(['get', 'post'], '/event/step4', [WebEventController::class,'step4'])->name('web.event.step4');
    Route::post('/event/save', [WebEventController::class,'event_save'])->name('web.event.save');
    Route::get('/event/finish/{slug}', [WebEventController::class,'eventFinish'])->name('web.event.finish');

    /**
     * Client
     */
    Route::get('/user/profile', [WebUserController::class,'profile'])->name('web.user.profile');
    Route::get('/user/history', [WebUserController::class,'history'])->name('web.user.history');
    Route::get('/user/history-detail/{id}', [WebUserController::class,'historyDetail'])->name('web.user.history.detail');
    Route::post('/user/profile-update/{id}', [WebUserController::class,'profileUpdate'])->name('web.user.profile_update');
    Route::get('/user/payments', [WebUserController::class,'payments'])->name('web.user.payments');
    Route::get('/user/purchased_events', [WebUserController::class,'purchased_events'])->name('web.user.purchased_events');

    /*
     * Organizer
     */
    Route::get('/organizer', [OrganizerController::class,'index'])->name('web.organizer.index');
    Route::get('/organizer/event/{id}/edit', [OrganizerController::class,'eventEdit'])->name('web.organizer.event.edit');
    Route::get('/organizer/event/{id}/show', [OrganizerController::class,'eventShow'])->name('web.organizer.event.show');
    Route::get('/organizer/event/{id}/control/tickets', [OrganizerController::class,'controlTickets'])->name('web.organizer.control.tickets');
    Route::get('/organizer/event/{id}/sale/tickets', [OrganizerController::class,'saleTickets'])->name('web.organizer.sale.tickets');
    Route::get('/organizer/event/{id}/{order_details_id}/tickets/status', [OrganizerController::class,'eventShowTicketsStatus'])
        ->name('web.organizer.event.ticket.status');

    Route::get('/organizer/promotions/create', [OrganizerController::class,'promotionCreate'])->name('web.organizer.promotions.create');

    /**
     * Checkout
     */
    Route::get('/orders/checkout', [OrganizerController::class,'checkout'])->name('web.orders.checkout');
    Route::get('/orders/checkout/payment', [OrganizerController::class,'checkoutPayment'])->name('web.orders.checkout.payment');
    //Route::match(['get', 'post'], '/orders/checkout/finish/{hash?}', 'Web\OrdersController@checkoutFinish')->name('web.orders.checkout.finish');

    Route::post('/coupons', [CouponsController::class,'store'])->name('web.coupons.store');

});

Route::group(['middleware' => ['web','auth']], function () {
    Route::match(['get', 'post'], '/orders/checkout/finish/{hash?}', [OrdersController::class,'checkoutFinish'])->name('web.orders.checkout.finish');
});

/*
 * Pagopar
 */
Route::get('/insertPagopar', [PaymentController::class,'insert']);
Route::post('/confirmPagopar', [PaymentController::class,'response']);
Route::get('/checkOrder', [PaymentController::class,'checkOrder']);
Route::get('/checkResponse', [PaymentController::class,'checkResponse']);
Route::get('/getPaymentMethods', [PaymentController::class,'pagoparPaymentMethods']);


Route::get('/usercheck/{email}', [RegisterController::class,'userCheck'])->name('web.user.check');

/**
 * Cart
 */
Route::post('/orders/addtocart', [OrdersController::class,'addToCart'])->name('web.orders.addtocart');
Route::get('/orders/removefromcart/{id}', [OrdersController::class,'removeFromCart'])->name('web.orders.removefromcart');
Route::get('/orders/showcart', [OrdersController::class,'showCart'])->name('web.orders.showcart');

/**
 * Event
 */
Route::get('/event/{slug}', [WebEventController::class,'show'])->name('web.event.show');
Route::get('/event/{event_id}/{sector_id}/sum_price/{count}',  [WebEventController::class,'sum_price'])->name('web.event.sum_price');
Route::get('/event/{event_id}/getsectors/{date_id?}',  [WebEventController::class,'getSectors'])->name('web.event.getsectors');


Route::get('/resetpass-new', function () {
    return view('web.auth.resetpass-new');
})->name('newpass');

Route::get('/resetpass-succes', function () {
    return view('web.auth.resetpass-succes');
})->name('succespass');


Route::get('/resetpass', function () {
    return view('web.auth.resetpass');
})->name('reset');

Route::get('/confirm-mail', function () {
    return view('web.auth.mail-confirm');
})->name('email');


Route::get('/mail-succes', function () {
    return view('web.auth.mail-succes');
})->name('mail-succes');

Route::domain('covid.entrasinpapel.com')->group(function () {
    Route::get('/{place}', 'Web\PlacesController@index')->name('web.places.choices');
});


Route::group(['domain' => 'covid.entrasinpapel.com'], function () {

    Route::get('/{place}', [WebPlacesController::class,'index'])->name('web.places.choices');

    Route::get('/{slug}/register', [WebPlacesController::class,'register'])->name('web.places.register');

    Route::post('/register/store/{slug}', [WebPlacesController::class,'registerStore'])->name('web.places.register.store');

    Route::get('/{slug}/reservation', [WebPlacesController::class,'reservation'])->name('web.places.reservation');
    Route::post('/reservation/store/{slug}', [WebPlacesController::class,'reservationStore'])->name('web.places.reservation.store');

    Route::get('/{slug}/suggestion', [WebPlacesController::class,'suggestion'])->name('web.places.suggestion');
    Route::post('/suggestion/store/{slug}', [WebPlacesController::class,'suggestionStore'])->name('web.places.suggestion.store');

    Route::get('/{slug}/showfile', [WebPlacesController::class,'showFile'])->name('web.places.showfile');

    Route::get('/{slug}/thanks', [WebPlacesController::class,'showThanks'])->name('web.places.thanks');

    Route::get('/Long%20Beach', function (){
        return redirect('/longbeach');
    });
});

Route::get('/choices/{slug}', [WebPlacesController::class,'index'])->name('web.places.choices');

Route::get('/{slug}/register', [WebPlacesController::class,'register'])->name('web.places.register');
Route::post('/register/store/{slug}', [WebPlacesController::class,'registerStore'])->name('web.places.register.store');

Route::get('/{slug}/reservation', [WebPlacesController::class,'reservation'])->name('web.places.reservation');
Route::post('/reservation/store/{slug}', [WebPlacesController::class,'reservationStore'])->name('web.places.reservation.store');

Route::get('/{slug}/suggestion', [WebPlacesController::class,'suggestion'])->name('web.places.suggestion');
Route::post('/suggestion/store/{slug}', [WebPlacesController::class,'suggestionStore'])->name('web.places.suggestion.store');

Route::get('/{slug}/showfile', [WebPlacesController::class,'showFile'])->name('web.places.showfile');

Route::get('/{slug}/thanks', [WebPlacesController::class,'showThanks'])->name('web.places.thanks');

Route::get('/validate/{place}/{ci}', [WebPlacesController::class,'validateCI'])->name('web.places.validate');

Route::get('/terminos', function () {
    return view('web.about.termino');
})->name('termino');

Route::get('/politicas', function () {
    return view('web.about.index');
})->name('politicas');

Route::get('/faqs', function () {
    return view('web.about.faqs');
})->name('faqs');

Route::get('/mail', function () {
    return view('web.auth.mail-succes');
})->name('mail');

Route::get('/mailing', function () {
    return view('web.mailing.mail');
})->name('mailing');

Route::get('/pdf', function () {
    return view('web.mailing.pdf');
})->name('pdf');

Route::get('/mailing-pass', function () {
    return view('web.mailing.mailing-pass-reset');
})->name('mail-pass');

Route::get('/mailing-paymet', function () {
    return view('web.mailing.mailing-paymet');
})->name('mail-paymet');


Route::get('/mailing-mail', function () {
    return view('web.mailing.mailing-mail-confirm');
})->name('mail-confirm');

/*Route::get('/organizer', function () {
    return view('web.organizer-profile.index');
})->name('organizer');*/

Route::get('/edit-event', function () {
    return view('web.organizer-profile.edit-event');
})->name('edit-event');

Route::get('/list-event', function () {
    return view('web.organizer-profile.list-event');
})->name('list-event');

Route::get('/list-sale', function () {
    return view('web.organizer-profile.list-sale');
})->name('list-sale');

Route::get('/create-sale', function () {
    return view('web.organizer-profile.create-sale');
})->name('create-sale');

Route::get('/detail-sale', function () {
    return view('web.organizer-profile.detail-sale');
})->name('detail-sale');

Route::get('/evento-ejemplo', function () {
    return view('web.event.ejemplo');
})->name('ejemplo');

Route::get('/testmail', 'Web\OrdersController@index');




