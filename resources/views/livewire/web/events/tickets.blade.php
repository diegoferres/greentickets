<div>

        <div class="row" id="add_to_cart">
            <div class="col-lg-9">
                <div class="pt-5 pt-lg-4">
                    <h2 class="mb-4 text-primary">Comprar entradas </h2>
                    {{--@if ($event->restrictions->first())
                        <i class="badge badge-warning fas fa-exclamation-triangle"> Este evento admite solo {{ $event->restrictions->first()->value }} entradas por usuario</i>
                    @endif--}}
                    <div class="form-group pb-5">

                        {{--{!! Form::open(['route' => 'web.orders.addtocart', 'method' => 'post']) !!}--}}
                        <form wire:submit.prevent="submit">
                        {!! Form::hidden('event_id', $event->id) !!}
                        <div class="row">
                            @if ($event->event_sale_type == 2)
                                <div class="col-md-4">
                                    <h6 class="pt-3 pt-lg-auto">Elige una fecha</h6>
                                    {!! Form::select('date_id', $event->dates->pluck('event_date', 'id') , [] , [
                                        'wire:model' => 'date_id',
                                        /*'wire:change' => 'updateSectors()',*/
                                        'class' => 'custom-select form-control',
                                        'placeholder' => 'Para todos los días'
                                        ]) !!}
                                </div>
                            @endif
                            <div class="col-md-{{ $event->event_sale_type == 1 ? '8' : '4'}}">
                                <h6 class="pt-3 pt-lg-auto">Sector</h6>
                                {!! Form::select('sector_id', $sectors, null , [
                                            'wire:model' => 'sector_id',
                                            'class' => $errors->has('sector_id') ? 'custom-select form-control is-invalid' : 'custom-select form-control',
                                            'placeholder' => $event->event_sale_type == 2 ? 'Elige una entrada' : 'Elige una entrada'
                                            ]) !!}
                                @error('sector_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="col-md-4">
                                <h6 class="pt-3 pt-lg-auto">Cantidad {{ isset($event->restrictions->first()->value) ? '(Cant. Max: '.$event->restrictions->first()->value.')' : '' }}</h6>
                                <input type="number"
                                       class="form-control @error('count') is-invalid @enderror"
                                       wire:model="count"
                                       name="count"
                                       placeholder="Cantidad">
                                @error('count')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                        </div>
                        @if (isset($event->restrictions->first()->restriction_id) && $event->restrictions->first()->restriction_id == 1)
                            <div class="row">
                                <div class="col-md-8">
                                    <h6 class="pt-3 pt-lg-auto">Nombre y Apellido portador</h6>
                                    {!! Form::text('ticket_data_name', old('ticket_data_name'),
                                        [
                                            'class' => $errors->has('ticket_data_name') ? 'form-control is-invalid' : 'form-control',
                                            'placeholder' => 'Nombre y apellido',
                                            'wire:model' => 'ticket_data_name'
                                        ]) !!}
                                    @error('ticket_data_name')
                                    <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                    @enderror
                                </div>
                                <div class="col-md-4">
                                    <h6 class="pt-3 pt-lg-auto">Cédula de Identidad portador</h6>
                                    {!! Form::number('ticket_data_doc', old('ticket_data_doc'),
                                        [
                                            'class' => $errors->has('ticket_data_doc') ? 'form-control is-invalid' : 'form-control',
                                            'placeholder' => 'Documento',
                                            'wire:model' => 'ticket_data_doc'
                                        ]) !!}
                                    @error('ticket_data_doc')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        @endif
                        <div class="pt-3">
                            <div class="d-flex flex-column flex-md-row form-control bg-light">
                                <div class="d-flex flex-md-fill align-items-center my-2">
                                    <h6 class="mb-0 mr-3">Precio Unitario</h6>
                                    Gs. &nbsp; <p class="mb-0" id="unit_price"> {{ isset($sector->sector_price) ? $sector->sector_price : 0 }}</p>
                                </div>
                                <div class="d-flex flex-md-fill align-items-center my-2">
                                    <h6 class="mb-0 mr-3">Total</h6>
                                    Gs. &nbsp; <p class="mb-0" id="subtotal"> {{ number_format($subtotal, 0, ',', '.') }}</p>
                                </div>
                            </div>

                            <button class="btn btn-primary mt-4 float-right"
                                    type="submit">Añadir al carrito
                            </button>
                        </div>
                        </form>
                        {{--{!! Form::close() !!}--}}
                    </div>
                </div>
            </div>
        </div>
    {{--@endif--}}
</div>
