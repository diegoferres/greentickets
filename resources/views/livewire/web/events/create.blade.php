<div>
    <main class="d-flex justify-content-center">

        {{-- STEP 1 --}}
        <div style="display: @if($step != 1) none @endif" class="col-sm-10 col-md-8 col-xl-6 mt-5">
            <p>Pasos <b class="text-primary">1 de 4</b></p>
            <div class="progress mb-4">
                <div class="progress-bar bg-primary" role="progressbar" style="width: 25%" aria-valuenow="25"
                     aria-valuemin="0" aria-valuemax="100">
                </div>
            </div>
            <div class="pt-4 pb-5">
                <h2 class="mb-4">Información del Evento</h2>
                {{--{!! Form::open(['route' => 'web.event.step2', 'method' => 'post']) !!}--}}

                <div class="form-group mb-4">
                    <input wire:model="event_name" id="name" name="event_name"
                           type="text"
                           placeholder="Nombre" autofocus=""
                           class="form-control @error('event_name') is-invalid @enderror">
                    @error('event_name')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group mb-4">
                    <input wire:model="event_organizer" id="organizer" name="event_organizer"
                           type="text"
                           placeholder="Organizador" autofocus=""
                           class="form-control @error('event_organizer') is-invalid @enderror">
                    @error('event_organizer')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group mb-4">
                    {!! Form::select('event_category', $categories , null,
                        ['class' => $errors->has('event_category') ? 'custom-select form-control is-invalid' : 'custom-select form-control',
                         'placeholder' => 'Categoría',
                         'wire:model' => 'event_category']) !!}
                    @error('event_category')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group mb-4">
                    <div class="d-flex align-items-center flex-fill mb-3">
                        <input wire:keydown.enter="addTag" wire:model="addedTag" class="form-control col-md-8" type="text" placeholder="Etiqueta">
                        <button wire:click="addTag" class="btn btn-tertiary ml-4" id="tagCreate">Añadir etiqueta
                        </button>
                    </div>
                    <div id="tagsList">
                        @isset ($event_tags)
                            @foreach($event_tags as $key => $tag)
                                <span class="badge badge-pill tags p-3 m-2" wire:click="removeTag({{ $key }})">{{ $tag }}<i
                                        class="fas fa-times ml-3"></i></span>
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="form-group float-right">
                    <a href="/" class="btn btn-secondary mr-2">Volver</a>
                    <button wire:click="secondStep" class="btn btn-primary mr-2">Siguiente
                    </button>
                </div>
                {{--{!! Form::close() !!}--}}
            </div>
        </div>
        {{-- END STEP 1 --}}

        {{-- STEP 2 --}}
        <div style="display: @if($step != 2) none @endif" class="col-sm-10 col-md-8 col-xl-6 mt-5">
            <p>Pasos <b class="text-primary">2 de 4</b></p>
            <div class="progress mb-4">
                <div class="progress-bar bg-primary" role="progressbar" style="width: 50%" aria-valuenow="25"
                     aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <div class="pt-4 pb-5">
                @if ($event_format == 1 || $event_format == 3)
                    <h2 class="mb-4">¿Cómo, cuándo y donde será el Evento?</h2>
                @else
                    <h2 class="mb-4">¿Cómo y cuándo será el Evento?</h2>
                @endif
                {{--{!! Form::open(['route' => 'web.event.step3', 'method' => 'post']) !!}--}}
                <div class="row mb-4">
                    <div class="col-md-12">
                        <div class="form-group">
                            <h5 class="mb-2 py-2">Formato de evento</h5>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input wire:model="event_format" type="radio"
                                       class="custom-control-input @error('event_format') is-invalid @enderror"
                                       id="1" name="event_format" value="1">
                                <label for="1" class="custom-control-label">Presencial</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input wire:model="event_format" type="radio"
                                       class="custom-control-input @error('event_format') is-invalid @enderror"
                                       id="2" name="event_format" value="2">
                                <label for="2" class="custom-control-label">Online</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input wire:model="event_format" type="radio"
                                       class="custom-control-input @error('event_format') is-invalid @enderror"
                                       id="3" name="event_format" value="3">
                                <label for="3" class="custom-control-label">Online/Presencial</label>
                            </div>
                        </div>
                    </div>
                </div>
                @if($event_format == 1 || $event_format == 3)
                    <div class="eventVenue">
                        <div class="form-group mb-4">
                            <input wire:model="venue_name" id="venue_name" name="venue_name"
                                   type="text"
                                   placeholder="Nombre del lugar" autofocus=""
                                   class="form-control @error('venue_name') is-invalid @enderror">
                            @error('venue_name')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group mb-4">
                            <input wire:model="venue_address" id="venue_address" name="venue_address"
                                   type="text" autofocus=""
                                   class="form-control @error('venue_address') is-invalid @enderror"
                                   placeholder="Dirección del lugar">
                            @error('venue_address')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        @if ($event_format == 3)
                            <div class="eventLink">
                                <div class="form-group mb-4">
                                    <input wire:model="event_link" id="event_link" name="event_link"
                                           type="text" autofocus=""
                                           class="form-control @error('event_link') is-invalid @enderror"
                                           placeholder="Link del evento">
                                    @error('event_link')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        @endif
                    </div>
                @elseif($event_format == 2)
                    <div class="eventLink">
                        <div class="form-group mb-4">
                            <input wire:model="event_link" id="event_link" name="event_link"
                                   type="text" autofocus=""
                                   class="form-control @error('event_link') is-invalid @enderror"
                                   placeholder="Link del evento">
                            @error('event_link')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                @endif

                <div class="form-group">
                    <h5 class="mb-2 py-2">Frecuencia del evento</h5>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input wire:model="event_frecuency" type="radio"
                               class="custom-control-input @error('event_frecuency') is-invalid @enderror"
                               id="fre1" name="event_frecuency" value="1">
                        <label for="fre1" class="custom-control-label">Evento único</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input wire:model="event_frecuency" type="radio"
                               class="custom-control-input @error('event_frecuency') is-invalid @enderror"
                               id="fre2" name="event_frecuency" value="2">
                        <label for="fre2" class="custom-control-label">Recurrente</label>
                    </div>
                </div>
                <div class="form-group pb-5">
                    @if ($event_frecuency == 1)
                        <div class="row" wire:key="1" style="padding-bottom: 20px">
                            <div class="col-12 col-lg-6">
                                <h6>Fecha Inicio</h6>
                                <input wire:model="event_date_start"
                                       type="date" autofocus=""
                                       class="form-control pl-lg-4 pr-lg-1 @error('event_date_start') is-invalid @enderror">
                                @error('event_date_start')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="col-md-6 col-lg-4 mt-4 mt-lg-0">
                                <h6>Hora de Inicio</h6>
                                <input wire:model="event_time_start"
                                       type="time" autofocus=""
                                       class="form-control @error('event_time_start') is-invalid @enderror"
                                       placeholder="Hora">
                                @error('event_time_start')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>
                        <div class="row" wire:key="2" style="padding-bottom: 20px">
                            <div class="col-12 col-lg-6">
                                <h6>Fecha Fin</h6>
                                <input wire:model="event_date_end" id="date" name="event_date_end"
                                       type="date" autofocus=""
                                       class="form-control pl-lg-4 pr-lg-1 @error('event_date_end') is-invalid @enderror">
                                @error('event_date_end')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="col-md-6 col-lg-4 mt-4 mt-lg-0">
                                <h6>Hora de Termino</h6>
                                <input wire:model="event_time_end" id="hora-final" name="event_time_ed"
                                       type="time" autofocus=""
                                       class="form-control @error('event_time_end') is-invalid @enderror"
                                       placeholder="Hora">
                                @error('event_time_end')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>

                    @elseif($event_frecuency == 2)
                        @isset($dates)
                            @foreach ($dates as $key => $date)
                                <div class="row" style="padding-bottom: 20px">
                                    <div class="col-12 col-lg-4">
                                        <h6>Fecha</h6>
                                        <input wire:key="event-date-{{ $key }}"
                                               wire:model="dates.{{$key}}.event_date"
                                               type="date" autofocus=""
                                               class="form-control pl-lg-4 pr-lg-1 @error('dates.'.$key.'.event_date') is-invalid @enderror">
                                        @error('dates.'.$key.'.event_date')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="col-md-6 col-lg-4 mt-4 mt-lg-0">
                                        <h6>Hora de Inicio</h6>
                                        <input wire:key="event-time-start-{{ $key }}"
                                               wire:model="dates.{{$key}}.event_time_start"
                                               type="time" autofocus=""
                                               class="form-control @error('dates.'.$key.'.event_time_start') is-invalid @enderror"
                                               placeholder="Hora">
                                        @error('dates.'.$key.'.event_time_start')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="col-md-6 col-lg-4 mt-4 mt-lg-0">
                                        <h6>Hora de Termino</h6>
                                        <input wire:key="event-time-end-{{ $key }}" id="hora-final"
                                               wire:model="dates.{{$key}}.event_time_end"
                                               type="time" autofocus=""
                                               class="form-control @error('dates.'.$key.'.event_time_end') is-invalid @enderror"
                                               placeholder="Hora">
                                        @error('dates.'.$key.'.event_time_end')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                @if ($loop->last)
                                    <a wire:click="addDate" class="btn btn-tertiary mt-3" href="javascript:void(0)">Añadir
                                        otra fecha</a>
                                    @if (count($dates) > 1)
                                        <a wire:click="removeDate({{$key}})" class="btn btn-tertiary mt-3"
                                           href="javascript:void(0)">Eliminar una fecha</a>
                                    @endif
                                @endif
                            @endforeach
                        @endisset
                    @endif
                </div>
                <div class="form-group float-right">
                    <button wire:click="back" class="btn btn-secondary mr-2">Volver</button>
                    <button wire:click="thirdStep" class="btn btn-primary">
                        Siguiente
                    </button>
                </div>
                {{--{!! Form::close() !!}--}}
            </div>
        </div>
        {{-- END STEP 2 --}}

        {{-- STEP 3 --}}
        <div style="display: @if($step != 3) none @endif" class="col-sm-10 col-md-8 col-xl-6 mt-5">
            <p>Pasos <b class="text-primary">3 de 4</b></p>
            <div class="progress mb-4">
                <div class="progress-bar bg-primary" role="progressbar" style="width: 75%" aria-valuenow="25"
                     aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <div class="pt-4 pb-5">
                <h2 class="mb-4">Datos y entradas del evento</h2>
                {{--{!! Form::open(['route' => 'web.event.step4', 'method' => 'post']) !!}--}}
                <div class="mb-4">
                   {{--  <div class="col-md-6"> --}}
                        <div class="form-group">
                            <h5 class="mb-2 py-2">Tipo de entrada</h5>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input wire:model="sector_type" type="radio" class="custom-control-input"
                                       name="sector_type"
                                       value="1" id="st1">
                                <label for="st1" class="custom-control-label">Pago</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input wire:model="sector_type" type="radio" class="custom-control-input"
                                       name="sector_type"
                                       value="2" id="st2">
                                <label for="st2" class="custom-control-label">Gratis</label>
                            </div>
                        </div>
                    {{-- </div>
                    <div class="col-md-6"> --}}
                        <div class="form-group">
                            <h5 class="mb-2 py-2">Restricciones</h5>
                            <div class="d-flex flex-column flex-lg-row align-items-lg-center">
                                <div class="custom-control custom-radio py-2">
                                    <input type="radio" class="custom-control-input" id="checkRest_0" value="" name="restrict_type"  wire:model="restrict_type">
                                    <label class="custom-control-label" for="checkRest_0">Ninguna</label>
                                </div>
                            </div>
                            <div class="d-flex flex-column flex-lg-row align-items-lg-center">
                                <div class="custom-control custom-radio py-2">
                                    <input type="radio" class="custom-control-input" id="checkRest_1" value="1" name="restrict_type" wire:model="restrict_type">
                                    <label class="custom-control-label" for="checkRest_1">Restringir por número de cédula</label>
                                </div>
                                <div class="ml-0 ml-lg-4">
                                    @if ($restrict_type == 1)
                                        <input wire:model="restrict_value" type="text" class="form-control @error('restrict_value') is-invalid @enderror"
                                               placeholder="Cantidad por cédula">
                                        @error('restrict_value')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    @endif
                                </div>
                            </div>
                            <div class="d-flex flex-column flex-lg-row align-items-lg-center">
                                <div class="custom-control custom-radio py-2">
                                    <input type="radio" class="custom-control-input" id="checkRest_2" value="2" name="restrict_type" wire:model="restrict_type">
                                    <label class="custom-control-label" for="checkRest_2">Cantidad máxima</label>
                                </div>
                                <div class="ml-0 ml-lg-4">
                                    @if ($restrict_type == 2)
                                        <input wire:model="restrict_value" type="text" class="form-control @error('restrict_value') is-invalid @enderror"
                                               placeholder="Cantidad">
                                        @error('restrict_value')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    @endif
                                </div>
                            </div>

                            {{-- <div class="custom-control custom-radio custom-control-inline">
                                <label>
                                    <input type="checkbox" value="1" wire:model="restrict_type">
                                    Cédula
                                </label>
                            </div> --}}
                            {{-- <div class="custom-control custom-radio custom-control-inline">
                                @if ($restrict_type)
                                    <input wire:model="restrict_value" type="text" class="form-control"
                                           placeholder="Cantidad por cédula">
                                @endif
                            </div>--}}
                        </div>
                    {{-- </div> --}}
                    @if ($event_frecuency == 2)
                        {{-- <div class="col-md-6"> --}}
                            <div class="form-group">
                                <h5 class="mb-2 py-2">Tipo de venta</h5>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input wire:model="event_sale_type" type="radio"
                                           class="custom-control-input @error('event_sale_type') is-invalid @enderror"
                                           id="est1" name="event_sale_type" value="1">
                                    <label for="est1" class="custom-control-label">Venta única</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input wire:model="event_sale_type" type="radio"
                                           class="custom-control-input @error('event_sale_type') is-invalid @enderror"
                                           id="est2" name="event_sale_type" value="2">
                                    <label for="est2" class="custom-control-label">Venta por fecha</label>
                                </div>
                            </div>
                        {{-- </div> --}}
                    @endif
                </div>
                <div class="form-group pt-4 pb-5" id="slot">
                    @foreach ($sectors as $key => $sector)
                        <div class="row" style="padding-bottom: 20px">
                            <div class="col-12 col-lg-4">
                                <h6>Nombre de entrada</h6>
                                <input wire:key="sector-name-{{ $key }}"
                                       wire:model="sectors.{{$key}}.sector_name"
                                       type="text" autofocus=""
                                       class="form-control pl-lg-4 pr-lg-1 @error('sectors.'.$key.'.sector_name') is-invalid @enderror"
                                       placeholder="General">
                                @error('sectors.'.$key.'.sector_name')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="col-md-6 col-lg-4 mt-4 mt-lg-0">
                                <h6>Precio</h6>
                                @if($sector_type == 1)
                                    <input wire:key="sector-price-{{ $key }}"
                                           wire:model="sectors.{{$key}}.sector_price"
                                           type="number" autofocus=""
                                           class="form-control @error('sectors.'.$key.'.sector_price') is-invalid @enderror"
                                           placeholder="Precio del sector">
                                    @error('sectors.'.$key.'.sector_price')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                @else
                                    <input type="number" value="0" class="form-control" disabled>
                                @endif

                            </div>
                            <div class="col-md-6 col-lg-4 mt-4 mt-lg-0">
                                <h6>Cantidad</h6>
                                <input wire:key="sector-count-{{ $key }}" id="hora-final"
                                       wire:model="sectors.{{$key}}.sector_count"
                                       type="number" autofocus=""
                                       class="form-control @error('sectors.'.$key.'.sector_count') is-invalid @enderror"
                                       placeholder="Cantidad de entradas">
                                @error('sectors.'.$key.'.sector_count')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="col-md-4 mt-4 mt-lg-0 pt-2">
                                @isset($event_sale_type)
                                    @if ($event_sale_type == 2)
                                        <h6>Fecha</h6>
                                        {!! Form::select('date_id', $array_dates, null,
                                            ['class'        => $errors->has('sectors.'.$key.'.date_id') ? 'form-control is-invalid' : 'custom-select form-control',
                                            'placeholder'   => 'Todas las fechas',
                                            'wire:key'      => 'sectors-date_id-'.$key,
                                            'wire:model'    => 'sectors.'.$key.'.date_id'
                                            ]) !!}
                                        @error('sectors.'.$key.'.date_id')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    @endif
                                @endisset
                            </div>

                        </div>
                        @if ($loop->last)
                            <a wire:click="addSector" class="btn btn-tertiary mt-3" href="javascript:void(0)">Añadir
                                otra entrada</a>
                            @if (count($sectors) > 1)
                                <a wire:click="removeSector({{$key}})" class="btn btn-tertiary mt-3"
                                   href="javascript:void(0)">Eliminar una entrada</a>
                            @endif
                        @endif
                    @endforeach
                </div>
                <div class="form-group float-right">
                    <a href="javascript:void(0)" wire:click="back" class="btn btn-secondary mr-2">Volver</a>
                    <button href="javascript:void(0)" wire:click="fourthStep" class="btn btn-primary">Siguiente</button>
                </div>
            </div>
        </div>
        {{-- END STEP 3 --}}
        {{-- STEP 4 --}}

        <div style="display: @if($step != 4) none @endif" class="col-sm-10 col-md-8 col-xl-6 mt-5">
            <p>Pasos <b class="text-primary">4 de 4</b></p>
            <div class="progress mb-4">
                <div class="progress-bar bg-primary" role="progressbar" style="width: 100%" aria-valuenow="25"
                     aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <div class="pt-4 pb-5">
                <h2 class="mb-4">Portada y Descripción del Evento</h2>
                <div class="form-group pb-5">
                    <input type="hidden" id="img-nw" wire:model="bannerAdded">
                    <div class="row">
                    @if (!empty($banners))
                        @foreach ($banners as $key => $banner)
                            <div class="col mb-4">
                                <a wire:click="removeBanner({{$key}})" href="javascript:void(0)" class="btn btn-primary px-3" style="position: absolute; top: -16px; left: -1px;"><i class="fas fa-times"></i></a>
                                <img height="150px" wire:key="{{ $key }}" src="{{ $banner }}" alt="">
                            </div>
                        @endforeach
                    @endif
                    </div>
                    <h6 class="pt-4">Añade uno o más banners (imagen cuadrada)</h6>


                    <div wire:ignore>

                        <div class="image-editor">
                            <input type="file" class="cropit-image-input form-control mb-4" style="overflow: hidden;">
                            <div class="cropit-preview"></div>
                            <div class="image-size-label">
                                Redimencionar Imagen
                            </div>
                            <input type="range" class="cropit-image-zoom-input w-100 mb-3">
                            <div class="d-flex justify-content-center mb-5">
                                <button class="btn btn-tertiary rotate-ccw mr-3">Rotar <i class="fas fa-undo-alt"></i></button>
                                <button class="btn btn-tertiary rotate-cw mr-3">Rotar <i class="fas fa-redo-alt"></i></button>

<!--                                <button class="btn btn-tertiary export">Aplicar</button>-->
                                <button wire:loading.attr="disabled" wire:target.model="bannerAdded" class="btn btn-info export"
                                        type="button">
                                    <div wire:loading wire:target.model="bannerAdded">
                                        <i wire:loading.class="fas fa-ban"> Guardando</i>
                                    </div>
                                    <div wire:loading.remove wire:target.model="bannerAdded">
                                        Añadir imagen
                                    </div>
                                </button>
                            </div>
                        </div>
                    </div>

<!--                    <input type="file" name="event_covers[]" class="form-control mb-4" style="overflow: hidden;" id="event_cover" multiple-->
                    <div wire:ignore>
                        <textarea id="mytextarea" wire:model="event_description" name="mytextarea">
                          Hello, World!
                        </textarea>
                    </div>
                    <span id="total-characters">{{ strlen($event_description) }} de 30000
                        @error('event_description')
                            <b style="color: red;">({{--{{ $message }}--}})</b>
                        @enderror
                    </span>
<!--                    <div wire:ignore>

                        <h6 class="pt-4">Descripción completa</h6>
                        {{--{!! Form::textarea('event_description', old('event_description'), ['placeholder' => 'Descripción más breve de tu evento', 'class' => 'form-control', 'id' => 'summernote']) !!}--}}
                        <span id="total-characters">0 de 30000 @error('event_description') <b
                                style="color: red;">({{--{{ $message }}--}})</b>@enderror</span>
                    </div>-->
                    {{--<textarea id="event_description" name="event_description" placeholder="Descripción" required class="form-control" rows="5"></textarea>--}}
                </div>

                <div class="form-group float-right">
                    <a wire:click="back" class="btn btn-secondary mr-2">Volver</a>
<!--                    <button wire:click="saveEvent" class="btn btn-primary" type="button">Finalizar</button>-->

                    <button wire:click="saveEvent" wire:loading.attr="disabled" wire:target="saveEvent"
                            class="btn btn-primary" type="button">
                        <div wire:loading wire:target="saveEvent">
                            <i wire:loading.class="fas fa-ban"> Guardando</i>
                        </div>
                        <div wire:loading.remove wire:target="saveEvent">
                            Finalizar
                        </div>
                    </button>
                </div>
            </div>
        </div>
        {{-- END STEP 4 --}}
    </main>
</div>
