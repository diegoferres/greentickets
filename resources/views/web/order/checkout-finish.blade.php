@extends('layout.app')

@section('content')

    <section class="cart">
        <div class="container mb-5">
            <h2 class="pt-5 pb-4">Checkout</h2>
            <div class="row">
                <div class="col-lg-9">
                    <div class="row">
                        <div class="col-4">
                            <p class="mb-2">Inicia sesión</p>
                        </div>
                        <div class="col-4">
                            <p class="mb-2">Metodo de Pago</p>
                        </div>
                        <div class="col-4">
                            <p class="text-primary mb-2">Finalizar</p>
                        </div>
                    </div>
                    <div class="progress mb-4">
                        <div class="progress-bar bg-primary" role="progressbar" style="width: 100%" aria-valuenow="100"
                             aria-valuemin="0" aria-valuemax="100">
                        </div>
                    </div>
                </div>
                <div class="col-lg-9">
                    {{-- Entrada Generada --}}
                    <div class="py-4">
                        {{--@if ($order->payment_method_id == 1)
                            <h2 class="">Tu orden se ha generado con éxito</h2>
                            <p>Una vez comprobada la transferencia, le llegará un correo para acceder a la misma.</p>
                            <p>Las entradas generadas aparecen en la sección <a href="{{ route('web.user.purchased_events') }}"> <b>Eventos Comprados</b></a> del perfil</p>
                        @else
                            <h2 class="">Tus entradas fueron generados con éxito</h2>
                            <p>Las entradas generadas aparecen en la sección <a href="{{ route('web.user.purchased_events') }}"> <b>Eventos Comprados</b></a> del perfil</p>
                        @endif--}}
                        <h2 class="">Tu orden se ha generado con éxito.</h2>
                        <p>Las entradas generadas aparecen en la sección <a
                                    href="{{ route('web.user.purchased_events') }}"> <b>Eventos Comprados</b></a> del
                            perfil</p>
                        <h4>Detalle del pedido</h4>
                        @if($pay)
                            <p>Seleccionó <b>{{ $pay->payment_method->method }}</b> como método de pago.</p>
                            @if ($pay->method_id == 2 || $pay->method_id == 3 || $pay->method_id == 4)
                                <p>Debe acercarse a una boca de cobranza de <b>{{ $pay->method }}</b>, mencionando el
                                    comercio
                                    <b>PAGOPAR</b> y el número de pedido <b>{{ $pay->order_number }}</b> o mencionando su CI.</p>
                            @endif
                            <p><b>Pagado:</b> {{ $pay->paid ? 'Si' : 'No' }}</p>
                            <p><b>Medio de pago:</b> {{ $pay->method }}</p>
                            <p><b>Fecha de pago:</b> {{ $pay->paid_date ?: 'Sin fecha' }}</p>
                            <p><b>Monto:</b> {{ number_format($pay->amount, 0, ',', '.') }} Gs.</p>
                            <p><b>Fecha máxima de pago:</b> {{ $pay->max_pay_date }}</p>
                        @endif
                        <h3>Orden #{{ $order->id }}</h3>
                    </div>
                    <div class="row">
                        @foreach($order->detail as $detail)
                            @foreach($detail->tickets as $ticket)
                                <div class="col-md-6 col-xl-4">
                                    <div class="ticket">
                                        <div class="ticket-content">
                                            <h3 class="mb-3">{{ $ticket->order_detail->sector->event->event_name }}</h3>
                                            <h5 class="text-primary mb-3">{{ $ticket->order_detail->sector->sector_name }}</h5>
                                            @if ($ticket->order_detail->sector->event->event_sale_type == 2)
                                                <p>{{ strftime('%d, %B', strtotime($ticket->order_detail->date->event_date)) }}</p>
                                            @else
                                                @foreach ($ticket->order_detail->sector->event->dates as $date)
                                                    @if ($loop->first)
                                                        <p>De {{ strftime('%d, %B', strtotime($date->event_date)) }}</p>
                                                    @elseif ($loop->last)
                                                        <p>A {{ strftime('%d, %B', strtotime($date->event_date)) }}</p>
                                                    @endif
                                                @endforeach

                                                {{--@foreach($ticket->order_detail->sector->event->dates as $date)
                                                    @if (loop)

                                                    @endif
                                                @endforeach
                                                <p>De {{ strftime('%d, %B', strtotime($date->event_date)).', '.strftime('%H:%M', strtotime($date->event_time_start)) }}
                                                    Hs</p>
                                                <p>A {{ strftime('%d, %B', strtotime($date->event_date)).', '.strftime('%H:%M', strtotime($date->event_time_start)) }}
                                                    Hs</p>--}}
                                            @endif
                                            @foreach($ticket->order_detail->sector->event->dates as $date)

                                            @endforeach
                                            {{--  <p>25 febrero, 21:00 hs.</p>--}}
                                            @isset($ticket->order_detail->sector->event->venue)
                                                <p>{{ $ticket->order_detail->sector->event->venue->venue_name.', '.$ticket->order_detail->sector->event->venue->venue_address }}</p>
                                            @endisset
                                            {{--<p>Valido para <b class="text-primary">{{ $ticket->quantity }} personas</b></p>--}}
                                            {{--h2 class="text-center pt-4">2505622DC</h2>--}}
                                        </div>
                                        {{--<div class="ticket-qr">
                                            {!! QrCode::size(100)->generate($ticket->id); !!}
                                            --}}{{--<img src="{!!$message->embedData(QrCode::format('png')->generate('Embed me into an e-mail!'), 'QrCode.png', 'image/png')!!}">--}}{{--
                                            --}}{{--<img src="{{asset('/img/qr.png')}}" alt="QR" class="qr">--}}{{--
                                        </div>--}}
                                    </div>
                                </div>
                            @endforeach
                        @endforeach
                    </div>
                </div>
                <?php

                use Illuminate\Support\Facades\Route;

                ?>
                <div class="col-lg-3">
                    <div class="price pt-4">
                        <h3 class="mb-4">Detalles del pedido</h3>
                        <div class="d-flex justify-content-between">
                            <p>Cantidad total:</p>
                            <h5>{{ \Cart::getTotalQuantity() }}</h5>
                        </div>
                        <div class="d-flex justify-content-between">
                            <p>Sub Total:</p>
                            <h5>Gs. {{ number_format(\Cart::getSubTotal(),0,',','.') }}</h5>
                        </div>
                        <div class="d-flex justify-content-between">
                            <p>Cupón:</p>
                            <div class="d-flex flex-column">

                                @foreach(\Cart::getConditionsByType('coupon') as $coupon)
                                    @if ($coupon->getAttributes()['type_value'] == 'fixed')
                                        <p>{{ number_format($coupon->getValue(), 0, ",", ".") }} Gs <span class="cupon"><i
                                                        class="fas fa-ticket-alt"></i> {{ $coupon->getName()  }}</span></p>
                                    @elseif ($coupon->getAttributes()['type_value'] == 'percent')
                                        <p>{{ number_format($coupon->getValue(), 0, ",", ".") }} % <span class="cupon"><i
                                                        class="fas fa-ticket-alt"></i> {{ $coupon->getName()  }}</span></p>
                                    @endif
                                @endforeach
                                {{--<p>-Gs. 20.000 <span class="cupon"><i class="fas fa-ticket-alt"></i> ADC05325</span></p>
                                <p>-Gs. 20.000 <span class="cupon"><i class="fas fa-ticket-alt"></i> ADC05330</span></p>
                                <p>-Gs. 20.000 <span class="cupon"><i class="fas fa-ticket-alt"></i> ADC05340</span></p>--}}
                            </div>
                        </div>
                        {{--<div class="d-flex justify-content-between">
                            <p>Cupón:</p>
                            <div class="d-flex flex-column">
                                <p>-Gs. 20.000 <span class="cupon"><i class="fas fa-ticket-alt"></i> ADC05321</span></p>
                                <p>-Gs. 20.000 <span class="cupon"><i class="fas fa-ticket-alt"></i> ADC05325</span></p>
                                <p>-Gs. 20.000 <span class="cupon"><i class="fas fa-ticket-alt"></i> ADC05330</span></p>
                                <p>-Gs. 20.000 <span class="cupon"><i class="fas fa-ticket-alt"></i> ADC05340</span></p>
                            </div>
                        </div>--}}
                        <div class="d-flex justify-content-between">
                            <p>Descuento:</p>
                            <h5>0%</h5>
                        </div>

                        <div class="d-flex justify-content-between">
                            <p>Total:</p>
                            <h5>Gs. {{ number_format(\Cart::getTotal(),0,',','.') }}</h5>
                        </div>

                        @if (Route::currentRouteName() == 'web.orders.showcart')
                            <div class="d-flex flex-column mt-4">
                                <a class="btn btn-primary" href="{{ route('web.orders.checkout') }}">Proceder a la
                                    compra</a>
                            </div>
                        @endif

                    </div>
                    <div class="d-flex flex-column">
                        <!-- Button modal -->
                        <a href="{{ route('web.user.history.detail', $order->id) }}" class="btn btn-tertiary mt-4" data-toggle="modal" data-target="#ModalHistory">
                            Ver comprobante
                        </a>
                        <a href="{{ route('web.user.profile') }}" class="btn btn-primary mt-2">Ir al Perfil</a>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal Detail -->
        <div class="modal fade" id="ModalHistory" tabindex="-1" role="dialog" aria-labelledby="ModalHistoryTitle"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="ModalHistoryTitle">Detalle de la compra</h3>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><i class="fas fa-times"></i></span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="history cart">
                            <div class="cart-list">
                                <ul>
                                    <li class="border-0">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <img src="{{asset('/img/logo.svg')}}" alt="Logo" class="py-2 mb-3">
                                            </div>
                                            <div class="col-md-6">
                                                <p><b>N.º de orden:</b> #{{ $order->id }}</p>
                                                <p>
                                                    <b>Fecha:</b> @php(setlocale(LC_TIME,"es_ES.UTF-8")){{ strftime("%A, %d de %B", strtotime($order->created_at)) }}
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="border-0 p-3 bg-light">
                                        <h6 class="mb-0 d-flex d-md-none">Lista de eventos añadidos</h6>
                                        <div class="row align-items-center d-none d-md-flex">
                                            <div class="col-md-5 text-center">
                                                <h6 class="mb-0">Evento</h6>
                                            </div>
                                            <div class="col-md-2 text-center">
                                                <h6 class="mb-0">Entrada</h6>
                                            </div>
                                            <div class="col-md-3 text-center">
                                                <h6 class="mb-0">Precio</h6>
                                            </div>
                                            <div class="col-md-2 text-center">
                                                <h6 class="mb-0">Cantidad</h6>
                                            </div>
                                        </div>
                                    </li>
                                    @foreach ($order->detail as $detail)
                                        <li>
                                            <div class="row align-items-center">
                                                <div class="col-md-5">
                                                    <div class="row align-items-center">
                                                        <div class="col-4">
                                                            <img src="{{asset('storage/'.$detail->sector->event->covers->first()->url . $detail->sector->event->covers->first()->file_name )}}"
                                                                 alt="{{ $detail->sector->event->event_name }}"
                                                                 class="w-100">
                                                        </div>
                                                        <div class="col-8">
                                                            <h5 class="mb-md-0">{{ $detail->sector->event->event_name }}</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 text-md-center">
                                                    <h5 class="mt-4 mt-md-0 mb-md-0">{{ $detail->sector->sector_name }}</h5>
                                                </div>
                                                <div class="col-md-3 text-md-center">
                                                    {{--<p class="mb-0 price-old d-inline-block d-md-block mr-3 mr-md-0">Gs.
                                                        500.000</p>--}}
                                                    <span class="mr-3 mr-md-0"><b>Gs. {{ number_format($detail->price, 0, ",", ".") }}</b></span>
                                                    {{--<p class="cupon mb-md-0 d-inline-block d-md-block"><i
                                                                class="fas fa-ticket-alt"></i> Cupón ADC05321</p>--}}
                                                </div>
                                                <div class="col-md-2 text-md-center">
                                                    <h5 class="mb-md-0">{{ $detail->tickets->count() }}</h5>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                    <li>
                                        <div class="row py-3">
                                            @if (isset($order->payment->payment_method->method))


                                            <div class="col-md-6">
                                                <h6 class="text-primary">Metodo de Pago</h6>
                                                <h5 class="mb-4">{{ $order->payment->payment_method->method }}</h5>
                                            </div>
                                            @endif
                                            <div class="col-md-6">
                                                <div class="price">
                                                    <div class="d-flex justify-content-between">
                                                        <h6 class="pl-md-3 mb-4">Cantidad Total:</h6>
                                                        <h5 class="mb-4 pr-md-3">{{ $order->tickets->count() }}</h5>
                                                    </div>
                                                    <div class="d-flex justify-content-between">
                                                        <h6 class="pl-md-3 mb-4">Sub Total:</h6>
                                                        <h5 class="mb-4 pr-md-3">
                                                            Gs. {{ number_format($order->subtotal, 0, ",", ".") }}</h5>
                                                    </div>
                                                    <div class="d-flex justify-content-between">
                                                        <h6 class="pl-md-3 mb-4">Descuento:</h6>
                                                        <h5 class="mb-4 pr-md-3">0%</h5>
                                                    </div>
                                                    <div class="d-flex justify-content-between bg-light">
                                                        <h6 class="p-3 mb-0">Total:</h6>
                                                        <h5 class="p-3 mb-0">
                                                            Gs. {{ number_format($order->subtotal, 0, ",", ".") }}</h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="row">
                                            <div class="col-md-5">
                                                <p>Cliente <b>{{ Auth::user()->name }}</b></p>
                                            </div>
                                            <div class="col-md-7">
                                                <p>Ante cualquier consulta, comuniquese a <a href="#">info@entrasinpapel.com</a>
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
        {{--<div class="modal fade" id="ModalHistory" tabindex="-1" role="dialog" aria-labelledby="ModalHistoryTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="ModalHistoryTitle">Detalle de la compra</h3>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><i class="fas fa-times"></i></span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="history cart">
                            <div class="cart-list">
                                <ul>
                                    <li class="border-0">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <img src="{{asset('/img/logo.svg')}}" alt="Logo" class="py-2 mb-3">
                                            </div>
                                            <div class="col-md-6">
                                                <p><b>N.º de orden:</b> #5</p>
                                                <p><b>Fecha:</b> 16 de Mayo de 2020</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="border-0 p-3 bg-light">
                                        <h6 class="mb-0 d-flex d-md-none">Lista de eventos añadidos</h6>
                                        <div class="row align-items-center d-none d-md-flex">
                                            <div class="col-md-5 text-center">
                                                <h6 class="mb-0">Evento</h6>
                                            </div>
                                            <div class="col-md-2 text-center">
                                                <h6 class="mb-0">Entrada</h6>
                                            </div>
                                            <div class="col-md-3 text-center">
                                                <h6 class="mb-0">Precio</h6>
                                            </div>
                                            <div class="col-md-2 text-center">
                                                <h6 class="mb-0">Cantidad</h6>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="row align-items-center">
                                            <div class="col-md-5">
                                                <div class="row align-items-center">
                                                    <div class="col-4">
                                                        <img src="{{asset('/img/party.jpg')}}" alt="Imagen del evento" class="w-100">
                                                    </div>
                                                    <div class="col-8">
                                                        <h5 class="mb-md-0">Blockchain Express Webinar</h5>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2 text-md-center">
                                                <h5 class="mt-4 mt-md-0 mb-md-0">VIP</h5>
                                            </div>
                                            <div class="col-md-3 text-md-center">
                                                <p class="mb-0 price-old d-inline-block d-md-block mr-3 mr-md-0">Gs. 500.000</p>
                                                <span class="mr-3 mr-md-0"><b>Gs. 250.000</b></span>
                                                <p class="cupon mb-md-0 d-inline-block d-md-block"><i class="fas fa-ticket-alt"></i> Cupón ADC05321</p>
                                            </div>
                                            <div class="col-md-2 text-md-center">
                                                <h5 class="mb-md-0">2</h5>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="row py-3">
                                            <div class="col-md-6">
                                                <h6 class="text-primary">Metodo de Pago</h6>
                                                <h5 class="mb-4">Transferencia bancaria</h5>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="price">
                                                    <div class="d-flex justify-content-between">
                                                        <h6 class="pl-md-3 mb-4">Cantidad Total:</h6>
                                                        <h5 class="mb-4 pr-md-3">2</h5>
                                                    </div>
                                                    <div class="d-flex justify-content-between">
                                                        <h6 class="pl-md-3 mb-4">Sub Total:</h6>
                                                        <h5 class="mb-4 pr-md-3">Gs. 1.000.000</h5>
                                                    </div>
                                                    <div class="d-flex justify-content-between">
                                                        <h6 class="pl-md-3 mb-4">Descuento:</h6>
                                                        <h5 class="mb-4 pr-md-3">50%</h5>
                                                    </div>
                                                    <div class="d-flex justify-content-between bg-light">
                                                        <h6 class="p-3 mb-0">Total:</h6>
                                                        <h5 class="p-3 mb-0">Gs. 500.000</h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="row">
                                            <div class="col-md-5">
                                                <p>Cliente <b>Mathias Medina</b></p>
                                            </div>
                                            <div class="col-md-7">
                                                <p>Ante cualquier consulta, comuniquese a <a href="#">info@entrasinpapel.com</a></p>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>--}}
    </section>

@stop
