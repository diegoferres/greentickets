@extends('layout.app')

@section('content')

    <section class="cart">
        <div class="container">
            <h2 class="pt-5 pb-4">Carrito</h2>
            <div class="row">
                <div class="col-lg-9">
                    <div class="cart-list">
                        <ul>
                            <li class="border-0 p-3 bg-light">
                                <h6 class="mb-0 d-flex d-md-none">Lista de eventos añadidos</h6>
                                <div class="row align-items-center d-none d-md-flex">
                                    <div class="col-md-4 text-center">
                                        <h6 class="mb-0">Evento</h6>
                                    </div>
                                    <div class="col-md-2 text-center">
                                        <h6 class="mb-0">Entrada</h6>
                                    </div>
                                    <div class="col-md-3 text-center">
                                        <h6 class="mb-0">Precio</h6>
                                    </div>
                                    <div class="col-md-2 text-center">
                                        <h6 class="mb-0">Cantidad</h6>
                                    </div>
                                </div>
                            </li>
                            @foreach(\Cart::getContent() as $item)
                                <li>
                                    <div class="row align-items-center">
                                        <div class="col-md-4">
                                            <h5 class="mb-md-0 pl-md-2">{{ $item->attributes->event_name }}</h5>
                                        </div>
                                        <div class="col-md-2 text-md-center">
                                            <h5 class="mb-md-0 pl-md-2">
                                                {{ $item->name }}
                                            </h5>
                                            <p class="cupon mb-md-0 d-inline-block d-md-block">{{ $item->attributes->date ? strftime("%d, %B", strtotime($item->attributes->date)) : null }}</p>
                                            <p class="cupon mb-md-0 d-inline-block d-md-block">{{ number_format($item->attributes->ticket_data_doc, 0, ',','.') }}</p>
                                        </div>
                                        <div class="col-md-3 text-md-center">
                                            {{--<p class="mb-0 price-old d-inline-block d-md-block mr-3 mr-md-0">Gs. {{ $item->price }}</p>--}}
                                            <span class="mr-3 mr-md-0"><b>Gs. {{ number_format($item->price,0,',','.') }}</b></span>
                                            {{--<p class="cupon mb-md-0 d-inline-block d-md-block"><i
                                                        class="fas fa-ticket-alt"></i> Cupón ADC05321</p>--}}
                                        </div>
                                        <div class="col-10 col-md-2">
                                            {{ $item->quantity }}
                                            {{--{!! Form::open(['route' => 'web.orders.addtocart', 'method' => 'post', 'id' => 'formaddtocart'.$item->id]) !!}
                                            <input id="sector_id" name="sector_id" type="hidden"
                                                   value="{{ $item->id }}">
                                            --}}{{--<input id="count" name="count" value="{{ $item->quantity }}"
                                                   type="number"--}}{{--
                                            <input type="number"
                                                   class="form-control {{ $errors->has('count') ? 'is-invalid' : '' }}"
                                                   {{ $errors->has('count') ? 'autofocus' : '' }}
                                                   id="count" name="count"
                                                   aria-describedby="numberOperation" value="{{ $item->quantity }}">
                                            @error('count')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                            --}}{{--class="form-control">--}}{{--
                                            {!! Form::close() !!}--}}
                                        </div>
                                        <div class="col-2 col-md-1">
                                            <a href="{{ route('web.orders.removefromcart', $item->id) }}"
                                               class="icon-cart" title="Quitar del carrito"><i
                                                        class="far fa-trash-alt"></i></a>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                {{--<div class="col-lg-3">
                    <div class="price">
                        <h3 class="mb-4">Detalles</h3>
                        <div class="d-flex justify-content-between">
                            <p>Cantidad total:</p>
                            <h5>{{ \Cart::getTotalQuantity() }}</h5>
                        </div>
                        <div class="d-flex justify-content-between">
                            <p>Sub Total:</p>
                            <h5>Gs. {{ \Cart::getSubTotal() }}</h5>
                        </div>
                        <div class="d-flex justify-content-between">
                            <p>Descuento:</p>
                            <h5>0%</h5>
                        </div>
                        <div class="d-flex justify-content-between">
                            <p>Total:</p>
                            <h5>Gs. {{ \Cart::getSubTotal() }}</h5>
                        </div>
                        <div class="d-flex flex-column mt-4">
                            <a class="btn btn-primary" href="{{ route('web.orders.checkout') }}">Proceder a la
                                compra</a>
                        </div>
                    </div>
                    <div class="cupon-canje pt-4">
                        <div class="d-flex flex-column">
                            <input type="text" placeholder="Ingrese su cupón" class="form-control">
                            <a href="#" class="btn btn-tertiary mt-2">Aplicar</a>
                        </div>
                        <div id="cuponAplicado">
                            <div class="mt-4">
                                --}}{{--<span class="cupon"><i
                                            class="fas fa-ticket-alt"></i> Se ha aplicado <b>ADC05321</b></span>--}}{{--
                            </div>
                        </div>
                    </div>
                </div>--}}
                @include('web.order.amounts_partial')
            </div>
            <div class="pt-5 mb-5">
                <a href="#" class="btn btn-tertiary"><i class="fas fa-arrow-left mr-3"></i>
                    Continuar buscando eventos</a>
            </div>
        </div>
    </section>

@endsection
@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
        $('input#count').on('keyup change', function () {
            console.log($(this).siblings('input#sector_id').val())
            console.log($('form#formaddtocart' + $(this).siblings('input#sector_id').val()));
            //$('form#formaddtocart' + $(this).siblings('input#sector_id').val()).submit();
        })
    </script>
@endsection
