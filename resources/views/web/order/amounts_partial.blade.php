<?php

use Illuminate\Support\Facades\Route;

?>
<div class="col-lg-3">
    <div class="price pt-4">
        <h3 class="mb-4">Detalles del pedido</h3>
        <div class="d-flex justify-content-between">
            <p>Cantidad total:</p>
            <h5>{{ \Cart::getTotalQuantity() }}</h5>
        </div>
        <div class="d-flex justify-content-between">
            <p>Sub Total:</p>
            <h5>Gs. {{ number_format(\Cart::getSubTotal(),0,',','.') }}</h5>
        </div>
        <div class="d-flex justify-content-between">
            <p>Cupón:</p>
            <div class="d-flex flex-column">

                @foreach(\Cart::getConditionsByType('coupon') as $coupon)
                    @if ($coupon->getAttributes()['type_value'] == 'fixed')
                        <p>{{ number_format($coupon->getValue(), 0, ",", ".") }} Gs <span class="cupon"><i
                                        class="fas fa-ticket-alt"></i> {{ $coupon->getName()  }}</span></p>
                    @elseif ($coupon->getAttributes()['type_value'] == 'percent')
                        <p>{{ number_format($coupon->getValue(), 0, ",", ".") }} % <span class="cupon"><i
                                        class="fas fa-ticket-alt"></i> {{ $coupon->getName()  }}</span></p>
                    @endif
                @endforeach
                {{--<p>-Gs. 20.000 <span class="cupon"><i class="fas fa-ticket-alt"></i> ADC05325</span></p>
                <p>-Gs. 20.000 <span class="cupon"><i class="fas fa-ticket-alt"></i> ADC05330</span></p>
                <p>-Gs. 20.000 <span class="cupon"><i class="fas fa-ticket-alt"></i> ADC05340</span></p>--}}
            </div>
        </div>
        {{--<div class="d-flex justify-content-between">
            <p>Cupón:</p>
            <div class="d-flex flex-column">
                <p>-Gs. 20.000 <span class="cupon"><i class="fas fa-ticket-alt"></i> ADC05321</span></p>
                <p>-Gs. 20.000 <span class="cupon"><i class="fas fa-ticket-alt"></i> ADC05325</span></p>
                <p>-Gs. 20.000 <span class="cupon"><i class="fas fa-ticket-alt"></i> ADC05330</span></p>
                <p>-Gs. 20.000 <span class="cupon"><i class="fas fa-ticket-alt"></i> ADC05340</span></p>
            </div>
        </div>--}}
        <div class="d-flex justify-content-between">
            <p>Descuento:</p>
            <h5>0%</h5>
        </div>

        <div class="d-flex justify-content-between">
            <p>Total:</p>
            <h5>Gs. {{ number_format(\Cart::getTotal(),0,',','.') }}</h5>
        </div>

        @if (Route::currentRouteName() == 'web.orders.showcart')
            <div class="d-flex flex-column mt-4">
                <a class="btn btn-primary" href="{{ route('web.orders.checkout') }}">Proceder a la
                    compra</a>
            </div>
        @endif

    </div>
    <div class="cupon-canje pt-4">
        {!! Form::open(['route' => 'web.coupons.store', 'method' => 'post']) !!}
        <div class="d-flex flex-column">
            <input type="text" placeholder="Ingrese su cupón" class="form-control">
            <button type="submit" class="btn btn-tertiary mt-2">Aplicar</button>
        </div>
        {!! Form::close() !!}
        <div id="cuponAplicado">
            <div class="mt-4">
<!--                <span class="cupon"><i class="fas fa-ticket-alt"></i> Se ha aplicado <b>ADC05321</b></span>-->
            </div>
        </div>
    </div>
</div>
