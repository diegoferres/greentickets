@extends('layout.app')

    @section('content')

    <section class="cart">
        <div class="container mb-5">
            <h2 class="pt-5 pb-4">Checkout</h2>
            <div class="row">
                <div class="col-lg-9">
                    <div class="row">
                        <div class="col-4">
                            <p class="text-primary mb-2">Inicia sesión</p>
                        </div>
                        <div class="col-4">
                            <p class="mb-2">Metodo de Pago</p>
                        </div>
                        <div class="col-4">
                            <p class="mb-2">Finalizar</p>
                        </div>
                    </div>
                    <div class="progress mb-4">
                        <div class="progress-bar bg-primary" role="progressbar" style="width: 33.3%" aria-valuenow="33.3"
                            aria-valuemin="0" aria-valuemax="100">
                        </div>
                    </div>
                </div>
                <div class="col-lg-9">
                    {{-- Usuario logueado --}}
                    <div class="pt-4">
                        <h3 class="mb-3">Inciaste sesión cómo <a href="{{ route('web.user.profile') }}">{{ \Auth::user()->name }}</a></h3>
                        <p>Si no es usted <a id="logout" href="javascript:void(0)"><b>cierra la sesión</b></a>
                        </p>
                    </div>
                    <div class="pt-5 mb-5">
                        <a href="{{ route('web.orders.showcart') }}" class="btn btn-tertiary mb-4 mb-md-0"><i class="fas fa-arrow-left mr-3"></i> Volver al carrito</a>
                        <a href="{{ route('web.orders.checkout.payment') }}" class="btn btn-primary float-md-right mr-lg-4">Continuar con la compra</a>
                    </div>

                    {{-- Formulario de Registro --}}

                    {{-- <div class="pt-4">
                        <h2 class="mb-3">Registrate</h2>
                        <p class="mb-4">Registrate con tu correo. Si ya tienes una cuenta, aquí puedes <a href="{{ route('login') }}"><b>Iniciar Sesión</b></a>
                        </p>
                        {!! Form::open(['route' => 'register', 'method' => 'post']) !!}
                        <div class="form-group mb-4">
                            <label for="name" class="label">Nombre y Apellido</label>
                                <input id="name" name="name" type="text" autofocus=""
                                    class="form-control @error('name') is-invalid @enderror" placeholder="Nombre y Apellido" value="{{ old('name') }}">
                            @error('name')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group mb-4">
                            <label for="name" class="label">Correo</label>
                                <input id="email" name="email" type="email" autofocus=""
                                    class="form-control @error('email') is-invalid @enderror"
                                    placeholder="Ej.: micorreo@gmail.com" value="{{ old('email') }}">
                            @error('email')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group mb-4">
                            <label for="name" class="label">Fecha de Nacimiento</label>
                                <input id="born_date" name="born_date" type="date" autofocus="" maxlength="10"
                                    class="form-control @error('born_date') is-invalid @enderror"
                                    value="{{ old('born_date') }}">
                            @error('born_date')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group mb-4">
                            <label for="name" class="label">Celular</label>
                                <input id="cell_phone" name="cell_phone" type="number" autofocus=""
                                    class="form-control @error('cell_phone') is-invalid @enderror" placeholder="Ej.: 0981999888" value="{{ old('cell_phone') }}">
                            @error('cell_phone')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group mb-4">
                            <label for="name" class="label">Contraseña</label>
                                <input id="password" name="password" type="password"
                                    class="form-control mb-2 @error('password') is-invalid @enderror"
                                    placeholder="Contraseña" value="{{ old('name') }}">
                            @error('password')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                            <small class="text-muted pl-4">La contraseña debe tener por lo menos 8 caracteres</small>
                        </div>
                        <div class="form-group mb-4">
                            <label for="name" class="label">Confirmar contraseña</label>
                                <input id="password_confirm" name="password_confirmation" type="password" class="form-control mb-2"
                                    placeholder="Repite la contraseña">
                        </div>
                        <div class="d-flex flex-column flex-md-row justify-content-md-between align-items-md-center pt-4">
                            <p class="mb-md-0 pr-md-3">Al registrarte aceptas las <a href="#"><b>políticas de privacidad</b></a> y los <a
                                        href="#"><b>términos y condiciones</b></a> de uso.</p>
                            <button type="submit" class="btn btn-primary">Registrate</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- Registro con redes sociales -->
                    <div class="mb-4 pt-5">
                        <p class="mb-3">Tambien puedes registrarte con tus redes</p>
                        <div class="d-flex flex-column flex-md-row">
                            <a href="#" class="btn-social btn-f mb-2 mb-md-0 mr-md-2 flex-md-fill">Registrate con <strong>Facebook</strong></a>
                            <a href="#" class="btn-social btn-g flex-md-fill">Registrate con <strong>Google</strong></a>
                        </div>
                    </div> --}}
                </div>
                @include('web.order.amounts_partial')
            </div>
        </div>
    </section>

@stop