@extends('layout.app')

@section('content')

    <section class="cart">
        <div class="container mb-5">
            <h2 class="pt-5 pb-4">Checkout</h2>
            <div class="row">
                <div class="col-lg-9">
                    <div class="row">
                        <div class="col-4">
                            <p class="mb-2">Inicia sesión</p>
                        </div>
                        <div class="col-4">
                            <p class="text-primary mb-2">Metodo de Pago</p>
                        </div>
                        <div class="col-4">
                            <p class="mb-2">Finalizar</p>
                        </div>
                    </div>
                    <div class="progress mb-4">
                        <div class="progress-bar bg-primary" role="progressbar" style="width: 69%" aria-valuenow="69"
                             aria-valuemin="0" aria-valuemax="100">
                        </div>
                    </div>
                </div>
                <div class="col-lg-9">
                    {{-- Metodo de Pago --}}
                    <div class="pt-4">
                        <h3 class="mb-4">Elegi tu metodo de Pago</h3>
                    </div>
                    {!! Form::open(['route' => 'web.orders.checkout.finish', 'method' => 'post']) !!}
                    <div class="accordion" id="accordionPayment">

                        <div class="card">
                            <div class="card-header">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="paymentBank" name="payment_method_id" value="1"
                                           class="custom-control-input" {{ old('payment_method_id') == 1 || is_null(old('payment_method_id')) ? 'checked' : '' }}>
                                    {{--<label class="custom-control-label collapsed" for="paymentBank"
                                           data-toggle="collapse" data-target="#collapseBank" aria-expanded="false"
                                           aria-controls="collapseBank">--}}
                                    <label class="custom-control-label {{ old('payment_method_id') == 1 ? '' : 'collapsed' }}"
                                           for="paymentBank"
                                           data-toggle="collapse" data-target="#collapseBank"
                                           aria-expanded="{{ old('payment_method_id') == 1 || is_null(old('payment_method_id')) ? 'true' : 'false' }}"
                                           aria-controls="collapseBank">
                                        Transferencia Bancaria
                                    </label>
                                </div>
                            </div>

                            <div id="collapseBank"
                                 class="collapse {{ old('payment_method_id') == 1 || is_null(old('payment_method_id')) ? 'show' : '' }} "
                                 aria-labelledby="headingBank"
                                 data-parent="#accordionPayment">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5 class="text-primary">Banco Itau</h5>
                                            <p class="mb-1">Tipo de Cuenta:</p>
                                            <h5>Cuenta Corriente - En Guaraníes</h5>
                                            <p class="mb-1">Nro. de Cuenta:</p>
                                            <h5>2545451215</h5>
                                            <p class="mb-1">Titular:</p>
                                            <h5>Entra sin papel S.A</h5>
                                            <p class="mb-1">R.U.C:</p>
                                            <h5>8552222-8</h5>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group mt-3">
                                                <label for="numberOperation" class="text-primary">Número de
                                                    operacion</label>
                                                <input type="number"
                                                       class="form-control {{ $errors->has('voucher') ? 'is-invalid' : '' }}"
                                                       {{ $errors->has('voucher') ? 'autofocus' : '' }} id="numberOperation"
                                                       name="voucher"
                                                       aria-describedby="numberOperation">
                                                @error('voucher')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                                @enderror
                                                <small id="numberOperation" class="form-text text-muted">Para validar la
                                                    transferencia debes de colocar el número de operación, está se
                                                    encuentra en el comprobante de tu banco.
                                                    <br><br>La verificación del pago demora entre 24 a 48 hs. Mientras
                                                    no sea verificado el pago la entrada generada no tendrá validez.
                                                </small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="paymentPagopar" name="payment_method_id" value="2"
                                           class="custom-control-input">
                                    <label class="custom-control-label collapsed" for="paymentPagopar"
                                           data-toggle="collapse" data-target="#collapsePagopar" aria-expanded="false"
                                           aria-controls="collapsePagopar">
                                        Pagopar
                                    </label>
                                </div>
                            </div>

                            <div id="collapsePagopar" class="collapse" aria-labelledby="headingPagopar"
                                 data-parent="#accordionPayment">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="px-2">
                                                <div class="w-50 mb-3">
                                                    <img src="{{asset('/img/logo-pagopar.png')}}" alt="Logo Pagopar"
                                                         class="img-fluid">
                                                </div>
                                                <p>Paga seguro con los medios de pago<br>que ofrece Pagopar</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group mt-3 px-2">
                                                <div class="custom-control custom-radio py-2">
                                                    <input type="radio" id="pagoparCard" name="pagoparCard"
                                                           class="custom-control-input">
                                                    <label class="custom-control-label" for="pagoparCard">Tarjeta de
                                                        Crédito/Débito</label>
                                                </div>
                                                <div class="custom-control custom-radio py-2">
                                                    <input type="radio" id="pagoparPersonal" name="pagoparCard"
                                                           class="custom-control-input">
                                                    <label class="custom-control-label" for="pagoparPersonal">Billetera
                                                        Personal</label>
                                                </div>
                                                <div class="custom-control custom-radio py-2">
                                                    <input type="radio" id="pagoparTigomoney" name="pagoparCard"
                                                           class="custom-control-input">
                                                    <label class="custom-control-label" for="pagoparTigomoney">Tigo
                                                        Money</label>
                                                </div>
                                                <div class="custom-control custom-radio py-2">
                                                    <input type="radio" id="pagoparTigogiros" name="pagoparCard"
                                                           class="custom-control-input">
                                                    <label class="custom-control-label" for="pagoparTigogiros">Giros
                                                        Tigo</label>
                                                </div>
                                                <div class="custom-control custom-radio py-2">
                                                    <input type="radio" id="pagoparInfonet" name="pagoparCard"
                                                           class="custom-control-input">
                                                    <label class="custom-control-label" for="pagoparInfonet">Infonet
                                                        Cobranzas</label>
                                                </div>
                                                <small class="form-text text-muted pt-4">Al selecionar el metodo de pago
                                                    se redicionara a la plataforma de
                                                    Pagopar para realizar el pago.
                                                </small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="pt-5 mb-5">
                        <a href="{{ route('web.orders.checkout') }}" class="btn btn-tertiary mb-4 mb-md-0"><i
                                    class="fas fa-arrow-left mr-3"></i> Volver atras</a>
                        {{--<a href="{{ route('web.orders.checkout.finish') }}" class="btn btn-primary float-md-right mr-lg-4">Continuar
                            con la compra</a>--}}
                        <button type="submit" class="btn btn-primary float-md-right mr-lg-4">Finalizar la compra
                        </button>
                    </div>
                    {!! Form::close() !!}
                </div>
                @include('web.order.amounts_partial')
            </div>
        </div>
    </section>

@stop