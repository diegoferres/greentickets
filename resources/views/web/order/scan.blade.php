@extends('layout.app')

@section('content')
    <div class="profile">

        <div class="container mb-12">
            <div class="row pt-5">
                <div class="col-md-12 col-lg-3">
                    <div class="ticket">
                        <div class="ticket-content">
                            <h3 class="mb-3">Blockchain Express Webinar</h3>
                            <h5 class="text-primary mb-3">Entrada VIP</h5>
                            <p>25 febrero, 21:00 hs.</p>
                            <p>El Granel, Asunción, Paraguay</p>
                            <p>Valido para <b class="text-primary">2 personas</b></p>
                            <h2 class="text-center pt-4">2505622DC</h2>
                        </div>
                        <div class="ticket-qr justify-content-center">
                            <img src="{{asset('/img/qr.png')}}" alt="QR" class="qr">
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
