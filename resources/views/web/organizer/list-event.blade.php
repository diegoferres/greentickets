@extends('layout.app')
@section('content')
    <div class="organizer">
        <div class="list-event">
            <div class="container">
                <div class="pt-4 pb-5">
                    <div class="row align-items-center">
                        <div class="col-lg-2">
                            <img src="{{asset('storage/'.$event->covers->first()->url.$event->covers->first()->file_name)}}" alt="{{ $event->event_name }}" class="img-fluid">
                        </div>
                        <div class="col-lg-7 my-4 mb-lg-0">
                            <h1 class="px-lg-3">{{ $event->event_name }}</h1>
                        </div>
                        <div class="col-lg-3 flex-row flex-lg-column">
                            <a href="{{ route('web.organizer.event.edit', $event->id) }}" class="btn btn-primary btn-block mb-2">Editar</a>
                            <a href="{{ route('web.organizer.control.tickets', $event->id) }}" class="btn btn-warning btn-block mb-2">Controlar Entradas</a>
                            <a href="{{ route('web.organizer.sale.tickets', $event->id) }}" class="btn btn-info btn-block mb-2">Vender Entradas</a>
                            <a href="#" class="btn btn-danger btn-block">Eliminar</a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive-md pt-4">
                            <table class="table status">
                                {{--<caption>
                                    <small class="d-inline-block pt-3">Total de compradores: 3</small>
                                    <nav class="d-inline-block float-right pt-2" aria-label="...">
                                        <ul class="pagination">
                                            <li class="page-item disabled">
                                                <span class="page-link">Anterior</span>
                                            </li>
                                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                                            <li class="page-item active" aria-current="page">
                                                <span class="page-link">
                                                2
                                                <span class="sr-only">(current)</span>
                                                </span>
                                            </li>
                                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                                            <li class="page-item">
                                                <a class="page-link" href="#">Siguiente</a>
                                            </li>
                                        </ul>
                                    </nav>
                                </caption>--}}
                                <thead class="thead-event">
                                    <tr>
                                        <th colspan="2" scope="col">Nombre y apellido</th>
                                        <th scope="col">Entrada</th>
                                        <th scope="col">Cantidad</th>
                                        <th scope="col">Fecha de compra</th>
                                        <th scope="col">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($event->orders_detail as $detail)
                                    <tr>
                                        <td class="avatar-event"><img src="{{asset('/img/cart.jpg')}}" alt="Imagen de perfil" class="img-fluid"></td>
                                        <td><h5 class="mb-md-0 text-left">{{ $detail->order->client->name }}</h5></td>
                                        <td><p class="mb-0">{{ $detail->sector->sector_name }}</p></td>
                                        <td><p class="mb-0">{{ $detail->quantity }}</p></td>
                                        <td><p class="mb-0">{{ date('d/m/Y', strtotime($detail->order->created_at)) }}</p></td>
                                        <td>
                                            <a href="https://wa.me/595{{ $detail->order->client->cell_phone }}" class="btn btn-tertiary">Contactar</a>
                                            <a href="{{ route('web.organizer.event.ticket.status',[$event->id,$detail->id]) }}" class="btn px-3 ml-md-2 btn-info"><i class="fa fa-ticket-alt"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <a href="{{ route('web.organizer.index') }}" class="btn btn-primary">Volver</a>
                </div>
            </div>
        </div>
    </div>
@endsection
