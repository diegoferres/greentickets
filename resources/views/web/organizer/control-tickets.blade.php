@extends('layout.app')
@section('content')
    <div class="organizer">
        <div class="list-event">
            <div class="container">
                <div class="row pb-3 mb-3">
                    <div class="col-12 col-md-12 col-lg-12">
                        <button type="button" id="btnControlCamara" class="btn btn-warning">Control con Camara</button>
                        <button type="button" id="btnControlPistolaQR" class="btn btn-secondary">Control con Pistola QR</button>
                    </div>
                </div>
                <div class="row" id="controlQr">
                    <div class="col-12 col-md-12 col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>Scanear entrada</h4>
                            </div>
                            <div class="card-body">
                                <div class="form-row">
                                    <div id="container">
                                        <h1>QR Code Scanner</h1>

                                        <a id="btn-scan-qr">
                                            <img src="/assets/img/qr_icon.svg">
                                        </a>

                                        <canvas hidden="" id="qr-canvas" style="width: 100%"></canvas>

                                        <div id="qr-result" hidden="">
                                            <b>Data:</b> <span id="outputData"></span>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="card-footer">
                                <a href="{{ route('web.organizer.event.show',[$event->id]) }}" class="btn btn-primary">Volver</a>

                            </div>

                        </div>
                    </div>
                </div>

                <div class="row" style="display: none" id="controlPistola">
                    <div class="col-12 col-md-12 col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>Scanear entrada</h4>
                            </div>
                            <div class="card-body">
                                <div class="form-row">
                                    <label for="hash">Hash</label>
                                    <input type="text" name="hash" id="inputControlPistola" class="form-control" >
                                </div>
                            </div>

                            <div class="card-footer">
                                <a href="{{ route('web.organizer.event.show',[$event->id]) }}" class="btn btn-primary">Volver</a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src=" {{asset('assets/js/jsqrcode/qr_packed.js')}}"></script>
    <script>
        $(document).ready(function () {

            let url = '{!! url('check/tikect/hash') !!}';

            $('#btnControlCamara').on('click',function(){
                console.log("evento onclick de btnControlcamara");
                $('#controlPistola').hide();
                $('#controlQr').show();
            });

            $('#btnControlPistolaQR').on('click',function(){
                console.log("evento onclick de btnControlPistolaQR");
                $('#controlQr').hide();
                $('#controlPistola').show();
                $('#inputControlPistola').focus();
            });

            $( "#inputControlPistola" ).keydown(function() {
                let code = $(this).val();
                console.log("el tamaño del codigo");
                console.log(code.length);
                if(code.length >= 60)
                {
                    $.get( url + '/' +  code , function(data) {
                        console.dir(data);
                        if (data.status)
                        {
                            $('#inputControlPistola').val('').focus();
                            alert('Entrada habilitada');
                        }else
                        {
                            $('#inputControlPistola').val('').focus();
                            alert('Entrada no habilitada');
                        }
                    }).fail(function() {
                        $('#inputControlPistola').val('').focus();
                        alert( "error" );
                    });
                }
            });
        });

        //const qrcode = window.qrcode;

        const video = document.createElement("video");
        const canvasElement = document.getElementById("qr-canvas");
        const canvas = canvasElement.getContext("2d");

        const qrResult = document.getElementById("qr-result");
        const outputData = document.getElementById("outputData");
        const btnScanQR = document.getElementById("btn-scan-qr");
        const ruta = '{!! url('check/tikect/hash') !!}';
        console.log(ruta);

        let scanning = false;

        qrcode.callback = res => {
            if (res) {
                console.log("En el callback del qrcode");
                console.dir(res);

                $.get( ruta + '/' +  res , function(data) {
                    console.dir(data);
                    if (data.status)
                    {
                        alert('Entrada habilitada');
                    }else
                    {
                        alert('Entrada no habilitada');
                    }
                }).fail(function() {
                    alert( "error" );
                });


                outputData.innerText = res;
                scanning = false;

                video.srcObject.getTracks().forEach(track => {
                    track.stop();
                });

                qrResult.hidden = false;
                canvasElement.hidden = true;
                btnScanQR.hidden = false;
            }
        };

        btnScanQR.onclick = () => {
            navigator.mediaDevices
                .getUserMedia({ video: { facingMode: "environment" } })
                .then(function(stream) {
                    scanning = true;
                    qrResult.hidden = true;
                    btnScanQR.hidden = true;
                    canvasElement.hidden = false;
                    video.setAttribute("playsinline", true); // required to tell iOS safari we don't want fullscreen
                    video.srcObject = stream;
                    video.play();
                    tick();
                    scan();
                });
        };

        function tick() {
            canvasElement.height = video.videoHeight;
            canvasElement.width = video.videoWidth;
            canvas.drawImage(video, 0, 0, canvasElement.width, canvasElement.height);

            scanning && requestAnimationFrame(tick);
        }

        function scan() {
            try {
                qrcode.decode();
            } catch (e) {
                setTimeout(scan, 300);
            }
        }
    </script>
@endsection
