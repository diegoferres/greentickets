@extends('layout.app')
@section('content')
    <div class="organizer">
        <div class="sale">
            <main class="d-flex justify-content-center">
                <div class="col-sm-10 col-md-8 col-xl-6 pt-5">
                    <div class="pt-4 pb-4">
                        <h2 class="mb-4">Editar promoción</h2>
                        <div class="form-group mb-4">
                            <p>Seleccione un evento</p>
                            <select class="custom-select form-control" id="inlineFormCustomSelect">
                                <option selected>Evento 1...</option>
                                <option value="1">Evento 2</option>
                                <option value="2">Evento 3</option>
                                <option value="3">Evento 4</option>
                            </select>
                        </div>
                        <div class="form-group mb-4">
                            <p>Seleccione una entrada</p>
                            <select class="custom-select form-control" id="inlineFormCustomSelect">
                                <option selected>Entrada 1...</option>
                                <option value="1">Entrada 2</option>
                                <option value="2">Entrada 3</option>
                                <option value="3">Entrada 4</option>
                            </select>
                        </div>
                        <div class="form-group mb-4">
                            <p>Seleccione la oferta</p>
                            <select class="custom-select form-control" id="inlineFormCustomSelect">
                                <option selected>Entrada 2x1...</option>
                                <option value="3">Entrada 3x2</option>
                                <option value="1">10% de descuento</option>
                                <option value="2">20% de descuento</option>
                                <option value="3">30% de descuento</option>
                                <option value="3">40% de descuento</option>
                                <option value="3">50% de descuento</option>
                                <option value="3">60% de descuento</option>
                                <option value="3">70% de descuento</option>
                                <option value="3">80% de descuento</option>
                                <option value="3">90% de descuento</option>
                                <option value="3">100% de descuento</option>
                            </select>
                        </div>
                        <div class="form-group mb-4">
                            <p>Cantidad disponible</p>
                            <input type="number" class="form-control" id="exampleInpu" aria-describedby="emailcantidadHelp">
                            <small id="cantidad" class="form-text text-muted">La cantidad no puede superar el total de tu entradas.</small>
                        </div>
                        <div class="row align-items-center">
                            <div class="col-md-6">
                                <div class="sale-cupon pt-4">
                                    <p class="mb-1">Tu cupón</p>
                                    <h2>ADC654</h2>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group float-right pt-4">
                                    <a href="#" class="btn btn-primary">Guardar cambios</a>
                                    <a href="#" class="btn px-3 ml-md-2 btn-danger"><i class="far fa-trash-alt"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    </div>
@endsection