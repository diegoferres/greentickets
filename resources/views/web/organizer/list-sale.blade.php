@extends('layout.app')
@section('content')
    <div class="organizer">
        <div class="sale">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="pt-md-5 pb-4 d-md-flex justify-content-md-between">
                            <h3>Promociones activas</h3>
                            <a href="{{ route('create-sale') }}" class="btn btn-primary">Crear promoción</a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card-sale">
                            <div class="header-sale">
                                <img src="{{asset('/img/cart.jpg')}}" alt="Imagen del evento" class="img-fluid">
                                <h5 class="mb-md-0 text-left">Nombre del evento</h5>
                            </div>
                            <div class="body-sale">
                                <small>Tipo</small>
                                <h5 class="mb-3">Entrada 2x1</h5>
                                <small>Entrada</small>
                                <h5 class="mb-3">Basica</h5>
                                <small>Cupón</small>
                                <h5 class="mb-3">ADC456</h5>
                                <small>Cantidad canjeada</small>
                                <h5>500/1000</h5>
                                <div class="progress">
                                    <div class="progress-bar bg-primary" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <small class="float-right text-muted pt-1">disponible el 50%</small>
                            </div>
                            <div class="footer-sale">
                                <a href="{{ route('detail-sale') }}" class="btn btn-tertiary w-75">Editar</a>
                                <a href="#" class="btn px-3 ml-2 btn-danger"><i class="far fa-trash-alt"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card-sale">
                            <div class="header-sale">
                                <img src="{{asset('/img/cart.jpg')}}" alt="Imagen del evento" class="img-fluid">
                                <h5 class="mb-md-0 text-left">Nombre del evento</h5>
                            </div>
                            <div class="body-sale">
                                <small>Tipo</small>
                                <h5 class="mb-3">Entrada 2x1</h5>
                                <small>Entrada</small>
                                <h5 class="mb-3">Basica</h5>
                                <small>Cupón</small>
                                <h5 class="mb-3">ADC456</h5>
                                <small>Cantidad canjeada</small>
                                <h5>500/1000</h5>
                                <div class="progress">
                                    <div class="progress-bar bg-primary" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <small class="float-right text-muted pt-1">disponible el 50%</small>
                            </div>
                            <div class="footer-sale">
                                <a href="{{ route('detail-sale') }}" class="btn btn-tertiary w-75">Editar</a>
                                <a href="#" class="btn px-3 ml-2 btn-danger"><i class="far fa-trash-alt"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card-sale">
                            <div class="header-sale">
                                <img src="{{asset('/img/cart.jpg')}}" alt="Imagen del evento" class="img-fluid">
                                <h5 class="mb-md-0 text-left">Nombre del evento</h5>
                            </div>
                            <div class="body-sale">
                                <small>Tipo</small>
                                <h5 class="mb-3">Entrada 2x1</h5>
                                <small>Entrada</small>
                                <h5 class="mb-3">Basica</h5>
                                <small>Cupón</small>
                                <h5 class="mb-3">ADC456</h5>
                                <small>Cantidad canjeada</small>
                                <h5>500/1000</h5>
                                <div class="progress">
                                    <div class="progress-bar bg-primary" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <small class="float-right text-muted pt-1">disponible el 50%</small>
                            </div>
                            <div class="footer-sale">
                                <a href="{{ route('detail-sale') }}" class="btn btn-tertiary w-75">Editar</a>
                                <a href="#" class="btn px-3 ml-2 btn-danger"><i class="far fa-trash-alt"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card-sale">
                            <div class="header-sale">
                                <img src="{{asset('/img/cart.jpg')}}" alt="Imagen del evento" class="img-fluid">
                                <h5 class="mb-md-0 text-left">Nombre del evento</h5>
                            </div>
                            <div class="body-sale">
                                <small>Tipo</small>
                                <h5 class="mb-3">Entrada 2x1</h5>
                                <small>Entrada</small>
                                <h5 class="mb-3">Basica</h5>
                                <small>Cupón</small>
                                <h5 class="mb-3">ADC456</h5>
                                <small>Cantidad canjeada</small>
                                <h5>500/1000</h5>
                                <div class="progress">
                                    <div class="progress-bar bg-primary" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <small class="float-right text-muted pt-1">disponible el 50%</small>
                            </div>
                            <div class="footer-sale">
                                <a href="{{ route('detail-sale') }}" class="btn btn-tertiary w-75">Editar</a>
                                <a href="#" class="btn px-3 ml-2 btn-danger"><i class="far fa-trash-alt"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card-sale">
                            <div class="header-sale">
                                <img src="{{asset('/img/cart.jpg')}}" alt="Imagen del evento" class="img-fluid">
                                <h5 class="mb-md-0 text-left">Nombre del evento</h5>
                            </div>
                            <div class="body-sale">
                                <small>Tipo</small>
                                <h5 class="mb-3">Entrada 2x1</h5>
                                <small>Entrada</small>
                                <h5 class="mb-3">Basica</h5>
                                <small>Cupón</small>
                                <h5 class="mb-3">ADC456</h5>
                                <small>Cantidad canjeada</small>
                                <h5>500/1000</h5>
                                <div class="progress">
                                    <div class="progress-bar bg-primary" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <small class="float-right text-muted pt-1">disponible el 50%</small>
                            </div>
                            <div class="footer-sale">
                                <a href="{{ route('detail-sale') }}" class="btn btn-tertiary w-75">Editar</a>
                                <a href="#" class="btn px-3 ml-2 btn-danger"><i class="far fa-trash-alt"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card-sale">
                            <div class="header-sale">
                                <img src="{{asset('/img/cart.jpg')}}" alt="Imagen del evento" class="img-fluid">
                                <h5 class="mb-md-0 text-left">Nombre del evento</h5>
                            </div>
                            <div class="body-sale">
                                <small>Tipo</small>
                                <h5 class="mb-3">Entrada 2x1</h5>
                                <small>Entrada</small>
                                <h5 class="mb-3">Basica</h5>
                                <small>Cupón</small>
                                <h5 class="mb-3">ADC456</h5>
                                <small>Cantidad canjeada</small>
                                <h5>500/1000</h5>
                                <div class="progress">
                                    <div class="progress-bar bg-primary" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <small class="float-right text-muted pt-1">disponible el 50%</small>
                            </div>
                            <div class="footer-sale">
                                <a href="{{ route('detail-sale') }}" class="btn btn-tertiary w-75">Editar</a>
                                <a href="#" class="btn px-3 ml-2 btn-danger"><i class="far fa-trash-alt"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card-sale">
                            <div class="header-sale">
                                <img src="{{asset('/img/cart.jpg')}}" alt="Imagen del evento" class="img-fluid">
                                <h5 class="mb-md-0 text-left">Nombre del evento</h5>
                            </div>
                            <div class="body-sale">
                                <small>Tipo</small>
                                <h5 class="mb-3">Entrada 2x1</h5>
                                <small>Entrada</small>
                                <h5 class="mb-3">Basica</h5>
                                <small>Cupón</small>
                                <h5 class="mb-3">ADC456</h5>
                                <small>Cantidad canjeada</small>
                                <h5>500/1000</h5>
                                <div class="progress">
                                    <div class="progress-bar bg-primary" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <small class="float-right text-muted pt-1">disponible el 50%</small>
                            </div>
                            <div class="footer-sale">
                                <a href="{{ route('detail-sale') }}" class="btn btn-tertiary w-75">Editar</a>
                                <a href="#" class="btn px-3 ml-2 btn-danger"><i class="far fa-trash-alt"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card-sale">
                            <div class="header-sale">
                                <img src="{{asset('/img/cart.jpg')}}" alt="Imagen del evento" class="img-fluid">
                                <h5 class="mb-md-0 text-left">Nombre del evento</h5>
                            </div>
                            <div class="body-sale">
                                <small>Tipo</small>
                                <h5 class="mb-3">Entrada 2x1</h5>
                                <small>Entrada</small>
                                <h5 class="mb-3">Basica</h5>
                                <small>Cupón</small>
                                <h5 class="mb-3">ADC456</h5>
                                <small>Cantidad canjeada</small>
                                <h5>500/1000</h5>
                                <div class="progress">
                                    <div class="progress-bar bg-primary" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <small class="float-right text-muted pt-1">disponible el 50%</small>
                            </div>
                            <div class="footer-sale">
                                <a href="{{ route('detail-sale') }}" class="btn btn-tertiary w-75">Editar</a>
                                <a href="#" class="btn px-3 ml-2 btn-danger"><i class="far fa-trash-alt"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card-sale">
                            <div class="header-sale">
                                <img src="{{asset('/img/cart.jpg')}}" alt="Imagen del evento" class="img-fluid">
                                <h5 class="mb-md-0 text-left">Nombre del evento</h5>
                            </div>
                            <div class="body-sale">
                                <small>Tipo</small>
                                <h5 class="mb-3">Entrada 2x1</h5>
                                <small>Entrada</small>
                                <h5 class="mb-3">Basica</h5>
                                <small>Cupón</small>
                                <h5 class="mb-3">ADC456</h5>
                                <small>Cantidad canjeada</small>
                                <h5>500/1000</h5>
                                <div class="progress">
                                    <div class="progress-bar bg-primary" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <small class="float-right text-muted pt-1">disponible el 50%</small>
                            </div>
                            <div class="footer-sale">
                                <a href="{{ route('detail-sale') }}" class="btn btn-tertiary w-75">Editar</a>
                                <a href="#" class="btn px-3 ml-2 btn-danger"><i class="far fa-trash-alt"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card-sale">
                            <div class="header-sale">
                                <img src="{{asset('/img/cart.jpg')}}" alt="Imagen del evento" class="img-fluid">
                                <h5 class="mb-md-0 text-left">Nombre del evento</h5>
                            </div>
                            <div class="body-sale">
                                <small>Tipo</small>
                                <h5 class="mb-3">Entrada 2x1</h5>
                                <small>Entrada</small>
                                <h5 class="mb-3">Basica</h5>
                                <small>Cupón</small>
                                <h5 class="mb-3">ADC456</h5>
                                <small>Cantidad canjeada</small>
                                <h5>500/1000</h5>
                                <div class="progress">
                                    <div class="progress-bar bg-primary" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <small class="float-right text-muted pt-1">disponible el 50%</small>
                            </div>
                            <div class="footer-sale">
                                <a href="{{ route('detail-sale') }}" class="btn btn-tertiary w-75">Editar</a>
                                <a href="#" class="btn px-3 ml-2 btn-danger"><i class="far fa-trash-alt"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card-sale">
                            <div class="header-sale">
                                <img src="{{asset('/img/cart.jpg')}}" alt="Imagen del evento" class="img-fluid">
                                <h5 class="mb-md-0 text-left">Nombre del evento</h5>
                            </div>
                            <div class="body-sale">
                                <small>Tipo</small>
                                <h5 class="mb-3">Entrada 2x1</h5>
                                <small>Entrada</small>
                                <h5 class="mb-3">Basica</h5>
                                <small>Cupón</small>
                                <h5 class="mb-3">ADC456</h5>
                                <small>Cantidad canjeada</small>
                                <h5>500/1000</h5>
                                <div class="progress">
                                    <div class="progress-bar bg-primary" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <small class="float-right text-muted pt-1">disponible el 50%</small>
                            </div>
                            <div class="footer-sale">
                                <a href="{{ route('detail-sale') }}" class="btn btn-tertiary w-75">Editar</a>
                                <a href="#" class="btn px-3 ml-2 btn-danger"><i class="far fa-trash-alt"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card-sale">
                            <div class="header-sale">
                                <img src="{{asset('/img/cart.jpg')}}" alt="Imagen del evento" class="img-fluid">
                                <h5 class="mb-md-0 text-left">Nombre del evento</h5>
                            </div>
                            <div class="body-sale">
                                <small>Tipo</small>
                                <h5 class="mb-3">Entrada 2x1</h5>
                                <small>Entrada</small>
                                <h5 class="mb-3">Basica</h5>
                                <small>Cupón</small>
                                <h5 class="mb-3">ADC456</h5>
                                <small>Cantidad canjeada</small>
                                <h5>500/1000</h5>
                                <div class="progress">
                                    <div class="progress-bar bg-primary" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <small class="float-right text-muted pt-1">disponible el 50%</small>
                            </div>
                            <div class="footer-sale">
                                <a href="{{ route('detail-sale') }}" class="btn btn-tertiary w-75">Editar</a>
                                <a href="#" class="btn px-3 ml-2 btn-danger"><i class="far fa-trash-alt"></i></a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-12 pt-4 text-center">
                        <nav class="d-inline-block float-right pt-2" aria-label="...">
                            <ul class="pagination">
                                <li class="page-item disabled">
                                    <span class="page-link">Anterior</span>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item active" aria-current="page">
                                    <span class="page-link">
                                    2
                                    <span class="sr-only">(current)</span>
                                    </span>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#">Siguiente</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection