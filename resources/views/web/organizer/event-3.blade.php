<div class="pt-4 pb-4">
    <h2 class="mb-4">Datos y entradas del evento</h2>
    {!! Form::open(['route' => 'web.event.step4', 'method' => 'post']) !!}
    {{-- <div class="form-group pt-3 mb-4">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="public" name="event-type" value="public">
            <label for="public" class="custom-control-label">Publico</label>
        </div>
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="private" name="event-type" value="private">
            <label for="private" class="custom-control-label">Privado</label>
        </div>
    </div> --}}
    <div class="row mb-4">
        <div class="col-md-6">
            <div class="form-group">
                <h5 class="mb-2 py-2">Tipo de entrada</h5>
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" class="custom-control-input" id="free" name="event-type" value="free">
                    <label for="free" class="custom-control-label">Gratis</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" class="custom-control-input" id="pago" name="event-type" value="pago">
                    <label for="pago" class="custom-control-label">Pago</label>
                </div>
            </div>
        </div>
        {{--<div class="col-md-6">
            <div class="form-group">
                <h5 class="mb-2 py-2">Formato del evento</h5>
                <div class="custom-control custom-checkbox custom-control-inline">
                    <input type="checkbox" class="custom-control-input" id="checkOnline">
                    <label class="custom-control-label" for="checkOnline">Online</label>
                </div>
                <div class="custom-control custom-checkbox custom-control-inline">
                    <input type="checkbox" class="custom-control-input" id="checkPresencial">
                    <label class="custom-control-label" for="checkPresencial">Presencial</label>
                </div>
            </div>
        </div>--}}
    </div>
    <div class="form-group pt-4 pb-5" id="slot">
        @if (isset(session('step3')['ticket_name']))
            @foreach(session('step3')['ticket_name'] as $key => $ticket)
                <div class="row" style="padding-bottom: 20px">
                    <div class="col-md-4 mt-4 mt-lg-0">
                        <h6>Tipo de Entrada</h6>
                        <input id="ticket_name" name="ticket_name[]"
                               value="{{ session('step3')['ticket_name'][$key] }}" type="text" required=""
                               class="form-control" placeholder="Ej.: VIP">
                    </div>
                    <div class="col-md-4 mt-4 mt-lg-0">
                        <h6>Precio por Entrada</h6>
                        <input id="ticket_price" name="ticket_price[]"
                               value="{{ session('step3')['ticket_price'][$key] }}" type="number"
                               required="" class="form-control" placeholder="Ej.: 250000">
                    </div>
                    <div class="col-md-4 mt-4 mt-lg-0">
                        <h6>Cantidad disponible</h6>
                        <input id="ticket_count" name="ticket_count[]"
                               value="{{ session('step3')['ticket_count'][$key] }}" type="number"
                               required="" class="form-control" placeholder="Ej.: 200">
                    </div>
                    <div class="col-md-12 mt-4 mt-lg-0">
                        <h6>Gratis</h6>
                        <input type="checkbox" checked data-toggle="toggle">
                    </div>
                </div>
            @endforeach
            <a id="add" class="btn btn-tertiary mt-3" href="javascript:add_slot()">Añadir otra entrada</a>
            <a style="display: {{ $key == 0 ? 'none' : '' }}" id="delete" class="btn btn-tertiary mt-3"
               href="javascript:delete_slot()">Eliminar una entrada</a>
        @else
            <div class="row" style="padding-bottom: 20px">
                <div class="col-md-4 mt-4 mt-lg-0">
                    <h6>Tipo de Entrada</h6>
                    <input id="ticket_name" name="ticket_name[]" type="text" required="" autofocus=""
                           class="form-control" placeholder="Ej.: VIP">
                </div>
                <div class="col-md-4 mt-4 mt-lg-0">
                    <h6>Precio por Entrada</h6>
                    <input id="ticket_price" name="ticket_price[]" type="number" required="" autofocus=""
                           class="form-control" placeholder="Ej.: 250000">
                </div>
                <div class="col-md-4 mt-4 mt-lg-0">
                    <h6>Cantidad disponible</h6>
                    <input id="ticket_count" name="ticket_count[]" type="number" required="" autofocus=""
                           class="form-control" placeholder="Ej.: 200">
                </div>
            </div>
            <a id="add" class="btn btn-tertiary mt-3" href="javascript:add_slot()">Añadir otra entrada</a>
            <a style="display: none" id="delete" class="btn btn-tertiary mt-3"
               href="javascript:delete_slot()">Eliminar una entrada</a>
        @endif
    </div>
    {!! Form::close() !!}
</div>