@extends('layout.app')
@section('content')
    <div class="organizer">
        <div class="list-event">
            <div class="container">
                <div class="pt-4 pb-5">
                    <div class="row align-items-center">
                        <div class="col-lg-2">
                            <img src="{{asset('storage/'.$event->covers->first()->url.$event->covers->first()->file_name)}}" alt="{{ $event->event_name }}" class="img-fluid">
                        </div>
                        <div class="col-lg-7 my-4 mb-lg-0">
                            <h1 class="px-lg-3">{{ $event->event_name }}</h1>
                        </div>
                        <div class="col-lg-3 flex-row flex-lg-column">
                            <a href="{{ route('web.organizer.event.edit', $event->id) }}" class="btn btn-primary btn-block mb-2">Editar</a>
                            <a href="#" class="btn btn-danger btn-block">Eliminar</a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive-md pt-4">
                            <table class="table status">
                                <thead class="thead-event">
                                    <tr>
                                        <th scope="col">Status</th>
                                        <th scope="col">Sector</th>
                                        <th scope="col">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($details->tickets as $ticket)
                                    <tr>
                                        <td><h5>{{ $ticket->status->status }}</h5></td>
                                        <td><p class="mb-0">{{ $details->sector->sector_name }}</p></td>
                                        <td>
                                            <a href="#" class="btn px-3 ml-md-2 btn-info"><i class="fa fa-ticket-alt"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <a href="{{ route('web.organizer.event.show',$event->id) }}" class="btn btn-primary">Volver</a>
                </div>
            </div>
        </div>
    </div>
@endsection
