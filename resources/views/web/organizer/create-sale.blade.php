@extends('layout.app')
@section('content')
    <div class="organizer">
        <div class="sale">
            <main class="d-flex justify-content-center">
                <div class="col-sm-10 col-md-8 col-xl-6 pt-5">
                    <div class="pt-4 pb-4">
                        <h2 class="mb-4">Crear una promoción</h2>
                        <div class="form-group mb-4">
                            <p>Tipo de promoción</p>
                            <select class="custom-select form-control" id="inlineFormCustomSelect">
                                <option selected>Descuento</option>
                                <option value="1">Cupón</option>
                            </select>
                        </div>
                        <div class="form-group mb-4">
                            <p>Evento</p>
                            <select class="custom-select form-control" id="inlineFormCustomSelect">
                                <option selected>Evento 1...</option>
                                <option value="1">Evento 2</option>
                            </select>
                        </div>
                        <div class="form-group mb-4">
                            <p>Seleccione una entrada</p>
                            <select class="custom-select form-control" id="inlineFormCustomSelect">
                                <option selected>Entrada 1...</option>
                                <option value="1">Entrada 2</option>
                                <option value="2">Entrada 3</option>
                                <option value="3">Entrada 4</option>
                            </select>
                        </div>
                        <div class="form-group mb-4">
                            <p>Inicio validez</p>
                            <input type="date" class="form-control" id="exampleInpu">
                        </div>
                        <div class="form-group mb-4">
                            <p>Fin validez</p>
                            <input type="date" class="form-control" id="exampleInpu">
                        </div>
                        <div class="form-group mb-4">
                            <p>Tipo de descuento</p>
                            <select class="custom-select form-control" id="inlineFormCustomSelect">
                                <option selected>Monto fijo</option>
                                <option selected>Porcentaje</option>
                            </select>
                        </div>
                        <div class="form-group mb-4">
                            <p>Monto / Porcentaje</p>
                            <input type="number" class="form-control" id="exampleInpu" aria-describedby="emailcantidadHelp">
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio"
                                   class="custom-control-input @error('event_format') is-invalid @enderror"
                                   id="1" name="event_format" value="1"
                                   @if (!isset(session('step2')['event_format']))
                                   checked
                                   @endif
                                   @if (isset(session('step2')['event_format']) && session('step2')['event_format'] == 1)checked
                                @endif>
                            <label for="1" class="custom-control-label">Única aplicación <a href="" data-toggle="tooltip" data-placement="right" title="Descripción">(?)</a></label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio"
                                   class="custom-control-input @error('event_format') is-invalid @enderror"
                                   id="2" name="event_format" value="2"
                                   @if (isset(session('step2')['event_format']) && session('step2')['event_format'] == 2)
                                   checked
                                @endif>
                            <label for="2" class="custom-control-label">Múltiple aplicación <a href="" data-toggle="tooltip" data-placement="right" title="Descripción">(?)</a></label>
                        </div>
                        <div class="form-group mb-4 mt-4">
                            <p>Cantidad disponible</p>
                            <input type="number" class="form-control" id="exampleInpu" aria-describedby="emailcantidadHelp">
                            <small id="cantidad" class="form-text text-muted">La cantidad no puede superar el total de tu entradas.</small>
                        </div>
                        <div class="row align-items-center">
                            <div class="col-md-6">
                                <div class="sale-cupon pt-4">
                                    <p class="mb-1">Tu cupón se ha generado</p>
                                    <h2>ADC654</h2>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group float-right pt-4">
                                    <a href="#" class="btn btn-primary">Guardar cambios</a>
                                    <a href="#" class="btn px-3 ml-md-2 btn-danger"><i class="far fa-trash-alt"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    </div>
@endsection

