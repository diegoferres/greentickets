<div class="pt-4 pb-5">
    <h2 class="mb-4">Portada y Descripción del Evento</h2>
    {!! Form::open(['route' => 'web.event.save', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
        <div class="form-group pb-5">
            <h6 class="pt-4">Añade una portada</h6>
            <input type="file" name="event_cover" class="form-control mb-4" id="event_cover">
            {!! Form::textarea('event_description', old('event_description'), ['class' => 'form-control', 'id' => 'summernote']) !!}
            <span id="total-characters">0 de 30000 @error('event_description') <b
                        style="color: red;">({{ $message }})</b>@enderror</span>
            {{--<textarea id="event_description" name="event_description" placeholder="Descripción" required class="form-control" rows="5"></textarea>--}}
        </div>
        <div class="form-group float-right">
            <a href="#" class="btn btn-primary">Guardar cambios</a>
            <a href="#" class="btn px-3 ml-md-2 btn-danger"><i class="far fa-trash-alt"></i></a>
        </div>
    {!! Form::close() !!}
</div>