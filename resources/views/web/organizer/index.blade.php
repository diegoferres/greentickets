@extends('layout.app')

@section('content')

    <div class="organizer">
        <div class="container">
            <div class="row pt-5">
                <div class="col-12 mb-3">
                    <h2>Perfil Organizador</h2>
                    <p>Las estadisticas son del mes. Para tener muchos mas datos, vuelvete <a
                            href="#"><strong>premium</strong></a></p>
                </div>
                <div class="col-md-4">
                    <div class="status">
                        <p class="mb-2">Eventos publicados</p>
                        <h2 class="text-primary">{{ $events->count() }}</h2>
                        <small>Datos hasta el {{ date('d/m/Y') }}</small>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="status">
                        <p class="mb-2">Total de entradas vendidas</p>
                        <h2 class="text-primary">0</h2>
                        <small>Datos hasta el {{ date('d/m/Y') }}</small>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="status">
                        <p class="mb-2">Ingreso estimado</p>
                        <h2 class="text-primary">Gs. 0</h2>
                        <small>Datos hasta el {{ date('d/m/Y') }}</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="list-event">
            <div class="container">
                <div class="col-12">
                    <h3 class="pt-md-5">Eventos publicados</h3>
                    <div class="table-responsive-md pt-4">
                        <table class="table status">
                            {{--<caption>
                                <small class="d-inline-block pt-3">Total de eventos: 3</small>
                                <nav class="d-inline-block float-right pt-2" aria-label="...">
                                    <ul class="pagination">
                                        <li class="page-item disabled">
                                            <span class="page-link">Anterior</span>
                                        </li>
                                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                                        <li class="page-item active" aria-current="page">
                                            <span class="page-link">
                                            2
                                            <span class="sr-only">(current)</span>
                                            </span>
                                        </li>
                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                        <li class="page-item">
                                            <a class="page-link" href="#">Siguiente</a>
                                        </li>
                                    </ul>
                                </nav>
                            </caption>--}}
                            <thead class="thead-event">
                            <tr>
                                <th colspan="2" scope="col">Evento</th>
                                <th scope="col">Fecha</th>
                                <th scope="col">Stock</th>
                                <th scope="col">Estado</th>
                                <th scope="col">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($events as $event)

                                <tr>
                                    <td class="avatar-event"><img src="{{asset('/img/cart.jpg')}}"
                                                                  alt="Imagen del evento" class="img-fluid"></td>
                                    <td><h5 class="mb-md-0 text-left">{{ $event->event_name }}</h5></td>
                                    <td><p class="mb-0">24/07/2020</p></td>
                                    <td class="text-left">
                                        <div class="progress">
                                            <div class="progress-bar bg-primary" role="progressbar" style="width: 25%"
                                                 aria-valuenow="2" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                        <small>vendidos el 25%</small>
                                    </td>
                                    <td><p class="mb-0 text-primary">Activado</p></td>
                                    <td>
                                        <a href="{{ route('web.organizer.event.edit', $event->id) }}"
                                           class="btn btn-tertiary">Editar</a>
                                        <a href="{{ route('web.organizer.event.show', $event->id) }}"
                                           class="btn px-3 ml-md-2 btn-tertiary"><i class="fas fa-search"></i></a>
                                        {{--<a href="#" class="btn px-3 ml-md-2 btn-danger"><i class="far fa-trash-alt"></i></a>--}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="sale">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="pt-md-5 pb-4 d-md-flex justify-content-md-between">
                            <h3>Promociones activas</h3>
                            <a href="{{ route('web.organizer.promotions.create') }}" class="btn btn-primary">Crear
                                promoción</a>
                        </div>
                    </div>


                    <div class="col-12 pt-4 text-center">
                        <a href="{{ route('list-sale') }}">Ver todas las promociones</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
