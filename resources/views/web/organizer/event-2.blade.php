
<div class="pt-4 pb-4 mb-0">
    @if (isset(session('step2')['event_format']) && session('step2')['event_format'] == 1 || session('step2')['event_format'] == 3)
        <h2 class="mb-4">¿Cómo, cuándo y donde será el Evento?</h2>
    @else
        <h2 class="mb-4">¿Cómo y cuándo será el Evento?</h2>
    @endif
    {!! Form::open(['route' => 'web.event.step3', 'method' => 'post']) !!}
    <div class="row mb-4">
        <div class="col-md-12">
            <div class="form-group">
                <h5 class="mb-2 py-2">Formato de evento</h5>

                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio"
                        class="custom-control-input @error('event_format') is-invalid @enderror"
                        id="1" name="event_format" value="1"
                        @if (!isset(session('step2')['event_format']))
                        checked
                        @endif
                        @if (isset(session('step2')['event_format']) && session('step2')['event_format'] == 1)checked
                            @endif>
                    <label for="1" class="custom-control-label">Presencial</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio"
                        class="custom-control-input @error('event_format') is-invalid @enderror"
                        id="2" name="event_format" value="2"
                        @if (isset(session('step2')['event_format']) && session('step2')['event_format'] == 2)
                        checked
                            @endif>
                    <label for="2" class="custom-control-label">Online</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio"
                        class="custom-control-input @error('event_format') is-invalid @enderror"
                        id="3" name="event_format" value="3"
                        @if (isset(session('step2')['event_format']) && session('step2')['event_format'] == 3)
                        checked
                            @endif>
                    <label for="3" class="custom-control-label">Online/Presencial</label>
                </div>
            </div>
        </div>
    </div>
    <div class="eventVenue" {{isset(session('step2')['event_format']) ? session('step2')['event_format'] == 1 || session('step2')['event_format'] == 3 ? '' : 'style=display:none' : '' }}>
        <div class="form-group mb-4">
            <input id="venue_name" name="venue_name"
                value="{{ isset(session('step2')['venue_name']) ? session('step2')['venue_name'] : old('venue_name') }}"
                type="text"
                placeholder="Nombre del lugar" autofocus=""
                class="form-control @error('venue_name') is-invalid @enderror">
            @error('venue_name')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group mb-4">
            <input id="venue_address" name="venue_address"
                value="{{ isset(session('step2')['venue_address']) ? session('step2')['venue_address'] : '' }}"
                type="text" autofocus=""
                class="form-control @error('venue_address') is-invalid @enderror"
                placeholder="Dirección del lugar">
            @error('venue_address')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
    </div>
    <div class="form-group pb-5" id="slot">
        @if (isset(session('step2')['event_date']))
            @foreach(session('step2')['event_date'] as $key => $ticket)
                <div class="row" style="padding-bottom: 20px">
                    <div class="col-12 col-lg-4">
                        <h6>Fecha</h6>
                        <input id="date" name="event_date[]"
                            value="{{ isset(session('step2')['event_date'][$key]) ? session('step2')['event_date'][$key] : '' }}"
                            type="date" autofocus=""
                            class="form-control pl-lg-4 pr-lg-1 @error('event_date.'.$key) is-invalid @enderror">
                        @error('event_date.'.$key)
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="col-6 col-lg-4 mt-4 mt-lg-0">
                        <h6>Hora de Inicio</h6>
                        <input id="hora-inicio" name="event_time_start[]"
                            value="{{ isset(session('step2')['event_time_start'][$key]) ? session('step2')['event_time_start'][$key] : '' }}"
                            type="time" autofocus=""
                            class="form-control @error('event_time_start.'.$key) is-invalid @enderror"
                            placeholder="Hora">
                        @error('event_time_start.'.$key)
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="col-6 col-lg-4 mt-4 mt-lg-0">
                        <h6>Hora de Termino</h6>
                        <input id="hora-final" name="event_time_end[]"
                            value="{{ isset(session('step2')['event_time_end'][$key]) ? session('step2')['event_time_end'][$key] : '' }}"
                            type="time" autofocus=""
                            class="form-control @error('event_time_end.'.$key) is-invalid @enderror"
                            placeholder="Hora">
                        @error('event_time_end.'.$key)
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>
                @if($loop->last)
                    <a id="add" class="btn btn-tertiary mt-3" href="javascript:add_slot()">Añadir otra
                        fecha</a>
                    @if ($key > 0)
                        <a id="delete" class="btn btn-tertiary mt-3" href="javascript:delete_slot()">Eliminar
                            una fecha</a>
                    @else
                        <a style="display: none" id="delete" class="btn btn-tertiary mt-3"
                        href="javascript:delete_slot()">Eliminar una fecha</a>
                    @endif
                @endif
            @endforeach
        @else
            <div class="row" style="padding-bottom: 20px">
                <div class="col-12 col-lg-4">
                    <h6>Fecha</h6>
                    <input id="date" name="event_date[]"
                        type="date" autofocus=""
                        class="form-control pl-lg-4 pr-lg-1 @error('event_date.*') is-invalid @enderror">
                    @error('event_date.*')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="col-md-6 col-lg-4 mt-4 mt-lg-0">
                    <h6>Hora de Inicio</h6>
                    <input id="hora-inicio" name="event_time_start[]"
                        type="time" autofocus=""
                        class="form-control @error('event_time_start.*') is-invalid @enderror"
                        placeholder="Hora">
                    @error('event_time_start.*')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="col-md-6 col-lg-4 mt-4 mt-lg-0">
                    <h6>Hora de Termino</h6>
                    <input id="hora-final" name="event_time_end[]"
                        type="time" autofocus=""
                        class="form-control @error('event_time_end.*') is-invalid @enderror"
                        placeholder="Hora">
                    @error('event_time_end.*')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
            </div>
            <a id="add" class="btn btn-tertiary mt-3" href="javascript:add_slot()">Añadir otra fecha</a>
            <a style="display: none" id="delete" class="btn btn-tertiary mt-3"
            href="javascript:delete_slot()">Eliminar una fecha</a>
        @endif
        {{--<div class="row" style="padding-bottom: 20px">
            <div class="col-12 col-lg-4">
                <h6>Fecha</h6>
                <input id="date" name="event_date[]"
                    value="{{ isset(session('step2')['event_date']) ? session('step2')['event_date'] : '' }}"
                    type="date" required="" autofocus="" class="form-control pl-lg-4 pr-lg-1">
            </div>
            <div class="col-6 col-lg-4 mt-4 mt-lg-0">
                <h6>Hora de Inicio</h6>
                <input id="hora-inicio" name="event_time_start[]"
                    value="{{ isset(session('step2')['event_time_start']) ? session('step2')['event_time_start'] : '' }}"
                    type="time" required="" autofocus="" class="form-control" placeholder="Hora">
            </div>
            <div class="col-6 col-lg-4 mt-4 mt-lg-0">
                <h6>Hora de Termino</h6>
                <input id="hora-final" name="event_time_end[]"
                    value="{{ isset(session('step2')['event_time_end']) ? session('step2')['event_time_end'] : '' }}"
                    type="time" required="" autofocus="" class="form-control" placeholder="Hora">
            </div>
        </div>--}}
        {{--<a id="add" class="btn btn-tertiary mt-3" href="javascript:add_slot()">Añadir otra fecha</a>
        <a style="display: none" id="delete" class="btn btn-tertiary mt-3" href="javascript:delete_slot()">Eliminar una fecha</a>--}}
    </div>
    {!! Form::close() !!}
</div>