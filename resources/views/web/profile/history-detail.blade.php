@extends('layout.app')

@section('content')

    <section class="profile">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('web.user.history') }}"><i class="fas fa-arrow-left mr-2"></i>
                        Historial de Compra</a></li>
                <li class="breadcrumb-item active" aria-current="page">Comprobante</li>
            </ol>
        </nav>
        <div class="d-flex justify-content-center">
            <div class="col-lg-8">
                <div class="history cart">
                    <div class="cart-list">
                        <ul>
                            <li class="border-0">
                                <div class="row">
                                    <div class="col-md-6">
                                        <img src="{{asset('/img/logo.svg')}}" alt="Logo" class="py-2 mb-3">
                                    </div>
                                    <div class="col-md-6">
                                        <p><b>N.º de orden:</b> #{{ $order->id }}</p>
                                        <p>
                                            <b>Fecha:</b> @php(setlocale(LC_TIME,"es_ES.UTF-8")){{ strftime("%A, %d de %B", strtotime($order->created_at)) }}
                                        </p>
                                    </div>
                                </div>
                            </li>
                            <li class="border-0 p-3 bg-light">
                                <h6 class="mb-0 d-flex d-md-none">Lista de eventos añadidos</h6>
                                <div class="row align-items-center d-none d-md-flex">
                                    <div class="col-md-5 text-center">
                                        <h6 class="mb-0">Evento</h6>
                                    </div>
                                    <div class="col-md-2 text-center">
                                        <h6 class="mb-0">Entrada</h6>
                                    </div>
                                    <div class="col-md-3 text-center">
                                        <h6 class="mb-0">Precio</h6>
                                    </div>
                                    <div class="col-md-2 text-center">
                                        <h6 class="mb-0">Cantidad</h6>
                                    </div>
                                </div>
                            </li>
                            @foreach ($order->detail as $detail)
                                <li>
                                    <div class="row align-items-center">
                                        <div class="col-md-5">
                                            <div class="row align-items-center">
                                                <div class="col-4">
                                                    <img src="{{asset('storage/'.$detail->sector->event->event_cover )}}" alt="{{ $detail->sector->event->event_name }}"
                                                         class="w-100">
                                                </div>
                                                <div class="col-8">
                                                    <h5 class="mb-md-0">{{ $detail->sector->event->event_name }}</h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 text-md-center">
                                            <h5 class="mt-4 mt-md-0 mb-md-0">{{ $detail->sector->sector_name }}</h5>
                                        </div>
                                        <div class="col-md-3 text-md-center">
                                            {{--<p class="mb-0 price-old d-inline-block d-md-block mr-3 mr-md-0">Gs.
                                                500.000</p>--}}
                                            <span class="mr-3 mr-md-0"><b>Gs. {{ number_format($detail->price, 0, ",", ".") }}</b></span>
                                            {{--<p class="cupon mb-md-0 d-inline-block d-md-block"><i
                                                        class="fas fa-ticket-alt"></i> Cupón ADC05321</p>--}}
                                        </div>
                                        <div class="col-md-2 text-md-center">
                                            <h5 class="mb-md-0">{{ $detail->tickets->count() }}</h5>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                            <li>
                                <div class="row py-3">
                                    <div class="col-md-6">
                                        <h6 class="text-primary">Metodo de Pago</h6>
                                        <h5 class="mb-4">{{ isset($order->payment_method->method) ? $order->payment_method->method : 'Sin medio' }}</h5>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="price">
                                            <div class="d-flex justify-content-between">
                                                <h6 class="pl-md-3 mb-4">Cantidad Total:</h6>
                                                <h5 class="mb-4 pr-md-3">{{ $order->tickets->count() }}</h5>
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <h6 class="pl-md-3 mb-4">Sub Total:</h6>
                                                <h5 class="mb-4 pr-md-3">Gs. {{ number_format($order->subtotal, 0, ",", ".") }}</h5>
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <h6 class="pl-md-3 mb-4">Descuento:</h6>
                                                <h5 class="mb-4 pr-md-3">0%</h5>
                                            </div>
                                            <div class="d-flex justify-content-between bg-light">
                                                <h6 class="p-3 mb-0">Total:</h6>
                                                <h5 class="p-3 mb-0">Gs. {{ number_format($order->subtotal, 0, ",", ".") }}</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="row">
                                    <div class="col-md-5">
                                        <p>Cliente <b>{{ Auth::user()->name }}</b></p>
                                    </div>
                                    <div class="col-md-7">
                                        <p>Ante cualquier consulta, comuniquese a <a href="#">info@entrasinpapel.com</a>
                                        </p>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <a href="{{ route('web.user.history') }}" class="btn btn-primary float-right mt-4">Volver al Historial</a>
            </div>
        </div>
    </section>

@stop
