@php
    $route = Route::currentRouteName();
@endphp

<ul class="nav">
    <li class="pl-md-5 nav-item">
      <a class="nav-link {{ $route == 'web.user.profile' ? 'active' : ''}}" href="{{ route('web.user.profile') }}">Información Personal</a>
    </li>
    <li class="nav-item">
      <a class="nav-link {{ $route == 'web.user.history' ? 'active' : ''}}" href="{{ route('web.user.history') }}">Historial de Compra</a>
    </li>
    <li class="nav-item">
      <a class="nav-link {{ $route == 'web.user.purchased_events' ? 'active' : ''}}" href="{{ route('web.user.purchased_events') }}">Eventos Comprados</a>
    </li>
</ul>