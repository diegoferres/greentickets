@extends('layout.app')

@section('content')
    <div class="profile">

        @include('web.profile.subnav')
        
        <div class="container mb-5">
            <div class="row flex-column-reverse flex-lg-row pt-5">
                <div class="col-lg-6">
                    <h3>Tus tarjetas</h3>
                    <div class="form-control">
                        <div class="tittle-card">
                            <h4 class="text-primary py-2">VISA - PREPAGA - VISION BCO.</h4>
                        </div>
                        <div class="date-card">
                            <div class="d-flex">
                                <p class="mb-1 pr-4">457231******3003</p>
                                <p class="mb-1">Expiración: <strong>12/21</strong></p>
                            </div>
                            <div class="d-flex justify-content-between align-items-center">
                                <p class="mb-0">Tipo de Tarjeta: <strong>Crédito</strong></p>
                                <button type="submit" class="btn btn-tertiary float-right">Eliminar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 pb-5">
                    <h3>Añadir tarjeta</h3>
                    <div class="form-control">
                        <h1>Iframe <br> de <br> Bancard</h1>
                    </div>
                </div>
            </div>
        </div>

    </div>

@stop
