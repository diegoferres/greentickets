@extends('layout.app')

@section('content')
    <div class="profile">

        @include('web.profile.subnav')

        <div class="container mb-5">
            <div class="row align-items-md-center pt-5 pt-md-4">
                <div class="col-md-6 col-lg-8">
                    <h3 class="mb-4 mb-md-0">Eventos Comprados</h3>
                </div>
                <div class="col-md-6 col-lg-4">
                    <select class="custom-select form-control">
                        <option selected>Odenar de la a -z</option>
                        <option value="1">Ordenar</option>
                    </select>
                </div>
            </div>
            {{-- Evento vacio --}}
            {{-- <div class="row pt-5 mt-4">
                <div class="col-12 text-center pb-5">
                    <h3 class="pt-5 mb-4">Aún no has comprado ningun evento</h3>
                    <a href="{{ route('search') }}" class="btn btn-primary">Buscar eventos</a>
                </div>
            </div> --}}
            
            {{-- Eventos cargados --}}
            <div class="row pt-5">
                    @foreach($tickets as $ticket)
                    <div class="col-md-6 col-lg-3">
                        <div class="ticket">
                            <div class="ticket-content">
                                <h3 class="mb-3">{{ $ticket->order_detail->sector->event->event_name }}</h3>
                                <h5 class="text-primary mb-3">{{ $ticket->order_detail->sector->sector_name }}</h5>
                                @foreach($ticket->order_detail->sector->event->dates as $date)
                                    <p>{{ strftime('%d, %B', strtotime($date->event_date)).', '.strftime('%H:%M', strtotime($date->event_time_start)) }} Hs</p>
                                @endforeach
                                @isset($ticket->order_detail->sector->event->venue)
                                    <p>{{ $ticket->order_detail->sector->event->venue->venue_name.', '.$ticket->order_detail->sector->event->venue->venue_address }}</p>
                                @endisset
                                {{--<p>{{ $ticket->order_detail->sector->event->venue->venue_name.', '.$ticket->order_detail->sector->event->venue->venue_address }}</p>--}}
                                {{--<p>Valido para <b class="text-primary">{{ $ticket->quantity }} personas</b></p>--}}
                                {{--<h2 class="text-center pt-4">2505622DC</h2>--}}
                            </div>
                            {{--<div class="ticket-qr">
                                {!! QrCode::size(100)->generate($ticket->id); !!}
                                --}}{{--<img src="{{asset('/img/qr.png')}}" alt="QR" class="qr">--}}{{--
                            </div>--}}
                        </div>
                    </div>
                    @endforeach
                    {{--<div class="col-md-6 col-lg-3">
                        <div class="ticket">
                            <div class="ticket-content">
                                <h3 class="mb-3">Blockchain Express Webinar</h3>
                                <h5 class="text-primary mb-3">Entrada VIP</h5>
                                <p>25 febrero, 21:00 hs.</p>
                                <p>El Granel, Asunción, Paraguay</p>
                                <p>Valido para <b class="text-primary">2 personas</b></p>
                                <h2 class="text-center pt-4">2505622DC</h2>
                            </div>
                            <div class="ticket-qr justify-content-center">
                                <img src="{{asset('/img/qr.png')}}" alt="QR" class="qr">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <div class="ticket">
                            <div class="ticket-content">
                                <h3 class="mb-3">Blockchain Express Webinar</h3>
                                <h5 class="text-primary mb-3">Entrada VIP</h5>
                                <p>25 febrero, 21:00 hs.</p>
                                <p>El Granel, Asunción, Paraguay</p>
                                <p>Valido para <b class="text-primary">2 personas</b></p>
                                <h2 class="text-center pt-4">2505622DC</h2>
                            </div>
                            <div class="ticket-qr">
                                <img src="{{asset('/img/qr.png')}}" alt="QR" class="qr">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <div class="ticket">
                            <div class="ticket-content">
                                <h3 class="mb-3">Blockchain Express Webinar</h3>
                                <h5 class="text-primary mb-3">Entrada VIP</h5>
                                <p>25 febrero, 21:00 hs.</p>
                                <p>El Granel, Asunción, Paraguay</p>
                                <p>Valido para <b class="text-primary">2 personas</b></p>
                                <h2 class="text-center pt-4">2505622DC</h2>
                            </div>
                            <div class="ticket-qr">
                                <img src="{{asset('/img/qr.png')}}" alt="QR" class="qr">
                            </div>
                        </div>
                    </div>--}}
                </div>
            </div>
        </div>

    </div>

@stop
