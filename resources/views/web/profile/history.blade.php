@extends('layout.app')

@section('content')

    <section class="profile">

        @include('web.profile.subnav')

        <div class="container">
            <div class="col-12 pb-5">
                <h3 class="pt-5">Historial de Compras</h3>
                <div class="cart py-4">
                    <div class="cart-list">
                        <ul>
                            <li class="border-0 p-3 bg-light">
                                <div class="row align-items-center d-none d-md-flex">
                                    <div class="col-md-4 text-center">
                                        <h6 class="mb-0">Eventos</h6>
                                    </div>
                                    <div class="col-md-2 text-center">
                                        <h6 class="mb-0">Fecha</h6>
                                    </div>
                                    <div class="col-md-2 text-center">
                                        <h6 class="mb-0">Precio total</h6>
                                    </div>
                                    <div class="col-md-2 text-center">
                                        <h6 class="mb-0">Metodo de Pago</h6>
                                    </div>
                                </div>
                            </li>
                            {{-- <li class="text-center">
                                <h5>Aún no has comprado ningun evento</h5>
                                <a href="{{ route('search') }}" class="btn btn-secondary">Buscar eventos</a>
                            </li> --}}
                            @foreach ($orders as $order)
                            <li>
                                <div class="row align-items-center">
                                    <div class="col-md-4">
                                        <div class="row align-items-center">
                                            <div class="col-4">
                                                <img src="{{asset('/img/cart.jpg')}}" alt="Imagen del evento" class="w-100">
                                            </div>
                                            <div class="col-8">
                                                <h5 class="mb-md-0">{{ $order->tickets->count() }} entradas compradas</h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 text-md-center">
                                        <p class="mt-4 mt-md-0 mb-md-0">@php(setlocale(LC_TIME,"es_ES.UTF-8")){{ strftime("%A, %d de %B", strtotime($order->created_at)) }}</p>
                                    </div>
                                    <div class="col-md-2 text-md-center">
                                        <p class="mb-md-0">Gs. {{ number_format($order->subtotal, 0, ",", ".") }}</p>
                                    </div>
                                    <div class="col-md-2 text-md-center">
                                        <p class="mb-md-0">{{ isset($order->payment->payment_method->method) ? $order->payment->payment_method->method : 'Sin medio' }}</p>
                                    </div>
                                    <div class="col-md-2 text-md-center">
                                        <a href="{{ route('web.user.history.detail', $order->id) }}" class="btn btn-primary">Ver comprobante</a>
                                    </div>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop
