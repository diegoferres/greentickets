@extends('layout.app')

@section('content')

    <div class="profile">

        @include('web.profile.subnav')

        <div class="d-flex justify-content-center">
            <div class="div col-sm-10 col-md-8 col-xl-6 pt-5">
                <div class="d-flex flex-column flex-lg-row align-items-center">
                    <div class="profile-img mr-lg-4">
                        <img src="{{asset('/img/photo.jpg')}}" alt="Foto de perfil">
                    </div>
                    <div class="form-group w-100 pt-4 pt-lg-1">
                        <h6>Cambiar foto de perfil</h6>
                        <input type="file" class="form-control" id="inputPerfil">
                    </div>
                </div>
                <div class="pt-5">
                    {!! Form::open(['route' => ['web.user.profile_update', $user->id], 'method' => 'post']) !!}
                    <h2 class="mb-4">Datos Personales</h2>
                    <div class="form-group">
                        <h6>Nombre y Apellido</h6>
                        <input name="name" type="text" value="{{ $user->name }}" class="form-control mb-4">
                        <h6>Fecha de Nacimiento</h6>
                        <input name="born_date" type="date" value="{{ $user->born_date }}" class="form-control mb-4">
                        <h6>Sexo</h6>
                        {!! Form::select('sex_id', $sexs , $user->sex_id ? $user->sex_id : [], ['class' => 'custom-select form-control mb-4', 'placeholder' => 'Selecciona tu sexo']) !!}
                        {{--<select class="custom-select form-control mb-4">
                            <option selected>Masculino</option>
                            <option value="1">Femenino</option>
                        </select>--}}
                        <h6>Correo electrónico</h6>
                        <input id="email" type="text" value="{{ $user->email }}" class="form-control mb-4">
                        <div class="row mb-4">
                            <div class="col">
                                <h6>Documentación</h6>
                                {!! Form::select('doc_type_id', $doctypes , $user->doc_type_id ? $user->doc_type_id : [] , ['class' => 'custom-select form-control']) !!}
                            </div>
                            <div class="col">
                                <h6>Número Documento</h6>
                                <input name="doc_number" type="text" value="{{ $user->doc_number }}"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <h6>Celular</h6>
                                <input name="cell_phone" type="text" value="{{ $user->cell_phone }}"
                                       class="form-control">
                            </div>
                            <div class="col">
                                <h6>Teléfono</h6>
                                <input name="telephone" type="text" value="{{ $user->telephone }}" class="form-control">
                            </div>
                        </div>
                    </div>
                    <h3 class="pt-5 mb-4">Dirección</h3>
                    <div class="form-group">
                        <h6>Calle 1</h6>
                        <input name="street1" type="text" value="{{ $user->street1 }}" class="form-control mb-4">
                        <h6>Calle 2</h6>
                        <input name="street2" type="text" value="{{ $user->street2 }}" class="form-control mb-4">
                        <div class="row mb-4">
                            <div class="col">
                                <h6>Ciudad</h6>
                                <select class="custom-select form-control">
                                    <option selected>Asunción</option>
                                    <option value="1">Lambare</option>
                                    <option value="2">Villa Elisa</option>
                                </select>
                            </div>
                            <div class="col">
                                <h6>Barrio</h6>
                                <input id="barriotProfile" type="text" value="Terminal" class="form-control">
                            </div>
                        </div>
                    </div>
                    <h3 class="pt-5 mb-4">Datos de facturación</h3>
                    <div class="form-group">
                        <h6>Nombre y Apellido</h6>
                        <input name="business_name" type="text" value="{{ $user->business_name }}"
                               class="form-control mb-4">
                        <div class="row mb-4">
                            {{--<div class="col">
                                <h6>Documentación</h6>
                                <select class="custom-select form-control">
                                    <option selected>R.U.C</option>
                                    <option value="1">Cedula de identidad</option>
                                </select>
                            </div>--}}
                            <div class="col">
                                <h6>R.U.C.</h6>
                                <input name="ruc_number" type="text" value="{{ $user->ruc_number }}"
                                       class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group pt-5">
                        <button type="submit" class="btn btn-primary btn-block">Guardar cambios</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

    </div>

@stop