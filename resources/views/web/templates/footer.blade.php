<footer class="mt-5 py-4 bg-light">
    <div class="container-fluid d-flex flex-column-reverse flex-lg-row justify-content-lg-between">

        <p class="">Copyright © 2020 - EntraSinPapel. &nbsp Power by <a href="http://www.nano.com.py/" target="_blanck">@nanodeveloper</a>&nbsp diseñado por <a href="https://www.instagram.com/mathiasm_9/" target="_blanck">Mathias</a></p>
        <div class="float-xl-right d-flex flex-column flex-md-row mb-3 mb-lg-0">
            <a href="{{ route('faqs') }}" class="mr-md-4 mb-4 mb-lg-0">Preguntas Frecuentes</a>
            <a href="{{ route('politicas') }}" class="mr-md-4 mb-4 mb-lg-0">Políticas de Privacidad</a>
            <a href="{{ route('termino') }}" class="mb-4 mb-lg-0">Términos y Condiciones</a>
            <div class="d-flex mb-4 mb-lg-0">
                <span class="icon">
                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                </span>
                <span class="icon">
                    <a href="#"><i class="fab fa-twitter"></i></a>
                </span>
                <span class="icon">
                    <a href="#"><i class="fab fa-instagram"></i></a>
                </span>
            </div>
        </div>
    </div>
</footer>