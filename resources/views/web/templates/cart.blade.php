<li class="nav-item">
    <div class="dropdown cart-dropdown">
        <a class="btn nav-link text-left" type="button" id="dropdownCart" data-toggle="dropdown" aria-haspopup="true"
           aria-expanded="false">
            Carrito <span class="badge badge-primary">{{ \Cart::getContent()->count() }}</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg-right cart-menu" aria-labelledby="dropdownCart">
            @if (\Cart::getContent()->count() == 0)
                <h6> El carrito está vacío</h6>
            @else
                <div class="cart">
                    <ul>

                        @foreach(\Cart::getContent() as $item)
                            <li>
                                <div class="row align-items-center">
                                    <div class="col-4">
                                        <img class="card-img-top"
                                             src="{{asset('storage/'.$item->attributes->event_cover)}}"
                                             alt="Card image cap">
                                    </div>
                                    <div class="col-6">
                                        <a href="{{ route('web.event.show', $item->attributes->event_slug) }}">
                                            <h5>{{ $item->attributes->event_name }}</h5></a>
                                        <span>
                                            {{ $item->name }}<br>{{ $item->quantity }} x <b>Gs. {{ number_format($item->price, '0', ',', '.') }}</b><br>
                                            @php(setlocale(LC_TIME,"es_PY.UTF-8"))
                                            {{ $item->attributes->date ? strftime("%d, %B", strtotime($item->attributes->date)) : null }}
                                            {{ number_format($item->attributes->ticket_data_doc, 0, ',','.') }}

                                            {{--{{ date('Y-m-d', strtotime($item->attributes->date)) }}--}}
                                        </span>
                                    </div>
                                    <div class="col-2">
                                        <a href="{{ route('web.orders.removefromcart', $item->id) }}" class="icon-cart"
                                           title="Quitar del carrito"><i class="far fa-trash-alt"></i></a>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                    <div class="price">
                        <div class="d-flex justify-content-between">
                            <p>Sub Total:</p>
                            <h5>Gs. {{ number_format(\Cart::getSubTotal(), '0', ',', '.') }}</h5>
                        </div>
                    </div>
                    <div class="d-flex flex-column">
                        <a class="btn btn-tertiary mb-2" href="{{ route('web.orders.showcart') }}">Ver Carrito</a>
                        <a class="btn btn-tertiary" href="{{ route('web.orders.checkout') }}">Proceder a la compra</a>
                    </div>
                </div>
            @endif
        </div>
    </div>
</li>
