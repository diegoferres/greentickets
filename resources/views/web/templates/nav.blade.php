
<nav class="navbar navbar-expand-lg navbar-light">
    <div class="container-fluid">
    <a class="navbar-brand" href="{{ route('home') }}"><img src="{{asset('/img/logo.svg')}}" alt="Logo"></a>
    <button class="navbar-toggler border-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('web.event.step1') }}">Crear Evento</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('web.search') }}">Buscar Evento</a>
            </li>
            @include('web.templates.cart')
            @auth()
                <li class="nav-item my-auto">
                    <div class="user-profile">
                        <div class="dropdown">
                            <a class="nav-link py-0" href="javascript:void(0)" role="button" id="dropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <div class="d-flex align-items-center">
                                    <div class="image">
                                        <img src="{{asset('/img/photo.jpg')}}" alt="Foto de perfil" class="w-100">
                                    </div>
                                    <div class="ml-4">
                                        {{ \Illuminate\Support\Facades\Auth::user()->name }}
                                    </div>
                                    <div class="ml-auto">
                                        <i class="ml-3 fas fa-angle-down"></i>
                                    </div>
                                </div>
                            </a>

                            <div class="dropdown-menu dropdown-menu-lg-right" aria-labelledby="dropdownProfile">
                                <a class="dropdown-item" href="{{ route('web.user.profile') }}">Ir al perfil</a>
                                <a class="dropdown-item" href="{{ route('web.organizer.index') }}">Perfil organizador</a>
                                <a class="dropdown-item" id="logout" href="javascript:void(0)" onclick="signOut()">Cerrar Sesión</a>
                            </div>
                        </div>
                    </div>
                </li>
                {{--  <li class="nav-item my-auto">
                    <a class="btn btn-secondary" href="javascript:void(0)">Bienvenido {{ \Illuminate\Support\Facades\Auth::user()->name }}</a>
                </li>
                <li class="nav-item">

                    <a class="nav-link" id="logout" href="javascript:void(0)">Cerrar Sesión</a>
                </li> --}}
            @else
                <li class="nav-item my-auto">
                    <a class="btn btn-secondary" href="{{ route('login') }}">Iniciar Sesión</a>
                    <a class="btn btn-primary ml-1" href="{{ route('register') }}">Registrate</a>
                </li>
            @endauth
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </ul>
    </div>
</div>
</nav>
