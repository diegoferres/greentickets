@extends('layout.app')

@section('content')
    <main class="d-flex justify-content-center">
        <div class="col-sm-10 col-md-8 col-lg-5 mt-5">
            <h2>Confirmar tu cuenta</h2>
            <p class="pb-4">Ingresa tu correo electronico para verificar tu correo. En el te enviaremos los pasos para verificar tu cuenta.</p>
            <input id="password_reset" name="password_reset" type="email" class="form-control mb-2" placeholder="Correo electronico">
            <a href="{{ route('mail-succes') }}" class="btn btn-primary mt-4 btn-block">Confirmar</a>
        </div>
    </main>
@endsection