@extends('layout.app')
@section('head')
    <meta name="google-signin-client_id"
          content="650646695179-96fegdumu78r79ct9pkhh71385dp47fk.apps.googleusercontent.com">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css">
    <script src="https://apis.google.com/js/api:client.js"></script>

@endsection
@section('content')
    <main class="d-flex justify-content-center">
        <div class="col-sm-10 col-md-8 col-xl-6 mt-5"
             id="formLogin" {{ $showForm == 'login' ? '' : 'style=display:none' }}>
            <div class="pb-5">
                <h1 class="mb-3" id="inisesion">Iniciar Sesión</h1>
                <p class="mb-4">Inicia sesión con tu correo. Si aún no tienes una cuenta, aquí puedes <a
                            href="{{ route('register') }}"><b>Registrarte</b></a></p>
                {!! Form::open(['route' => 'login', 'method' => 'post', 'id' => 'loginForm']) !!}
                <input type="hidden" name="showForm" value="{{ old('showForm') ? old('showForm') : $showForm }}">
                <div class="form-group mb-4">
                    <label for="name" class="label">Correo</label>
                    <input id="login_email" name="email" type="email" required="" autofocus=""
                           class="form-control @error('email') is-invalid @enderror" placeholder="Correo electrónico"
                           value="{{ old('email') }}">
                    @error('email')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group mb-4">
                    <label for="name" class="label">Contraseña</label>
                    <input id="login_password" name="password" type="password" required=""
                           class="form-control @error('email') is-invalid @enderror"
                           placeholder="Contraseña">
                    @error('password')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="d-flex justify-content-md-between flex-column-reverse flex-md-row align-items-md-center pt-4">
                    <a href="{{ route('password.request') }}">Olvide mi contraseña</a>
                    <button type="submit" class="btn btn-primary mb-4 mb-md-0">Iniciar Sesión</button>
                </div>
                {!! Form::close() !!}
            </div>
            <div class="mb-4 pt-1">
                <p class="mb-3">Tambien puedes inciar sesión con tus redess</p>
                <div class="d-flex flex-column flex-md-row">
                    <a href="{{ route('social.login', 'facebook') }}"
                       class="btn-social btn-f mb-2 mb-md-0 mr-md-2 flex-md-fill">Iniciar
                        Sesión con <strong>Facebook</strong></a>
                    <a href="{{ route('social.login', 'google') }}" class="btn-social btn-g flex-md-fill">Iniciar Sesión
                        con
                        <strong>Google</strong></a>
                    {{--<button id="FacebookLoginBtn" class="btn-social btn-f mb-2 mb-md-0 mr-md-2 flex-md-fill">Iniciar
                        Sesión con <strong>Facebook</strong></button>--}}
                    {{--<button id="GoogleLoginBtn" class="btn-social btn-g flex-md-fill">Iniciar Sesión con
                        <strong>Google</strong></button>--}}
                </div>
            </div>
        </div>
        <div class="col-sm-10 col-md-8 col-xl-6 mt-5"
             id="formRegister" {{ $showForm == 'register' ? '' : 'style=display:none' }}>
            <div class="pb-5">
                <h1 class="mb-3">Registrate</h1>
                <p class="mb-4">Registrate con tu correo. Si ya tienes una cuenta, aquí puedes <a
                            href="{{ route('login') }}"><b>Iniciar Sesión</b></a>
                </p>
                {!! Form::open(['route' => 'register', 'method' => 'post', 'id' => 'registerForm']) !!}
                <input type="hidden" name="showForm" value="{{ old('showForm') ? old('showForm') : $showForm }}">
                <div class="form-group mb-4">
                    <label for="name" class="label">Nombre y Apellido</label>
                    <input id="reg_name" name="name" type="text" autofocus=""
                           class="form-control @error('name') is-invalid @enderror" placeholder="Nombre y Apellido"
                           value="{{ old('name') }}">
                    @error('name')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group mb-4">
                    <label for="name" class="label">Correo</label>
                    <input id="reg_email" name="email" type="email" autofocus=""
                           class="form-control @error('email') is-invalid @enderror"
                           placeholder="Ej.: micorreo@gmail.com" value="{{ old('email') }}">
                    @error('email')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group mb-4">
                    <label for="name" class="label">Fecha de Nacimiento</label>
                    <input id="reg_born_date" name="born_date" type="date" autofocus="" maxlength="10"
                           class="form-control @error('born_date') is-invalid @enderror"
                           value="{{ old('born_date') }}">
                    @error('born_date')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group mb-4">
                    <label for="name" class="label">Celular</label>
                    <input id="reg_cell_phone" name="cell_phone" type="number" autofocus=""
                           class="form-control @error('cell_phone') is-invalid @enderror" placeholder="Ej.: 0981999888"
                           value="{{ old('cell_phone') }}">
                    @error('cell_phone')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group mb-4" id="divPwd">
                    <label for="name" class="label">Contraseña</label>
                    <input id="reg_password" name="password" type="password"
                           class="form-control mb-2 @error('password') is-invalid @enderror"
                           placeholder="Contraseña" value="{{ old('name') }}">
                    @error('password')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                    <small class="text-muted pl-4">La contraseña debe tener por lo menos 8 caracteres</small>
                </div>
                <div class="form-group mb-4" id="divPwdConfirm">
                    <label for="name" class="label">Confirmar contraseña</label>
                    <input id="reg_password_confirm" name="password_confirmation" type="password"
                           class="form-control mb-2"
                           placeholder="Repite la contraseña">
                </div>
                <div class="d-flex flex-column flex-md-row justify-content-md-between align-items-md-center pt-4">
                    <p class="mb-md-0 pr-md-3">Al registrarte aceptas las <a href="{{ route('politicas') }}"><b>políticas de privacidad</b></a>
                        y los <a
                                href="{{ route('termino') }}"><b>términos y condiciones</b></a> de uso.</p>
                    <button type="submit" class="btn btn-primary">Registrate</button>
                </div>
                {!! Form::close() !!}
            </div>
            <div class="mb-4 pt-5">
                <p class="mb-3">Tambien puedes registrarte con tus redes</p>
                <div class="d-flex flex-column flex-md-row">
                    <a href="{{ route('social.login', 'facebook') }}"
                       class="btn-social btn-f mb-2 mb-md-0 mr-md-2 flex-md-fill">Iniciar
                        Sesión con <strong>Facebook</strong></a>
                    <a href="{{ route('social.login', 'google') }}" class="btn-social btn-g flex-md-fill">Iniciar Sesión
                        con
                        <strong>Google</strong></a>
                </div>
            </div>
        </div>
    </main>
@endsection
@section('scripts')
    <script>
        $(document).ready(function () {

            if ($('input[name="showForm"]').val() == 'login') {
                $("div#formLogin").css('display', '');
                $("div#formRegister").css('display', 'none');
            } else {
                $("div#formLogin").css('display', 'none');
                $("div#formRegister").css('display', '');
            }
        })
    </script>
@endsection