@extends('layout.app')

@section('content')
    <main class="d-flex justify-content-center">
        <div class="col-sm-10 col-md-8 col-lg-5 mt-5">
            <h2>Restablecer Contraseña</h2>
            <p class="pb-4">Ingresa tu correo electronico con el cual tienes la cuenta, en el te enviaremos un enlace para restablecer la contraseña.</p>
            <input id="password_reset" name="password_reset" type="email" class="form-control mb-2" placeholder="Correo electronico">
            <a href="{{ route('newpass') }}" class="btn btn-primary mt-4 btn-block">Solicitar</a>
        </div>
    </main>
@endsection