@extends('layout.app')

@section('content')
    <main class="d-flex justify-content-center">
        <div class="col-sm-10 col-md-8 col-lg-5 mt-5">
            <h2>Aún no has verificado tu correo</h2>
            <p class="pb-4">Antes de continuar, por favor, confirme su correo electrónico con el enlace de verificación que le fue enviado. Si no ha recibido el correo electrónico,</p>
            <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                @csrf
                <button type="submit" class="btn btn-primary mt-4 btn-block">{{ __('click here to request another') }}</button>.
            </form>
            {{--<a href="{{ route('newpass') }}" class="btn btn-primary mt-4 btn-block">Solicitar</a>--}}
        </div>
    </main>
@endsection
