@extends('layout.app')
@section('head')
    <meta name="google-signin-client_id"
          content="650646695179-96fegdumu78r79ct9pkhh71385dp47fk.apps.googleusercontent.com">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css">
    <script src="https://apis.google.com/js/api:client.js"></script>
@endsection
@section('content')
    <main class="d-flex justify-content-center">
        <div class="col-sm-10 col-md-8 col-xl-6 mt-5">
            <div class="pb-5">
                <h1 class="mb-3">Iniciar Sesión</h1>
                <p class="mb-4">Inicia sesión con tu correo. Si aún no tienes una cuenta, aquí puedes <a
                            href="{{ route('register') }}"><b>Registrarte</b></a></p>
                {!! Form::open(['route' => 'login', 'method' => 'post']) !!}
                <div class="form-group mb-4">
                    <label for="name" class="label">Correo</label>
                    <input id="email" name="email" type="email" required="" autofocus=""
                           class="form-control @error('email') is-invalid @enderror" placeholder="Correo electrónico"
                           value="{{ old('email') }}">
                    @error('email')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group mb-4">
                    <label for="name" class="label">Contraseña</label>
                    <input id="password" name="password" type="password" required=""
                           class="form-control @error('email') is-invalid @enderror"
                           placeholder="Contraseña">
                    @error('password')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="d-flex justify-content-md-between flex-column-reverse flex-md-row align-items-md-center pt-4">
                    <a href="{{ route('reset') }}">Olvide mi contraseña</a>
                    <button type="submit" class="btn btn-primary mb-4 mb-md-0">Iniciar Sesión</button>
                </div>
                {!! Form::close() !!}
            </div>
            <div class="mb-4 pt-1">
                <p class="mb-3">Tambien puedes inciar sesión con tus redess</p>
                <div class="d-flex flex-column flex-md-row">
                    <button id="FacebookLoginBtn" class="btn-social btn-f mb-2 mb-md-0 mr-md-2 flex-md-fill">Iniciar
                        Sesión con <strong>Facebook</strong></button>
                    <div class="btn btn-light btn-block btn-google" id="customBtn"> Google</div>
                    {{--<button id="GoogleLoginBtn" class="btn-social btn-g flex-md-fill">Iniciar Sesión con <strong>Google</strong></button>--}}
                </div>
            </div>
        </div>
    </main>
@endsection
@section('scripts')
@include('web.auth.auth_scripts.blade.php')
@endsection