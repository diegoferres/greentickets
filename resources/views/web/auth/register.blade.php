@extends('layout.app')

@section('content')
    <main class="d-flex justify-content-center" {{ $showForm == 'login' ? '' : 'style="display:none"' }}>
        <div class="col-sm-10 col-md-8 col-xl-6 mt-5">
            <div class="pb-5">
                <h1 class="mb-3">Registrate</h1>
                <p class="mb-4">Registrate con tu correo. Si ya tienes una cuenta, aquí puedes <a href="{{ route('login') }}"><b>Iniciar Sesión</b></a>
                </p>
                {!! Form::open(['route' => 'register', 'method' => 'post']) !!}
                <div class="form-group mb-4">
                    <label for="name" class="label">Nombre y Apellido</label>
                    <input id="name" name="name" type="text" autofocus=""
                           class="form-control @error('name') is-invalid @enderror" placeholder="Nombre y Apellido" value="{{ old('name') }}">
                    @error('name')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group mb-4">
                    <label for="name" class="label">Correo</label>
                    <input id="email" name="email" type="email" autofocus=""
                           class="form-control @error('email') is-invalid @enderror"
                           placeholder="Ej.: micorreo@gmail.com" value="{{ old('email') }}">
                    @error('email')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group mb-4">
                    <label for="name" class="label">Fecha de Nacimiento</label>
                    <input id="born_date" name="born_date" type="date" autofocus="" maxlength="10"
                           class="form-control @error('born_date') is-invalid @enderror"
                           value="{{ old('born_date') }}">
                    @error('born_date')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group mb-4">
                    <label for="name" class="label">Celular</label>
                    <input id="cell_phone" name="cell_phone" type="number" autofocus=""
                           class="form-control @error('cell_phone') is-invalid @enderror" placeholder="Ej.: 0981999888" value="{{ old('cell_phone') }}">
                    @error('cell_phone')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group mb-4">
                    <label for="name" class="label">Contraseña</label>
                    <input id="password" name="password" type="password"
                           class="form-control mb-2 @error('password') is-invalid @enderror"
                           placeholder="Contraseña" value="{{ old('name') }}">
                    @error('password')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                    <small class="text-muted pl-4">La contraseña debe tener por lo menos 8 caracteres</small>
                </div>
                <div class="form-group mb-4">
                    <label for="name" class="label">Confirmar contraseña</label>
                    <input id="password_confirm" name="password_confirmation" type="password" class="form-control mb-2"
                           placeholder="Repite la contraseña">
                </div>
                <div class="d-flex flex-column flex-md-row justify-content-md-between align-items-md-center pt-4">
                    <p class="mb-md-0 pr-md-3">Al registrarte aceptas las <a href="#"><b>políticas de privacidad</b></a> y los <a
                                href="#"><b>términos y condiciones</b></a> de uso.</p>
                    <button type="submit" class="btn btn-primary">Registrate</button>
                </div>
                {!! Form::close() !!}
            </div>
            <div class="mb-4 pt-5">
                <p class="mb-3">Tambien puedes registrarte con tus redes</p>
                <div class="d-flex flex-column flex-md-row">
                    <a href="#" class="btn-social btn-f mb-2 mb-md-0 mr-md-2 flex-md-fill">Registrate con <strong>Facebook</strong></a>
                    <a href="#" class="btn-social btn-g flex-md-fill">Registrate con <strong>Google</strong></a>
                </div>
            </div>
        </div>
    </main>
@endsection
@section('scripts')
    <script>
        /**
         * Login Facebook
         */
        function getUserData() {
            FB.api('/me', {fields: 'name,email'}, (response) => {
                console.log(response)
                //SignupUser(false, response)
            });
        }

        window.fbAsyncInit = () => {
            //SDK loaded, initialize it
            FB.init({
                appId: '1423323124528512',
                xfbml: true,
                version: 'v4.0'
            });

            //check user session and refresh it
            FB.getLoginStatus((response) => {
                if (response.status === 'connected') {
                    //user is authorized
                    //document.getElementById('loginBtn').style.display = 'none';
                    getUserData();
                } else {
                    //user is not authorized
                }
            });
        };

        //load the JavaScript SDK
        ((d, s, id) => {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "https://connect.facebook.net/es_ES/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        })(document, 'script', 'facebook-jssdk');

        //add event listener to login button
        document.getElementById('FacebookLoginBtn').addEventListener('click', () => {
            //do the login
            FB.login((response) => {
                if (response.authResponse) {
                    //user just authorized your app
                    //document.getElementById('loginBtn').style.display = 'none';
                    getUserData();
                }
            }, {scope: 'email,public_profile,user_birthday,user_gender', return_scopes: true});
        }, false);
        /**
         * END Facebook Login
         */

        /****
         * Google Login
         */
        var googleUser = {};
        var startApp = function () {
            console.log('startApp');
            gapi.load('auth2', function () {
                // Retrieve the singleton for the GoogleAuth library and set up the client.
                auth2 = gapi.auth2.init({
                    client_id: '650646695179-96fegdumu78r79ct9pkhh71385dp47fk.apps.googleusercontent.com',
                    cookiepolicy: 'http://helpers.com.py/',
                    // Request scopes in addition to 'profile' and 'email'
                    //scope: 'additional_scope'
                });
                attachSignin(document.getElementById('customBtn'));
            });
        };

        function attachSignin(element) {
            console.log(element.id);
            auth2.attachClickHandler(element, {},
                function (googleUser) {
                    var data = {
                        name: googleUser.getBasicProfile().getName(),
                        email: googleUser.getBasicProfile().getEmail(),
                        password: googleUser.getBasicProfile().getId()
                    };

                    //SignupUser(data);

                }, function (error) {
                    console.log(JSON.stringify(error, undefined, 2));
                });
        }

        /**
         * SignUp User
         */
        function SignupUser(GoogleUserData = false, FacebookUserData = false) {
            if (GoogleUserData) {
                var token = $("input[name=_token]").val();
                var id = GoogleUserData.password;
                var name = GoogleUserData.name;
                var email = GoogleUserData.email;
                var password = GoogleUserData.password;
                var social = true;
            } else if (FacebookUserData) {
                var token = $("input[name=_token]").val();
                var id = FacebookUserData.id;
                var name = FacebookUserData.name;
                var email = FacebookUserData.email;
                var password = FacebookUserData.id;
                var social = true;
            } else {
                var token = $("input[name=_token]").val();
                var id = false;
                var name = $("input[name=name]").val();
                var email = $("input[name=reg_email]").val();
                var password = $("input[name=reg_password]").val();
                var social = false;
            }

            // garnish the data
            var data = {
                _token: token,
                id: id,
                name: name,
                email: email,
                password: password,
                social: social
            };

            console.log('User Data: ', data);

            id = "register-form"
            // ajax post
            $.ajax({
                type: "post",
                url: "/register/user",
                data: data,
                cache: false,
                success: function (data) {

                    if (data.status == 3) {

                        if (GoogleUserData) {
                            console.log('Google')
                            $('#soc_name').val(GoogleUserData.name);
                            $('#soc_email').val(GoogleUserData.email);
                            $('#soc_password').val(GoogleUserData.password);
                            $('#soc_password_confirm').val(GoogleUserData.password);
                            $('#soc_password').attr('type', 'hidden');
                            $('#soc_password_confirm').attr('type', 'hidden');
                            $('#social_datos').text('Tus datos fueron obtenidos de Google');

                        } else if (FacebookUserData) {
                            console.log('Facebook')
                            $('#soc_name').val(FacebookUserData.name);
                            $('#soc_email').val(FacebookUserData.email);
                            $('#soc_password').val(FacebookUserData.id);
                            $('#soc_password_confirm').val(FacebookUserData.id);
                            $('#soc_password').attr('type', 'hidden');
                            $('#soc_password_confirm').attr('type', 'hidden');
                            $('#social_datos').text('Tus datos fueron obtenidos de Facebook');
                        }

                        $('#soc_register').modal('show');

                        $('#login').modal('hide');
                    } else if (data.status == 1) {
                        $('#register').modal('hide');

                        $('#login').modal('hide');

                        $('li#usr_li').css('display', '');

                        $('a#usr_name').html('Hola ' + data.usr_name + '<span class="caret"></span>');

                        $('li.unlogin_li').css('display', 'none');

                        const Toast = Swal.mixin({
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 5000,
                            timerProgressBar: true,
                            onOpen: (toast) => {
                                toast.addEventListener('mouseenter', Swal.stopTimer)
                                toast.addEventListener('mouseleave', Swal.resumeTimer)
                            }
                        })

                        Toast.fire({
                            icon: 'success',
                            title: '¡Sesión iniciada correctamente!'
                        })
                    }
                },
                error: function (data) {
                    $("ul#list_errors_reg").empty();
                    $.each(data.responseJSON.errors, function (index, value) {
                        $.each(value, function (index, val) {

                            $("ul#list_errors_reg").append('<li>' + val + '</li>');

                            $("div#error_display_reg").fadeIn(1000);

                            $("div#error_display_reg").fadeTo(5000, 500).slideUp(500, function () {
                                $("div#error_display_reg").slideUp(500);
                            });
                        });
                    });

                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 5000,
                        timerProgressBar: true,
                        onOpen: (toast) => {
                            toast.addEventListener('mouseenter', Swal.stopTimer)
                            toast.addEventListener('mouseleave', Swal.resumeTimer)
                        }
                    })

                    Toast.fire({
                        icon: 'error',
                        title: '¡Error al registrarse!'
                    })
                }
            });

            // this make sure the form doesn't load
            // a form pause
            return false;
        }

        /**
         * END SignUp User
         */

        /**
         * SignOut Facebook, Google, Laravel
         */
        function signOut() {
            var auth2 = gapi.auth2.getAuthInstance();
            auth2.signOut().then(function () {

            });

            auth2.disconnect();

            FB.getLoginStatus(function (response) {
                if (response.status != 'unknown') {

                    //$.post( "/killsession");
                    FB.logout(function (response) {

                        console.log('You have successfully logout from Facebook.');
                    });
                }
            });

            $('form#logout-form').submit();
        }

        startApp();
    </script>
@endsection