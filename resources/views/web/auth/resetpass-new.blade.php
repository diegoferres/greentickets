@extends('layout.app')

@section('content')
    <main class="d-flex justify-content-center">
        <div class="col-sm-10 col-md-8 col-lg-6 mt-5">
            <h2>Restablecer Contraseña</h2>
            <p class="pb-3">Elige una contraseña de 8 caracteres que puedas recordar.</p>
            <div class="form-group mb-4">
                <label for="name" class="label">Contraseña</label>
                <input id="password" name="password" type="password"
                    class="form-control mb-2 @error('password') is-invalid @enderror"
                    placeholder="Contraseña" value="{{ old('name') }}">
                @error('password')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
                @enderror
                <small class="text-muted pl-4">La contraseña debe tener por lo menos 8 caracteres</small>
            </div>
            <div class="form-group mb-4">
                <label for="name" class="label">Confirmar contraseña</label>
                <input id="password_confirm" name="password_confirmation" type="password" class="form-control mb-2"
                    placeholder="Repite la contraseña">
            </div>
            <a href="{{ route('succespass') }}" class="btn btn-primary mt-4 btn-block">Cambiar contraseña</a>
        </div>
    </main>
@endsection