@extends('layout.app')

@section('content')
    <main class="d-flex justify-content-center">
        <div class="col-sm-10 col-md-8 col-lg-5 text-center mt-5">
            <h1 class="mt-2">¡Restablecimiento de contraseña exitoso!</h2>
            <h4 class="py-3">Ahora puedes usar su nueva contraseña para <br> iniciar sesión en su cuenta</h4>
            <a href="{{ route('login') }}" class="btn btn-primary mt-4 px-5">Iniciar Sesión</a>
        </div>
    </main>
@endsection