@extends('layout.app')

@section('content')
    <main class="d-flex justify-content-center">
        <div class="col-sm-10 col-md-8 col-lg-5 text-center mt-5">
            <h1 class="mt-3">¡Confirmación exitosa!</h2>
            <h4 class="py-3">Hemos verificador con exito tu correo <br>electronico y cuenta con exito.</h4>
            <a href="{{ route('home') }}" class="btn btn-primary mt-4 px-5">Volver al inicio</a>
        </div>
    </main>
@endsection