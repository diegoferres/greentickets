@extends('web.places.templates.layout')
@section('content_place')
    <ul>
        <li class="border-0">
            <div class="col-md-12 d-flex justify-content-center">
                <img height="175px" src="{{asset('/storage/places/logo/'.$place->logo)}}" alt="Logo"
                     class="py-0 mb-1">
            </div>
        </li>
        <li class="border-0 p-3">
            <div class="pt-1 pb-5">
                <h2 class="mb-4 font-color">Reserva de mesas</h2>
                {{--<h5>Especificar mensaje a mostrar</h5>--}}
                <hr>
                {!! Form::open(['route' => ['web.places.reservation.store', $place->slug], 'method' => 'post']) !!}
                <div class="form-group mb-4">
                    {!! Form::number('doc_number', old('doc_number'),
                    ['class' => $errors->has('doc_number') ? 'form-control is-invalid' : 'form-control', 'placeholder' => 'Cédula', 'autofocus']) !!}
                    @error('doc_number')
                    <div class="invalid-feedback">
                        <label class="badge badge-danger">
                            {{ $message }}
                        </label>
                    </div>
                    @enderror
                </div>
                <div class="form-group mb-4">
                    {!! Form::text('name', old('name'),
                    ['class' => $errors->has('name') ? 'form-control is-invalid' : 'form-control', 'placeholder' => 'Nombre y Apellido']) !!}
                    @error('name')
                    <div class="invalid-feedback">
                        <label class="badge badge-danger">
                            {{ $message }}
                        </label>
                    </div>
                    @enderror
                </div>

                <div class="form-group mb-4">
                    {!! Form::text('reserved_date', old('reserved_date'),
                   ['class' => $errors->has('reserved_date') ? 'form-control is-invalid' : 'form-control', 'maxlength'=>'10']) !!}
                    @error('reserved_date')
                    <div class="invalid-feedback">
                        <label class="badge badge-danger">
                            {{ $message }}
                        </label>
                    </div>
                    @enderror
                </div>

                <div class="form-group mb-4">
                    {!! Form::number('amount_people', old('amount_people'),
                    ['class' => $errors->has('amount_people') ? 'form-control is-invalid' : 'form-control', 'placeholder' => 'Cantidad de personas']) !!}
                    @error('amount_people')
                    <div class="invalid-feedback">
                        <label class="badge badge-danger">
                            {{ $message }}
                        </label>
                    </div>
                    @enderror
                </div>

                <div class="form-group mb-4">
                    {!! Form::number('cell_phone', old('cell_phone'),
                    ['class' => $errors->has('cell_phone') ? 'form-control is-invalid' : 'form-control', 'placeholder' => 'Teléfono']) !!}
                    @error('cell_phone')
                    <div class="invalid-feedback">
                        <label class="badge badge-danger">
                            {{ $message }}
                        </label>
                    </div>
                    @enderror

                </div>
                <div class="form-group float-right">
                    <button type="submit"
                            class="btn btn-primary mr-2"
                            type="submit">Enviar
                    </button>
                </div>
                <div class="form-group float-left">
                    <a href="/{{$place->slug}}" class="btn btn-info">
                        <i class="fa fa-home"></i>
                    </a>
                </div>
                {!! Form::close() !!}
            </div>
        </li>
    </ul>
@endsection
@section('background')
    $.backstretch('{{ asset('storage/places/background/'.$place->background_img) }}');
@endsection
@section('script')
    <script src="/js/jquery.mask.js"></script>
    <script>
        $(document).ready(function () {
            $('input[name="reserved_date"]').mask("00-00-0000", {
                selectOnFocus: false,
                placeholder: 'Fecha Nac. __-__-____'
            });
        })
    </script>
@endsection