@extends('web.places.templates.layout')
@section('content_place')
    <ul>
        <li class="border-0">
            <div class="col-md-12 d-flex justify-content-center">
                <img height="175px" src="{{asset('/storage/places/logo/'.$place->logo)}}" alt="Logo"
                     class="py-0 mb-1">
            </div>
        </li>
        <li class="border-0 p-3">
            <div class="pt-1 pb-5">
                <h2 class="mb-4 font-color">Sugerencias</h2>
                {{--<h5>Especificar mensaje a mostrar</h5>--}}
                <hr>
                {!! Form::open(['route' => ['web.places.suggestion.store', $place->slug], 'method' => 'post']) !!}
                @csrf
                <div class="form-group mb-4">
                    {!! Form::text('name', old('name'),
                    ['class' => $errors->has('name') ? 'form-control is-invalid' : 'form-control', 'placeholder' => 'Nombre', 'autofocus']) !!}
                    @error('name')
                    <div class="invalid-feedback">
                        <label class="badge badge-danger">
                            {{ $message }}
                        </label>
                    </div>
                    @enderror
                </div>
                <div class="form-group mb-4">
                    {!! Form::textarea('message', old('message'),
                    ['class' => $errors->has('message') ? 'form-control is-invalid' : 'form-control', 'placeholder' => 'Sugerencia']) !!}
                    @error('message')
                    <div class="invalid-feedback">
                        <label class="badge badge-danger">
                            {{ $message }}
                        </label>
                    </div>
                    @enderror
                </div>
                <div class="form-group float-right">
                    <button type="submit"
                            class="btn btn-primary mr-2"
                            type="submit">Enviar
                    </button>
                </div>
                <div class="form-group float-left">
                    <a href="/{{$place->slug}}" class="btn btn-info">
                        <i class="fa fa-home"></i>
                    </a>
                </div>
                {!! Form::close() !!}
            </div>
        </li>
    </ul>
@endsection
@section('background')
    $.backstretch('{{ asset('storage/places/background/'.$place->background_img) }}');
@endsection