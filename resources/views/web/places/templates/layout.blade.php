@extends('layout.app_places')
@section('content')
    <style>
        .event-category .cine, .event-category .Cine {
            background-image: linear-gradient(to bottom right, #ffc400e6, #ffc400e6);

        }

        .event-category .category {
            height: 60px;
            width: 100%;
        }

        h3 {
            font-size: 17px;
        }

        .history {
            padding: 0px;
            box-shadow: 0px 0px 0px #00000024;
        }

        .badge {
            font-size: 90%;!important;
            font-weight: 500;!important;
        }
        input[type="date"]:before {
            content: attr(placeholder) !important;
            color: #aaa;
            margin-right: 0.5em;
        }
        input[type="date"]:focus:after,
        input[type="date"]:valid:after {
            content: ""; !important;
        }
        input:invalid {
            color: red;
        }

        [type="date"]::-webkit-inner-spin-button {
            display: none;
        }
        [type="date"]::-webkit-calendar-picker-indicator {
            display: none;
        }
        @media (max-width: 767px) {
            input::-webkit-calendar-picker-indicator {
                display: none;
            }
            input[type="date"]::-webkit-input-placeholder {
                visibility: hidden !important;
            }
        }

        .shart .icon {
            background-color: rgb(120,120,120, 0.5);
            font-size: 18px;
            color: #ffffff;
            padding: 15px 0px;
            border-radius: 50%;
            margin-right: 8px;
            width: 50px;
            height: 50px;
            text-align: center;
        }
        .font-color {
           color: {{ $place->font_color }}
        }

        .btn-info {
            font-size: 12px;
        }

        .btn-warning {
            font-size: 12px;
        }
    </style>
    <div class="d-flex justify-content-center">
        <div class="col-lg-4" style="background-color: rgba(255,255,255, 0.0);">
            <div class="history cart">
                <div class="cart-list">
                    @yield('content_place')
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-backstretch/2.1.18/jquery.backstretch.min.js"></script>
    <script>
        $(document).ready(function () {
            @yield ('background')
        })
    </script>
    @yield('script')
@endsection