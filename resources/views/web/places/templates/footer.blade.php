<footer class="mt-5 py-4" style="background-color: rgba(255,255,255, 0.5);">
    <div class="container-fluid d-flex flex-column-reverse flex-lg-row justify-content-lg-between">
        <p class="">Copyright © 2020 - EntraSinPapel. &nbsp Power by <a href="#">@nanodeveloper</a>&nbsp diseñado por <a href="#">Mathias</a></p>
        <div class="float-xl-right d-flex flex-column flex-md-row mb-3 mb-lg-0">
            <a href="#" class="mr-md-4 mb-4 mb-lg-0">Políticas de Privacidad</a>
            <a href="#" class="mb-4 mb-lg-0">Términos y Condiciones</a>
            <div class="d-flex mb-4 mb-lg-0">
                <span class="icon">
                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                </span>
                <span class="icon">
                    <a href="#"><i class="fab fa-twitter"></i></a>
                </span>
                <span class="icon">
                    <a href="#"><i class="fab fa-instagram"></i></a>
                </span>
            </div>
        </div>
    </div>
</footer>