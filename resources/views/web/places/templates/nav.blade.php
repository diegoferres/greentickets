<nav class="navbar navbar-expand-lg navbar-light border-bottom" style="background-color: rgba(255,255,255, 0.5);">
    <div class="container-fluid">
        <a class="navbar-brand" href="{{ route('home') }}"><img src="{{asset('/img/logo.svg')}}" alt="Logo"></a>
    </div>
</nav>