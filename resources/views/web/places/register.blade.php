@extends('web.places.templates.layout')
@section('head')
    <style rel="stylesheet">
        .next-form {
            display: none;
        }
    </style>
@endsection
@section('content_place')
    <ul>
        <li class="border-0">
            <div class="col-md-12 d-flex justify-content-center">
                <img height="175px" src="{{asset('/storage/places/logo/'.$place->logo)}}" alt="Logo"
                     class="py-0 mb-1">
            </div>
        </li>
        <li class="border-0 p-3">
            <div class="pt-1 pb-5">
                <div class="col-md-12 justify-content-center">
                    <h2 class="mb-4 font-color">Registro de personas</h2>
                </div>
                {{--<h5>Especificar mensaje a mostrar</h5>--}}
                <hr>
                {!! Form::open(['route' => ['web.places.register.store', $place->slug], 'method' => 'post', 'id' => 'formRegister']) !!}
                <div class="form-group mb-4">
                    {!! Form::number('doc_number', old('doc_number'),
                    ['class' => $errors->has('doc_number') ? 'form-control is-invalid' : 'form-control', 'placeholder' => 'Cédula', 'autofocus']) !!}
                    @error('doc_number')
                    <div class="invalid-feedback">
                        <label class="badge badge-danger">
                            {{ $message }}
                        </label>
                    </div>
                    @enderror
                </div>
                <div class="form-group col-md-12 float-right next-form-btn">
                    <a href="javascript:void(0)"
                            class="btn btn-primary mr-2 disabled next-form-btn">Siguiente
                    </a>
                </div>
                <div class="form-group mb-4 next-form">
                    {!! Form::text('name', old('name'),
                    ['class' => $errors->has('name') ? 'form-control is-invalid' : 'form-control', 'placeholder' => 'Nombre y Apellido']) !!}
                    @error('name')
                    <div class="invalid-feedback">
                        <label class="badge badge-danger">
                            {{ $message }}
                        </label>
                    </div>
                    @enderror
                </div>

                <div class="form-group mb-4 next-form">
                    {!! Form::text('born_date', old('born_date'),
                    ['class' => $errors->has('born_date') ? 'form-control is-invalid validate' : 'form-control validate', 'maxlength'=>'10']) !!}
                    @error('born_date')
                    <div class="invalid-feedback">
                        <label class="badge badge-danger">
                            {{ $message }}
                        </label>
                    </div>
                    @enderror
                </div>

                <div class="form-group mb-4 next-form">
                    {!! Form::text('address', old('address'),
                    ['class' => $errors->has('address') ? 'form-control is-invalid' : 'form-control', 'placeholder' => 'Domicilio']) !!}
                    @error('address')
                    <div class="invalid-feedback">
                        <label class="badge badge-danger">
                            {{ $message }}
                        </label>
                    </div>
                    @enderror
                </div>

                <div class="form-group mb-4 next-form">
                    {!! Form::text('city', old('city'),
                    ['class' => $errors->has('city') ? 'form-control is-invalid' : 'form-control', 'placeholder' => 'Ciudad']) !!}
                    @error('city')
                    <div class="invalid-feedback">
                        <label class="badge badge-danger">
                            {{ $message }}
                        </label>
                    </div>
                    @enderror
                </div>

                <div class="form-group mb-4 next-form">
                    {!! Form::number('table_number', old('table_number'),
                    ['class' => $errors->has('table_number') ? 'form-control is-invalid' : 'form-control', 'placeholder' => 'Nro. de Mesa']) !!}
                    @error('table_number')
                    <div class="invalid-feedback">
                        <label class="badge badge-danger">
                            {{ $message }}
                        </label>
                    </div>
                    @enderror
                </div>

                <div class="form-group mb-4 next-form">
                    {!! Form::text('cell_phone', old('cell_phone'),
                    ['class' => $errors->has('cell_phone') ? 'form-control is-invalid' : 'form-control', 'placeholder' => 'Teléfono']) !!}
                    @error('cell_phone')
                    <div class="invalid-feedback">
                        <label class="badge badge-danger">
                            {{ $message }}
                        </label>
                    </div>
                    @enderror
                </div>
                <div class="form-group mb-4 next-form">
                    {!! Form::text('email', old('email'),
                    ['class' => $errors->has('email') ? 'form-control is-invalid' : 'form-control', 'placeholder' => 'Correo Electrónico']) !!}
                    @error('email')
                    <div class="invalid-feedback">
                        <label class="badge badge-danger">
                            {{ $message }}
                        </label>
                    </div>
                    @enderror
                </div>
                <div class="form-group float-right next-form">
                    <button type="submit"
                            class="btn btn-primary mr-2"
                            type="submit">Enviar
                    </button>
                </div>
                <div class="form-group float-left">
                    <a href="/{{$place->slug}}" class="btn btn-info">
                        <i class="fa fa-home"></i>
                    </a>
                </div>
                {!! Form::close() !!}
            </div>
        </li>
    </ul>
@endsection
@section('background')
    $.backstretch('{{ asset('storage/places/background/'.$place->background_img) }}');
@endsection
@section('script')
    <script src="/js/jquery.mask.js"></script>
    <script>
        $(document).ready(function () {
            $('input[name="born_date"]').mask("00-00-0000", {
                selectOnFocus: false,
                placeholder: 'Fecha Nac. __-__-____'
            });

            if ($('input[name="doc_number"]').val().length >= 6){
                $.get( "/validate/{{ $place->id }}/"+$('input[name="doc_number"]').val(), function( data ) {
                    $('.next-form-btn').fadeOut("fast");
                    $('.next-form').fadeIn("fast");
                });
            }


            $('input[name="doc_number"]').on('keyup',function () {

                $('.next-form-btn').removeClass('disabled');

                $('.next-form-btn').fadeIn();
                $('.next-form').fadeOut();

                if ($(this).val().length >= 6){
                    $('.next-form-btn').removeClass('disabled');
                }else{
                    $('.next-form-btn').addClass('disabled');
                }
            });

            $('.next-form-btn').on("click", function () {
                var doc_number = $('input[name="doc_number"]').val();
                $.get( "/validate/{{ $place->id }}/"+doc_number, function( data ) {
                    if (data.exists){
                        $('#formRegister').submit();
                    } else{
                        $('.next-form-btn').fadeOut("slow");
                        $('.next-form').fadeIn("slow");
                    }
                });
            });

        })
    </script>
@endsection