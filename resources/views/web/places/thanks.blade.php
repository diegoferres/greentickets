@extends('web.places.templates.layout')
@section('content_place')
    <ul>
        <li class="border-0">

            <div class="col-md-12 d-flex justify-content-center">
                <img height="175px" src="{{asset('/storage/places/logo/'.$place->logo)}}" alt="Logo"
                     class="py-0 mb-1">

            </div>
            <div class="col-md-12 d-flex justify-content-center py-0 mb-1">
                <h2 class="font-color"><b>¡Muchas Gracias!</b></h2>
            </div>
            <div class="col-md-12 d-flex justify-content-center py-0 mb-1">
                <a href="/{{$place->slug}}" class="btn btn-info">
                    <i class="fa fa-home"></i>
                </a>
            </div>
        </li>
    </ul>
@endsection
@section('background')
    $.backstretch('{{ asset('storage/places/background/'.$place->background_img) }}');
@endsection