@extends('web.places.templates.layout')
@section('content_place')
    <ul>
        <li class="border-0">

            <div class="col-md-12 d-flex justify-content-center">
                <img height="175px" src="{{asset('/storage/places/logo/'.$place->logo)}}" alt="Logo"
                     class="py-0 mb-1">
            </div>
            <div class="col-md-12">
                <div style="text-align:center">
                    <h3 class="font-color">{{ $place->file_label }}</h3>
                    <iframe id="file1"
                            src="https://docs.google.com/gview?embedded=true&url=https://www.entrasinpapel.com/storage/places/menu/{{$place->file_name}}&embedded=true"
                            height="350px" width="100%" style="border: 0;"></iframe>
                </div>
                <div class="row">
                    <div class="col-md-12 d-flex justify-content-center mb-3  mb-3">
                        <a href="https://www.entrasinpapel.com/storage/places/menu/{{$place->file_name}}"><i
                                class="fas fa-info-circle font-color"> ¿No ves nada? Haz click aquí</i></a>
                    </div>
                </div>
                <div class="row pb-3">
                    <div class="col-md-6">
                        <a href="{{ asset('storage/places/menu/'.$place->file_name) }}" target="_blank"
                           class="btn btn-primary mr-0">
                            <i class="fas fa-bars"></i> {{ $place->file_label }}
                        </a>
                    </div>
                    @if ($place->file_extra)
                    <div class="col-md-6">
                        <a href="{{ asset('storage/places/menu/'.$place->file_extra) }}" target="_blank" class="btn btn-primary mr-0">
                            <i class="fas fa-bars"></i> {{ $place->file_extra_label }}
                        </a>
                    </div>
                    @endif
                </div>

                <div class="row">
                    @foreach ($place->files->where('active', 1)->sortBy('sort') as $key => $file)
                        <div class="col-md-6 pb-3">
                            <a href="{{ asset('storage/places/'.$place->slug.'/files/'.$file->file_name) }}" target="_blank"
                               class="btn btn-primary mr-0">
                                <i class="fas fa-bars"></i> {{ $file->file_label }}
                            </a>
                        </div>
                    @endforeach
                </div>
                <div class="row ">
                    <div class="col-md-12">
                        <a href="/{{$place->slug}}" class="btn btn-info">
                            <i class="fa fa-home"></i>
                        </a>
                    </div>
                </div>
            </div>
        </li>
    </ul>
@endsection
@section('background')
    $.backstretch('{{ asset('storage/places/background/'.$place->background_img) }}');
@endsection
