@extends('web.places.templates.layout')
@section('head')
    <style>
        .btn-warning {
            width: 100%;

        }
        .btn {
            font-weight: 600    ;
            border-radius: 13px;

        }
        .event-category .category .text {
            color: #fff;
            padding-left: 0px;
        }
    </style>
@endsection
@section('content_place')
    <ul>
        <li class="border-0">

            <div class="col-md-12 d-flex justify-content-center">
                <img height="175px" src="{{asset('/storage/places/logo/'.$place->logo)}}" alt="Logo"
                     class="py-0 mb-1">

            </div>
            <div class="col-md-12 d-flex justify-content-center py-0 mb-1">
                <h2 class="font-color"><b>Bienvenido</b></h2>
            </div>
            <div class="col-md-2 d-flex justify-content-center mt-4 mb-1">
                @if ($place->facebook)
                    <a href="{{ $place->facebook }}"><span class="shart"><i class="fab fa-1x fa-facebook-f icon"></i></span></a>
                @endif
                @if($place->instagram)
                    <a href="{{ $place->instagram }}"><span class="shart"><i class="fab fa-1x fa-instagram icon"></i></span></a>
                @endif
                @if($place->twitter)
                    <a href="{{ $place->twitter }}"><span class="shart"><i class="fab fa-1x fa-twitter icon"></i></span></a>
                @endif
                @if($place->whatsapp)
                    <a href="{{ $place->whatsapp }}"><span class="shart"><i class="fab fa-1x fa-whatsapp icon"></i></span></a>
                @endif
            </div>

        </li>
        <li class="border-0 p-0">

            <div class="event-category">
                <a href="@if($place->id == 1 || $place->id == 2 || $place->id == 4)
                {{ route('web.places.showfile', $place->slug) }} @else {{ route('web.places.register', $place->slug) }} @endif">
                    <div class="category d-flex justify-content-center align-items-center cine">
                        <h3 class="text">Registro de personas</h3>
                    </div>
                </a>
            </div>
            <br>
            <div class="event-category">
                <a href="{{ route('web.places.reservation', $place->slug) }}">
                    <div class="category d-flex justify-content-center align-items-center cine">
                        <h3 class="text">Reserva de mesas</h3>
                    </div>
                </a>
            </div>
            <br>
            <div class="event-category">
                <a href="{{ route('web.places.suggestion', $place->slug) }}">
                    <div class="category d-flex justify-content-center align-items-center cine">
                        <h3 class="text">Buzón de sugerencias</h3>
                    </div>
                </a>
            </div>
        </li>
    </ul>
@endsection
@section('background')
    $.backstretch('{{ asset('storage/places/background/'.$place->background_img) }}');
@endsection