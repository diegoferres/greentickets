@extends('layout.app')

    @section('content')

    <div class="container search-events">
        <div class="row">
            <div class="col-lg-11">
                {!! Form::open(['route' => 'web.search', 'method' => 'get']) !!}
                <div class="form-group d-flex flex-column flex-md-row mt-5 pb-lg-5">

                    <h2>Encuentra tu proximo evento</h2>

                        <input id="search" name="q" type="text" class="form-control" placeholder="Buscar..." value="{{ $q }}" autofocus>
                        <a class="btn btn-tertiary d-block d-md-none mt-3" data-toggle="collapse" href="#collapseFilter" role="button" aria-expanded="false" aria-controls="collapseExample">
                            <i class="fas fa-filter mr-2"></i> Filtrar Busqueda
                        </a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="d-md-block collapse" id="collapseFilter">
                    <h3 class="pt-4">Filtra tu busqueda</h3>
                    <div class="form-group">
                        <h5 class="mb-2 pt-3">Precio</h5>
                        <div class="custom-control custom-checkbox py-2">
                            <input type="checkbox" class="custom-control-input" id="checkFree">
                            <label class="custom-control-label" for="checkFree">Gratis</label>
                        </div>
                        <div class="custom-control custom-checkbox py-2">
                            <input type="checkbox" class="custom-control-input" id="checkPrice">
                            <label class="custom-control-label" for="checkPrice">Pago</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <h5 class="mb-2 pt-3">Fecha</h5>
                        <div class="custom-control custom-checkbox py-2">
                            <input type="checkbox" class="custom-control-input" id="checkHoy">
                            <label class="custom-control-label" for="checkHoy">Hoy</label>
                        </div>
                        <div class="custom-control custom-checkbox py-2">
                            <input type="checkbox" class="custom-control-input" id="checkMana">
                            <label class="custom-control-label" for="checkMana">Mañana</label>
                        </div>
                        <div class="custom-control custom-checkbox py-2">
                            <input type="checkbox" class="custom-control-input" id="checkPm">
                            <label class="custom-control-label" for="checkPm">Pasado mañana</label>
                        </div>
                        <div class="custom-control custom-checkbox py-2">
                            <input type="checkbox" class="custom-control-input" id="checkEs">
                            <label class="custom-control-label" for="checkEs">Esta Semana</label>
                        </div>
                        <div class="custom-control custom-checkbox py-2">
                            <input type="checkbox" class="custom-control-input" id="checkEm">
                            <label class="custom-control-label" for="checkEm">Este mes</label>
                        </div>
                        <div class="custom-control custom-checkbox py-2">
                            <input type="checkbox" class="custom-control-input" id="checkEa">
                            <label class="custom-control-label" for="checkEa">Este año</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <h5 class="mb-2 pt-3">Categorias</h5>
                        <div class="custom-control custom-checkbox py-2">
                            <input type="checkbox" class="custom-control-input" id="checkMus">
                            <label class="custom-control-label" for="checkMus">Música</label>
                        </div>
                        <div class="custom-control custom-checkbox py-2">
                            <input type="checkbox" class="custom-control-input" id="checkTea">
                            <label class="custom-control-label" for="checkTea">Teatro</label>
                        </div>
                        <div class="custom-control custom-checkbox py-2">
                            <input type="checkbox" class="custom-control-input" id="checkCur">
                            <label class="custom-control-label" for="checkCur">Cursos</label>
                        </div>
                        <div class="custom-control custom-checkbox py-2">
                            <input type="checkbox" class="custom-control-input" id="checkCat1">
                            <label class="custom-control-label" for="checkCat1">Categoria</label>
                        </div>
                        <div class="custom-control custom-checkbox py-2">
                            <input type="checkbox" class="custom-control-input" id="checkCat2">
                            <label class="custom-control-label" for="checkCat2">Categoria</label>
                        </div>
                        <div class="custom-control custom-checkbox py-2">
                            <input type="checkbox" class="custom-control-input" id="checkCat3">
                            <label class="custom-control-label" for="checkCat3">Categoria</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9 mt-3 events">
                @foreach($events as $event)
                    {{--<div class="col-md-6 col-lg-3 mb-4 filterDiv {{ strtolower($event->category->category_name) }}">
                        <a href="{{ route('web.event.show', $event->event_slug) }}">
                            <div class="card">
                                <div class="card-img-top">
                                    <img class="card-img-top" src="{{asset('storage/'.$event->event_cover)}}" alt="{{ $event->event_name }}">
                                </div>
                                <div class="card-body">
                                    @php(setlocale(LC_TIME,"es_ES.UTF-8"))
                                    @foreach ($event->dates as $date)
                                        <span class="date">{{ strftime('%d, %B', strtotime($date->event_date)).', '.strftime('%H:%M', strtotime($date->event_time_start)) }} Hs</span>
                                    @endforeach
                                    <h3 class="card-title">{{ $event->event_name }}</h3>
                                    @if ($event->venue)
                                        <p class="card-text">{{ $event->venue->venue_name }}, Asunción, Paraguay</p>
                                    @endif
                                </div>
                            </div>
                        </a>
                    </div>--}}
                    <a href="{{ route('web.event.show', $event->event_slug) }}">
                        <div class="card mt-3">
                            <div class="row no-gutters">
                                <div class="col-md-4">
                                    <div class="card-img-top">
                                        <img class="img-fluid" src="{{asset('storage/'.$event->covers->first()->url.$event->covers->first()->file_name)}}" alt="{{ $event->event_name }}">
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="card-body mt-lg-3">
                                        @php(setlocale(LC_TIME,"es_ES.UTF-8"))
                                        @foreach ($event->dates as $date)
                                            <span class="date">{{ strftime('%d, %B', strtotime($date->event_date)).', '.strftime('%H:%M', strtotime($date->event_time_start)) }} Hs</span>
                                        @endforeach
                                        <h3 class="card-title">{{ $event->event_name }}</h3>
                                        @if ($event->venue)
                                            <p class="card-text">{{ $event->venue->venue_name }}, Asunción, Paraguay</p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                @endforeach
                {{--<a href="#">
                    <div class="card mt-3">
                        <div class="row no-gutters">
                            <div class="col-md-4">
                                <div class="card-img-top">
                                    <img class="img-fluid" src="{{asset('/img/concert.jpg')}}" alt="Card image cap">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="card-body mt-lg-3">
                                    <span class="date position-relative mt-1">25, febrero, 21:00hs</span>
                                    <h2 class="card-title mt-2 mb-1">Blockchain Express Webinar</h2>
                                    <p class="card-text">Sheraton, Asunción, Paraguay</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="#">
                    <div class="card mt-3">
                        <div class="row no-gutters">
                            <div class="col-md-4">
                                <div class="card-img-top">
                                    <img class="img-fluid" src="{{asset('/img/concert.jpg')}}" alt="Card image cap">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="card-body mt-lg-3">
                                    <span class="date position-relative mt-1">25, febrero, 21:00hs</span>
                                    <h2 class="card-title mt-2 mb-1">Blockchain Express Webinar</h2>
                                    <p class="card-text">Sheraton, Asunción, Paraguay</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="#">
                    <div class="card mt-3">
                        <div class="row no-gutters">
                            <div class="col-md-4">
                                <div class="card-img-top">
                                    <img class="img-fluid" src="{{asset('/img/concert.jpg')}}" alt="Card image cap">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="card-body mt-lg-3">
                                    <span class="date position-relative mt-1">25, febrero, 21:00hs</span>
                                    <h2 class="card-title mt-2 mb-1">Blockchain Express Webinar</h2>
                                    <p class="card-text">Sheraton, Asunción, Paraguay</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="#">
                    <div class="card mt-3">
                        <div class="row no-gutters">
                            <div class="col-md-4">
                                <div class="card-img-top">
                                    <img class="img-fluid" src="{{asset('/img/concert.jpg')}}" alt="Card image cap">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="card-body mt-lg-3">
                                    <span class="date position-relative mt-1">25, febrero, 21:00hs</span>
                                    <h2 class="card-title mt-2 mb-1">Blockchain Express Webinar</h2>
                                    <p class="card-text">Sheraton, Asunción, Paraguay</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="#">
                    <div class="card mt-3">
                        <div class="row no-gutters">
                            <div class="col-md-4">
                                <div class="card-img-top">
                                    <img class="img-fluid" src="{{asset('/img/concert.jpg')}}" alt="Card image cap">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="card-body mt-lg-3">
                                    <span class="date position-relative mt-1">25, febrero, 21:00hs</span>
                                    <h2 class="card-title mt-2 mb-1">Blockchain Express Webinar</h2>
                                    <p class="card-text">Sheraton, Asunción, Paraguay</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>--}}
            </div>
        </div>
    </div>
@stop
