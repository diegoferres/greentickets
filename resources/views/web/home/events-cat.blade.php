<div class="events mt-5 pt-3">
    <h2 class="mb-4">Eventos publicados recientemente</h2>
    <div class="pb-5" id="myBtnContainer">
        <button class="btn-tertiary btn mb-1" onclick="filterSelection('all')"> Todos</button>
        @foreach ($categories as $category)
            <button class="btn-tertiary btn mb-1" onclick="filterSelection('{{strtolower($category->category_name)}}')"> {{ $category->category_name }}</button>
        @endforeach
    {{--<button class="btn-tertiary btn" onclick="filterSelection('fiestas')"> Fiestas</button>
        <button class="btn-tertiary btn" onclick="filterSelection('deportes')"> Deportes</button>
        <button class="btn-tertiary btn" onclick="filterSelection('online')"> Online</button>
        <button class="btn-tertiary btn" onclick="filterSelection('gratis')"> Gratis</button>--}}
    </div>
    <div class="row pb-3">
        @foreach($events->sortByDesc('created_at') as $event)
            {{--<div class="col-md-6 col-lg-3 mb-4 filterDiv {{ strtolower($event->category->category_name) }}">
                <a href="{{ route('web.event.show', $event->event_slug) }}">
                    <div class="card">
                        <div class="card-img-top">
                            <img class="img-fluid" src="{{asset('storage/events/cover/'.$event->event_cover)}}" alt="{{ $event->event_name }}">
                        </div>
                        <div class="card-body">
                            @php(setlocale(LC_TIME,"es_ES.UTF-8"))
                            @foreach ($event->dates as $date)
                                <span class="date">{{ strftime('%d, %B', strtotime($date->event_date)).', '.strftime('%H:%M', strtotime($date->event_time_start)) }} Hs</span>
                            @endforeach
                            <h3 class="card-title">{{ $event->event_name }}</h3>
                            @if ($event->venue)
                                <p class="card-text">{{ $event->venue->venue_name }}, Asunción, Paraguay</p>
                            @endif
                        </div>
                    </div>
                </a>
            </div>--}}

            <div class="col-md-3 mb-4 filterDiv {{ strtolower($event->category->category_name) }}">
                <a href="{{ route('web.event.show', $event->event_slug) }}">
                    <div class="card">
                        <img class="card-img-top" src="{{asset('storage/'.$event->covers->first()->url.$event->covers->first()->file_name)}}" alt="{{ $event->event_name }}">
                        <div class="card-body">
                            @php(setlocale(LC_TIME,"es_ES.UTF-8"))
                            @if ($event->event_frecuency == 1)
                                <span class="date">{{ strftime('%d, %B', strtotime($event->event_date_start)).', '.strftime('%H:%M', strtotime($event->event_time_start)) }} Hs</span>
                            @elseif ($event->event_frecuency == 2)
                                <span class="date">{{ strftime('%d, %B', strtotime($event->dates->first()->event_date)).', '.strftime('%H:%M', strtotime($event->dates->first()->event_time_start)) }} Hs</span>
                            @endif
                            {{--@foreach ($event->dates as $date)
                                <span class="date">{{ strftime('%d, %B', strtotime($date->event_date)).', '.strftime('%H:%M', strtotime($date->event_time_start)) }} Hs</span>
                            @endforeach--}}

                            {{--<span class="date">25, febrero, 21:00hs</span>--}}
                            <h3 class="card-title">{{ $event->event_name }}</h3>
                            @if ($event->event_format == 2 || $event->event_format == 3)
                                <br>
                                <br>
                                <span class="date">Online</span>
                            @endif
                            @if ($event->venue)
                                <p class="card-text">{{ $event->venue->venue_name }}, Asunción, Paraguay</p>
                                {{--<p class="card-text">Sheraton, Asunción, Paraguay</p>--}}
                            @endif
                        </div>
                    </div>
                </a>
            </div>
        @endforeach
        {{--<div class="col-md-3 mb-4 filterDiv Curso">

            <a href="{{ route('ejemplo') }}">
                <div class="card">
                    <img class="card-img-top" src="{{asset('/img/Post-05-06.png')}}" alt="Card image cap">
                    <div class="card-body">
                        <span class="date">25, febrero, 21:00hs</span>
                        <h3 class="card-title">Blockchain Express Webinar</h3>
                        <p class="card-text">Sheraton, Asunción, Paraguay</p>
                    </div>
                </div>
            </a>
        </div>--}}
        {{--<div class="col-md-3 mb-4 filterDiv conciertos gratis">
            <a href="#">
                <div class="card">
                    <img class="card-img-top" src="{{asset('/img/party.jpg')}}" alt="Card image cap">
                    <div class="card-body">
                        <span class="date">25, febrero, 21:00hs</span>
                        <h3 class="card-title">Blockchain Express Webinar</h3>
                        <p class="card-text">Sheraton, Asunción, Paraguay</p>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-3 mb-4 filterDiv conciertos">
            <a href="#">
                <div class="card">
                    <img class="card-img-top" src="{{asset('/img/party.jpg')}}" alt="Card image cap">
                    <div class="card-body">
                        <span class="date">25, febrero, 21:00hs</span>
                        <h3 class="card-title">Blockchain Express Webinar</h3>
                        <p class="card-text">Sheraton, Asunción, Paraguay</p>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-3 mb-4 filterDiv conciertos">
            <a href="#">
                <div class="card">
                    <img class="card-img-top" src="{{asset('/img/party.jpg')}}" alt="Card image cap">
                    <div class="card-body">
                        <span class="date">25, febrero, 21:00hs</span>
                        <h3 class="card-title">Blockchain Express Webinar</h3>
                        <p class="card-text">Sheraton, Asunción, Paraguay</p>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-3 mb-4 filterDiv deportes">
            <a href="#">
                <div class="card">
                    <img class="card-img-top" src="{{asset('/img/party.jpg')}}" alt="Card image cap">
                    <div class="card-body">
                        <span class="date">25, febrero, 21:00hs</span>
                        <h3 class="card-title">Blockchain Express Webinar</h3>
                        <p class="card-text">Sheraton, Asunción, Paraguay</p>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-3 mb-4 filterDiv online">
            <a href="#">
                <div class="card">
                    <img class="card-img-top" src="{{asset('/img/party.jpg')}}" alt="Card image cap">
                    <div class="card-body">
                        <span class="date">25, febrero, 21:00hs</span>
                        <h3 class="card-title">Blockchain Express Webinar</h3>
                        <p class="card-text">Sheraton, Asunción, Paraguay</p>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-3 mb-4 filterDiv online gratis">
            <a href="#">
                <div class="card">
                    <img class="card-img-top" src="{{asset('/img/party.jpg')}}" alt="Card image cap">
                    <div class="card-body">
                        <span class="date">25, febrero, 21:00hs</span>
                        <h3 class="card-title">Blockchain Express Webinar</h3>
                        <p class="card-text">Sheraton, Asunción, Paraguay</p>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-3 mb-4 filterDiv fiestas">
            <a href="#">
                <div class="card">
                    <img class="card-img-top" src="{{asset('/img/party.jpg')}}" alt="Card image cap">
                    <div class="card-body">
                        <span class="date">25, febrero, 21:00hs</span>
                        <h3 class="card-title">Blockchain Express Webinar</h3>
                        <p class="card-text">Sheraton, Asunción, Paraguay</p>
                    </div>
                </div>
            </a>
        </div>--}}
    </div>
</div>
