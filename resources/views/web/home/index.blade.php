@extends('layout.app')
@section('meta')
    <meta name="description"
          content="Conoce a tus artistas favoritos, equipos deportivos y fiestas"/>
    <meta property="og:title" content="Entra Sin Papel">
    <meta property="og:type" content="website"/>
    <meta property="og:description" content="Conoce a tus artistas favoritos, equipos deportivos y fiestas">
    <meta property="og:image" content="{{ asset('/img/icon.png') }}">
    <meta property="og:url" content="/">
@endsection
@section('head')

@endsection
@section('content')


    <section class="jumbotron text-center">
        <div class="container">
            <h1>Album example</h1>
            <p class="lead text-muted">Something short and leading about the collection below—its contents, the creator, etc. Make it short and sweet, but not too short so folks don’t simply skip over it entirely.</p>
            <p>
                <a href="#" class="btn btn-primary my-2">Main call to action</a>
                <a href="#" class="btn btn-secondary my-2">Secondary action</a>
            </p>
        </div>
    </section>


    <div class="container-fluid">
        <div class="row justify-content-center mt-5">

            <div class="col-md-7 text-center mt-4">
                <h1>Haz tu sueño realidad</h1>
                <p>Conoce a tus artistas favoritos, equipos deportivos y fiestas</p>
                <form class="typeahead" role="search">
                    <div class="search pt-3 mb-5">
                        <i class="fas fa-search icon" style="z-index: 2000;"></i>
                        <input type="search" name="q" autocomplete="off"
                               placeholder="Buscar eventos, organizaciones, etc."
                               aria-label="Buscar eventos, organizaciones, etc." aria-describedby="btnsearch"
                               class="search-input">
{{--                        <div class="tt-menu" style="position: absolute; left: 0px;">--}}
{{--                            <div class="tt-dataset tt-dataset-usersList">--}}
{{--                                <div class="list-group">--}}
{{--                                    <a href="#" class="list-group-item list-group-item-action">--}}
{{--                                        Cras justo odio--}}
{{--                                    </a>--}}
{{--                                    <a href="#" class="list-group-item list-group-item-action">Dapibus ac facilisis in</a>--}}
{{--                                    <a href="#" class="list-group-item list-group-item-action">Morbi leo risus</a>--}}
{{--                                    <a href="#" class="list-group-item list-group-item-action">Porta ac consectetur ac</a>--}}
{{--                                    <a href="#" class="list-group-item list-group-item-action disabled">Vestibulum at eros</a>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <input type="search" name="q" class="form-control search-input" placeholder="Search" autocomplete="off">--}}
                    </div>

{{--                    <div class="form-group">--}}
{{--                        <input type="search" name="q" class="form-control" placeholder="Search" autocomplete="off">--}}
{{--                    </div>--}}
                </form>

            </div>
        </div>
    </div>

    <div class="mt-5">
        <div class="container">
            <h2 class="mb-4">Buscar eventos por Categorías</h2>
        </div>
        <div class="glider-contain">
            <div class="glider">
                @foreach ($categories as $key => $category)
                    @if ($key == 0)
                        <div class="ml-glider event-category">
                            <a href="#">
                                <div class="category d-flex align-items-center {{ $category->category_name }}">
                                    <h3 class="text">{{ $category->category_name }}</h3>
                                </div>
                            </a>
                        </div>
                    @else
                        <div class="event-category">
                            <a href="#">
                                <div class="category d-flex align-items-center {{ $category->category_name }}">
                                    <h3 class="text">{{ $category->category_name }}</h3>
                                </div>
                            </a>
                        </div>
                    @endif
                @endforeach
            </div>
            <div class="d-none d-md-flex justify-content-center">
                <button role="button" aria-label="Previous" class="glider-prev"><i class="fas fa-chevron-left"></i>
                </button>
                <button role="button" aria-label="Next" class="glider-next"><i class="fas fa-chevron-right"></i>
                </button>
            </div>
        </div>
    </div>
    <div class="container">
        @include('web.home.events-cat')
        <div class="events mt-5 pt-3">
            <h2 class="mb-4">Eventos destacados</h2>
            <div class="row">
{{--                @foreach($events->sortByDesc('created_at') as $event)--}}
{{--                    <div class="col-md-6 col-lg-3 mb-4 filterDiv {{ strtolower($event->category->category_name) }}">--}}
{{--                        <a href="{{ route('web.event.show', $event->event_slug) }}">--}}
{{--                            <div class="card">--}}
{{--                                <div class="card-img-top">--}}
{{--                                    <img class="img-fluid" src="{{asset('storage/events/cover/'.$event->event_cover)}}" alt="{{ $event->event_name }}">--}}
{{--                                </div>--}}
{{--                                <div class="card-body">--}}

{{--                                    @php(setlocale(LC_TIME,"es_ES.UTF-8"))--}}
{{--                                    @foreach ($event->dates as $date)--}}
{{--                                        <span class="date">{{ strftime('%d, %B', strtotime($date->event_date)).', '.strftime('%H:%M', strtotime($date->event_time_start)) }} Hs</span>--}}
{{--                                    @endforeach--}}
{{--                                    <h3 class="card-title">{{ $event->event_name }}</h3>--}}
{{--                                    @if ($event->venue)--}}
{{--                                        <p class="card-text">{{ $event->venue->venue_name }}, Asunción, Paraguay</p>--}}
{{--                                    @endif--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                    <div class="col-md-6 col-lg-3 mb-4">--}}

{{--                        <a href="{{ route('web.event.show', $event->event_slug) }}">--}}
{{--                            <div class="card">--}}
{{--                                <div class="card-img-top">--}}
{{--                                    <img class="img-fluid" src="{{asset('storage/'.$event->covers->first()->url.$event->covers->first()->file_name)}}" alt="{{ $event->event_name }}">--}}
{{--                                </div>--}}
{{--                                <div class="card-body">--}}
{{--                                    @php(setlocale(LC_TIME,"es_ES.UTF-8"))--}}
{{--                                    @if ($event->event_frecuency == 1)--}}
{{--                                        <span class="date">{{ strftime('%d, %B', strtotime($event->event_date_start)).', '.strftime('%H:%M', strtotime($event->event_time_start)) }} Hs</span>--}}
{{--                                    @elseif ($event->event_frecuency == 2)--}}
{{--                                    <span class="date">{{ strftime('%d, %B', strtotime($event->dates->first()->event_date)).', '.strftime('%H:%M', strtotime($event->dates->first()->event_time_start)) }} Hs</span>--}}
{{--                                    @endif--}}
{{--                                    <span class="date">{{ $event->dates->first()->event_date }}, 21:00hs</span>--}}
{{--                                    <h3 class="card-title">{{ $event->event_name }}</h3>--}}
{{--                                    @if ($event->venue)--}}
{{--                                        <p class="card-text">{{ $event->venue->venue_name }}, Asunción, Paraguay</p>--}}
{{--                                    @endif--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                @endforeach--}}
                <div class="col-md-6 col-lg-3 mb-4">
                    <a href="{{ route('ejemplo') }}">
                        <div class="card">
                            <div class="card-img-top">
                                <img class="img-fluid" src="{{asset('/img/Post-05-06.png')}}" alt="Card image cap">
                            </div>
                            <div class="card-body">
                                <span class="date">25, febrero, 21:00hs</span>
                                <h2 class="card-title">NEW GAC GS5</h2>
                                <p class="card-text">Sheraton, Asunción, Paraguay</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-3 mb-4">
                    <a href="#">
                        <div class="card">
                            <div class="card-img-top">
                                <img class="img-fluid" src="{{asset('/img/concert.jpg')}}" alt="Card image cap">
                            </div>
                            <div class="card-body">
                                <span class="date">25, febrero, 21:00hs</span>
                                <h2 class="card-title">Blockchain Express Webinar</h2>
                                <p class="card-text">Sheraton, Asunción, Paraguay</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-3 mb-4">
                    <a href="#">
                        <div class="card">
                            <div class="card-img-top">
                                <img class="img-fluid" src="{{asset('/img/concert.jpg')}}" alt="Card image cap">
                            </div>
                            <div class="card-body">
                                <span class="date">25, febrero, 21:00hs</span>
                                <h2 class="card-title">Blockchain Express Webinar</h2>
                                <p class="card-text">Sheraton, Asunción, Paraguay</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-3 mb-4">
                    <a href="#">
                        <div class="card">
                            <div class="card-img-top">
                                <img class="img-fluid" src="{{asset('/img/concert.jpg')}}" alt="Card image cap">
                            </div>
                            <div class="card-body">
                                <span class="date">25, febrero, 21:00hs</span>
                                <h2 class="card-title">Blockchain Express Webinar</h2>
                                <p class="card-text">Sheraton, Asunción, Paraguay</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-3 mb-4">
                    <a href="#">
                        <div class="card">
                            <div class="card-img-top">
                                <img class="img-fluid" src="{{asset('/img/concert.jpg')}}" alt="Card image cap">
                            </div>
                            <div class="card-body">
                                <span class="date">25, febrero, 21:00hs</span>
                                <h2 class="card-title">Blockchain Express Webinar</h2>
                                <p class="card-text">Sheraton, Asunción, Paraguay</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-3 mb-4">
                    <a href="#">
                        <div class="card">
                            <div class="card-img-top">
                                <img class="img-fluid" src="{{asset('/img/concert.jpg')}}" alt="Card image cap">
                            </div>
                            <div class="card-body">
                                <span class="date">25, febrero, 21:00hs</span>
                                <h2 class="card-title">Blockchain Express Webinar</h2>
                                <p class="card-text">Sheraton, Asunción, Paraguay</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-3 mb-4">
                    <a href="#">
                        <div class="card">
                            <div class="card-img-top">
                                <img class="img-fluid" src="{{asset('/img/concert.jpg')}}" alt="Card image cap">
                            </div>
                            <div class="card-body">
                                <span class="date">25, febrero, 21:00hs</span>
                                <h2 class="card-title">Blockchain Express Webinar</h2>
                                <p class="card-text">Sheraton, Asunción, Paraguay</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-3 mb-4">
                    <a href="#">
                        <div class="card">
                            <div class="card-img-top">
                                <img class="img-fluid" src="{{asset('/img/concert.jpg')}}" alt="Card image cap">
                            </div>
                            <div class="card-body">
                                <span class="date">25, febrero, 21:00hs</span>
                                <h2 class="card-title">Blockchain Express Webinar</h2>
                                <p class="card-text">Sheraton, Asunción, Paraguay</p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('scripts')
    {{--<link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js-bootstrap-css/1.2.1/typeaheadjs.min.css">--}}
    <style rel="stylesheet">
        .tt-menu {
            width: 100%;
        }
    </style>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins  and Typeahead) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    {{-- <!-- Bootstrap JS -->
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>--}}
    <!-- Typeahead.js Bundle -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
    <script>
        new Glider(document.querySelector('.glider'), {
            slidesToShow: 4.5,
            dots: '#scrollLockDelay',
            draggable: true,
            arrows: {
                next: '.glider-next',
                prev: '.glider-prev'
            }
        });
    </script>
    <script>
        filterSelection("all")

        function filterSelection(c) {
            var x, i;
            x = document.getElementsByClassName("filterDiv");
            if (c == "all") c = "";
            for (i = 0; i < x.length; i++) {
                w3RemoveClass(x[i], "show");
                if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");
            }
        }

        function w3AddClass(element, name) {
            var i, arr1, arr2;
            arr1 = element.className.split(" ");
            arr2 = name.split(" ");
            for (i = 0; i < arr2.length; i++) {
                if (arr1.indexOf(arr2[i]) == -1) {
                    element.className += " " + arr2[i];
                }
            }
        }

        function w3RemoveClass(element, name) {
            var i, arr1, arr2;
            arr1 = element.className.split(" ");
            arr2 = name.split(" ");
            for (i = 0; i < arr2.length; i++) {
                while (arr1.indexOf(arr2[i]) > -1) {
                    arr1.splice(arr1.indexOf(arr2[i]), 1);
                }
            }
            element.className = arr1.join(" ");
        }

        // Add active class to the current button (highlight it)
        var btnContainer = document.getElementById("myBtnContainer");
        var btns = btnContainer.getElementsByClassName("btn");
        for (var i = 0; i < btns.length; i++) {
            btns[i].addEventListener("click", function () {
                var current = document.getElementsByClassName("active");
                current[0].className = current[0].className.replace(" active", "");
                this.className += " active";
            });
        }

        $(document).ready(function () {
            jQuery(document).ready(function ($) {
                // Set the Options for "Bloodhound" suggestion engine
                var engine = new Bloodhound({
                    remote: {
                        url: '/find?q=%QUERY%',
                        wildcard: '%QUERY%'
                    },
                    datumTokenizer: Bloodhound.tokenizers.toString('q'),
                    queryTokenizer: Bloodhound.tokenizers.toString
                });

                $(".search-input").typeahead({
                    hint: true,
                    highlight: true,
                    minLength: 1
                }, {
                    limit: 5,
                    source: engine.ttAdapter(),

                    // This will be appended to "tt-dataset-" to form the class name of the suggestion menu.
                    name: 'usersList',
                    display: 'data.event_name',

                    // the key from the array we want to display (name,id,email,etc...)
                    templates: {
                        empty: [
                            '<div class="list-group search-results-dropdown"><div class="list-group-item">Sin resultados.</div></div>'
                        ],
                        /*header: [
                            /!*'<div class="list-group search-results-dropdown">'*!/
                            '<div class="list-group">'
                        ],*/
                        suggestion: function (data) {
                            console.log('DATA:', data)
                            //return '<a href="/event/detail/' + data.id + '" class="list-group-item">' + data.event_name + '- @' + data.category.category_name + '</a>'
                            return '<a href="/event/detail/' + data.id + '"><li class="list-group-item">'+data.event_name+ ' | '+data.category.category_name+'</li></a>'
                        },
                    }
                });
            });

        })
    </script>
@endsection
