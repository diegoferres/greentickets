<main class="d-flex justify-content-center">
    <div class="col-sm-10 col-md-8 col-xl-6 mt-5">
        <p>Pasos <b class="text-primary">1 de 4</b></p>
        <div class="progress mb-4">
            <div class="progress-bar bg-primary" role="progressbar" style="width: 25%" aria-valuenow="25"
                 aria-valuemin="0" aria-valuemax="100">
            </div>
        </div>
        <div class="pt-4 pb-5">
            <h2 class="mb-4">Información del Evento {{ $step }}</h2>
            {{--{!! Form::open(['route' => 'web.event.step2', 'method' => 'post']) !!}--}}

            <div class="form-group mb-4">
                <input wire:model="event_name" id="name" name="event_name"
                       type="text"
                       placeholder="Nombre" autofocus=""
                       class="form-control @error('event_name') is-invalid @enderror">
                @error('event_name')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group mb-4">
                <input wire:model="event_organizer" id="organizer" name="event_organizer"
                       type="text"
                       placeholder="Organizador" autofocus=""
                       class="form-control @error('event_organizer') is-invalid @enderror">
                @error('event_organizer')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group mb-4">
                {!! Form::select('event_category', $categories , null,
                    ['class' => $errors->has('event_category') ? 'custom-select form-control is-invalid' : 'custom-select form-control',
                     'placeholder' => 'Categoría',
                     'wire:model' => 'event_category']) !!}
                @error('event_category')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group mb-4">
                <div class="d-flex align-items-center flex-fill mb-3">
                    <input wire:model="addedTag" class="form-control col-md-8" type="text" placeholder="Etiqueta">
                    <button wire:click="addTag" class="btn btn-tertiary ml-4" id="tagCreate">Añadir etiqueta</button>
                </div>
                <div id="tagsList">
                    @isset ($event_tags)
                        @foreach($event_tags as $key => $tag)
                            <span class="badge badge-pill tags p-3 m-2" wire:click="removeTag({{ $key }})">{{ $tag }}<i
                                    class="fas fa-times ml-3"></i></span>
                        @endforeach
                    @endif
                </div>
            </div>
            <div class="form-group float-right">
                <a href="/" class="btn btn-secondary mr-2">Volver</a>
                <button wire:click="secondStep" class="btn btn-primary mr-2">Siguiente
                </button>
            </div>
            {{--{!! Form::close() !!}--}}
        </div>
    </div>
</main>
