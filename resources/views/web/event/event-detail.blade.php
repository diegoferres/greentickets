@extends('layout.app')
@section('title')
    {{ $event->event_name }}
@endsection
@section('meta')
    <meta name="description"
          content="{{ substr(strip_tags($event->event_description),0,158) }}"/>
    <meta property="og:title" content="{{ $event->event_name }} | Entra Sin Papel">
    <meta property="og:type" content="website"/>
    <meta property="og:description" content="{{ substr(strip_tags($event->event_description),0,199) }}">
    <meta property="og:image"
          content="{{asset('storage/'.$event->covers->first()->url.$event->covers->first()->file_name)}}">
    <meta property="og:url" content="/">
    @livewireStyles
@endsection
@section('content')
    <section class="event-public">
        <div class="container">
            <div class="row align-items-lg-center flex-column-reverse flex-lg-row pt-5">
                @if(app('router')->getRoutes()->match(app('request')->create(url()->previous()))->getName() == 'web.event.step4')
                    <div class="col-12">
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            Tu evento se creo con éxito
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                @endif
                <div class="col-lg-6">
                    <div class="cover-event">
                        <div id="carouselExampleInterval" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                @foreach($event->covers as $cover)
                                    <div class="carousel-item @if ($loop->first) active @endif" data-interval="10000">
                                        <img src="{{asset('storage/'.$cover->url.$cover->file_name)}}"
                                             class="d-block w-100" alt="{{ $event->event_name }}">
                                    </div>
                                @endforeach
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleInterval" role="button"
                               data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleInterval" role="button"
                               data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-5 offset-lg-1">
                    @if ($event->event_frecuency == 1)
                        <div class="mb-3">
                            <span class="date"
                                  style="margin-bottom: 50px">De @php(setlocale(LC_TIME,"es_PY.UTF-8")){{ strftime("%A, %d de %B", strtotime($event->event_date_start)) }}, {{ strftime("%H:%M", strtotime($event->event_time_start)) }} Hs.</span>
                        </div>
                        <div>
                            <span
                                class="date">A @php(setlocale(LC_TIME,"es_PY.UTF-8")){{ strftime("%A, %d de %B", strtotime($event->event_date_end)) }}, {{ strftime("%H:%M", strtotime($event->event_time_end)) }} Hs.</span>
                        </div>
                    @elseif($event->event_frecuency == 2)
                        @foreach ($event->dates as $date)
                            @if ($loop->first)
                                <div class="mb-3">
                                    <span
                                        class="date">De @php(setlocale(LC_TIME,"es_PY.UTF-8")){{ strftime("%A, %d de %B", strtotime($date->event_date)) }}</span>
                                </div>
                            @elseif ($loop->last)
                                <div>
                                    <span
                                        class="date">A @php(setlocale(LC_TIME,"es_PY.UTF-8")){{ strftime("%A, %d de %B", strtotime($date->event_date)) }}</span>
                                </div>
                            @endif
                        @endforeach
                    @endif

                    <h1 class="pt-4">{{ $event->event_name }}</h1>
                    <p class="mb-1 pt-4">Organizador</p>
                    <h3>{{ $event->event_organizer }}</h3>
                    <p class="mb-1 pt-2">Categoria</p>
                    <h3>{{ $event->category->category_name }}</h3>
                    @if(app('router')->getRoutes()->match(app('request')->create(url()->previous()))->getName() != 'web.event.step4')
                        <button id="#scrollto" onclick="scrollto()" class="btn btn-primary mt-4 float-left mb-2">
                            Adquirir
                            entrada <i class="fas fa-arrow-down"></i>
                        </button>
                    @endif
                </div>
            </div>
            <div class="row flex-column-reverse flex-lg-row pt-5">
                <div class="col-lg-9">
                    <h3 class="mb-3">Descripción</h3>
                    <div class="container">

                        {!! $event->event_description !!}
                    </div>
                </div>
                <div class="col-lg-3 pb-5 pb-lg-0">
                    <div class="mb-4">
                        <h3 class="mb-2">Ubicación</h3>
                        @isset($event->venue)
                            <p>{{ $event->venue->venue_address }},</p>
                            <p>{{ $event->venue->venue_name }}, Asunción, Paraguay</p>
                        @endisset
                    </div>
                    <div class="mb-4">
                        <h3 class="mb-2">Fechas</h3>
                        @if ($event->event_frecuency == 1)
                            <p>De {{ strftime("%A, %d de %B de %Y", strtotime($event->event_date_start)) }}<br>
                                {{ strftime("%H:%M", strtotime($event->event_time_start)).' hora de Paraguay' }}
                            </p>
                            <p>A {{ strftime("%A, %d de %B de %Y", strtotime($event->event_date_end)) }}<br>
                                {{ strftime("%H:%M", strtotime($event->event_time_end)).' hora de Paraguay' }}
                            </p>
                        @elseif($event->event_frecuency == 2)
                            @foreach ($event->dates as $date)
                                <p>{{ strftime("%A, %d de %B de %Y", strtotime($date->event_date)) }}<br>
                                    {{ strftime("%H:%M", strtotime($date->event_time_start)).'-'.strftime("%H:%M", strtotime($date->event_time_end)).' hora de Paraguay' }}
                                </p>
                            @endforeach
                        @endif
                    </div>
                    <div class="mb-4">
                        <h3 class="mb-2">Etiquetas</h3>
                        @foreach($event->tags as $tag)
                            <a href="#"><span class="badge badge-pill tags p-3 m-1">{{ $tag->event_tag }}</span></a>
                        @endforeach
                    </div>
                    <div class="mb-3">
                        <h3 class="mb-2">Compartir</h3>
                        <!-- Go to www.addthis.com/dashboard to customize your tools -->
                        <div class="addthis_inline_share_toolbox"></div>
                        {{--<a href="#"><span class="shart"><i class="fab fa-facebook-f icon"></i></span></a>
                        <a href="#"><span class="shart"><i class="fab fa-twitter icon"></i></span></a>
                        <a href="#"><span class="shart"><i class="fab fa-whatsapp icon"></i></span></a>--}}
                    </div>
                </div>
            </div>
            @livewire('web.events.tickets', ['event' => $event])
        </div>
    </section>
    <input type="hidden" name="has_error" value="{{ strlen(json_encode($errors->all())) }}">
@stop

@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5bce2c909b6111ef"></script>

    <script>

        function scrollto() {

            $('html, body').animate({
                scrollTop: $("#add_to_cart").offset().top
            }, 1000);
        }

        $(document).ready(function () {

            @if(!$errors->isEmpty())
                $('html, body').animate({
                    scrollTop: $("#add_to_cart").offset().top
                }, 1000);
            @endif

            /*$('select[name="date_id"]').on('change', function () {
                $.ajax({
                    type: "json",
                    method: "GET",
                    url: "/event/{{ $event->id }}/getsectors/"+this.value,
                    success: function (data) {
                        console.log(data)
                        $('select[name="sector_id"]')
                            .find('option')
                            .remove()
                            .end()
                            .append($("<option>Elige una entrada</option>"));

                        $.each(data, function (key, value) {
                            $('select[name="sector_id"]')
                                .append($("<option>" + value + "</option>")
                                    .attr("value", key)
                                    .text(value));
                        });
                    }
                })
            });
            $('input#count, select#sector_id').on('keyup change', function () {

                if ($('select#sector_id option:checked').val() && $('input#count').val()) {

                    var sector_id = $('select#sector_id option:checked').val();
                    var count = $('input#count').val();

                    $.ajax({
                        type: "json",
                        method: "GET",
                        url: "/event/{{--{{ $event->id }}--}}/"+sector_id+"/sum_price/"+count,
                        success: function (data) {
                            $("p#unit_price").text(data.unit_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
                            $("p#subtotal").text(data.subtotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
                        }
                    })
                } else {
                    $("p#unit_price").text(0);
                    $("p#subtotal").text(0);
                }
            });*/
        })

    </script>
    @livewireScripts
@endsection
