
    <main class="d-flex w-100 vh-100 justify-content-center">
        <div class="col-sm-10 col-md-8 col-xl-6 mt-5">
            <p>Pasos <b class="text-primary">2 de 4</b></p>
            <div class="progress mb-4">
                <div class="progress-bar bg-primary" role="progressbar" style="width: 50%" aria-valuenow="25"
                     aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <div class="pt-4 pb-5">
                @if (isset(session('step2')['event_format']) && session('step2')['event_format'] == 1 || session('step2')['event_format'] == 3)
                    <h2 class="mb-4">¿Cómo, cuándo y donde será el Evento?</h2>
                @else
                    <h2 class="mb-4">¿Cómo y cuándo será el Evento?</h2>
                @endif
                {{--{!! Form::open(['route' => 'web.event.step3', 'method' => 'post']) !!}--}}
                <div class="row mb-4">
                    <div class="col-md-12">
                        <div class="form-group">
                            <h5 class="mb-2 py-2">Formato de evento</h5>

                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio"
                                       class="custom-control-input @error('event_format') is-invalid @enderror"
                                       id="1" name="event_format" value="1"
                                    {{ isset(session('step2')['event_format']) ?
                         session('step2')['event_format'] == 1 ? 'checked' : '' :
                         old('event_format') ? old('event_format') == 1 ? 'checked' : '' : 'checked' }}>
                                <label for="1" class="custom-control-label">Presencial</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio"
                                       class="custom-control-input @error('event_format') is-invalid @enderror"
                                       id="2" name="event_format" value="2"
                                    {{ isset(session('step2')['event_format']) ?
                          session('step2')['event_format'] == 2 ? 'checked' : '' :
                          old('event_format') == 2 ? 'checked' : '' }}>
                                <label for="2" class="custom-control-label">Online</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio"
                                       class="custom-control-input @error('event_format') is-invalid @enderror"
                                       id="3" name="event_format" value="3"
                                    {{ isset(session('step2')['event_format']) ?
                           session('step2')['event_format'] == 3 ? 'checked' : '' :
                           old('event_format') == 3 ? 'checked' : '' }}>
                                <label for="3" class="custom-control-label">Online/Presencial</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="eventVenue" {{ isset(session('step2')['event_format']) ?
                            session('step2')['event_format'] == 1 || session('step2')['event_format'] == 3 ? '' : 'style=display:none' :
                            old('event_format') ? old('event_format') == 1 || old('event_format') == 3 ? '' : 'style=display:none' : '' }}>
                    <div class="form-group mb-4">
                        <input id="venue_name" name="venue_name"
                               value="{{ isset(session('step2')['venue_name']) ? session('step2')['venue_name'] : old('venue_name') }}"
                               type="text"
                               placeholder="Nombre del lugar" autofocus=""
                               class="form-control @error('venue_name') is-invalid @enderror">
                        @error('venue_name')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group mb-4">
                        <input id="venue_address" name="venue_address"
                               value="{{ isset(session('step2')['venue_address']) ? session('step2')['venue_address'] : '' }}"
                               type="text" autofocus=""
                               class="form-control @error('venue_address') is-invalid @enderror"
                               placeholder="Dirección del lugar">
                        @error('venue_address')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>
                {{--{{ dd(session('step2')['event_format'] == 2) }}--}}
                <div class="eventLink" {{ isset(session('step2')['event_format']) ?
                            session('step2')['event_format'] == 2 || session('step2')['event_format'] == 3 ? '' : 'style=display:none' :
                            old('event_format') ? old('event_format') == 2 || old('event_format') == 3 ? '' : 'style=display:none' : '' }}>
                    <div class="form-group mb-4">
                        <input id="event_link" name="event_link"
                               value="{{ isset(session('step2')['event_link']) ? session('step2')['event_link'] : old('event_link') }}"
                               type="text" autofocus=""
                               class="form-control @error('event_link') is-invalid @enderror"
                               placeholder="Link del evento">
                        @error('event_link')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <h5 class="mb-2 py-2">Frecuencia del evento</h5>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio"
                               class="custom-control-input @error('event_frecuency') is-invalid @enderror"
                               id="fre1" name="event_frecuency" value="1"
                               @if (old('event_frecuency'))
                               @if (old('event_frecuency') == 1)
                               checked
                               @endif
                               @elseif (session('step2')['event_frecuency'])
                               @if (session('step2')['event_frecuency'] == 1)
                               checked
                               @endif
                               @else
                               checked
                            @endif
                        >
                        <label for="fre1" class="custom-control-label">Evento único</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio"
                               class="custom-control-input @error('event_frecuency') is-invalid @enderror"
                               id="fre2" name="event_frecuency" value="2"
                               @if (old('event_frecuency'))
                               @if (old('event_frecuency') == 2)
                               checked
                               @endif
                               @elseif (session('step2')['event_frecuency'])
                               @if (session('step2')['event_frecuency'] == 2)
                               checked
                            @endif
                            @endif
                        >
                        <label for="fre2" class="custom-control-label">Recurrente</label>
                    </div>
                </div>
                    <div class="form-group pb-5" id="frecuency_2">
                        <div class="row" style="padding-bottom: 20px">
                            <div class="col-12 col-lg-4">
                                <h6>Fechaa</h6>
                                <input id="date" name="event_date[]"
                                       type="date" autofocus=""
                                       class="form-control pl-lg-4 pr-lg-1 @error('event_date.*') is-invalid @enderror">
                                @error('event_date.*')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="col-md-6 col-lg-4 mt-4 mt-lg-0">
                                <h6>Hora de Inicio</h6>
                                <input id="hora-inicio" name="event_time_start[]"
                                       type="time" autofocus=""
                                       class="form-control @error('event_time_start.*') is-invalid @enderror"
                                       placeholder="Hora">
                                @error('event_time_start.*')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="col-md-6 col-lg-4 mt-4 mt-lg-0">
                                <h6>Hora de Terminso</h6>
                                <input id="hora-final" name="event_time_end[]"
                                       type="time" autofocus=""
                                       class="form-control @error('event_time_end.*') is-invalid @enderror"
                                       placeholder="Hora">
                                @error('event_time_end.*')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                <div class="form-group pb-5" id="frecuency_1"
                     @if (old('event_frecuency'))
                     @if (old('event_frecuency') != 1)
                     style="display: none"
                     @endif
                     @elseif (session('step2')['event_frecuency'])
                     @if (session('step2')['event_frecuency'] != 1)
                     style="display: none"
                    @endif
                    @endif
                >
                    <div class="row" style="padding-bottom: 20px">
                        <div class="col-12 col-lg-6">

                            <h6>Fecha Inicio</h6>
                            <input id="date" name="event_date_start"
                                   value="{{ isset(session('step2')['event_date_start']) ? session('step2')['event_date_start'] : old('event_date_start') }}"
                                   type="date" autofocus=""
                                   class="form-control pl-lg-4 pr-lg-1 @error('event_date_start') is-invalid @enderror">
                            @error('event_date_start')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror

                        </div>
                        <div class="col-md-6 col-lg-4 mt-4 mt-lg-0">
                            <h6>Hora de Inicio</h6>
                            <input id="hora-inicio" name="event_time_st"
                                   value="{{ isset(session('step2')['event_time_st']) ? session('step2')['event_time_st'] : old('event_time_st') }}"
                                   type="time" autofocus=""
                                   class="form-control @error('event_time_st') is-invalid @enderror"
                                   placeholder="Hora">
                            @error('event_time_st')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="row" style="padding-bottom: 20px">
                        <div class="col-12 col-lg-6">
                            <h6>Fecha Fin</h6>
                            <input id="date" name="event_date_end"
                                   value="{{ isset(session('step2')['event_date_end']) ? session('step2')['event_date_end'] : old('event_date_end') }}"
                                   type="date" autofocus=""
                                   class="form-control pl-lg-4 pr-lg-1 @error('event_date_end') is-invalid @enderror">
                            @error('event_date_end')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="col-md-6 col-lg-4 mt-4 mt-lg-0">
                            <h6>Hora de Termino</h6>
                            <input id="hora-final" name="event_time_ed"
                                   value="{{ isset(session('step2')['event_time_ed']) ? session('step2')['event_time_ed'] : old('event_time_ed') }}"
                                   type="time" autofocus=""
                                   class="form-control @error('event_time_ed') is-invalid @enderror"
                                   placeholder="Hora">
                            @error('event_time_ed')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="form-group pb-5" id="frecuency_2"
                     @if (old('event_frecuency'))
                     @if (old('event_frecuency') != 2)
                     style="display: none"
                     @endif
                     @elseif (session('step2')['event_frecuency'])
                     @if (session('step2')['event_frecuency'] != 2)
                     style="display: none"
                     @endif
                     @else
                     style="display: none"
                    @endif
                >
                    @if (isset(session('step2')['event_date']))
                        @foreach(session('step2')['event_date'] as $key => $ticket)
                            <div class="row" style="padding-bottom: 20px">
                                <div class="col-12 col-lg-4">
                                    <h6>Fecha</h6>
                                    <input id="date" name="event_date[]"
                                           value="{{ isset(session('step2')['event_date'][$key]) ? session('step2')['event_date'][$key] : '' }}"
                                           type="date" autofocus=""
                                           class="form-control pl-lg-4 pr-lg-1 @error('event_date.'.$key) is-invalid @enderror">
                                    @error('event_date.'.$key)
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="col-6 col-lg-4 mt-4 mt-lg-0">
                                    <h6>Hora de Inicio</h6>
                                    <input id="hora-inicio" name="event_time_start[]"
                                           value="{{ isset(session('step2')['event_time_start'][$key]) ? session('step2')['event_time_start'][$key] : '' }}"
                                           type="time" autofocus=""
                                           class="form-control @error('event_time_start.'.$key) is-invalid @enderror"
                                           placeholder="Hora">
                                    @error('event_time_start.'.$key)
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="col-6 col-lg-4 mt-4 mt-lg-0">
                                    <h6>Hora de Termino</h6>
                                    <input id="hora-final" name="event_time_end[]"
                                           value="{{ isset(session('step2')['event_time_end'][$key]) ? session('step2')['event_time_end'][$key] : '' }}"
                                           type="time" autofocus=""
                                           class="form-control @error('event_time_end.'.$key) is-invalid @enderror"
                                           placeholder="Hora">
                                    @error('event_time_end.'.$key)
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            @if($loop->last)
                                <a id="add" class="btn btn-tertiary mt-3" href="javascript:add_slot()">Añadir otra
                                    fecha</a>
                                @if ($key > 0)
                                    <a id="delete" class="btn btn-tertiary mt-3" href="javascript:delete_slot()">Eliminar
                                        una fecha</a>
                                @else
                                    <a style="display: none" id="delete" class="btn btn-tertiary mt-3"
                                       href="javascript:delete_slot()">Eliminar una fecha</a>
                                @endif
                            @endif
                        @endforeach
                    @else

                        <div class="row" style="padding-bottom: 20px">
                            <div class="col-12 col-lg-4">
                                <h6>Fecha</h6>
                                <input id="date" name="event_date[]"
                                       type="date" autofocus=""
                                       class="form-control pl-lg-4 pr-lg-1 @error('event_date.*') is-invalid @enderror">
                                @error('event_date.*')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="col-md-6 col-lg-4 mt-4 mt-lg-0">
                                <h6>Hora de Inicio</h6>
                                <input id="hora-inicio" name="event_time_start[]"
                                       type="time" autofocus=""
                                       class="form-control @error('event_time_start.*') is-invalid @enderror"
                                       placeholder="Hora">
                                @error('event_time_start.*')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="col-md-6 col-lg-4 mt-4 mt-lg-0">
                                <h6>Hora de Terminso</h6>
                                <input id="hora-final" name="event_time_end[]"
                                       type="time" autofocus=""
                                       class="form-control @error('event_time_end.*') is-invalid @enderror"
                                       placeholder="Hora">
                                @error('event_time_end.*')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>
                        <a id="add" class="btn btn-tertiary mt-3" href="javascript:add_slot()">Añadir otra fecha</a>
                        <a style="display: none" id="delete" class="btn btn-tertiary mt-3"
                           href="javascript:delete_slot()">Eliminar una fecha</a>
                    @endif
                </div>
                <div class="form-group float-right">
                    <button wire:click="back" class="btn btn-secondary mr-2">Volver</button>
                    <button wire:click="thirdStep" class="btn btn-primary">
                        Siguiente
                    </button>
                </div>
                {{--{!! Form::close() !!}--}}
            </div>
        </div>
    </main>
{{--@endsection
@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        function add_slot() {

            var addBtn = $('a#add').clone();
            var deleteBtn = $('a#delete').clone();

            $('a#add').remove();
            $('a#delete').remove();

            var appendHtml = $('div#frecuency_2').children().first().clone().appendTo('div#frecuency_2');

            appendHtml.children().children('input').val('')

            $('div#frecuency_2').append(addBtn);
            $('div#frecuency_2').append(deleteBtn);

            if ($('div#frecuency_2').children('div').length >= 2) {
                $(deleteBtn).css('display', '')
            }
        }

        function delete_slot() {

            var addBtn = $('a#add').clone();
            var deleteBtn = $('a#delete').clone();

            $('a#add').remove();
            $('a#delete').remove();

            $('div#frecuency_2').children().last().remove();

            $('div#frecuency_2').append(addBtn);
            $('div#frecuency_2').append(deleteBtn);

            if ($('div#frecuency_2').children('div').length == 1) {
                $(deleteBtn).css('display', 'none')
            }
        }

        $(document).ready(function () {
            $("input[name='event_format']").on("change", function () {

                if (this.value == 1) {
                    $(".eventVenue").fadeIn('slow');
                    $(".eventLink").fadeOut('slow');
                } else if (this.value == 2) {
                    $(".eventVenue").fadeOut('slow');
                    $(".eventLink").fadeIn('slow');
                } else if (this.value == 3) {
                    $(".eventVenue").fadeIn('slow');
                    $(".eventLink").fadeIn('slow');
                }
            });

            $("input[name='event_frecuency']").on("change", function () {

                if (this.value == 1) {
                    $("#frecuency_1").fadeIn('slow');
                    $("#frecuency_2").fadeOut('slow');
                } else if (this.value == 2) {
                    $("#frecuency_2").fadeIn('slow');
                    $("#frecuency_1").fadeOut('slow');
                }
            });
        })
    </script>
@endsection--}}
