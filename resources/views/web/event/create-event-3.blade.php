@extends('layout.app')

@section('content')
    <main class="d-flex w-100 vh-100 justify-content-center">
        <!-- Demo content-->
        <div class="col-sm-10 col-md-8 col-xl-6 mt-5">
            <p>Pasos <b class="text-primary">3 de 4</b></p>
            <div class="progress mb-4">
                <div class="progress-bar bg-primary" role="progressbar" style="width: 75%" aria-valuenow="25"
                     aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <div class="pt-4 pb-5">
                <h2 class="mb-4">Datos y entradas del evento</h2>
                {!! Form::open(['route' => 'web.event.step4', 'method' => 'post']) !!}
                <div class="row mb-4">
                    <div class="col-md-6">
                        <div class="form-group">
                            <h5 class="mb-2 py-2">Tipo de entrada</h5>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="free" name="event-type"
                                       value="free">
                                <label for="free" class="custom-control-label">Gratis</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="pago" name="event-type"
                                       value="pago">
                                <label for="pago" class="custom-control-label">Pago</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <h5 class="mb-2 py-2">Restricciones</h5>
                            <div class="custom-control custom-radio custom-control-inline">
                                <label>
                                	{!! Form::checkbox('restric_type', '1', null,  ['id' => 'restric_type']) !!}
                                	Cédula
                                </label>
                                {{--<input type="radio"
                                       class="custom-control-input @error('event_sale_type') is-invalid @enderror"
                                       id="fre1" name="event_sale_type" value="1"
                                       @if (old('event_sale_type'))
                                       @if (old('event_sale_type') == 1)
                                       checked
                                       @endif
                                       @elseif (isset(session('step2')['event_sale_type']))
                                       @if (session('step2')['event_sale_type'] == 1)
                                       checked
                                       @endif
                                       @else
                                       checked
                                    @endif
                                >
                                <label for="fre1" class="custom-control-label">Venta única</label>--}}
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                {!! Form::text('restric_value', null, ['class' => 'form-control', 'placeholder' => 'Cantidad por cédula', 'style' => 'display:none']) !!}
                                {{--<input type="radio"
                                       class="custom-control-input @error('event_sale_type') is-invalid @enderror"
                                       id="fre2" name="event_sale_type" value="2"
                                       @if (old('event_sale_type'))
                                       @if (old('event_sale_type') == 2)
                                       checked
                                       @endif
                                       @elseif (isset(session('step2')['event_sale_type']))
                                       @if (session('step2')['event_sale_type'] == 2)
                                       checked
                                    @endif
                                    @endif
                                >
                                <label for="fre2" class="custom-control-label">Venta por fecha</label>--}}
                            </div>
                        </div>
                    </div>
                    @if (session('step2')['event_frecuency'] == 2)
                        <div class="col-md-6">
                            <div class="form-group">
                                <h5 class="mb-2 py-2">Tipo de venta</h5>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio"
                                           class="custom-control-input @error('event_sale_type') is-invalid @enderror"
                                           id="fre1" name="event_sale_type" value="1"
                                           @if (old('event_sale_type'))
                                           @if (old('event_sale_type') == 1)
                                           checked
                                           @endif
                                           @elseif (isset(session('step2')['event_sale_type']))
                                           @if (session('step2')['event_sale_type'] == 1)
                                           checked
                                           @endif
                                           @else
                                           checked
                                            @endif
                                    >
                                    <label for="fre1" class="custom-control-label">Venta única</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio"
                                           class="custom-control-input @error('event_sale_type') is-invalid @enderror"
                                           id="fre2" name="event_sale_type" value="2"
                                           @if (old('event_sale_type'))
                                           @if (old('event_sale_type') == 2)
                                           checked
                                           @endif
                                           @elseif (isset(session('step2')['event_sale_type']))
                                           @if (session('step2')['event_sale_type'] == 2)
                                           checked
                                            @endif
                                            @endif
                                    >
                                    <label for="fre2" class="custom-control-label">Venta por fecha</label>
                                </div>
                            </div>
                        </div>
                    @endif

                </div>
                <div class="form-group pt-4 pb-5" id="slot">
                    @if (isset(session('step3')['sector_name']))
                        @foreach(session('step3')['sector_name'] as $key => $ticket)
                            <div class="row" style="padding-bottom: 20px">
                                <div class="col-md-4 mt-4 mt-lg-0">
                                    <h6>Sector</h6>
                                    <input id="sector_name" name="sector_name[]"
                                           value="{{ session('step3')['sector_name'][$key] }}" type="text" required=""
                                           class="form-control" placeholder="Ej.: VIP">
                                </div>
                                <div class="col-md-4 mt-4 mt-lg-0">
                                    <h6>Precio por Entrada</h6>
                                    <input id="sector_price" name="sector_price[]"
                                           value="{{ session('step3')['sector_price'][$key] }}" type="number"
                                           required="" class="form-control" placeholder="Ej.: 250000">
                                </div>
                                <div class="col-md-4 mt-4 mt-lg-0">
                                    <h6>Cantidad</h6>
                                    <input id="sector_count" name="sector_count[]"
                                           value="{{ session('step3')['sector_count'][$key] }}" type="number"
                                           required="" class="form-control" placeholder="Ej.: 200">
                                </div>
                                <div class="col-md-4 mt-4 mt-lg-0 pt-2 sector_date"

                                    {{ isset(session('step3')['event_sale_type']) ?
                                     session('step3')['event_sale_type'] == 2 ? '' : 'style=display:none' :
                                     old('event_sale_type') ? old('event_sale_type') == 2 ? '' : 'style=display:none' : 'style=display:none' }}
                                >
                                    <h6>Fecha</h6>
                                    {!! Form::select('date_id[]', isset(session('step2')['event_date']) ? session('step2')['event_date'] : [], session('step3')['date_id'][$key] , ['class' => $errors->has('date_id.'.$key) ? 'form-control date_id' : 'form-control date_id is-invalid','placeholder' => 'Elegir fecha']) !!}
                                </div>
                            </div>
                        @endforeach
                        <a id="add" class="btn btn-tertiary mt-3" href="javascript:add_slot()">Añadir otra entrada</a>
                        <a style="display: {{ $key == 0 ? 'none' : '' }}" id="delete" class="btn btn-tertiary mt-3"
                           href="javascript:delete_slot()">Eliminar una entrada</a>
                    @else
                        <div class="row" style="padding-bottom: 20px">
                            <div class="col-md-4 mt-4 mt-lg-0">
                                <h6>Tipo de Entrada</h6>
                                <input id="sector_name" name="sector_name[]" value="{{ old('sector_name.*') }}" type="text" required="" autofocus=""
                                       class="form-control" placeholder="Ej.: VIP">
                            </div>
                            <div class="col-md-4 mt-4 mt-lg-0">
                                <h6>Precio por Entrada</h6>
                                <input id="sector_price" name="sector_price[]" type="number" required="" autofocus=""
                                       class="form-control" placeholder="Ej.: 250000">
                            </div>
                            <div class="col-md-4 mt-4 mt-lg-0">
                                <h6>Cantidad</h6>
                                <input id="sector_count" name="sector_count[]" type="number" required="" autofocus=""
                                       class="form-control" placeholder="Ej.: 200">
                            </div>
                            <div class="col-md-4 mt-4 mt-lg-0 pt-2 sector_date"
                                    {{ isset(session('step3')['event_sale_type']) ?
                                             session('step3')['event_sale_type'] == 2 ? '' : 'style=display:none' :
                                             old('event_sale_type') ? old('event_sale_type') == 2 ? '' : 'style=display:none' : 'style=display:none' }}
                            >
                                <h6>Fecha</h6>
                                {!! Form::select('date_id[]', isset(session('step2')['event_date']) ? session('step2')['event_date'] : [], null , ['class' => 'form-control date_id','placeholder' => 'Elegir fecha']) !!}
                                @error('date_id.*')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>
                        <a id="add" class="btn btn-tertiary mt-3" href="javascript:add_slot()">Añadir otra entrada</a>
                        <a style="display: none" id="delete" class="btn btn-tertiary mt-3"
                           href="javascript:delete_slot()">Eliminar una entrada</a>
                    @endif
                </div>
                <div class="form-group float-right">
                    <a href="{{ route('web.event.step2') }}" class="btn btn-secondary mr-2">Volver</a>
                    <button type="submit" class="btn btn-primary">Siguiente</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </main>
@endsection
@section('scripts')
    <script>
        function add_slot() {

            var addBtn = $('a#add').clone();
            var deleteBtn = $('a#delete').clone();

            $('a#add').remove();
            $('a#delete').remove();

            var appendHtml = $('div#slot').children().first().clone().appendTo('div#slot');

            appendHtml.children().children('input').val('')

            $('div#slot').append(addBtn);
            $('div#slot').append(deleteBtn);

            if ($('div#slot').children('div').length >= 2) {
                $(deleteBtn).css('display', '')
            }
        }

        function delete_slot() {

            var addBtn = $('a#add').clone();
            var deleteBtn = $('a#delete').clone();

            $('a#add').remove();
            $('a#delete').remove();

            $('div#slot').children().last().remove();

            $('div#slot').append(addBtn);
            $('div#slot').append(deleteBtn);

            if ($('div#slot').children('div').length == 1) {
                $(deleteBtn).css('display', 'none')
            }
        }

        $(document).ready(function () {
            $("input[name='event_sale_type']").on("change", function () {

                if (this.value == 1) {
                    $(".sector_date").fadeOut('slow');
                    $(".date_id").removeAttr('required');
                } else if (this.value == 2) {
                    $(".sector_date").fadeIn('slow');
                    $(".date_id").attr('required', 'true');

                }
            });

            $('#restric_type').on("change", function () {

                if (this.checked){

                    $("input[name='restric_value']").removeProp('display').fadeIn('slow');
                }else{

                    $("input[name='restric_value']").prop('display', 'none').fadeOut('slow');
                }
            });
        });
    </script>
@endsection
