@extends('layout.app')
@section('head')
<!--    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.css" rel="stylesheet">
<style>
    .cropit-preview {
        background-color: #f8f8f8;
        background-size: cover;
        border: 5px solid #ccc;
        border-radius: 3px;
        margin-top: 7px;
    }

    .cropit-preview-image-container {
        cursor: move;
    }

    .cropit-preview-background {
        opacity: .2;
        cursor: auto;
    }

    .image-size-label {
        margin-top: 10px;
    }

    input, .export {
        /* Use relative position to prevent from being covered by image background */
        position: relative;
        z-index: 10;
        display: block;
    }
    .preview-images-zone {
        width: 100%;
        border: 1px solid #ddd;
        min-height: 180px;
        /* display: flex; */
        padding: 5px 5px 0px 5px;
        position: relative;
        overflow:auto;
    }
    .preview-images-zone > .preview-image:first-child {
        height: 185px;
        width: 185px;
        position: relative;
        margin-right: 5px;
    }
    .preview-images-zone > .preview-image {
        height: 90px;
        width: 90px;
        position: relative;
        margin-right: 5px;
        float: left;
        margin-bottom: 5px;
    }
    .preview-images-zone > .preview-image > .image-zone {
        width: 100%;
        height: 100%;
    }
    .preview-images-zone > .preview-image > .image-zone > img {
        width: 100%;
        height: 100%;
    }
    .preview-images-zone > .preview-image > .tools-edit-image {
        position: absolute;
        z-index: 100;
        color: #fff;
        bottom: 0;
        width: 100%;
        text-align: center;
        margin-bottom: 10px;
        display: none;
    }
    .preview-images-zone > .preview-image > .image-cancel {
        font-size: 18px;
        position: absolute;
        top: 0;
        right: 0;
        font-weight: bold;
        margin-right: 10px;
        cursor: pointer;
        display: none;
        z-index: 100;
    }
    .preview-image:hover > .image-zone {
        cursor: move;
        opacity: .5;
    }
    .preview-image:hover > .tools-edit-image,
    .preview-image:hover > .image-cancel {
        display: block;
    }
    .ui-sortable-helper {
        width: 90px !important;
        height: 90px !important;
    }

    .container {
        padding-top: 50px;
    }
</style>

    @livewireStyles
@endsection
@section('content')
    @livewire('web.events.create')
@endsection

@section('scripts')
   <script
         src="https://cdn.tiny.cloud/1/le0xnl72caevg2xl8a4noobxfd3cuv10352omp6pa0ieawfi/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
   <script>

        $(document).ready(function () {
            /**
             * Tinymce Text Editor
             */
            tinymce.init({
                selector: '#mytextarea',
                width: 600,
                height: 300,
                setup:function(ed) {
                    ed.on('init change', function () {
                        ed.save();
                    });
                    ed.on('change', function (e) {
                        /*
                        * Dipatch event to livewire
                        * */
                        mytextarea = document.getElementById('mytextarea');
                        mytextarea.value = ed.getContent()

                        var ev = new InputEvent("input");
                        mytextarea.dispatchEvent(ev);
                    });
                },
                language:'es',
                plugins: [
                    'advlist autolink link image lists charmap print preview hr anchor pagebreak',
                    'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
                    'table emoticons template paste help'
                ],
                toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | ' +
                    'bullist numlist outdent indent | link image | print preview media fullpage | ' +
                    'forecolor backcolor emoticons | help',
                menu: {
                    favs: {title: 'Mis favoritos', items: 'code visualaid | searchreplace | emoticons'}
                },
                menubar: 'favs file edit view insert format tools table help',
                content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
            });
        })
    </script>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
   <script src="/cropit/dist/jquery.cropit.js"></script>
   <script>
       $(function () {
           /**
            * Cropit Image Editor
            */
           $('.image-editor').cropit({
               exportZoom: 1.25,
               imageBackground: true,
               imageBackgroundBorderWidth: 20,
               imageState: {
                   src: '',
               },
           });

           $('.export').click(function () {
               var imageData = $('.image-editor').cropit('export');

               bannerAdded = document.getElementById('img-nw');
               bannerAdded.value = imageData

               var ev = new InputEvent("input");
               bannerAdded.dispatchEvent(ev);

               $('.image-editor').cropit('destroy');

               $('.cropit-preview').removeClass('cropit-image-loaded');
               $('.cropit-preview-background').attr('src','');
               $('.cropit-preview-background').removeAttr('style');
               $('.cropit-preview-background').attr('style','position: relative; left: 30px; top: 30px; transform-origin: left top 0px; will-change: transform;');
               $('.cropit-preview-image').attr('src','');
               $('.cropit-preview-image').removeAttr('style');
               $('.cropit-preview-image').attr('style','transform-origin: left top 0px; will-change: transform;');
               $('input.cropit-image-input').val('');
               $('.cropit-image-zoom-input').val('0');

               $('.image-editor').cropit();
               //document.getElementById('img-nw').src = imageData;
               //window.open(imageData);
           });

           $('.rotate-cw').click(function () {
               $('.image-editor').cropit('rotateCW');
           });
           $('.rotate-ccw').click(function () {
               $('.image-editor').cropit('rotateCCW');
           });

          /* $('.rotate-cw').click(function () {
               $('.image-editor').cropit('rotateCW');
           });
           $('.rotate-ccw').click(function () {
               $('.image-editor').cropit('rotateCCW');
           });

           $('.export').click(function () {
               var imageData = $('.image-editor').cropit('export');
               window.open(imageData);
           });*/
       });

       window.addEventListener('alert',function(e){
           console.log(e.detail.type);
           //console.log(e.detail['message']);


           //Notifications from Livewire
           switch (e.detail['type']) {
               case 'success':
                   iziToast.success({
                       title: e.detail.message,
                       //message: 'This awesome plugin is made by iziToast',
                       position: 'topRight'
                   });
                   break;

               case 'error':
                   iziToast.error({
                       title: e.detail.message,
                       //message: 'This awesome plugin is made by iziToast',
                       position: 'topRight'
                   });
                   break;

               case 'info':
                   iziToast.info({
                       title: e.detail.message,
                       //message: 'This awesome plugin is made by iziToast',
                       position: 'topRight'
                   });
                   break;

               case 'warning':
                   iziToast.warning({
                       title: e.detail.message,
                       //message: 'This awesome plugin is made by iziToast',
                       position: 'topRight'
                   });
                   break;
           }
           //END Notifications from Livewire
       })
   </script>
    @livewireScripts

@endsection

