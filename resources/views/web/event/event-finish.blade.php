@extends('layout.app')

@section('content')
    <section class="event-public">
        <div class="container">

            <div class="row align-items-lg-center flex-column-reverse flex-lg-row pt-5">
                <div class="col-12">
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        Tu evento se creo con éxito
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="cover-event">
                        <div id="carouselExampleInterval" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                @foreach($event->covers as $cover)
                                    <div class="carousel-item @if ($loop->first) active @endif" data-interval="10000">
                                        <img src="{{asset('storage/'.$cover->url.$cover->file_name)}}" class="d-block w-100" alt="{{ $event->event_name }}">
                                    </div>
                                @endforeach
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleInterval" role="button"
                               data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleInterval" role="button"
                               data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                        {{-- <img src="{{asset('storage/events/cover/'.$event->event_cover)}}" alt="{{ $event->event_name }}"
                        class="py-5 py-lg-0"> --}}
                    </div>
                </div>
                @php(setlocale(LC_TIME,"es_ES.UTF-8"))
                <div class="col-lg-5 offset-lg-1">
                    @if ($event->event_frecuency == 1)
                        <span class="date">De {{ strftime("%A, %d de %B", strtotime($event->event_date_start)) }}, {{ strftime("%H:%M", strtotime($event->event_time_start)) }} Hs.</span>
                        <br>
                        <span class="date">A {{ strftime("%A, %d de %B", strtotime($event->event_date_end)) }}, {{ strftime("%H:%M", strtotime($event->event_time_end)) }} Hs.</span>
                    @elseif($event->event_frecuency == 2)
                        @foreach ($event->dates as $date)
                            <span class="date">{{ strftime("%A, %d de %B", strtotime($date->event_date)) }}, {{ strftime("%H:%M", strtotime($date->event_time_start)) }} Hs.</span>
                            <br>
                        @endforeach
                    @endif

                    {{--<span class="date">fecha de Ejemplo</span>--}}
                    {{--<span class="date">@php(setlocale(LC_TIME,"es_ES.UTF-8")){{ strftime("%A, %d de %B de %Y", strtotime($date->event_date)) }}</span>--}}
                    <h1 class="pt-4">{{ $event->event_name }}</h1>
                    {{-- <h1 class="pt-4">{{ $event->event_name }}</h1> --}}
                    <p class="mb-1 pt-4">Organizador</p>
                    <h3>{{ $event->event_organizer }}</h3>
                    <p class="mb-1 pt-2">Categoria</p>
                    <h3>{{ $event->category->category_name }}</h3>
                </div>
            </div>
            <div class="row flex-column-reverse flex-lg-row pt-5">
                <div class="col-lg-9">
                    <h3 class="mb-3">Descripción</h3>
                    <p>Descripción de ejemplo</p>
                    {{-- <p>{!! $event->event_description !!}</p> --}}
                </div>
                {{-- <div class="row">
                    <div class="col-md-8 p-4">
                        <h2 class="event__title">{{ session('step1')['event_name'] }}</h2>
                        <div class="event__description mb-5">
                            <label>Descripción</label>
                            <textarea class="form-control"
                                    rows="5">{{ session('step4')['event_description'] }}</textarea>
                        </div>
                        <div class="col-md-4 p-5 border-left">
                            <div class="mb-5">
                                <h5>Fecha y Hora</h5>
                                <p>
                                    @php(setlocale(LC_TIME,"es_ES.UTF-8")){{ strftime("%A, %d de %B de %Y", strtotime(session('step2')['event_date'])) }}
                                    Miércoles, 25 de marzo de 2020<br>
                                    {{ date('H:i', strtotime(session('step2')['event_time_start'])) }} - {{ date('H:i', strtotime(session('step2')['event_time_end'])) }} hora de Paraguay
                                </p>
                            </div>
                            <div class="mb-5">
                                <h5>Ubicación</h5>
                                <p>
                                    {{ session('step2')['venue_name'] }}, <br>
                                    {{ session('step2')['venue_address'] }}
                                </p>
                            </div>
                            <div class="mb-5">
                                <h5>Etiquetas</h5>
                                <p>Tecnología, educación, ciberseguridad</p>
                            </div>
                        </div>
                    </div>
                </div> --}}
                <div class="col-lg-3 pb-5 pb-lg-0">
                    {{-- <div class="mb-4">
                        Organizador del evento
                        <h3 class="mb-2">Organizador</h3>
                        <p>Nombre de organizador</p>
                        <p>{{ $event->event_organizer }}</p>
                    </div> --}}

                    <div class="mb-4">
                        <h3 class="mb-2">Ubicación</h3>
                        @isset($event->venue)
                            <p>{{ $event->venue->venue_address }},</p>
                            <p>{{ $event->venue->venue_name }}, Asunción, Paraguay</p>
                        @endisset

                        {{--<p>Sheraton, Asunción, Paraguay</p>--}}
                    </div>
                    <div class="mb-4">
                        <h3 class="mb-2">Fechas</h3>
                        @foreach ($event->dates as $date)
                            <p>{{ strftime("%A, %d de %B de %Y", strtotime($date->event_date)) }}<br>
                                {{ strftime("%H:%M", strtotime($date->event_time_start)).'-'.strftime("%H:%M", strtotime($date->event_time_end)).' hora de Paraguay' }}
                            </p>
                        @endforeach
                        {{--<p>Miércoles, 25 de marzo de 2020, 19:00 - 22:00 hora de Paraguay</p>
                        <p>Miércoles, 26 de marzo de 2020, 19:00 - 22:00 hora de Paraguay</p>--}}
                    </div>
                    <div class="mb-4">
                        {{-- Categoria del Evento --}}
                        {{-- <h3 class="mb-2">Categoria</h3>
                            <p>{{ $event->category->category_name }}</p>
                            <p>Categoria ejemplo</p> --}}
                    </div>
                    <div class="mb-4">
                        <h3 class="mb-2">Etiquetas</h3>
                        @foreach($event->tags as $tag)
                            <a href="#"><span class="badge badge-pill tags p-3 m-1">{{ $tag->event_tag }}</span></a>
                        @endforeach
                        {{--<a href=""><span class="badge badge-pill tags p-3 m-1">Concierto</span></a>
                        <a href=""><span class="badge badge-pill tags p-3 m-1">Música</span></a>
                        <a href=""><span class="badge badge-pill tags p-3 m-1">Artista internacional</span></a>--}}
                    </div>
                    <div class="mb-3">
                        <h3 class="mb-2">Compartir</h3>
                        <a href="#"><span class="shart"><i class="fab fa-facebook-f icon"></i></span></a>
                        <a href="#"><span class="shart"><i class="fab fa-twitter icon"></i></span></a>
                        <a href="#"><span class="shart"><i class="fab fa-whatsapp icon"></i></span></a>
                        {{-- <div class="col-md-4 p-5 border-left">
                            <div class="mb-5">
                                <h5>Fecha y Hora</h5>
                                @foreach($event->eventdates as $date)
                                    <p>
                                        @php(setlocale(LC_TIME,"es_ES.UTF-8")){{ strftime("%A, %d de %B de %Y", strtotime($date->event_date)) }}
                                        Miércoles, 25 de marzo de 2020<br>
                                        {{ date('H:i', strtotime($date->event_time_start)) }}
                                        - {{ date('H:i', strtotime($date->event_time_end)) }} hora de Paraguay
                                    </p>
                                @endforeach
                            </div>
                            <div class="mb-5">
                                <h5>Ubicación</h5>
                                <p>
                                    {{ $event->venue->venue_name }}, <br>
                                    {{ $event->venue->venue_address }}
                                </p>
                            </div>
                            <div class="mb-5">
                                <h5>Etiquetas</h5>
                                <p>Tecnología, educación, ciberseguridad</p>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection