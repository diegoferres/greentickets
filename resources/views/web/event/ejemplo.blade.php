@extends('layout.app')

@section('content')
    <section class="event-public">
        <div class="container">
            <div class="row align-items-lg-center flex-column-reverse flex-lg-row pt-5">
                <div class="col-lg-6">
                    <div class="cover-event">
                        <div id="carouselExampleInterval" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active" data-interval="10000">
                                    <img src="{{asset('/img/Post-05-06.png')}}" class="d-block w-100" alt="...">
                                </div>
                                <div class="carousel-item" data-interval="2000">
                                    <img src="{{asset('/img/Post-05-06-1.png')}}" class="d-block w-100" alt="...">
                                </div>
                                <div class="carousel-item">
                                    <img src="{{asset('/img/Post-05-06-2.png')}}" class="d-block w-100" alt="...">
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleInterval" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleInterval" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>   
                        {{-- <img src="{{asset('storage/events/cover/'.$event->event_cover)}}" alt="{{ $event->event_name }}"
                        class="py-5 py-lg-0"> --}}
                    </div>
                </div>
                <div class="col-lg-5 offset-lg-1">
                        {{-- @foreach ($event->dates as $date)
                            <span class="date">@php(setlocale(LC_TIME,"es_ES.UTF-8")){{ strftime("%A, %d de %B", strtotime($date->event_date)) }}, {{ strftime("%H:%M", strtotime($date->event_time_start)) }} Hs.</span>
                        @endforeach --}}
                        <span class="date">fecha de Ejemplo</span>
                        {{--<span class="date">@php(setlocale(LC_TIME,"es_ES.UTF-8")){{ strftime("%A, %d de %B de %Y", strtotime($date->event_date)) }}</span>--}}
                        <h1 class="pt-4">Lanzamiento NEW GAC GS5</h1>
                        {{-- <h1 class="pt-4">{{ $event->event_name }}</h1> --}}
                        <p class="mb-1 pt-4">Organizador</p>
                        <h3>DLS Motors</h3>
                        <p class="mb-1 pt-2">Categoria</p>
                        <h3>Automotor</h3>
                </div>
            </div>
            <div class="row flex-column-reverse flex-lg-row pt-5">
                <div class="col-lg-9">
                    <h3 class="mb-3">Descripción</h3>
                    <p>Descripción de ejemplo</p>
                    {{-- <p>{!! $event->event_description !!}</p> --}}
                </div>
                {{-- <div class="row">
                    <div class="col-md-8 p-4">
                        <h2 class="event__title">{{ session('step1')['event_name'] }}</h2>
                        <div class="event__description mb-5">
                            <label>Descripción</label>
                            <textarea class="form-control"
                                    rows="5">{{ session('step4')['event_description'] }}</textarea>
                        </div>
                        <div class="col-md-4 p-5 border-left">
                            <div class="mb-5">
                                <h5>Fecha y Hora</h5>
                                <p>
                                    @php(setlocale(LC_TIME,"es_ES.UTF-8")){{ strftime("%A, %d de %B de %Y", strtotime(session('step2')['event_date'])) }}
                                    Miércoles, 25 de marzo de 2020<br>
                                    {{ date('H:i', strtotime(session('step2')['event_time_start'])) }} - {{ date('H:i', strtotime(session('step2')['event_time_end'])) }} hora de Paraguay
                                </p>
                            </div>
                            <div class="mb-5">
                                <h5>Ubicación</h5>
                                <p>
                                    {{ session('step2')['venue_name'] }}, <br>
                                    {{ session('step2')['venue_address'] }}
                                </p>
                            </div>
                            <div class="mb-5">
                                <h5>Etiquetas</h5>
                                <p>Tecnología, educación, ciberseguridad</p>
                            </div>
                        </div>
                    </div>
                </div> --}}
                <div class="col-lg-3 pb-5 pb-lg-0">
                    {{-- <div class="mb-4">
                        Organizador del evento
                        <h3 class="mb-2">Organizador</h3>
                        <p>Nombre de organizador</p>
                        <p>{{ $event->event_organizer }}</p>
                    </div> --}}

                    <div class="mb-4">
                        <h3 class="mb-2">Ubicación</h3>
                        {{-- @isset($event->venue)
                            <p>{{ $event->venue->venue_name }}, Asunción, Paraguay</p>
                        @endisset --}}

                        <p>Sheraton, Asunción, Paraguay</p>
                    </div>
                    <div class="mb-4">
                        <h3 class="mb-2">Fechas</h3>
                        {{-- @foreach ($event->dates as $date)
                            <p>{{ strftime("%A, %d de %B de %Y", strtotime($date->event_date)) }}<br>
                                {{ strftime("%H:%M", strtotime($date->event_time_start)).'-'.strftime("%H:%M", strtotime($date->event_time_end)).' hora de Paraguay' }}
                            </p>
                        @endforeach --}}
                        <p>Miércoles, 25 de marzo de 2020, 19:00 - 22:00 hora de Paraguay</p>
                        <p>Miércoles, 26 de marzo de 2020, 19:00 - 22:00 hora de Paraguay</p>
                    </div>
                    <div class="mb-4">
                        {{-- Categoria del Evento --}}
                        {{-- <h3 class="mb-2">Categoria</h3>
                            <p>{{ $event->category->category_name }}</p>
                            <p>Categoria ejemplo</p> --}}
                    </div>
                    <div class="mb-4">
                        <h3 class="mb-2">Etiquetas</h3>
                        {{-- @foreach($event->tags as $tag)
                            <a href="#"><span class="badge badge-pill tags p-3 m-1">{{ $tag->event_tag }}</span></a>
                        @endforeach --}}
                        <a href=""><span class="badge badge-pill tags p-3 m-1">Concierto</span></a>
                        <a href=""><span class="badge badge-pill tags p-3 m-1">Música</span></a>
                        <a href=""><span class="badge badge-pill tags p-3 m-1">Artista internacional</span></a>
                    </div>
                    <div class="mb-3">
                        <h3 class="mb-2">Compartir</h3>
                        <a href="#"><span class="shart"><i class="fab fa-facebook-f icon"></i></span></a>
                        <a href="#"><span class="shart"><i class="fab fa-twitter icon"></i></span></a>
                        <a href="#"><span class="shart"><i class="fab fa-whatsapp icon"></i></span></a>
                        {{-- <div class="col-md-4 p-5 border-left">
                            <div class="mb-5">
                                <h5>Fecha y Hora</h5>
                                @foreach($event->eventdates as $date)
                                    <p>
                                        @php(setlocale(LC_TIME,"es_ES.UTF-8")){{ strftime("%A, %d de %B de %Y", strtotime($date->event_date)) }}
                                        Miércoles, 25 de marzo de 2020<br>
                                        {{ date('H:i', strtotime($date->event_time_start)) }}
                                        - {{ date('H:i', strtotime($date->event_time_end)) }} hora de Paraguay
                                    </p>
                                @endforeach
                            </div>
                            <div class="mb-5">
                                <h5>Ubicación</h5>
                                <p>
                                    {{ $event->venue->venue_name }}, <br>
                                    {{ $event->venue->venue_address }}
                                </p>
                            </div>
                            <div class="mb-5">
                                <h5>Etiquetas</h5>
                                <p>Tecnología, educación, ciberseguridad</p>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-9">
                    <div class="pt-5 pt-lg-4">
                        <h2 class="mb-4 text-primary">Comprar entradas</h2>
                        {{-- <div class="form-group pb-5">
                            {!! Form::open(['route' => 'web.orders.addtocart', 'method' => 'post']) !!}
                            {!! Form::hidden('event_id', $event->id) !!}
                            <div class="row">
                                <div class="col-md-4">
                                    <h6 class="pt-3 pt-lg-auto">Fecha</h6>
                                    {!! Form::select('date_id', $event->dates->pluck('event_date', 'id') , [] , ['class' => 'custom-select form-control', 'placeholder' => 'Selecciona la fecha']) !!}
                                </div>
                                <div class="col-md-4">
                                    <h6 class="pt-3 pt-lg-auto">Tipo de Entrada</h6>
                                    {!! Form::select('sector_id', $event->sectors->pluck('sector_name', 'id') , [] , ['id' => 'sector_id','class' => 'custom-select form-control', 'placeholder' => 'Selecciona la entrada']) !!}
                                </div>
                                <div class="col-md-4">
                                    <h6 class="pt-3 pt-lg-auto">Cantidad</h6>
                                    <input name="count" id="count" type="number" required="" class="form-control"
                                           placeholder="Cantidad">
                                </div>
                            </div>
                            <div class="pt-3">
                                <div class="d-flex flex-column flex-md-row form-control bg-light">
                                    <div class="d-flex flex-md-fill align-items-center my-2">
                                        <h6 class="mb-0 mr-3">Precio Unitario</h6>
                                        Gs. &nbsp; <p class="mb-0" id="unit_price"> 0</p>
                                    </div>
                                    <div class="d-flex flex-md-fill align-items-center my-2">
                                        <h6 class="mb-0 mr-3">Total</h6>
                                        Gs. &nbsp; <p class="mb-0" id="subtotal"> 0</p>
                                    </div>
                                </div>
                                {{--<a href="{{ route('web.event.step1') }}" class="btn btn-primary mt-4 float-right"
                                   type="submit">Comprar</a>--}}
                                <button class="btn btn-primary mt-4 float-right"
                                   type="submit">Añadir al carrito</button>
                            </div>
                            {!! Form::close() !!}
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
            $('input#count, select#sector_id').on('keyup change', function () {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                if ($('select#sector_id option:checked').val() && $('input#count').val()) {

                    $.ajax({
                        type: "json",
                        method: "POST",
                        url: '/event/sum_price',
                        data: {
                            sector_id: $('select#sector_id option:checked').val(),
                            count: $('input#count').val()
                        },
                        success: function (data) {
                            $("p#unit_price").text(data.unit_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
                            $("p#subtotal").text(data.subtotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
                        }
                    })
                } else {
                    $("p#unit_price").text(0);
                    $("p#subtotal").text(0);
                }
            });
        })
    </script>
@endsection
