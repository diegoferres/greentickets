@extends('layout.app')
@section('head')
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
@endsection
@section('content')
    <section class="event">
        <main class="d-flex justify-content-center">
            <div class="col-sm-10 col-md-8 col-xl-6 mt-5">
                <p>Pasos <b class="text-primary">4 de 4</b></p>
                <div class="progress mb-4">
                    <div class="progress-bar bg-primary" role="progressbar" style="width: 100%" aria-valuenow="25"
                         aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="pt-4 pb-5">
                    <h2 class="mb-4">Portada y Descripción del Evento</h2>
                    {!! Form::open(['route' => 'web.event.save', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
                    <div class="form-group pb-5">
                        <h6 class="pt-4">Añade uno o más banners</h6>
                        <input type="file" name="event_covers[]" class="form-control mb-4" id="event_cover" multiple required>

                        <h6 class="pt-4">Descripción corta</h6>
                        {!! Form::textarea('event_subtitle', old('event_subtitle'), ['class' => 'form-control', 'rows' => 2, 'required']) !!}
                        @error('event_subtitle')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                        <span><span id="subtitle_lenght">0</span> de 158</span>

                        <h6 class="pt-4">Descripción completa</h6>
                        {!! Form::textarea('event_description', old('event_description'), ['placeholder' => 'Descripción más breve de tu evento', 'class' => 'form-control', 'id' => 'summernote', 'style' => 'display:none']) !!}
                        <span id="total-characters">0 de 30000 @error('event_description') <b
                                    style="color: red;">({{ $message }})</b>@enderror</span>
                        {{--<textarea id="event_description" name="event_description" placeholder="Descripción" required class="form-control" rows="5"></textarea>--}}
                    </div>
                    <div class="form-group float-right">
                        <a href="{{ route('web.event.step3') }}" class="btn btn-secondary mr-2">Volver</a>
                        <button class="btn btn-primary" type="submit">Finalizar</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </main>
    </section>
@endsection
@section('scripts')

    {{--<script src="/js/summernote.min.js"></script>--}}
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
    <script src="/js/summernote-es-ES.js"></script>

    <style>

    </style>
    <script>
        $(document).ready(function () {
            $('textarea[name="event_subtitle"]').on('keyup', function (){
                var lenght = $(this).val().length
                $('#subtitle_lenght').text(lenght);
            });

            $('#summernote').summernote({
                placeholder: '',
                tabsize: 2,
                height: 200,
                lang: 'es-ES',
                toolbar: [
                    ['font', ['bold', 'underline', 'clear']],
                    ['style', ['style']],
                    ['color', ['color']],
                    ['fontname', ['fontname']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    //['table', ['table']],
                    ['insert', ['link', 'picture', 'video']],
                    ['view', ['help']],
                ],
                callbacks: {
                    onChange: function(contents, $editable) {
                        $('#total-characters').text(contents.length + ' de ' + 30000);
                    }
                }
            });
        })
    </script>
@endsection
