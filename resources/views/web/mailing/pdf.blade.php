    <!doctype html>
<html lang="es">
<head>
    <title>Entra Sin Papel</title>
    <!-- Required meta tags -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    {{--<link href="https://fonts.googleapis.com/css?family=Poppins:400,600,800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">--}}
    <style>
        body {
            background: #fff;
            font-family: 'Poppins', sans-serif;
            margin: 0;
            padding: 0;
        }

        .ticket {
            display: flex;
            justify-content: left;
            align-items: left;
            width: 700px;
            margin: 20px auto;
            border-radius: 5px;
        }

        .stub, .check {
            border-radius: 5px;
            box-sizing: border-box;
        }


        .stub {
            /*background: #55C940;*/
            height: 500px;
            width: 500px;
            color: white;
            padding: 20px;
            position: relative;

        }
        .qr{
            /*width: 500px;*/
            /*height: 500px;*/
            margin: 40px auto;
        }

        .check {
            background: #f9f9f9;
            height: 250px;
            width: 450px;
            padding: 40px;
            position: relative;

        &:before {
             content: '';
             position: absolute;
             top: 0; left: 0;
             border-top: 20px solid #fff;
             border-right: 20px solid #f9f9f9;
             width: 0;
         }
        &:after {
             content: '';
             position: absolute;
             bottom: 0; left: 0;
             border-bottom: 20px solid #fff;
             border-right: 20px solid #f9f9f9;
             width: 0;
         }

        .big {
            font-size: 30px;
            font-weight: 900;
            line-height: 1em;
            margin-top: 12px;
        }
        .number {
            position: absolute;
            top: 16px;
            color: #55C940;
            font-size:16px;
        }
        .info {
            display: flex;
            justify-content: flex-start;
            font-size: 12px;
            margin-top: 20px;
            width: 100%;

        section {
            margin-right: 25px;
            &:before {
                 content: '';
                 background: #55C940;
                 display: block;
                 width: 40px;
                 height: 3px;
                 margin-bottom: 5px;
             }
        .title {
            font-size: 10px;
            font-weight: 900;
            text-transform: uppercase;
        }
        }
        }
        }

    </style>
</head>
<body>



<div class="ticket">
    <div class="stub">
        <div class="qr">
            <img src="{{ asset($qr) }}" width="100%" height="100%" alt="QR">
        </div>
    </div>
</div>
</body>
</html>



















