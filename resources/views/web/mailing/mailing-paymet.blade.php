<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office"
      style="width:100%;font-family:Poppins, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta name="x-apple-disable-message-reformatting">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="telephone=no" name="format-detection">
    <title>Entra Sin Papel - Reset PASS</title>
    <!--[if (mso 16)]>
    <style type="text/css">
        a {
            text-decoration: none;
        }
    </style>
    <![endif]-->
    <!--[if gte mso 9]>
    <style>sup {
        font-size: 100% !important;
    }</style><![endif]-->
    <!--[if gte mso 9]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG></o:AllowPNG>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <!--[if !mso]><!-- -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,600,800&display=swap" rel="stylesheet">
    <!--<![endif]-->
    <style type="text/css">
        .history {
            padding: 30px;
            box-shadow: 0px 2px 6px #00000024;
            border-radius: 12px;
        }

        .ticket {
            padding: 30px;
            box-shadow: 0px 2px 6px #00000024;
            border-radius: 12px;
            margin-bottom: 30px;
        }

        .ticket .ticket-content {
            padding-bottom: 20px;
            border-bottom: 1px dashed #55c940;
        }

        .ticket .ticket-content p {
            font-size: 14px;
            margin-bottom: 6px;
        }

        .ticket .ticket-qr {
            padding-top: 20px;
            width: 110px;
            margin: 0 auto;
        }

        .ticket .ticket-qr .qr {
            width: 100%;
        }

        .cart .cupon {
            color: #b10f0a;
            font-size: 12px;
        }

        .cart .cupon-canje .cupon {
            color: #b10f0a;
            font-size: 16px;
        }

        .cart ul {
            list-style: none;
            padding-left: 0;
            display: inline-block;
        }

        .cart ul li {
            padding-top: 15px;
            padding-bottom: 15px;
            margin-bottom: 15px;
            border-bottom: 1px solid #00000024;
        }

        .cart ul li img {
            border-radius: 4px;
        }

        .cart ul li .btn-primary {
            font-size: 12px;
            width: 100%;
        }

        .cart ul li span {
            font-size: 14px;
            color: #7E7D83;
        }

        .cart ul li span b {
            color: #02020C;
        }

        .cart ul li .price-old {
            text-decoration: line-through;
            font-size: 12px;
        }

        .cart ul li .icon-cart {
            color: #55C940;
            font-size: 18px;
        }

        .cart .accordion .card {
            border: 0;
            border-radius: 10px;
            box-shadow: 0px 0px 2px #00000024;
        }

        .cart .accordion .card .card-header {
            border-radius: 0;
            background-color: #F9F9F9;
            padding: 20px 25px;
            border-bottom: 0;
        }

        .cart .form-control {
            padding: 10px 8px 10px 20px;
            box-shadow: 0px 0px 4px #00000024;
        }

        @media only screen and (max-width: 600px) {
            p, ul li, ol li, a {
                font-size: 16px !important;
                line-height: 150% !important
            }

            h1 {
                font-size: 30px !important;
                text-align: left;
                line-height: 120% !important
            }

            h2 {
                font-size: 26px !important;
                text-align: center;
                line-height: 120% !important
            }

            h3 {
                font-size: 20px !important;
                text-align: center;
                line-height: 120% !important
            }

            h1 a {
                font-size: 30px !important
            }

            h2 a {
                font-size: 26px !important
            }

            h3 a {
                font-size: 20px !important
            }

            .es-menu td a {
                font-size: 16px !important
            }

            .es-header-body p, .es-header-body ul li, .es-header-body ol li, .es-header-body a {
                font-size: 16px !important
            }

            .es-footer-body p, .es-footer-body ul li, .es-footer-body ol li, .es-footer-body a {
                font-size: 16px !important
            }

            .es-infoblock p, .es-infoblock ul li, .es-infoblock ol li, .es-infoblock a {
                font-size: 12px !important
            }

            *[class="gmail-fix"] {
                display: none !important
            }

            .es-m-txt-c, .es-m-txt-c h1, .es-m-txt-c h2, .es-m-txt-c h3 {
                text-align: center !important
            }

            .es-m-txt-r, .es-m-txt-r h1, .es-m-txt-r h2, .es-m-txt-r h3 {
                text-align: right !important
            }

            .es-m-txt-l, .es-m-txt-l h1, .es-m-txt-l h2, .es-m-txt-l h3 {
                text-align: left !important
            }

            .es-m-txt-r img, .es-m-txt-c img, .es-m-txt-l img {
                display: inline !important
            }

            .es-button-border {
                display: block !important
            }

            a.es-button {
                font-size: 20px !important;
                display: block !important;
                border-width: 15px 25px 15px 25px !important
            }

            .es-btn-fw {
                border-width: 10px 0px !important;
                text-align: center !important
            }

            .es-adaptive table, .es-btn-fw, .es-btn-fw-brdr, .es-left, .es-right {
                width: 100% !important
            }

            .es-content table, .es-header table, .es-footer table, .es-content, .es-footer, .es-header {
                width: 100% !important;
                max-width: 600px !important
            }

            .es-adapt-td {
                display: block !important;
                width: 100% !important
            }

            .adapt-img {
                width: 100% !important;
                height: auto !important
            }

            .es-m-p0 {
                padding: 0px !important
            }

            .es-m-p0r {
                padding-right: 0px !important
            }

            .es-m-p0l {
                padding-left: 0px !important
            }

            .es-m-p0t {
                padding-top: 0px !important
            }

            .es-m-p0b {
                padding-bottom: 0 !important
            }

            .es-m-p20b {
                padding-bottom: 20px !important
            }

            .es-mobile-hidden, .es-hidden {
                display: none !important
            }

            tr.es-desk-hidden, td.es-desk-hidden, table.es-desk-hidden {
                display: table-row !important;
                width: auto !important;
                overflow: visible !important;
                float: none !important;
                max-height: inherit !important;
                line-height: inherit !important
            }

            .es-desk-menu-hidden {
                display: table-cell !important
            }

            table.es-table-not-adapt, .esd-block-html table {
                width: auto !important
            }

            table.es-social {
                display: inline-block !important
            }

            table.es-social td {
                display: inline-block !important
            }
        }

        #outlook a {
            padding: 0;
        }

        .ExternalClass {
            width: 100%;
        }

        .ExternalClass,
        .ExternalClass p,
        .ExternalClass span,
        .ExternalClass font,
        .ExternalClass td,
        .ExternalClass div {
            line-height: 100%;
        }

        .es-button {
            mso-style-priority: 100 !important;
            text-decoration: none !important;
        }

        a[x-apple-data-detectors] {
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }

        .es-desk-hidden {
            display: none;
            float: left;
            overflow: hidden;
            width: 0;
            max-height: 0;
            line-height: 0;
            mso-hide: all;
        }

        .btn-success {
            color: #fff;
            background-color: #5cb85c;
            border-color: #4cae4c;
        }

        .btn {
            display: inline-block;
            margin-bottom: 0;
            font-weight: 400;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            background-image: none;
            border: 1px solid transparent;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            border-radius: 4px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
    </style>
</head>
<body
    style="width:100%;font-family:Poppins, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0">
<div class="es-wrapper-color" style="background-color:#F9F9F9">
    <!--[if gte mso 9]>
    <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
        <v:fill type="tile" color="#F9F9F9"></v:fill>
    </v:background>
    <![endif]-->
    <table class="es-wrapper" width="100%" cellspacing="0" cellpadding="0"
           style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top">
        <tr style="border-collapse:collapse">
            <td valign="top" style="padding:0;Margin:0">
                <table class="es-header" cellspacing="0" cellpadding="0" align="center"
                       style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:#55c940;background-repeat:repeat;background-position:center top">
                    <tr style="border-collapse:collapse">
                        <td style="padding:0;Margin:0;background-color:#55C940" bgcolor="#55c940" align="center">
                            <table class="es-header-body" cellspacing="0" cellpadding="0" align="center"
                                   style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#55c940;width:600px">
                                <tr style="border-collapse:collapse">
                                    <td align="left"
                                        style="Margin:0;padding-bottom:10px;padding-left:10px;padding-right:10px;padding-top:20px">
                                        <table width="100%" cellspacing="0" cellpadding="0"
                                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                            <tr style="border-collapse:collapse">
                                                <td valign="top" align="center" style="padding:0;Margin:0;width:580px">
                                                    <table width="100%" cellspacing="0" cellpadding="0"
                                                           role="presentation"
                                                           style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                                        <tr style="border-collapse:collapse">
                                                            <td align="center"
                                                                style="Margin:0;padding-left:10px;padding-right:10px;padding-top:25px;padding-bottom:25px;font-size:0">
                                                                <img
                                                                    src="https://www.entrasinpapel.com/img/icon-white.png"
                                                                    style="display:block;border:0;outline:none;text-decoration:none;"
                                                                    width="60" height="60"></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table class="es-content" cellspacing="0" cellpadding="0" align="center"
                       style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%">
                    <tr style="border-collapse:collapse">
                        <td style="padding:0;Margin:0;background-color:#55c940" bgcolor="#55c940" align="center">
                            <table class="es-content-body"
                                   style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:transparent;width:600px"
                                   cellspacing="0" cellpadding="0" align="center">
                                <tr style="border-collapse:collapse">
                                    <td align="left" style="padding:0;Margin:0">
                                        <table width="100%" cellspacing="0" cellpadding="0"
                                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                            <tr style="border-collapse:collapse">
                                                <td valign="top" align="center" style="padding:0;Margin:0;width:600px">
                                                    <table
                                                        style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:separate;border-spacing:0px;background-color:#FFFFFF;border-radius:4px"
                                                        width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff"
                                                        role="presentation">
                                                        <tr style="border-collapse:collapse">
                                                            <td align="center"
                                                                style="Margin:0;padding-bottom:5px;padding-left:30px;padding-right:30px;padding-top:70px">
                                                                <h1 style="Margin:0;font-family:Poppins, sans-serif;font-weight: 800;font-size: px; color: #02020C;">
                                                                    Resumen de Compra</h1></td>
                                                        </tr>
                                                        <tr style="border-collapse:collapse">
                                                            <td bgcolor="#ffffff" align="center"
                                                                style="Margin:0;padding-top:5px;padding-bottom:5px;padding-left:20px;padding-right:20px;font-size:0">
                                                                <table width="100%" height="100%" cellspacing="0"
                                                                       cellpadding="0" border="0" role="presentation"
                                                                       style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                                                    <tr style="border-collapse:collapse">
                                                                        <td style="padding:0;Margin:0px;border-bottom:1px solid #FFFFFF;background:#FFFFFFnone repeat scroll 0% 0%;height:1px;width:100%;margin:0px"></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table class="es-content" cellspacing="0" cellpadding="0" align="center"
                       style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%">
                    <tr style="border-collapse:collapse">
                        <td align="center" style="padding:0;Margin:0">
                            <table class="es-content-body"
                                   style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px"
                                   cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center">
                                <tr style="border-collapse:collapse">
                                    <td align="left" style="padding:0;Margin:0">
                                        <table width="100%" cellspacing="0" cellpadding="0"
                                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                            <tr style="border-collapse:collapse">
                                                <td valign="top" align="center" style="padding:0;Margin:0;width:600px">
                                                    <table class="es-content-body"
                                                           style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:transparent;width:600px"
                                                           cellspacing="0" cellpadding="0" align="center">
                                                        <tr style="border-collapse:collapse">
                                                            <td align="left" style="padding:0;Margin:0">
                                                                <table width="100%" cellspacing="0" cellpadding="0"
                                                                       style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                                                    <tr style="border-collapse:collapse">
                                                                        <td valign="top" align="center"
                                                                            style="padding:0;Margin:0;width:600px">
                                                                            <table
                                                                                style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:separate;border-spacing:0px;background-color:#FFFFFF;border-radius:4px"
                                                                                width="100%" cellspacing="0"
                                                                                cellpadding="0" bgcolor="#ffffff"
                                                                                role="presentation">
                                                                                <tr style="border-collapse:collapse">
                                                                                    @php(setlocale(LC_TIME,"es_PY.UTF-8"))
                                                                                    <td style="Margin:0;padding-bottom:5px;padding-left:40px;padding-right:40px;padding-top:20px">
                                                                                        <p>
                                                                                            <b>Cliente:</b> {{ $order->client->name }}
                                                                                        </p>
                                                                                        <p><b>N.º de orden:</b>
                                                                                            #{{ $order->id }}</p>
                                                                                        <p>
                                                                                            <b>Fecha:</b> {{ strftime("%d de %B de %Y", strtotime($order->created_at)) }}
                                                                                        </p>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr style="border-collapse:collapse">
                                                                                    <td style="Margin:0;padding-bottom:5px;padding-left:40px;padding-right:40px;padding-top:20px">
                                                                                        <table
                                                                                            style="border-bottom: 2px solid #f9f9f9 ;border-top: 2px solid #f9f9f9;">
                                                                                            <tr style="padding-bottom:20px;padding-left:10px;padding-right:40px;padding-top:20px;">
                                                                                                <td style="width:40%; font-size:14px; padding-top:10px; font-weight:800;">
                                                                                                    Evento
                                                                                                </td>
                                                                                                @if ($order->detail->filter(function($item) {
                                                                                                      return $item->sector->event->event_link != null;
                                                                                                 })->count() > 0)
                                                                                                    <td style="width:20%; text-align: center; font-size:14px; padding-top:10px; font-weight:800;">
                                                                                                        Link
                                                                                                    </td>
                                                                                                @endif
                                                                                                <td style="width:20%; text-align: center; font-size:14px; padding-top:10px; font-weight:800;">
                                                                                                    Entrada
                                                                                                </td>
                                                                                                <td style="width:20%; text-align: center; font-size:14px; padding-top:10px; font-weight:800;">
                                                                                                    Precio
                                                                                                </td>
                                                                                                <td style="width:15%; text-align: center; font-size:14px; padding-top:10px; font-weight:800;">
                                                                                                    Cantidad
                                                                                                </td>
                                                                                            </tr>
                                                                                            @foreach ($order->detail as $detail)
                                                                                                <tr>
                                                                                                    <td style="width:40%; font-size:14px; padding-top:10px; padding-bottom:10px;">
                                                                                                        {{ $detail->sector->event->event_name }}</td>
                                                                                                    @if (isset($detail->sector->event->event_link))
                                                                                                        <td style="width:20%; text-align: center; font-size:14px; padding-top:10px; font-weight:800;">
                                                                                                            <a target="_blank" href="{{ $detail->sector->event->event_link }}"
                                                                                                                class="btn btn-success">
                                                                                                                Acceder
                                                                                                            </a>
                                                                                                        </td>
                                                                                                    @endif
                                                                                                    <td style="width:20%; text-align: center; font-size:14px; padding-top:10px; padding-bottom:10px;">{{ $detail->sector->sector_name }}</td>
                                                                                                    <td style="width:20%; text-align: center; font-size:14px; padding-top:10px; padding-bottom:10px;">
                                                                                                        Gs. {{ number_format($detail->price, 0, ',', '.') }}</td>
                                                                                                    <td style="width:15%; text-align: center; font-size:14px; padding-top:10px; padding-bottom:10px;">{{ $detail->quantity }}</td>
                                                                                                </tr>
                                                                                            @endforeach
                                                                                            {{--<tr>
                                                                                              <td style="width:40%; font-size:14px; padding-top:10px; padding-bottom:10px;">Blockchain Express Webinar</td>
                                                                                              <td style="width:20%; text-align: center; font-size:14px; padding-top:10px; padding-bottom:10px;">VIP</td>
                                                                                              <td style="width:20%; text-align: center; font-size:14px; padding-top:10px; padding-bottom:10px;">Gs. 500.000</td>
                                                                                              <td style="width:15%; text-align: center; font-size:14px; padding-top:10px; padding-bottom:10px;">2</td>
                                                                                            </tr>--}}
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr style="border-collapse:collapse">
                                                                                    <td bgcolor="#ffffff" align="center"
                                                                                        style="Margin:0;padding-top:5px;padding-bottom:5px;padding-left:20px;padding-right:20px;font-size:0">
                                                                                        <table width="100%"
                                                                                               height="100%"
                                                                                               cellspacing="0"
                                                                                               cellpadding="0"
                                                                                               border="0"
                                                                                               role="presentation"
                                                                                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                                                                            <tr style="border-collapse:collapse">
                                                                                                <td style="padding:0;Margin:0px;border-bottom:1px solid #FFFFFF;background:#FFFFFFnone repeat scroll 0% 0%;height:1px;width:100%;margin:0px"></td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="border-collapse:collapse">
                                    <td align="left"
                                        style="padding:0;Margin:0;padding-bottom:20px;padding-left:30px;padding-right:30px">
                                        <table width="100%" cellspacing="0" cellpadding="0"
                                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                            <tr style="border-collapse:collapse">
                                                <td valign="top" style="padding:0;Margin:0;width:540px">
                                                    <table>
                                                        @if ($order->subtotal > 0)
                                                            <tr style="Margin:0;padding-bottom:20px;padding-left:10px;padding-right:40px;padding-top:40px;">
                                                                <td style="width:50%; font-size:14px; padding-top:10px; padding-bottom:10px;">
                                                                    <b>Metodo de Pago</b></td>
                                                                <td style="width:50%; font-size:14px; padding-top:10px; padding-bottom:10px; text-align: right;">
                                                                    {{ isset($order->payment->payment_method->method) ? $order->payment->payment_method->method : 'Sin medio' }}</td>
                                                            </tr>
                                                        @endif
                                                        <tr style="Margin:0;padding-bottom:20px;padding-left:10px;padding-right:40px;padding-top:20px;">
                                                            <td style="width:50%; font-size:14px; padding-top:10px; padding-bottom:10px;">
                                                                <b>Cantidad Total</b></td>
                                                            <td style="width:50%; font-size:14px; padding-top:10px; padding-bottom:10px; text-align: right;">
                                                                {{ $order->detail->sum('quantity') }}</td>
                                                        </tr>
                                                        <tr style="Margin:0;padding-bottom:20px;padding-left:10px;padding-right:40px;padding-top:20px;">
                                                            <td style="width:50%; font-size:14px; padding-top:10px; padding-bottom:10px;">
                                                                <b>Descuento</b></td>
                                                            <td style="width:50%; font-size:14px; padding-top:10px; padding-bottom:10px; text-align: right;">
                                                                0%
                                                            </td>
                                                        </tr>
                                                        @if ($order->subtotal > 0)
                                                            <tr style="Margin:0;padding-bottom:20px;padding-left:10px;padding-right:40px;padding-top:20px;">
                                                                <td style="width:50%; font-size:14px; padding-top:10px; padding-bottom:10px;">
                                                                    <b>Total</b></td>
                                                                <td style="width:50%; font-size:14px; padding-top:10px; padding-bottom:10px; text-align: right;">
                                                                    Gs.
                                                                    {{ number_format($order->subtotal, 0, ',', '.') }}</td>
                                                            </tr>
                                                        @endif
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table class="es-content" cellspacing="0" cellpadding="0" align="center"
                       style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%">
                    <tr style="border-collapse:collapse">
                        <td align="center" style="padding:0;Margin:0">
                            <table class="es-content-body"
                                   style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:transparent;width:600px"
                                   cellspacing="0" cellpadding="0" align="center">
                                <tr style="border-collapse:collapse">
                                    <td align="left" style="padding:0;Margin:0">
                                        <table width="100%" cellspacing="0" cellpadding="0"
                                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                            <tr style="border-collapse:collapse">
                                                <td valign="top" align="center" style="padding:0;Margin:0;width:600px">
                                                    <table width="100%" cellspacing="0" cellpadding="0"
                                                           role="presentation"
                                                           style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                                        <tr style="border-collapse:collapse">
                                                            <td align="center"
                                                                style="Margin:0;padding-top:10px;padding-bottom:20px;padding-left:20px;padding-right:20px;font-size:0">
                                                                <table width="100%" height="100%" cellspacing="0"
                                                                       cellpadding="0" border="0" role="presentation"
                                                                       style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                                                    <tr style="border-collapse:collapse">
                                                                        <td style="padding:0;Margin:0px;border-bottom:1px solid #F9F9F9;background:#FFFFFFnone repeat scroll 0% 0%;height:1px;width:100%;margin:0px"></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table cellpadding="0" cellspacing="0" class="es-footer" align="center"
                       style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top">
                    <tr style="border-collapse:collapse">
                        <td align="center" style="padding:0;Margin:0">
                            <table class="es-footer-body" cellspacing="0" cellpadding="0" align="center"
                                   style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:transparent;width:600px">
                                <tr style="border-collapse:collapse">
                                    <td align="left"
                                        style="Margin:0;padding-top:30px;padding-bottom:80px;padding-left:30px;padding-right:30px">
                                        <table width="100%" cellspacing="0" cellpadding="0"
                                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                            <tr style="border-collapse:collapse">
                                                <td valign="top" align="center" style="padding:0;Margin:0;width:540px">
                                                    <table width="100%" cellspacing="0" cellpadding="0"
                                                           role="presentation"
                                                           style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">

                                                        <tr style="border-collapse:collapse">
                                                            <td align="left"
                                                                style="padding:0;Margin:0;padding-top:25px"><p
                                                                    style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:Poppins, sans-serif;line-height:21px;color:#666666">
                                                                    Recibiste este correo electrónico porque has
                                                                    realizado una compra.</p></td>
                                                        </tr>
                                                        <tr style="border-collapse:collapse">
                                                            <td align="left"
                                                                style="padding:0;Margin:0;padding-top:25px"><p
                                                                    style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:Poppins, sans-serif;line-height:21px;color:#666666">
                                                                    Ante cualquier consulta, comuniquese a
                                                                    &nbsp;<strong><a target="_blank" class="unsubscribe"
                                                                                     href="mailto:info@entrasinpapel.com"
                                                                                     style="font-family:Poppins, sans-serif;font-size:14px;text-decoration:underline;color:#111111">info@entrasinpapel.com</a></strong>.
                                                                </p></td>
                                                        </tr>
                                                        <tr style="border-collapse:collapse">
                                                            <td align="left"
                                                                style="padding:0;Margin:0;padding-top:25px"><p
                                                                    style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:Poppins, sans-serif;line-height:21px;color:#666666">
                                                                    Teniente Primero Silverio Molinas 1489, Asunción
                                                                    1760</p></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
</body>
</html>
