@extends('admin.templates.layout')
@section('head')
    {{-- DataTable --}}
    <link href="https://cdn.datatables.net/1.10.17/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
@endsection
@section('content')
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4>Clientes</h4>
                    <a class="btn btn-primary" href="{{ route('admin.places.index') }}">Atrás</a>
                </div>
                <div class="card-body p-3">
                    <div class="table-responsive">
                        {{--<input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input">
                        <span class="custom-switch-indicator"></span>--}}
                        <table class="table table-striped data-table">
                            <thead>

                            <tr>
                                <th class="text-center">
                                    #
                                </th>
                                <th>C.I.</th>
                                <th>Nombre y Apellido</th>
                                <th>Dirección</th>
                                <th>Ciudad</th>
                                <th>Fecha Nac.</th>
                                <th>Teléfono</th>
                                <th>Correo</th>
                                <th>Creado</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>

                            {{--@foreach($clients as $client)
                                <tr>
                                    <td class="p-0 text-center">{{ $client->id }}</td>
                                    <td>{{ $client->doc_number }}</td>
                                    <td>{{ $client->name }}</td>
                                    <td>{{ $client->address }}</td>
                                    <td>{{ $client->born_date }}</td>
                                    <td>{{ $client->cell_phone }}</td>
                                    <td>{{ $client->email }}</td>
                                    <td>{{ $client->created_at }}</td>
                                </tr>
                            @endforeach--}}
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="/assets/js/page/forms-advanced-forms.js"></script>

    {{-- DataTable --}}
    <script src="https://cdn.datatables.net/1.10.17/js/jquery.dataTables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

    <script>
        function submit() {
            console.log($('#usuariosForm'))
        }

        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('admin.places.clients', $place) }}",
                language: {
                    url:      "/assets/json/DataTableSpanish.json",
                },
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'doc_number', name: 'doc_number'},
                    {data: 'name', name: 'name'},
                    {data: 'address', name: 'address'},
                    {data: 'city', name: 'city'},
                    {data: 'born_date', name: 'born_date'},
                    {data: 'cell_phone', name: 'cell_phone'},
                    {data: 'email', name: 'email'},
                    {data: 'created_at', name: 'created_at'},
                    /*{data: 'action', name: 'action', orderable: false, searchable: false},*/
                ]
            })
        })
    </script>
@endsection