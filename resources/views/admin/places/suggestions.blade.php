@extends('admin.templates.layout')
@section('content')
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4>Sugerencias</h4>
                    <a class="btn btn-primary" href="{{ route('admin.places.index') }}">Atrás</a>
                </div>
                <div class="card-body p-0">
                    <div class="table-responsive">
                        {{--<input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input">
                        <span class="custom-switch-indicator"></span>--}}
                        <table class="table table-striped">
                            <tr>
                                <th class="text-center">
                                    #
                                </th>
                                <th>Nombre</th>
                                <th>Sugerencia</th>
                                <th>Creado</th>
                            </tr>

                            @foreach($suggestions as $suggestion)
                                <tr>
                                    <td class="p-0 text-center">{{ $suggestion->id }}</td>
                                    <td>{{ $suggestion->name }}</td>
                                    <td>{{ $suggestion->message }}</td>
                                    <td>{{ $suggestion->created_at }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')

@endsection