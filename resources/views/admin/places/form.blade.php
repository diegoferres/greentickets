@extends('admin.templates.layout')
@section('head')
    <link rel="stylesheet" href="/assets/bundles/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
@endsection
@section('content')
    <div class="row">
        <div class="col-12 col-md-6 col-lg-6">
            <div class="card">
                @isset($place)
                    {!! Form::model($place, ['route' => ['admin.places.update', $place->id], 'method' => 'put', 'files' => 'true']) !!}
                @else
                    {!! Form::open(['route' => 'admin.places.store', 'method' => 'post', 'files' => 'true']) !!}
                @endisset
                <div class="card-header">
                    <h4>Lugares</h4>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>

                {{--{!! Form::open(['route' => 'admin.categories.store', 'method' => 'post']) !!}--}}
                <div class="card-body">

                    <div class="row pb-3">
                        <div class="col-sm-6">
                            @hasanyrole('admin')
                            <label for="nombres">Nombre</label>
                            {!! Form::text('name', old('name'), ['class' => $errors->has('name') ? 'form-control is-invalid' : 'form-control']) !!}
                            @error('name')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                            @endhasanyrole
                        </div>
                        <div class="col-sm-6">
                            @hasanyrole('place|admin')
                            <label for="cell_phone">Número p/ WhatsApp</label>
                            {!! Form::text('cell_phone', old('cell_phone'), ['class' => $errors->has('cell_phone') ? 'form-control is-invalid' : 'form-control']) !!}
                            @error('cell_phone')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                            @endhasanyrole
                        </div>
                    </div>
                    <div class="row pb-3">
                        <div class="col-sm-6">
                            @hasanyrole('place|admin')
                            <label for="cell_phone">Número p/ WhatsApp</label>
                            {!! Form::text('cell_phone', old('cell_phone'), ['class' => $errors->has('cell_phone') ? 'form-control is-invalid' : 'form-control']) !!}
                            @error('cell_phone')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                            @endhasanyrole
                        </div>
                        <div class="col-sm-6">
                            @hasanyrole('admin')
                            <label for="nombres">Color de Titulos</label>
                            <div class="input-group colorpickerinput">
                                {!! Form::text('font_color', old('font_color'), ['class' => $errors->has('font_color') ? 'form-control is-invalid' : 'form-control']) !!}
                                {{--<input type="text" class="form-control @error('font_color') is-invalid @enderror" name="font_color" value="{{ old('font_color') }}">--}}
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <i class="fas fa-fill-drip"></i>
                                    </div>
                                </div>
                                @error('font_color')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            @endhasanyrole
                        </div>
                    </div>
                    @hasanyrole('place|admin')
                    <div class="row pb-3">
                        <div class="col-sm-6">
                            <label for="nombres">URL Facebook</label>
                            {!! Form::text('facebook', old('facebook'), ['class' => 'form-control']) !!}
                        </div>
                        <div class="col-sm-6">
                            <label for="nombres">URL Instagram</label>
                            {!! Form::text('instagram', old('instagram'), ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="row pb-3">
                        <div class="col-sm-6">
                            <label for="nombres">URL Twitter</label>
                            {!! Form::text('twitter', old('twitter'), ['class' => 'form-control']) !!}
                        </div>
                        <div class="col-sm-6">
                            <label for="nombres">URL WhatsApp</label>
                            {!! Form::text('whatsapp', old('whatsapp'), ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    @endhasanyrole
                    @hasanyrole('admin')
                    <div class="form-group">
                        <label for="nombres">Adjuntar logo</label>

                        <input type="file" class="form-control @error('logo') is-invalid @enderror"
                               name="logo">
                        @error('logo')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    @endhasanyrole
                    @hasanyrole('place|admin')
                    <div class="form-group">
                        <label for="nombres">Titulo Archivo 1</label>
                        {!! Form::text('file_label', old('file_label'), ['class' => $errors->has('file_label') ? 'form-control is-invalid' : 'form-control']) !!}
                        @error('file_label')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="nombres">Adjuntar archivo 1</label>
                        <input type="file" class="form-control @error('file') is-invalid @enderror"
                               name="file" value="{{ old('file') }}">
                        @error('file')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    @endhasanyrole
                    <hr>
                    @hasanyrole('place|admin')
                    <div class="form-group">
                        <label for="nombres">Titulo Archivo 2</label>
                        {!! Form::text('file_extra_label', old('file_extra_label'), ['class' => $errors->has('file_extra_label') ? 'form-control is-invalid' : 'form-control']) !!}
                        @error('file_extra_label')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="nombres">Archivo 2</label>
                        <input type="file" class="form-control @error('file_extra') is-invalid @enderror"
                               name="file_extra">
                        @error('file_extra')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    @endhasanyrole
                    <hr>
                    @hasanyrole('admin')
                    <div class="form-group">
                        <label for="nombres">Fondo de Formulario</label>

                        <input type="file" class="form-control @error('background_img') is-invalid @enderror"
                               name="background_img">
                        @error('background_img')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    @endhasanyrole


                    @isset($place)
                        <div class="form-row">
                            <div class="col-md-6">
                                <div class="gallery gallery-fw" data-item-height="200">
                                    <div class="gallery-item"
                                         data-image="{{ asset('storage/places/logo/'.$place->logo) }}"
                                         data-title="Image 1"></div>
                                </div>
                            </div>
                        </div>
                    @endisset

                </div>
                {!! Form::close() !!}
            </div>
        </div>
        @isset($place)
        <div class="col-12 col-md-6 col-lg-6">
            <div class="card">
                <div class="card-header">
                    <h4>Archivos extras</h4>
                </div>
                <div class="card-body">

                    <div class="row">
                        {!! Form::open(['route' => 'admin.places.file.store', 'method' => 'post', 'files' => true]) !!}
                        <input type="hidden" name="place_id" value="{{ $place->id }}">
                        <div class="col-sm-12">
                            <label for="cell_phone">Título de archivo</label>
                            {!! Form::text('file_label', old('file_label'), ['class' => $errors->has('file') ? 'form-control is-invalid' : 'form-control']) !!}
                            @error('cell_phone')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="col-sm-12 pt-3 pb-3">
                            {!! Form::file('file', old('file'), ['class' => $errors->has('file') ? 'form-control is-invalid' : '', 'required']) !!}
                            @error('cell_phone')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="col-sm-12 pt-3 pb-3">
                            <button class="btn btn-primary" type="submit">Agregar archivo</button>
                        </div>

                        {!! Form::close() !!}
                    </div>
                    <hr>
                    <div class="form-group">
                        <p>Arrastrar para ordenar</p>
                        {!! Form::open(['route' => 'admin.places.file.sort', 'method' => 'post']) !!}
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <td>Titulo</td>
                                <td>Visible</td>
                                <td>Orden</td>
                                <td></td>
                            </tr>
                            </thead>
                            <tbody class="sortable">
                            @forelse($place->files as $file)
                                <tr>
                                    <td>{{ $file->file_label }}</td>
                                    <td>
                                        <label class="custom-switch mt-2">
                                            {!! Form::checkbox('active['.$file->id.']', '1', $file->active ? $file->active : 0,  ['id' => 'name', 'class' => 'custom-switch-input']) !!}
                                            <span class="custom-switch-indicator"></span>
                                            <span class="custom-switch-description">Visible</span>
                                        </label>
                                        {{--{!! $file->active ? '<span class="badge badge-success">Si</span>' : '<span class="badge badge-default">No</span>' !!}--}}
                                    </td>
                                    <td style="max-width: 25px">
                                        {{ $file->sort }}
                                        <input type="hidden" name="sort[{{ $file->id }}]" class="form-control">
                                    </td>
                                    <td><a href="{{ route('admin.places.file.delete', [$file->id, $place->slug]) }}"
                                           class="btn btn-danger"><i class="fas fa-trash"></i></a></td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="5">No hay archivos</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        <button class="btn btn-primary" type="submit">Guardar orden</button>
                        {!! Form::close() !!}
                    </div>

                </div>
            </div>
        </div>
        @endisset
        @hasanyrole('admin')
        <div class="col-12 col-md-6 col-lg-6">
            <div class="card">

                <div class="card-header">
                    <h4>QR</h4>
                </div>
                <div class="card-body">
                    @isset($place)
                        <div class="col-md-12">
                            <img src="data:image/png;base64,
                                {!! base64_encode(QrCode::format('png')
                                /*->merge('https://covid.entrasinpapel.com/storage/places/logo/zkk0NEqJfo7POzKiCCAxxBYuVPkm2D7KCDS9jA28.png', 0.5, true)*/
                                ->size(500)->errorCorrection('H')
                                ->generate('https://covid.entrasinpapel.com/'.$place->slug)) !!}">
                        </div>

                        {{-- CON LOGO EN EL CENTRO --}}
                        {{--<div class="col-md-12">
                            <img src="data:image/png;base64,
                                {!! base64_encode(QrCode::format('png')
                                ->merge('https://covid.entrasinpapel.com/storage/places/logo/'.$place->logo, 0.5, true)
                                ->size(500)->errorCorrection('H')
                                ->generate('https://covid.entrasinpapel.com/'.$place->slug)) !!}">
                        </div>--}}
                    @endisset
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        @endhasanyrole
        <div class="col-12 col-md-6 col-lg-6">
            <div class="card">

                <div class="card-header">
                    <h4>Archivo</h4>
                </div>
                <div class="card-body">
                    @isset($place)
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <iframe src='{{ asset('storage/places/menu/'.$place->file_name) }}' width='100%'
                                        height='500px' frameborder='0'>
                                </iframe>
                            </div>
                        </div>
                        @isset($place->file_extra)
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <iframe src='{{ asset('storage/places/menu/'.$place->file_extra) }}' width='100%'
                                            height='500px' frameborder='0'>
                                    </iframe>
                                </div>
                            </div>
                        @endisset
                    @endisset
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@section('script')
    <!-- Page Specific JS File -->
    {{--<script src="/assets/bundles/apexcharts/apexcharts.min.js"></script>--}}
    <!-- Template JS File -->
    <script src="/assets/js/scripts.js"></script>
    <!-- Custom JS File -->
    <script src="/assets/js/custom.js"></script>
    <script src="/assets/js/page/gallery1.js"></script>
    <script src="/assets/bundles/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function () {
            $(".sortable").sortable();
            $(".sortable").disableSelection();
        });
    </script>
    <script>
        function submit() {
            console.log($('#usuariosForm'))
        }

        $(document).ready(function () {
            $(".colorpickerinput").colorpicker({
                format: 'hex',
                component: '.input-group-append',
            });
        })
    </script>
@endsection
