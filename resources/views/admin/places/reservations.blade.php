@extends('admin.templates.layout')
@section('content')
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4>Reservas</h4>
                    <a class="btn btn-primary" href="{{ route('admin.places.index') }}">Atrás</a>
                </div>
                <div class="card-body p-0">
                    <div class="table-responsive">
                        {{--<input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input">
                        <span class="custom-switch-indicator"></span>--}}
                        <table class="table table-striped">
                            <tr>
                                <th class="text-center">
                                    #
                                </th>
                                <th>C.I.</th>
                                <th>Nombre y Apellido</th>
                                <th>Fecha Reservada</th>
                                <th>Personas</th>
                                <th>Teléfono</th>
                                <th>Creado</th>
                            </tr>

                            @foreach($reservations as $reservation)
                                <tr>
                                    <td class="p-0 text-center">{{ $reservation->id }}</td>
                                    <td>{{ $reservation->client->doc_number }}</td>
                                    <td>{{ $reservation->client->name }}</td>
                                    <td>{{ $reservation->reserved_date }}</td>
                                    <td>{{ $reservation->amount_people }}</td>
                                    <td>{{ $reservation->client->cell_phone }}</td>
                                    <td>{{ $reservation->created_at }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')

@endsection