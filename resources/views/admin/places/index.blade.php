@extends('admin.templates.layout')
@section('content')
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4>Lugares</h4>
                    <div class="card-header-form">
                        <form>
                            <div class="input-group">
                                @role('admin')
                                    <div class="input-group-btn">
                                        <a href="{{ route('admin.places.create') }}" class="btn btn-primary"><i
                                                    class="fas fa-plus"> Crear</i>
                                        </a>
                                    </div>
                                @endrole
                                &nbsp;
                                <input type="text" class="form-control" placeholder="Search">
                                <div class="input-group-btn">
                                    <button class="btn btn-primary"><i class="fas fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="card-body p-0">
                    <div class="table-responsive">
                        {{--<input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input">
                        <span class="custom-switch-indicator"></span>--}}
                        <table class="table table-striped">
                            <tr>
                                <th class="text-center">
                                    #
                                </th>
                                <th>Lugar</th>
                                <th>Teléfono</th>
                                <th>Estado</th>
                                <th>Clientes</th>
                                <th>Registros</th>
                                <th>Reservas</th>
                                <th>Sugerencias</th>
                                <th>Creado</th>
                                <th>Actualizado</th>
                                <th>Acciones</th>
                            </tr>

                            @foreach($places as $place)
                                <tr>
                                    <td class="p-0 text-center">{{ $place->id }}</td>
                                    <td>{{ $place->name }}</td>
                                    <td>{{ $place->cell_phone }}</td>
                                    <td>
                                        <div class="badge badge-success">Activo</div>
                                    </td>
                                    <td><a href="{{ route('admin.places.clients', $place->id) }}"
                                           class="btn btn-outline-primary"><i class="far fa-eye"></i></a></td>
                                    <td><a href="{{ route('admin.places.registers', $place->id) }}"
                                           class="btn btn-outline-primary">{{ $place->registers->count() }} <i class="far fa-eye"></i></a></td>
                                    <td><a href="{{ route('admin.places.reservations', $place->id) }}"
                                           class="btn btn-outline-primary">{{ $place->reservations->count() }} <i class="far fa-eye"></i></a></td>
                                    <td><a href="{{ route('admin.places.suggestions', $place->id) }}"
                                           class="btn btn-outline-primary">{{ $place->suggestions->count() }} <i class="far fa-eye"></i></a></td>
                                    <td>{{ $place->created_at }}</td>
                                    <td>{{ $place->updated_at }}</td>
                                    <td>
                                        @hasanyrole('place|admin')
                                        <a href="{{ route('admin.places.edit', [$place->id]) }}"
                                           class="btn btn-primary">Detalle</a>
                                        @endhasanyrole
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')

@endsection