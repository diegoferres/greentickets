<?php

use Illuminate\Support\Facades\Route;

$method = Route::currentRouteName();
?>
<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="/"> <img alt="image" src="/img/icon.png" class="header-logo"/> <span
                        class="logo-name">EntraSinPapel</span>
            </a>
        </div>
        <ul class="sidebar-menu">
{{--            @role('admin')--}}
{{--            <li class="menu-header">Principal</li>--}}
{{--            <li class="dropdown {{ $method == 'home' ? 'active' : ''}}">--}}
{{--                <a href="{{ route('admin.home') }}" class="nav-link"><i data-feather="monitor"></i><span>Tablero</span></a>--}}
{{--            </li>--}}
{{--            @endrole--}}
            @role('admin')
            <li class="menu-header">Seguridad</li>
            <li class="{{ $method == 'usuarios' || $method == 'usuarios.create' || $method == 'usuarios.edit' ? 'active' : '' }}">
                <a class="nav-link active" href="{{ route('admin.users.index') }}"><i data-feather="users"></i><span>Usuarios</span></a>
            @endrole
            @role('admin')
            <li class="menu-header">Administración</li>
            <li class="{{ $method == 'admin.categories.index' || $method == 'admin.categories.create' || $method == 'admin.categories.edit' ? 'active' : '' }}">
                <a class="nav-link {{ $method == 'admin.categories.index' || $method == 'admin.categories.create' || $method == 'admin.categories.edit' ? 'toggle' : '' }}"
                   href="{{ route('admin.categories.index') }}"><i
                            data-feather="settings"></i><span>Categorias</span></a></li>
            @endrole

            @role('admin')
            <li class="{{ $method == 'admin.events.index' || $method == 'admin.events.create' || $method == 'admin.events.edit' ? 'active' : '' }}">
                <a class="nav-link {{ $method == 'admin.events.index' || $method == 'admin.events.create' || $method == 'admin.events.edit' ? 'active' : '' }}"
                   href="{{ route('admin.events.index') }}"><i
                            data-feather="calendar"></i><span>Eventos</span></a></li>
            @endrole
{{--            <li class="dropdown {{ $method == 'admin.places.index' || $method == 'admin.places.create' || $method == 'admin.places.edit' ? 'active' : '' }}">--}}
{{--                <a href="{{ route('admin.places.index') }}"--}}
{{--                   class="nav-link {{ $method == 'admin.places.index' || $method == 'admin.places.create' || $method == 'admin.places.edit' ? 'toggled' : '' }}"><i--}}
{{--                            data-feather="home"></i><span>Lugares</span></a>--}}
{{--            </li>--}}
                {{--<ul class="dropdown-menu">
                    <li class="{{ $method == 'admin.places.index' ? 'active' : ''}}">
                        <a class="nav-link {{ $method == 'admin.places.index' ? 'toggled' : ''}}"
                           href="{{ route('admin.places.index') }}">Mis lugares</a>
                    </li>

                    <li class="{{ $method == 'admin.places.registers' ? 'active' : ''}}">
                        <a class="nav-link {{ $method == 'admin.places.registers' ? 'toggled' : ''}}"
                           href="{{ route('admin.places.registers', $) }}">Registros</a>
                    </li>

                    <li class="{{ $method == 'admin.places.reservations' ? 'active' : ''}}">
                        <a class="nav-link {{ $method == 'admin.places.reservations' ? 'toggled' : ''}}"
                           href="{{ route('admin.places.reservations') }}">Reservas</a>
                    </li>

                    <li class="{{ $method == 'admin.places.suggestions' ? 'active' : ''}}">
                        <a class="nav-link {{ $method == 'admin.places.suggestions' ? 'toggled' : ''}}"
                           href="{{ route('admin.places.suggestions') }}">Sugerencias</a>
                    </li>

                    <li class="{{ $method == 'admin.places.clients' ? 'active' : ''}}">
                        <a class="nav-link {{ $method == 'admin.places.clients' ? 'toggled' : ''}}"
                           href="{{ route('admin.places.clients') }}">Clientes</a>
                    </li>
                </ul>--}}



            {{--<li class="{{ $method == 'admin.events.index' || $method == 'admin.events.create' || $method == 'admin.events.edit' ? 'active' : '' }}">
                <a class="nav-link {{ $method == 'admin.places.index' || $method == 'admin.places.create' || $method == 'admin.places.edit' ? 'active' : '' }}"
                   href="{{ route('admin.places.index') }}"><i
                            data-feather="home"></i><span>Lugares</span></a></li>--}}
        </ul>
    </aside>
</div>
