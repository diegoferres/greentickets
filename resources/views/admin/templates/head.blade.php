<meta charset="UTF-8">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
<title>Entra Sin Papel</title>
<!-- General CSS Files -->
<link rel="stylesheet" href="/assets/css/app.min.css">
<link rel="stylesheet" href="/assets/bundles/izitoast/css/iziToast.min.css">
<link rel="stylesheet" href="/assets/bundles/select2/dist/css/select2.min.css">
<!-- Template CSS -->
<link rel="stylesheet" href="/assets/css/style.css">
<link rel="stylesheet" href="/assets/css/components.css">
<!-- Custom style CSS -->
<link rel="stylesheet" href="/assets/css/custom.css">
<link rel='shortcut icon' type='image/x-icon' href='/assets/img/favicon.ico'/>
