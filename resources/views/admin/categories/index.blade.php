@extends('admin.templates.layout')
@section('content')
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4>Categorías</h4>
                    <div class="card-header-form">
                        <form>
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <a href="{{ route('admin.categories.create') }}" class="btn btn-primary"><i class="fas fa-plus"> Crear</i>
                                    </a>
                                </div>
                                &nbsp;
                                <input type="text" class="form-control" placeholder="Search">
                                <div class="input-group-btn">
                                    <button class="btn btn-primary"><i class="fas fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="card-body p-0">
                    <div class="table-responsive">
                        {{--<input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input">
                        <span class="custom-switch-indicator"></span>--}}
                        <table class="table table-striped">
                            <tr>
                                <th class="text-center">
                                    #
                                </th>
                                <th>Categoría</th>
                                <th>Estado</th>
                                <th>Creado</th>
                                <th>Actualizado</th>
                                <th>Acciones</th>
                            </tr>

                            @foreach($categories as $category)
                                <tr>
                                    <td class="p-0 text-center">{{ $category->id }}</td>
                                    <td>{{ $category->category_name }}</td>
                                    <td></td>
                                    {{--<td><div class="badge badge-success">Completed</div></td>--}}
                                    <td>{{ $category->created_at }}</td>
                                    <td>{{ $category->updated_at }}</td>
                                    <td><a href="{{ route('admin.categories.edit', [$category->id]) }}" class="btn btn-primary">Detail</a></td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="/assets/js/page/forms-advanced-forms.js"></script>

    <script>
        function submit() {
            console.log($('#usuariosForm'))
        }
    </script>
@endsection