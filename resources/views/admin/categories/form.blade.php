@extends('admin.templates.layout')
@section('content')
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4>Categorias</h4>
                </div>
                @isset($category)
                    {!! Form::model($category, ['route' => ['admin.categories.update', $category->id], 'method' => 'put']) !!}
                @else
                    {!! Form::open(['route' => 'admin.categories.store', 'method' => 'post']) !!}
                @endisset
                {{--{!! Form::open(['route' => 'admin.categories.store', 'method' => 'post']) !!}--}}
                <div class="card-body">

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="nombres">Categoría</label>

                            {!! Form::text('category_name', old('category_name'), ['class' => 'form-control']) !!}
                            {{--<input type="text" class="form-control @error('category_name') is-invalid @enderror"
                                   id="nombres"
                                   name="category_name" placeholder="Categoría" value="{{ old('category_name') }}">
                            @error('category_name')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror--}}
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        function submit() {
            console.log($('#usuariosForm'))
        }
    </script>
@endsection