@extends('admin.templates.layout')
@section('head')
    <script src="/assets/js/jsqrcode/qr_packed.js"></script>
@endsection
@section('content')
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4>Scanear entrada</h4>
                </div>
                <div class="card-body">
                    <div class="form-row">
                        <div id="container">
                            <h1>QR Code Scanner</h1>

                            <a id="btn-scan-qr">
                                <img src="/assets/img/qr_icon.svg">
                            </a>

                            <canvas hidden="" id="qr-canvas" style="width: 100%"></canvas>

                            <div id="qr-result" hidden="">
                                <b>Data:</b> <span id="outputData"></span>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@section('script')
    {{--<script type="text/javascript" src="/assets/js/jsqrcode/grid.js"></script>
    <script type="text/javascript" src="/assets/js/jsqrcode/version.js"></script>
    <script type="text/javascript" src="/assets/js/jsqrcode/detector.js"></script>
    <script type="text/javascript" src="/assets/js/jsqrcode/formatinf.js"></script>
    <script type="text/javascript" src="/assets/js/jsqrcode/errorlevel.js"></script>
    <script type="text/javascript" src="/assets/js/jsqrcode/bitmat.js"></script>
    <script type="text/javascript" src="/assets/js/jsqrcode/datablock.js"></script>
    <script type="text/javascript" src="/assets/js/jsqrcode/bmparser.js"></script>
    <script type="text/javascript" src="/assets/js/jsqrcode/datamask.js"></script>
    <script type="text/javascript" src="/assets/js/jsqrcode/rsdecoder.js"></script>
    <script type="text/javascript" src="/assets/js/jsqrcode/gf256poly.js"></script>
    <script type="text/javascript" src="/assets/js/jsqrcode/gf256.js"></script>
    <script type="text/javascript" src="/assets/js/jsqrcode/decoder.js"></script>
    <script type="text/javascript" src="/assets/js/jsqrcode/qrcode.js"></script>
    <script type="text/javascript" src="/assets/js/jsqrcode/findpat.js"></script>
    <script type="text/javascript" src="/assets/js/jsqrcode/alignpat.js"></script>
    <script type="text/javascript" src="/assets/js/jsqrcode/databr.js"></script>--}}

    <script>
        /*$(document).ready(function () {
            console.log(qrcode.decode(url))
        })*/

        //const qrcode = window.qrcode;

        const video = document.createElement("video");
        const canvasElement = document.getElementById("qr-canvas");
        const canvas = canvasElement.getContext("2d");

        const qrResult = document.getElementById("qr-result");
        const outputData = document.getElementById("outputData");
        const btnScanQR = document.getElementById("btn-scan-qr");
        const ruta = '{!! url('check/tikect/hash') !!}';
        console.log(ruta);

        let scanning = false;

        qrcode.callback = res => {
            if (res) {
                console.log("En el callback del qrcode");
                console.dir(res);

                $.get( ruta + '/' +  res , function(data) {
                    console.dir(data);
                    if (data.status)
                    {
                        alert('Entrada habilitada');
                    }else
                    {
                        alert('Entrada no habilitada');
                    }
                }).fail(function() {
                        alert( "error" );
                });


                outputData.innerText = res;
                scanning = false;

                video.srcObject.getTracks().forEach(track => {
                    track.stop();
                });

                qrResult.hidden = false;
                canvasElement.hidden = true;
                btnScanQR.hidden = false;
            }
        };

        btnScanQR.onclick = () => {
            navigator.mediaDevices
                .getUserMedia({ video: { facingMode: "environment" } })
                .then(function(stream) {
                    scanning = true;
                    qrResult.hidden = true;
                    btnScanQR.hidden = true;
                    canvasElement.hidden = false;
                    video.setAttribute("playsinline", true); // required to tell iOS safari we don't want fullscreen
                    video.srcObject = stream;
                    video.play();
                    tick();
                    scan();
                });
        };

        function tick() {
            canvasElement.height = video.videoHeight;
            canvasElement.width = video.videoWidth;
            canvas.drawImage(video, 0, 0, canvasElement.width, canvasElement.height);

            scanning && requestAnimationFrame(tick);
        }

        function scan() {
            try {
                qrcode.decode();
            } catch (e) {
                setTimeout(scan, 300);
            }
        }
    </script>
@endsection
