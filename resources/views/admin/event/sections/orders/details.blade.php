@extends('admin.event.templates.layout')
@section('head')
    <link rel="stylesheet" href="/assets/bundles/jquery-selectric/selectric.css">
@endsection
@section('content')
    <div class="row">

        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">

                <div class="card-header">
                    Detalles de la Orden: {{ $order->id }}
                </div>


                <div class="card-header">
                    <h4>Detalles</h4>
                </div>
                <div class="card-body p-0">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <tr>
                                <th>#</th>
                                <th>Subtotal</th>
                                <th>Cantidad</th>
                                <th>Sector</th>

                            </tr>
                            @foreach($details as $detail)

                                <tr>
                                    <td>{{ $detail->id }}</td>
                                    <td>{{ number_format($detail->price,0,',','.') }}</td>
                                    <td>{{ $detail->quantity }}</td>
                                    <td>{{ $detail->sector->sector_name }}</td>

                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>


            </div>
        </div>
    </div>

@endsection
@section('script')
    <script src="/assets/bundles/jquery-selectric/jquery.selectric.min.js"></script>
    <script src="/assets/js/page/gallery1.js"></script>

    <script>

        $(document).ready(function () {

        })
    </script>
@endsection
