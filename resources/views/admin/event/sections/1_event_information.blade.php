@extends('admin.event.templates.layout')
@section('head')
    <link rel="stylesheet" href="/assets/bundles/jquery-selectric/selectric.css">
    <link rel="stylesheet" href="/assets/bundles/summernote/summernote-bs4.css">
@endsection
@section('content')
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                @isset($event)
                    {!! Form::model($event, ['route' => ['admin.event.info.store', $event->id], 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
                @endisset
                <div class="card-header">
                    <h4>Datos del evento</h4>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
                <div class="card-body">
                    <div class="form-row">
                        @isset($event->event_cover)
                            <div class="form-group col-md-12 col-md-offset-6">
                                <div class="gallery gallery-fw" data-item-height="150">
                                    <div class="gallery-item" data-image="{{ asset('storage/'.$event->event_cover) }}"
                                         data-title="Image 1"></div>
                                </div>
                            </div>
                        @endisset
                        <div class="form-group col-md-12">
                            <label for="nombres">Imagen</label>
                            {!! Form::file('event_cover', old('event_cover'), ['class' => ($errors->has('event_cover')) ? 'form-control is-invalid' : 'form-control']) !!}
                            @error('event_cover')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-row col-md-12">
                        <div class="form-group col-md-4">
                            <label for="nombres">Nombre</label>
                            {!! Form::text('event_name', old('event_name'), ['class' => ($errors->has('event_name')) ? 'form-control is-invalid' : 'form-control']) !!}
                            @error('event_name')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label for="nombres">Organizador</label>
                            {!! Form::text('event_organizer', old('event_organizer'), ['class' => ($errors->has('event_organizer')) ? 'form-control is-invalid' : 'form-control']) !!}
                            @error('event_organizer')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label for="category_id">Categoría</label>
                            {!! Form::select('event_category', $categories, null, ['class' => ($errors->has('event_category')) ? 'form-control is-invalid' : 'form-control', 'placeholder' => 'Seleccionar una categoría']) !!}
                            @error('event_category')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-row col-md-12">
                        <div class="form-group col-md-12">
                            <label for="nombres">Descripción del evento</label>
                            {!! Form::textarea('event_description', old('event_description'), ['class' => 'summernote']) !!}
                            <span id="total-characters">{{ strlen($event->event_description) }} de 30000 @error('event_description') <b
                                        style="color: red;">({{ $message }})</b>@enderror</span>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group">
                            <label class="form-label">Tipo de evento</label>
                            <div class="selectgroup w-100">
                                @foreach($eventFormats as $format)
                                    <label class="selectgroup-item">
                                        <input type="radio" name="event_format" value="{{ $format->id }}"
                                               class="selectgroup-input-radio"
                                               @if($event->event_format == $format->id )checked @endif>
                                        <span class="selectgroup-button selectgroup-button-icon">{{ $format->event_format }}</span>
                                    </label>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-header eventVenue" @if($event->event_format != 1)style="display: none"@endif>
                    <h4>Lugar del evento</h4>
                </div>
                <div class="card-body eventVenue" @if($event->event_format != 1)style="display: none"@endif>
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="nombres">Nombre</label>
                            {!! Form::text('venue_name', old('venue_name', $event->venue->venue_name ?? ''), ['class' => ($errors->has('venue_name')) ? 'form-control is-invalid' : 'form-control']) !!}
                            @error('venue_name')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label for="nombres">Dirección</label>
                            {!! Form::text('venue_address', old('venue_address', $event->venue->venue_address ?? ''), ['class' => ($errors->has('venue_address')) ? 'form-control is-invalid' : 'form-control']) !!}
                            @error('venue_address')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                </div>
                @isset($event)
                    {!! Form::close() !!}
                @endisset

            </div>
        </div>
    </div>
@endsection
@section('script')
    <!-- Page Specific JS File -->
    {{--<script src="/assets/bundles/apexcharts/apexcharts.min.js"></script>--}}
    <!-- Template JS File -->
    <script src="/assets/bundles/jquery-selectric/jquery.selectric.min.js"></script>
    <script src="/assets/js/page/gallery1.js"></script>

    <script src="/assets/bundles/summernote/summernote-bs4.js"></script>
    <script>

        $(document).ready(function () {

            $("input[name='event_format']").on("change", function () {
                if (this.value == 1) {
                    $(".eventVenue").fadeIn('slow');
                } else if (this.value == 2) {
                    $(".eventVenue").fadeOut('slow');
                } else if (this.value == 3) {
                    $(".eventVenue").fadeIn('slow');
                }
            });

            $(".note-editable").on("keydown keyup paste", function () {
                let characters = $(this).html().length;
                $('#total-characters').text(characters + ' de ' + 30000);
            });
        })
    </script>
@endsection