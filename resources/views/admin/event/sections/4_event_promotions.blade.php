@extends('admin.event.templates.layout')
@section('head')
    <link rel="stylesheet" href="/assets/bundles/jquery-selectric/selectric.css">
@endsection
@section('content')
    @empty($event)
        {!! Form::open(['route' => 'admin.events.store', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
    @endempty
    <div class="row">

        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">

                <div class="card-header">
                    @isset($date)
                        <h4>Editar promoción</h4>
                    @else
                        <h4>Crear promoción</h4>
                    @endisset
                </div>
                <div class="card-body">
                    @isset($date)
                        {!! Form::model($promotion, ['route' => ['admin.event.dates.store', $promotion->id], 'method' => 'post']) !!}
                    @else
                        {!! Form::open(['route' => 'admin.event.dates.store', 'method' => 'post']) !!}
                    @endisset
                    {!! Form::open(['route' => 'admin.event.sectors.store', 'method' => 'post']) !!}
                        <input type="hidden" name="date_id" value="{{ $promotion ? $promotion->id : '' }}">
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="nombres">Nombre</label>
                            {!! Form::text('name', 'Email', ['class' => 'form-control']) !!}
                            @error('event_date')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group col-md-3">
                            <label for="nombres">Tipo Promoción</label>
                            {!! Form::select('promotion_type', ['cupón','descuento'], null, ['class' => ($errors->has('event_date')) ? 'form-control is-invalid' : 'form-control']) !!}
                            @error('event_date')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group col-md-2">
                            <label for="nombres">Tipo Valor</label>
                            {!! Form::select('promotion_count', ['porcentaje', 'fijo'], old('event_time_start'), ['class' => ($errors->has('event_time_start')) ? 'form-control is-invalid' : 'form-control']) !!}
                            @error('event_time_start')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group col-md-2">
                            <label for="nombres">Valor</label>
                            {!! Form::number('promotion_value', old('event_time_end'), ['class' => ($errors->has('event_time_end')) ? 'form-control is-invalid' : 'form-control']) !!}
                            @error('event_time_end')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group col-md-2">
                            <label for="nombres">Fecha Fin</label>
                            {!! Form::number('promotion_start', old('event_time_end'), ['class' => ($errors->has('event_time_end')) ? 'form-control is-invalid' : 'form-control']) !!}
                            @error('event_time_end')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group col-md-2">
                            <label for="nombres">Fecha Inicio</label>
                            {!! Form::number('promotion_end', old('event_time_end'), ['class' => ($errors->has('event_time_end')) ? 'form-control is-invalid' : 'form-control']) !!}
                            @error('event_time_end')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                        @isset($date)
                            <button type="submit" class="btn btn-primary">Actualizar</button>
                            <a href="{{ route('admin.event.dates') }}" class="btn btn-info">Ir a crear</a>
                        @else
                            <button type="submit" class="btn btn-primary">Crear</button>
                        @endisset
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Modal
                            With Form</button>
                    {!! Form::close() !!}
                </div>
                <div class="card-header">
                    <h4>Sectores creados</h4>
                </div>
                <div class="card-body p-0">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <tr>
                                {{--<th class="text-center">
                                    <div class="custom-checkbox custom-checkbox-table custom-control">
                                        <input type="checkbox" data-checkboxes="mygroup" data-checkbox-role="dad"
                                               class="custom-control-input" id="checkbox-all">
                                        <label for="checkbox-all" class="custom-control-label">&nbsp;</label>
                                    </div>
                                </th>--}}
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Tipo</th>
                                <th>Hora Fin</th>
                                <th>Actualización</th>
                                <th>Acciones</th>
                            </tr>
                            @foreach($event->dates()->orderBy('id', 'desc')->get() as $date)

                                <tr>
                                    {{--<td class="p-0 text-center">
                                        <div class="custom-checkbox custom-control">
                                            <input type="checkbox" data-checkboxes="mygroup" class="custom-control-input"
                                                   id="checkbox-1">
                                            <label for="checkbox-1" class="custom-control-label">&nbsp;</label>
                                        </div>
                                    </td>--}}
                                    <td>{{ $date->id }}</td>
                                    <td>{{ $date->event_date }}</td>
                                    <td>{{ date('H:s', strtotime($date->event_time_start)) }} Hs</td>
                                    <td>{{ date('H:s', strtotime($date->event_time_end)) }} Hs</td>
                                    <td>{{ $date->updated_at }}</td>
                                    {{--<td class="align-middle">
                                        <div class="progress-text">50%</div>
                                        <div class="progress" data-height="6">
                                            <div class="progress-bar bg-success" data-width="50%"></div>
                                        </div>
                                    </td>--}}
                                    {{--<td>2018-01-20</td>
                                    <td>2019-05-28</td>
                                    <td>
                                        <div class="badge badge-success">Low</div>
                                    </td>--}}
                                    <td><a href="{{ route('admin.event.dates', $date->id) }}" class="btn btn-outline-primary">Editar</a></td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>

    @empty($event)
        {!! Form::close() !!}
    @endempty
@endsection
@section('script')
    <!-- Modal with form -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="formModal"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="formModal">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card-body">
                        <form id="wizard_with_validation" method="POST">
                            <h3>Account Information</h3>
                            <fieldset>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Username*</label>
                                        <input type="text" class="form-control" name="username" required>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Password*</label>
                                        <input type="password" class="form-control" name="password" id="password" required>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Confirm Password*</label>
                                        <input type="password" class="form-control" name="confirm" required>
                                    </div>
                                </div>
                            </fieldset>
                            <h3>Profile Information</h3>
                            <fieldset>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">First Name*</label>
                                        <input type="text" name="name" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Last Name*</label>
                                        <input type="text" name="surname" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Email*</label>
                                        <input type="email" name="email" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Address*</label>
                                        <textarea name="address" cols="30" rows="3" class="form-control no-resize"
                                                  required></textarea>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Age*</label>
                                        <input min="18" type="number" name="age" class="form-control" required>
                                    </div>
                                    <div class="help-info">The warning step will show up if age is less than 18</div>
                                </div>
                            </fieldset>
                            <h3>Terms &amp; Conditions - Finish</h3>
                            <fieldset>
                                <input id="acceptTerms-2" name="acceptTerms" type="checkbox" required>
                                <label for="acceptTerms-2">I agree with the Terms and Conditions.</label>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page Specific JS File -->
    {{--<script src="/assets/bundles/apexcharts/apexcharts.min.js"></script>--}}
    <!-- Template JS File -->
    <script src="/assets/bundles/jquery-selectric/jquery.selectric.min.js"></script>


    <script src="/assets/bundles/jquery-validation/dist/jquery.validate.min.js"></script>
    <!-- JS Libraies -->
    <script src="/assets/bundles/jquery-steps/jquery.steps.min.js"></script>
    <!-- Page Specific JS File -->
    <script src="/assets/js/page/form-wizard.js"></script>

    <script>

        $(document).ready(function () {

            $('.add_slot').click(function () {
                var addBtn = $(this).clone().remove();
                var deleteBtn = $(this).siblings('a').clone().remove();

                var appendHtml = $(this).siblings().first().clone().appendTo($(this).parent());

                $.each(appendHtml.children().children('input'), function (index, input) {
                    $(input).attr('name', $(this).attr('id'))
                });

                appendHtml.css('display', '')

                //appendHtml.parent().append(addBtn);
                //appendHtml.parent().append(deleteBtn);
                $(this).remove()
                $(this).siblings('a').remove();

            });

            $('.delete_slot').click(function () {
                $(this).siblings().last().remove()
            })
        })
    </script>
@endsection