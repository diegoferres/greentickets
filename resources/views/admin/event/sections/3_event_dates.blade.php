@extends('admin.event.templates.layout')
@section('head')
    <link rel="stylesheet" href="/assets/bundles/jquery-selectric/selectric.css">
@endsection
@section('content')
    @empty($event)

        {!! Form::open(['route' => 'admin.events.store', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
    @endempty
    <div class="row">

        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">

                <div class="card-header">
                    @isset($date)
                        <h4>Editar fecha</h4>
                    @else
                        <h4>Crear fecha</h4>
                    @endisset
                </div>
                <div class="card-body">

                    {!! Form::model($event, ['route' => ['admin.event.dates.store', $event->id], 'method' => 'post']) !!}
                    <div class="form-row">
                        <div class="form-group">
                            <label class="form-label">Frecuencia del evento</label>
                            <div class="selectgroup w-100">
                                @foreach($eventFrecuency as $frecuency)
                                    <label class="selectgroup-item">
                                        <input type="radio" name="event_frecuency" value="{{ $frecuency->id }}"
                                               class="selectgroup-input-radio"
                                               @if($event->event_frecuency == $frecuency->id )checked @endif>
                                        <span class="selectgroup-button selectgroup-button-icon">{{ $frecuency->frecuency }}</span>
                                    </label>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="date_id" value="{{ $date ? $date->id : '' }}">
                    <div class="form-row" id="fre2" {{ $event->event_frecuency == 2 ?: 'style=display:none' }}>
                        <div class="form-group col-md-3">
                            <label for="nombres">Fecha</label>
                            {!! Form::date('event_date', old('event_date'), ['class' => ($errors->has('event_date')) ? 'form-control is-invalid' : 'form-control']) !!}
                            @error('event_date')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group col-md-3">
                            <label for="nombres">Hora Inicio</label>
                            {!! Form::time('event_time_start', old('event_time_start'), ['class' => ($errors->has('event_time_start')) ? 'form-control is-invalid' : 'form-control']) !!}
                            @error('event_time_start')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group col-md-3">
                            <label for="nombres">Hora Fin</label>
                            {!! Form::time('event_time_end', old('event_time_end'), ['class' => ($errors->has('event_time_end')) ? 'form-control is-invalid' : 'form-control']) !!}
                            @error('event_time_end')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-row" id="fre1" {{ $event->event_frecuency == 1 ?: 'style=display:none' }}>
                        <div class="form-group col-md-3">
                            <label for="nombres">Fecha Inicio</label>
                            {!! Form::date('event_date_start', old('event_date_start'), ['class' => ($errors->has('event_date_start')) ? 'form-control is-invalid' : 'form-control']) !!}
                            @error('event_date_start')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group col-md-3">
                            <label for="nombres">Hora Inicio</label>
                            {!! Form::time('event_time_st', old('event_time_st') ? old('event_time_st') : $event->event_time_start, ['class' => ($errors->has('event_time_st')) ? 'form-control is-invalid' : 'form-control']) !!}
                            @error('event_time_st')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group col-md-3">
                            <label for="nombres">Fecha Fin</label>
                            {!! Form::date('event_date_end', old('event_date_end'), ['class' => ($errors->has('event_date_end')) ? 'form-control is-invalid' : 'form-control']) !!}
                            @error('event_date_end')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group col-md-3">
                            <label for="nombres">Hora Fin</label>
                            {!! Form::time('event_time_ed', old('event_time_ed') ? old('event_time_ed') : $event->event_time_end, ['class' => ($errors->has('event_time_ed')) ? 'form-control is-invalid' : 'form-control']) !!}
                            @error('event_time_ed')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-row">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="card-header">
                    <h4>Sectores creados</h4>
                </div>
                <div class="card-body p-0">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <tr>
                                <th>#</th>
                                <th>Fecha</th>
                                <th>Hora Inicio</th>
                                <th>Hora Fin</th>
                                <th>Actualización</th>
                                <th>Acciones</th>
                            </tr>
                            @foreach($event->dates()->orderBy('id', 'desc')->get() as $date)

                                <tr>
                                    <td>{{ $date->id }}</td>
                                    <td>{{ date('d-m-Y', strtotime($date->event_date)) }}</td>
                                    <td>{{ date('H:s', strtotime($date->event_time_start)) }} Hs</td>
                                    <td>{{ date('H:s', strtotime($date->event_time_end)) }} Hs</td>
                                    <td>{{ $date->updated_at }}</td>
                                    <td>
                                        <form method="POST" action="{{ route('admin.event.dates.delete', $date->id) }}">
                                            @csrf
                                            @method('DELETE')

                                            <button class="btn btn-outline-primary" type="submit">Eliminar</button>
                                        </form>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
    @empty($event)
        {!! Form::close() !!}
    @endempty
@endsection
@section('script')
    <!-- Page Specific JS File -->
    {{--<script src="/assets/bundles/apexcharts/apexcharts.min.js"></script>--}}
    <!-- Template JS File -->
    <script src="/assets/bundles/jquery-selectric/jquery.selectric.min.js"></script>
    <script src="/assets/js/page/gallery1.js"></script>

    <script>
        $(document).ready(function () {

            $("input[name='event_frecuency']").on("change", function () {

                if (this.value == 1) {
                    $("#fre1").fadeIn('slow');
                    $("#fre2").fadeOut('slow');
                } else if (this.value == 2) {
                    $("#fre2").fadeIn('slow');
                    $("#fre1").fadeOut('slow');
                }
            });
        })
    </script>
@endsection