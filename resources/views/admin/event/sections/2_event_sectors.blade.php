@extends('admin.event.templates.layout')
@section('head')
    <link rel="stylesheet" href="/assets/bundles/jquery-selectric/selectric.css">
@endsection
@section('content')
    @empty($event)

        {!! Form::open(['route' => 'admin.events.store', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
    @endempty
    <div class="row">

        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">

                <div class="card-header">
                    @isset($sector)
                        <h4>Editar sector</h4>
                    @else
                        <h4>Crear sector</h4>
                    @endisset
                </div>
                <div class="card-body">

                    @isset($sector)
                        {!! Form::model($sector, ['route' => ['admin.event.sectors.store', $sector->id], 'method' => 'post']) !!}
                    @else
                        {!! Form::open(['route' => 'admin.event.sectors.store', 'method' => 'post']) !!}
                    @endisset
                        <input type="hidden" name="sector_id" value="{{ $sector ? $sector->id : '' }}">
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="nombres">Nombre</label>
                            {!! Form::text('sector_name', old('sector_name'), ['class' => ($errors->has('sector_name')) ? 'form-control is-invalid' : 'form-control']) !!}
                            @error('sector_name')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group col-md-3">
                            <label for="nombres">Precio</label>
                            {!! Form::number('sector_price', old('sector_price'), ['class' => ($errors->has('sector_price')) ? 'form-control is-invalid' : 'form-control']) !!}
                            @error('sector_price')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        {{--<div class="form-group col-md-2">
                            <label for="category_id">Categoría</label>
                            {!! Form::select('event_category', $categories, null, ['class' => ($errors->has('event_category')) ? 'form-control is-invalid' : 'form-control', 'placeholder' => 'Seleccionar una categoría']) !!}
                            @error('event_category')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>--}}
                        <div class="form-group col-md-4">
                            <label for="nombres">Capacidad</label>
                            {!! Form::number('sector_capacity', old('sector_capacity'), ['class' => ($errors->has('sector_capacity')) ? 'form-control is-invalid' : 'form-control']) !!}
                            @error('sector_capacity')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                        @isset($sector)
                            <button type="submit" class="btn btn-primary">Actualizar</button>
                            <a href="{{ route('admin.event.sectors') }}" class="btn btn-info">Ir a crear</a>
                        @else
                            <button type="submit" class="btn btn-primary">Crear</button>
                        @endisset
                    {!! Form::close() !!}
                </div>
                <div class="card-header">
                    <h4>Sectores creados</h4>
                </div>
                <div class="card-body p-0">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <tr>
                                <th>#</th>
                                <th>Sector</th>
                                <th>Precio</th>
                                <th>Capacidad</th>
                                <th>Actualización</th>
                                <th>Acciones</th>
                            </tr>
                            @foreach($event->sectors as $sector)

                                <tr>
                                    <td>{{ $sector->id }}</td>
                                    <td>{{ $sector->sector_name }}</td>
                                    <td>{{ number_format($sector->sector_price, 0, ",", ".") }}</td>
                                    <td>{{ number_format($sector->sector_capacity, 0, ",", ".") }}</td>
                                    <td>{{ $sector->updated_at }}</td>
                                    {{--<td class="align-middle">
                                        <div class="progress-text">50%</div>
                                        <div class="progress" data-height="6">
                                            <div class="progress-bar bg-success" data-width="50%"></div>
                                        </div>
                                    </td>--}}
                                    {{--<td>2018-01-20</td>
                                    <td>2019-05-28</td>
                                    <td>
                                        <div class="badge badge-success">Low</div>
                                    </td>--}}
                                    <td><a href="{{ route('admin.event.sectors', $sector->id) }}" class="btn btn-outline-primary">Editar</a></td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
    @empty($event)
        {!! Form::close() !!}
    @endempty
@endsection
@section('script')
    <!-- Page Specific JS File -->
    {{--<script src="/assets/bundles/apexcharts/apexcharts.min.js"></script>--}}
    <!-- Template JS File -->
    <script src="/assets/bundles/jquery-selectric/jquery.selectric.min.js"></script>
    <script src="/assets/js/page/gallery1.js"></script>

    <script>

        $(document).ready(function () {

        })
    </script>
@endsection