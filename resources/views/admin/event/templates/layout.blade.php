<!DOCTYPE html>
<html lang="en">
<!-- index.html  21 Nov 2019 03:44:50 GMT -->
<head>
    @include('admin.event.templates.head')
    @yield('head')
</head>
<body>
<div class="loader"></div>
<div id="app">
    <div class="main-wrapper main-wrapper-1">
        <div class="navbar-bg"></div>
        <!-- NAVBAR -->
        @include('admin.event.templates.nav')
        <!-- END NAVBAR -->
        <!-- SIDEBAR -->
        @include('admin.event.templates.sidebar')
        <!-- END SIDEBAR -->
        <!-- Main Content -->
        <div class="main-content">
            <section class="section">
                @yield('content')
            </section>
{{--            @include('admin.event.templates.settings_sidebar')--}}
        </div>
        <footer class="main-footer">
            <div class="footer-left">
                <a href="/">Entra Sin Papel</a></a>
            </div>
            <div class="footer-right">
            </div>
        </footer>
    </div>
</div>
@include('admin.event.templates.scripts')
@yield('script')
</body>
<!-- index.html  21 Nov 2019 03:47:04 GMT -->
</html>
