<?php

use Illuminate\Support\Facades\Route;

$method = Route::currentRouteName();
?>
<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="{{ route('admin.event.info') }}"> <img alt="image" src="/assets/img/logo.png" class="header-logo"/> <span
                        class="logo-name">{{ session('event')->event_name }}</span>
            </a>
        </div>
        <ul class="sidebar-menu">
{{--            <li class="menu-header">Principal</li>--}}
{{--            <li class="dropdown {{ $method == 'admin.event.dashboard' ? 'active' : ''}}">--}}
{{--                <a href="{{ route('admin.event.dashboard') }}" class="nav-link"><i data-feather="monitor"></i><span>Tablero</span></a>--}}
{{--            </li>--}}
            <li class="menu-header">Administración</li>
            <li class="{{ $method == 'admin.event.info' ? 'active' : '' }}">
                <a class="nav-link active" href="{{ route('admin.event.info') }}"><i data-feather="info"></i><span>Información básica</span></a>
            <li class="{{ $method == 'admin.event.sectors' ? 'active' : '' }}">
                <a class="nav-link active" href="{{ route('admin.event.sectors') }}"><i data-feather="copy"></i><span>Entradas</span></a>
            <li class="{{ $method == 'admin.event.dates' ? 'active' : '' }}">
                <a class="nav-link active" href="{{ route('admin.event.dates') }}"><i data-feather="calendar"></i><span>Fechas</span></a>
            <li class="{{ $method == 'usuarios' || $method == 'usuarios.create' || $method == 'usuarios.edit' ? 'active' : '' }}">
                <a class="nav-link active" href="{{ route('admin.event.payments') }}"><i data-feather="dollar-sign"></i><span>Pagos</span></a>
            <li class="{{ $method == 'usuarios' || $method == 'admin.event.promotions' ? 'active' : '' }}">
                <a class="nav-link active" href="{{ route('admin.event.promotions') }}"><i data-feather="users"></i><span>Promociones</span></a>
        </ul>
    </aside>
</div>
