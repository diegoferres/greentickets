@extends('admin.templates.layout')
@section('content')
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                @isset($user)
                    {!! Form::model($user, ['route' => ['admin.users.update', $user->id], 'method' => 'put']) !!}
                @else
                    {!! Form::open(['route' => 'admin.users.store', 'method' => 'post']) !!}
                @endisset
                <div class="card-header">
                    <h4>Datos del Usuario</h4>
                </div>
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="name">Nombres</label>
                            {!! Form::text('name', old('name'), ['class' => ($errors->has('name')) ? 'form-control is-invalid' : 'form-control' ]) !!}
                            @error('name')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label for="email">Correo</label>
                            {!! Form::text('email', old('email'), ['class' => ($errors->has('email')) ? 'form-control is-invalid' : 'form-control' ]) !!}
                            @error('email')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label for="born_date">Fecha Nac.</label>
                            {!! Form::date('born_date', old('born_date'), ['class' => ($errors->has('born_date')) ? 'form-control is-invalid' : 'form-control']) !!}
                            @error('born_date')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <label for="ci">Tipo Doc.</label>
                            {!! Form::select('doc_type_id', $doc_types , isset($user->doc_type_id) ? $user->doc_type_id : [], ['class' => ($errors->has('doc_type_id')) ? 'form-control is-invalid select2' : 'form-control select2', 'placeholder' => 'Selecciona el tipo']) !!}
                            @error('doc_type_id')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group col-md-2">
                            <label for="ci">Número Doc.</label>
                            {!! Form::number('doc_number', old('doc_number'), ['class' => ($errors->has('doc_number')) ? 'form-control is-invalid' : 'form-control' ]) !!}
                            @error('doc_number')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                            {{--{{ dd($errors->all()) }}--}}
                        </div>

                        <div class="form-group col-md-3">
                            <label for="cell_phone">Celular</label>
                            {!! Form::number('cell_phone', old('cell_phone'), ['class' => ($errors->has('cell_phone')) ? 'form-control is-invalid' : 'form-control', 'placeholder' => '0981 999 888' ]) !!}
                            @error('cell_phone')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group col-md-3">
                            <label for="telephone">Teléfono</label>
                            {!! Form::number('telephone', old('telephone'), ['class' => ($errors->has('telephone')) ? 'form-control is-invalid' : 'form-control', 'placeholder' => '021 222 333' ]) !!}
                            @error('telephone')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group col-md-2">
                            <label for="telefono">Sexo</label>
                            {!! Form::select('sex_id', $sexs , isset($user->sex_id) ? $user->sex_id: [], ['class' => ($errors->has('sex_id')) ? 'form-control is-invalid select2' : 'form-control select2', 'placeholder' => 'Selecciona el sexo']) !!}
                            @error('sex_id')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <label for="password">Contraseña</label>
                            {!! Form::password('password', ['class' => ($errors->has('password')) ? 'form-control is-invalid' : 'form-control',]) !!}
                            @error('password')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group col-md-2">
                            <label for="password_confirmation">Confirmar Contraseña</label>
                            {!! Form::password('password_confirmation', ['class' => 'form-control',]) !!}
                        </div>
                        <div class="form-group col-md-2">
                            <label for="ci">Roles</label>

                            {!! Form::select('role_id[]', $roles , isset($user->roles) ? $user->roles->pluck('id') : null, ['class' => ($errors->has('role_id')) ? 'form-control is-invalid select2' : 'form-control select2', 'placeholder' => 'Selecciona el rol', 'multiple']) !!}
                            @error('role_id')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="card-header">
                    <h4>Datos de la Dirección</h4>
                </div>
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="city_id">Ciudad</label>
                            {!! Form::select('city_id', $cities , isset($user->neighborhood->city_id) ? $user->neighborhood->city_id: [], ['class' => ($errors->has('sex_id')) ? 'form-control is-invalid select2' : 'form-control select2', 'placeholder' => 'Selecciona la ciudad']) !!}
                            @error('city_id')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group col-md-3">
                            <label for="neighborhood_id">Barrio</label>
                            {!! Form::select('neighborhood_id', $neighborhoods , isset($user->neighborhood_id) ? $user->neighborhood_id : [], ['class' => ($errors->has('neighborhood_id')) ? 'form-control is-invalid select2' : 'form-control select2', 'placeholder' => 'Selecciona el barrio']) !!}
                            @error('neighborhood_id')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group col-md-3">
                            <label for="street1">Calle 1</label>
                            {!! Form::text('street1', old('street1'), ['class' => ($errors->has('street1')) ? 'form-control is-invalid' : 'form-control' ]) !!}
                            @error('street1')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group col-md-3">
                            <label for="street2">Calle 2</label>
                            {!! Form::text('street2', old('street2'), ['class' => ($errors->has('street2')) ? 'form-control is-invalid' : 'form-control' ]) !!}
                            @error('street2')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-row">

                    </div>
                </div>
                <div class="card-header">
                    <h4>Datos de Facturación</h4>
                </div>
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="business_name">Razón Social</label>
                            {!! Form::text('business_name', old('business_name'), ['class' => ($errors->has('business_name')) ? 'form-control is-invalid' : 'form-control' ]) !!}
                            @error('business_name')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label for="ruc_number">R.U.C.</label>
                            {!! Form::text('ruc_number', old('ruc_number'), ['class' => ($errors->has('ruc_number')) ? 'form-control is-invalid' : 'form-control' ]) !!}
                            @error('ruc_number')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection