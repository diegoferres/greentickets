@extends('admin.templates.layout')
@section('content')
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4>Eventos</h4>
                    <div class="card-header-form">
                        <form>
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <a href="{{ route('admin.events.create') }}" class="btn btn-primary"><i
                                                class="fas fa-plus"> Crear</i>
                                    </a>
                                </div>
                                &nbsp;
                                <input type="text" class="form-control" placeholder="Search">
                                <div class="input-group-btn">
                                    <button class="btn btn-primary"><i class="fas fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="card-body p-0">
                    <div class="table-responsive">
                        {{--<input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input">
                        <span class="custom-switch-indicator"></span>--}}
                        <table class="table table-striped">
                            <tr>
                                <th class="text-center">
                                    #
                                </th>
                                <th>Evento</th>
                                <th>Organizador</th>
                                <th>Categoría</th>
                                <th>Fechas</th>
                                <th>Cantidad</th>
                                <th>Vendidas</th>
                                <th>Creado el</th>
                                <th>Actualizado el</th>
                                <th>Acciones</th>
                            </tr>

                            @foreach($events as $event)
                                <tr>
                                    <td class="p-0 text-center">{{ $event->id }}</td>
                                    <td>{{ $event->event_name }}</td>
                                    <td>{{ $event->event_organizer }}</td>
                                    <td>{{ $event->category->category_name }}</td>
                                    <td>
                                        @foreach($event->dates as $date)
                                            @if(!$loop->last)
                                                <span class="badge badge-info">{{ $date->event_date }}</span><br>
                                            @else
                                                <span class="badge badge-info">{{ $date->event_date }}</span>
                                            @endif

                                        @endforeach
                                    </td>
                                    <td>{{ $event->sectors->sum('sector_capacity') }}</td>
                                    <td>{{ $event->orders_detail->sum('quantity') }}</td>
                                    <td>{{ $event->created_at }}</td>
                                    <td>{{ $event->updated_at }}</td>
                                    <td>
                                        <form action="{{route('admin.event.show',[$event->id])}}" method="POST">
                                            @csrf
                                            <input type="hidden" name="event_id" value="{{ $event->id }}">
                                            <button style="margin-bottom: 5px" class="btn btn-outline-info" type="submit">Ver</button>
                                        </form>
                                        {{--<a href="{{ route('admin.events.edit', [$event->id]) }}"
                                           class="btn btn-primary">Editar</a>--}}
                                        <form action="{{route('admin.events.destroy',[$event->id])}}" method="POST">
                                            @method('DELETE')
                                            @csrf
                                            <button style="margin-bottom: 5px" class="btn btn-outline-danger" type="submit">Eliminar</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <!-- Page Specific JS File -->
    {{--<script src="/assets/bundles/apexcharts/apexcharts.min.js"></script>--}}
    <!-- Template JS File -->
    <script src="/assets/js/scripts.js"></script>
    <!-- Custom JS File -->
    <script src="/assets/js/custom.js"></script>
    <script src="/assets/js/page/forms-advanced-forms.js"></script>

    <script>
        function submit() {
            console.log($('#usuariosForm'))
        }
    </script>
@endsection