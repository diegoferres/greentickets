@extends('admin.templates.layout')
@section('head')
    <link rel="stylesheet" href="/assets/bundles/jquery-selectric/selectric.css">
@endsection
@section('content')
    @empty($event)

        {!! Form::open(['route' => 'admin.events.store', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
    @endempty
    <div class="row">

        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                @isset($event)
                    {!! Form::model($event, ['route' => ['admin.events.update', $event->id], 'method' => 'put', 'enctype' => 'multipart/form-data']) !!}
                    {!! Form::hidden('form_name', 'formEvent') !!}
                @endisset
                <div class="card-header">
                    <h4>Datos del evento</h4>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
                <div class="card-body">
                    <div class="form-row">
                        @isset($event->event_cover)
                            <div class="form-group col-md-5">
                                <div class="gallery gallery-fw" data-item-height="100">
                                    <div class="gallery-item" data-image="{{ asset('storage/'.$event->event_cover) }}"
                                         data-title="Image 1"></div>
                                </div>
                            </div>
                        @endisset
                        <div class="form-group col-md-3">
                            <label for="nombres">Imagen</label>
                            {!! Form::file('event_cover', old('event_cover'), ['class' => ($errors->has('event_cover')) ? 'form-control is-invalid' : 'form-control']) !!}
                            @error('event_cover')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>

                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="nombres">Nombre</label>
                            {!! Form::text('event_name', old('event_name'), ['class' => ($errors->has('event_name')) ? 'form-control is-invalid' : 'form-control']) !!}
                            @error('event_name')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group col-md-3">
                            <label for="nombres">Organizador</label>
                            {!! Form::text('event_organizer', old('event_organizer'), ['class' => ($errors->has('event_organizer')) ? 'form-control is-invalid' : 'form-control']) !!}
                            @error('event_organizer')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group col-md-2">
                            <label for="category_id">Categoría</label>
                            {!! Form::select('event_category', $categories, null, ['class' => ($errors->has('event_category')) ? 'form-control is-invalid' : 'form-control', 'placeholder' => 'Seleccionar una categoría']) !!}
                            @error('event_category')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label for="nombres">Descripción</label>
                            {!! Form::textarea('event_description', old('event_description'), ['class' => ($errors->has('event_name')) ? 'form-control is-invalid' : 'form-control']) !!}
                            @error('event_description')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        @error('event_description')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                </div>
                {{--<div class="card-footer">

                </div>--}}
                @isset($event)
                    {!! Form::close() !!}
                @endisset

                @isset($event)
                    {!! Form::model($event, ['route' => ['admin.events.update', $event->id], 'method' => 'put']) !!}
                    {!! Form::hidden('form_name', 'formVenue') !!}
                @endisset

                <div class="card-header">
                    <h4>Lugar del evento</h4>
                    @isset($event)
                        <button type="submit" class="btn btn-primary">Guardar</button>@endisset
                </div>
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="nombres">Nombre</label>
                            {!! Form::text('venue[venue_name]', old('venue.venue_name'), ['class' => ($errors->has('venue.venue_name')) ? 'form-control is-invalid' : 'form-control']) !!}
                            @error('venue.venue_name')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label for="nombres">Dirección</label>
                            {!! Form::text('venue[venue_address]', old('venue_address'), ['class' => ($errors->has('venue.venue_address')) ? 'form-control is-invalid' : 'form-control']) !!}
                            @error('venue.venue_address')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="card-footer">

                </div>
                @isset($event)
                    {!! Form::close() !!}
                @endisset

            </div>
        </div>
        @isset($event)
            <div class="col-12 col-md-6 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4>Sectores</h4>
                    </div>
                    {!! Form::model($event, ['route' => ['admin.events.update', $event->id], 'method' => 'put']) !!}
                    {!! Form::hidden('form_name', 'formSectors') !!}
                    <div class="card-body" id="slot">
                        {{--@isset($event->sectors)--}}
                        @foreach ($event->sectors as $key => $sector)
                            <div class="form-row">
                                <div class="form-group col-md-5">
                                    <label for="nombres">Tipo de Sector</label>
                                    {!! Form::text('sector_name['.$sector->id.']', $sector->sector_name, ['class' => ($errors->has('sector_name.'.$sector->id)) ? 'form-control is-invalid' : 'form-control']) !!}
                                    @error('sector_name.'.$sector->id)
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="nombres">Precio</label>
                                    {!! Form::text('sector_price['.$sector->id.']', $sector->sector_price, ['class' => ($errors->has('sector_price.'.$sector->id)) ? 'form-control is-invalid' : 'form-control']) !!}
                                    @error('sector_price.'.$sector->id)
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="nombres">Cantidad</label>
                                    {!! Form::text('sector_capacity['.$sector->id.']', $sector->sector_capacity, ['class' => ($errors->has('sector_capacity.'.$sector->id)) ? 'form-control is-invalid' : 'form-control']) !!}
                                    @error('sector_capacity.'.$sector->id)
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-1">
                                    <label for="nombres">Eliminar</label>
                                    <a href="{{ route('admin.events.delete_sector', ['event' => $event->id, 'sector' => $sector->id]) }}"
                                       class="btn btn-danger delete_row">X</a>
                                </div>
                            </div>
                        @endforeach
                        {{--@endisset--}}

                        <hr>
                        <div class="form-row">
                            <div class="form-group col-md-5">
                                <label for="nombres">Tipo de Sector</label>
                                {!! Form::text('sector_name_n', null, ['class' => ($errors->has('sector_name_n')) ? 'form-control is-invalid' : 'form-control']) !!}
                                @error('sector_name_n')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group col-md-3">
                                <label for="nombres">Precio</label>
                                {!! Form::text('sector_price_n', null, ['class' => ($errors->has('sector_price_n')) ? 'form-control is-invalid' : 'form-control']) !!}
                                @error('sector_price_n')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group col-md-3">
                                <label for="nombres">Cantidad</label>
                                {!! Form::text('sector_capacity_n', null, ['class' => ($errors->has('sector_capacity_n')) ? 'form-control is-invalid' : 'form-control']) !!}
                                @error('sector_capacity_n')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                    {{--@isset($event)--}}
                    {!! Form::close() !!}
                    {{--@endisset--}}

                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4>Fechas</h4>
                    </div>
                    {{--@isset($event)--}}
                    {!! Form::model($event, ['route' => ['admin.events.update', $event->id], 'method' => 'put']) !!}
                    {!! Form::hidden('form_name', 'formDates') !!}
                    {{--@endisset--}}

                    <div class="card-body" id="slot">
                        {{--<div class="form-row" style="display: none;">
                            <div class="form-group col-md-5">
                                <label for="nombres">Fecha</label>
                                {!! Form::text('', '', ['class' => 'form-control', 'id' => 'event_date[]']) !!}
                            </div>
                            <div class="form-group col-md-3">
                                <label for="nombres">Hora Inicio</label>
                                {!! Form::time('', '', ['class' => 'form-control', 'id' => 'event_time_start[]']) !!}
                            </div>
                            <div class="form-group col-md-3">
                                <label for="nombres">Hora Fin</label>
                                {!! Form::time('', '', ['class' => 'form-control', 'id' => 'event_time_end[]']) !!}
                            </div>
                        </div>--}}
                        {{--@isset($event->dates)--}}
                        @foreach ($event->dates as $key => $date)
                            <div class="form-row">
                                <div class="form-group col-md-5">
                                    <label for="nombres">Fecha</label>
                                    {!! Form::date('event_date['.$date->id.']', $date->event_date, ['class' => ($errors->has('event_date.'.$date->id)) ? 'form-control is-invalid' : 'form-control']) !!}
                                    @error('event_date.'.$date->id)
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="nombres">Hora Inicio</label>
                                    {!! Form::time('event_time_start['.$date->id.']', $date->event_time_start, ['class' => ($errors->has('event_time_start.'.$date->id)) ? 'form-control is-invalid' : 'form-control']) !!}
                                    @error('event_time_start.'.$date->id)
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="nombres">Hora Fin</label>
                                    {!! Form::time('event_time_end['.$date->id.']', $date->event_time_end, ['class' => ($errors->has('event_time_end.'.$date->id)) ? 'form-control is-invalid' : 'form-control']) !!}
                                    @error('event_time_end.'.$date->id)
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-1">
                                    <label for="nombres">Eliminar</label>
                                    <a href="{{ route('admin.events.delete_date', ['event' => $event->id, 'date' => $date->id]) }}"
                                       class="btn btn-danger delete_row">X</a>
                                </div>
                            </div>
                        @endforeach
                        {{--@endisset--}}
                        <hr>
                        <div class="form-row">
                            <div class="form-group col-md-5">
                                <label for="nombres">Fecha</label>
                                {!! Form::date('event_date_n', old('event_date_n'), ['class' => ($errors->has('event_date_n')) ? 'form-control is-invalid' : 'form-control']) !!}
                                @error('event_date_n')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group col-md-3">
                                <label for="nombres">Hora Inicio</label>
                                {!! Form::time('event_time_start_n', old('event_time_start_n'), ['class' => ($errors->has('event_time_start_n')) ? 'form-control is-invalid' : 'form-control']) !!}
                                @error('event_time_start_n')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group col-md-3">
                                <label for="nombres">Hora Fin</label>
                                {!! Form::time('event_time_end_n', old('event_time_end_n'), ['class' => ($errors->has('event_time_end_n')) ? 'form-control is-invalid' : 'form-control']) !!}
                                @error('event_time_end_n')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>

                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                    {{--@isset($event)--}}
                    {!! Form::close() !!}
                    {{--@endisset--}}
                </div>
            </div>
        @endisset
    </div>
    @empty($event)
        {!! Form::close() !!}
    @endempty
@endsection
@section('script')
    <!-- Page Specific JS File -->
    {{--<script src="/assets/bundles/apexcharts/apexcharts.min.js"></script>--}}
    <!-- Template JS File -->
    <script src="/assets/bundles/jquery-selectric/jquery.selectric.min.js"></script>
    <script src="/assets/js/page/gallery1.js"></script>
    <script src="/assets/js/scripts.js"></script>
    <!-- Custom JS File -->
    <script src="/assets/js/custom.js"></script>

    <script>

        function add_slot() {
            var addBtn = $('a#add').clone();
            var deleteBtn = $('a#delete').clone();

            $('a#add').remove();
            $('a#delete').remove();

            var appendHtml = $('div#slot').children().first().clone().appendTo('div#slot');

            $.each(appendHtml.children().children('input'), function (index, input) {
                $(input).attr('name', $(this).attr('id'))
            });
            //console.log(appendHtml.css('display', ''))
            //console.log(appendHtml.children().children('input')[0].attr('name', 'sector_name[]'));
            //appendHtml.children().children('input').val('').

            $('div#slot').append(addBtn);
            $('div#slot').append(deleteBtn);

            if ($('div#slot').children('div').length >= 2) {
                $(deleteBtn).css('display', '')
            }
        }

        function delete_slot() {

            var addBtn = $('a#add').clone();
            var deleteBtn = $('a#delete').clone();

            $('a#add').remove();
            $('a#delete').remove();

            $('div#slot').children().last().remove();

            $('div#slot').append(addBtn);
            $('div#slot').append(deleteBtn);

            if ($('div#slot').children('div').length == 1) {
                $(deleteBtn).css('display', 'none')
            }
        }

        $(document).ready(function () {
            /*$('a.delete_row').click(function () {
                $(this).parent().parent().remove()
            })*/

            $('.add_slot').click(function () {
                console.log($(this))
                var addBtn = $(this).clone().remove();
                var deleteBtn = $(this).siblings('a').clone().remove();

                /*$(this).remove()
                $(this).siblings('a').remove();*/

                var appendHtml = $(this).siblings().first().clone().appendTo($(this).parent());

                $.each(appendHtml.children().children('input'), function (index, input) {
                    $(input).attr('name', $(this).attr('id'))
                });

                appendHtml.css('display', '')

                //appendHtml.parent().append(addBtn);
                //appendHtml.parent().append(deleteBtn);
                $(this).remove()
                $(this).siblings('a').remove();
                //var appendHtml = $('div#slot').children().first().clone().appendTo('div#slot');
                /*


                    $.each(appendHtml.children().children('input'), function( index, input ) {
                        $(input).attr('name', $(this).attr('id'))
                    });

                    appendHtml.css('display', '')

                    $('div#slot').append(addBtn);
                    $('div#slot').append(deleteBtn);

                    if ($('div#slot').children('div').length >= 2){
                        $(deleteBtn).css('display', '')
                    }*/
            })

            $('.delete_slot').click(function () {
                $(this).siblings().last().remove()
            })
        })
    </script>
@endsection