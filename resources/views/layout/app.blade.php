<!doctype html>
<html lang="es">

<head>
    <title>@yield('title', 'Inicio') | Entra Sin Papel</title>
    <!-- Required meta tags -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    @yield('meta')

    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('/favicon/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('/favicon/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('/favicon/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('/favicon/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('/favicon/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('/favicon/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('/favicon/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('/favicon/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('/favicon/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ asset('/favicon/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('/favicon/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('/favicon/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#55C940">
    <meta name="apple-mobile-web-app-status-bar-style" content="#55C940">
    {{--<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">--}}
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,600,800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="{{ asset('css/styles.css')}}" rel="stylesheet">
    <link href="{{ asset('css/fontawesome.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/glider-js@1/glider.min.css">

    {{-- izitoast notification --}}
    <link rel="stylesheet" href="{{ asset('assets/bundles/izitoast/css/iziToast.min.css') }}">
    @yield('head')
</head>
<body>
<!-- PRELOADER -->
<div id="preloader">
    <div class="cssload-container">
        <div class="animate">
            <img src="{{ asset('img/icon-logo.png')}}" alt="cargando">
        </div>
    </div>
</div>
{{--Ejemplo mensaje de alerta--}}
{{--<div class="alert alert-warning alert-dismissible fade show mb-0 border-0 w-100 text-center" role="alert" style="border-radius: 0;">
    Revisa tu correo electrónico y sigue las instrucciones para verificar tu cuenta <a href="{{ route('email') }}" class="ml-3 ">Reenviar</a>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
</div>--}}
<!-- Navigation -->
@include('web.templates.nav')
<section class="page-content">
    @yield('content')
</section>

@include('web.templates.footer')
<!-- JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/glider-js@1/glider.min.js"></script>
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
    //===========================================================================================
    //Get IE Version Function
    //===========================================================================================
    function getInternetExplorerVersion() {
        var rv = -1
        var ua = navigator.userAgent
        var re = ""
        if (navigator.appName == "Microsoft Internet Explorer") {
            re = new RegExp("MSIE ([0-9]{1,}[.0-9]{0,})")
            if (re.exec(ua) !== null) {
                rv = parseFloat(RegExp.$1)
            }
        } else if (navigator.appName == "Netscape") {
            re = new RegExp("Trident/.*rv:([0-9]{1,}[.0-9]{0,})")
            if (re.exec(ua) !== null) {
                rv = parseFloat(RegExp.$1)
            }
        }
        return rv
    }

    //===========================================================================================
    //Window Load Function
    //===========================================================================================
    $(window).on("load", function () {
        //---------------------------------------------------------------------------------------
        //Preloader
        //---------------------------------------------------------------------------------------
        var $preloader = $("#preloader")
        if ($preloader.length) {
            var ie = getInternetExplorerVersion()
            if (ie == "-1" || ie == "11") {
                //Good Browsers
                $preloader.fadeOut("slow", function () {
                    $(this).remove()
                })
                if (window.complete) {
                    $(window).trigger("load")
                }
            } else {
                //Older versions of Internet Explorer
                var myPreloader = document.querySelector("#preloader")
                myPreloader.style.display = "none"
            }
        }
        //Modal avisos
        //$("#avisosModal").modal("show")
    })

</script>
{{-- izitoast notification --}}
<script src="/assets/bundles/izitoast/js/iziToast.min.js"></script>
<script>

    $(document).ready(function () {

        $('a#logout').click(function () {

            $('form#logout-form').submit();
        });

        //Notifications from laravel
        var notif_success = "{{ session('success') }}";
        var notif_error = "{{ session('error') }}";
        var notif_info = "{{ session('info') }}";
        var notif_warning = "{{ session('warning') }}";

        if (notif_success) {
            iziToast.success({
                title: notif_success,
                //message: 'This awesome plugin is made by iziToast',
                position: 'topRight'
            });
        } else if (notif_error) {
            iziToast.error({
                title: notif_error,
                //message: 'This awesome plugin is made by iziToast',
                position: 'topRight'
            });
        } else if (notif_info) {
            iziToast.info({
                title: notif_info,
                //message: 'This awesome plugin is made by iziToast',
                position: 'topRight'
            });
        } else if (notif_warning) {
            iziToast.warning({
                title: notif_warning,
                //message: 'This awesome plugin is made by iziToast',
                position: 'topRight'
            });
        }
        //END Notifications from laravel
    })

    /**
     * SignOut Laravel
     */
    function signOut() {
        $('form#logout-form').submit();
    }
</script>
@yield('scripts')
</body>
</html>
