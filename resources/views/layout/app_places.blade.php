<!doctype html>
<html lang="es">

    <head>
        <title>Bienvenido/a a {{ $place->name }}</title>
        <!-- Required meta tags -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Google Sign-In -->


        <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('/icons/apple-icon-57x57.png') }}">
        <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('/icons/apple-icon-60x60.png') }}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('/icons/apple-icon-72x72.png') }}">
        <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('/icons/apple-icon-76x76.png') }}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('/icons/apple-icon-114x114.png') }}">
        <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('/icons/apple-icon-120x120.png') }}">
        <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('/icons/apple-icon-144x144.png') }}">
        <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('/icons/apple-icon-152x152.png') }}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('/icons/apple-icon-180x180.png') }}">
        <link rel="icon" type="image/png" sizes="192x192" href="{{ asset('/icons/android-icon-192x192.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/icons/favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('/icons/favicon-96x96.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/icons/favicon-16x16.png') }}">
        {{--<link rel="manifest" href="{{ asset('/icons/manifest.json') }}">--}}
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
        <meta name="theme-color" content="#55C940">
        <meta name="apple-mobile-web-app-status-bar-style" content="#55C940">
        {{--<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">--}}
        <link href="https://fonts.googleapis.com/css?family=Poppins:400,600,800&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
            integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link href="{{ asset('css/styles.css')}}" rel="stylesheet">
        <link href="{{ asset('css/fontawesome.min.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/glider-js@1/glider.min.css">

        {{-- izitoast notification --}}
        <link rel="stylesheet" href="/assets/bundles/izitoast/css/iziToast.min.css">
        @yield('head')
    <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-171666987-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-171666987-1');
        </script>
    </head>
    <body>
            {{--Ejemplo mensaje de alerta--}}
            {{--<div class="alert alert-warning alert-dismissible fade show mb-0 border-0 w-100 text-center" role="alert" style="border-radius: 0;">
                Revisa tu correo electrónico y sigue las instrucciones para verificar tu cuenta <a href="{{ route('email') }}" class="ml-3 ">Reenviar</a>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>--}}
            <!-- Navigation -->
            @include('web.places.templates.nav')
            <section class="page-content">
                @yield('content')
            </section>
            @include('web.places.templates.footer')
            <!-- JavaScript -->
            <script src="https://cdn.jsdelivr.net/npm/glider-js@1/glider.min.js"></script>
            <!-- jQuery first, then Popper.js, then Bootstrap JS -->
            <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
                    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
                    crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
                    integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
                    crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
                    integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
                    crossorigin="anonymous"></script>


            {{-- izitoast notification --}}
            <script src="/assets/bundles/izitoast/js/iziToast.min.js"></script>
            <script>

                $(document).ready(function () {

                    $('a#logout').click(function () {

                        $('form#logout-form').submit();
                    });

                    //Notifications from laravel
                    var notif_success = "{{ session('success') }}";
                    var notif_error = "{{ session('error') }}";
                    var notif_info = "{{ session('info') }}";
                    var notif_warning = "{{ session('warning') }}";

                    if (notif_success){
                        iziToast.success({
                            title: notif_success,
                            //message: 'This awesome plugin is made by iziToast',
                            position: 'topRight'
                        });
                    } else if (notif_error){
                        iziToast.error({
                            title: notif_error,
                            //message: 'This awesome plugin is made by iziToast',
                            position: 'topRight'
                        });
                    } else if (notif_info){
                        iziToast.info({
                            title: notif_info,
                            //message: 'This awesome plugin is made by iziToast',
                            position: 'topRight'
                        });
                    } else if (notif_warning){
                        iziToast.warning({
                            title: notif_warning,
                            //message: 'This awesome plugin is made by iziToast',
                            position: 'topRight'
                        });
                    }
                    //END Notifications from laravel
                })


            </script>

            @yield('scripts')

    </body>
</html>
